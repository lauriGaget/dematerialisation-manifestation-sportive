# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver

from administration.models import Compagnie
from agreements.models import SDISAvis
from sub_agreements.models.compagnie import PreAvisCompagnie


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=PreAvisCompagnie)
    def notify_preavis_compagnie(created, instance, **kwargs):
        """ Notifier les agents de la création du préavis """
        if created:
            instance.notify_creation(agents=instance.get_agents(), content_object=instance.get_sdis())


    @receiver(m2m_changed, sender=SDISAvis.compagnies_concernees.through)
    def create_preavis_compagnies(instance, action, pk_set, **kwargs):
        """ Créer un préavis pour les avis SDIS """
        if action == 'post_add':
            for pk in pk_set:
                PreAvisCompagnie.objects.get_or_create(avis=instance, compagnie=Compagnie.objects.get(pk=pk))
