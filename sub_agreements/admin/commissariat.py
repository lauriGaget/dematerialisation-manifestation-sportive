# coding: utf-8
from django.contrib import admin

from ..models import PreAvisCommissariat


class PreAvisCommissariatInline(admin.StackedInline):
    """ Inline Préavis """

    # Configuration
    model = PreAvisCommissariat
    extra = 0
