# coding: utf-8
from django.contrib import admin

from ..models import PreAvisCompagnie


class PreAvisCompagnieInline(admin.StackedInline):
    """ Inline Préavis """

    # Configuration
    model = PreAvisCompagnie
    extra = 0
