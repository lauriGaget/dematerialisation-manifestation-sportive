# coding: utf-8
from django.contrib import admin

from ..models import PreAvisCGD


class PreAvisCGDInline(admin.StackedInline):
    """ Inline Préavis """

    # Configuration
    model = PreAvisCGD
    extra = 0
