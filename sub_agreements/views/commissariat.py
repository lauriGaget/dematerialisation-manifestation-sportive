# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from agreements.views import BaseAcknowledgeView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class PreAvisCommissariatDetail(DetailView):
    """ Détail des préavis commissariat """

    # Configuration
    model = PreAvisCommissariat

    # Overrides
    @method_decorator(require_role('commissariatagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisCommissariatDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.avis.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class PreAvisCommissariatAcknowledge(BaseAcknowledgeView):
    """ Rendu de préavis commissariat """

    # Configuration
    model = PreAvisCommissariat
    form_class = PreAvisForm

    # Overrides
    @method_decorator(require_role('commissariatagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisCommissariatAcknowledge, self).dispatch(*args, **kwargs)
