# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from agreements.views import BaseAcknowledgeView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class PreAvisServiceCGDetail(DetailView):
    """ Détail des préavis de service de Conseil départemental """

    # Configuration
    model = PreAvisServiceCG

    # Overrides
    @method_decorator(require_role('cgserviceagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisServiceCGDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.avis.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class PreAvisServiceCGAcknowledge(BaseAcknowledgeView):
    """ Rendu des préavis de service de Conseil départemental """

    # Configuration
    model = PreAvisServiceCG
    form_class = PreAvisForm

    # Overrides
    @method_decorator(require_role('cgserviceagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisServiceCGAcknowledge, self).dispatch(*args, **kwargs)
