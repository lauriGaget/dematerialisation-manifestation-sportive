# coding: utf-8
from django.apps import AppConfig


class SubAgreementsConfig(AppConfig):
    """ Configuration de l'application """

    name = 'sub_agreements'
    verbose_name = "Préavis"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from sub_agreements import listeners


default_app_config = 'sub_agreements.SubAgreementsConfig'
