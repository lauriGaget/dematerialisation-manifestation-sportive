# coding: utf-8

from django.urls import reverse
from django.test import TestCase

from administration.factories import CompagnieFactory
from administration.factories import SDISFactory
from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory
from administrative_division.factories import DepartementFactory
from agreements.factories import SDISAvisFactory
from authorizations.factories import ManifestationAuthorizationFactory
from events.factories import AutorisationNMFactory
from sub_agreements.tests.base import PreavisTestsBase
from ..factories import PreAvisCompagnieFactory


class PreAvisCompagnieTests(PreavisTestsBase):
    """ Tests des préavis de Compagnie """

    # Configuration
    def setUp(self):
        super().setUp()
        self.avis = SDISAvisFactory.create(authorization=self.authorization)

    # Tests
    def test_str(self):
        """ Tester les représentations chaîne """
        preavis = PreAvisCompagnieFactory.create(avis=self.avis, compagnie=self.compagnie)
        manifestation = preavis.avis.get_manifestation()
        self.assertEqual(str(preavis), ' - '.join([str(manifestation), str(preavis.compagnie)]))

    def test_access_url(self):
        """ Tester les URLs """
        preavis = PreAvisCompagnieFactory.create(avis=self.avis, compagnie=self.compagnie)
        self.assertEqual(preavis.get_absolute_url(), reverse('sub_agreements:company_subagreement_detail', kwargs={'pk': preavis.pk}))

    def test_workflow(self):
        """ Tester la création automatique des préavis """
        self.assertEqual(self.avis.get_preavis_count(), 0)
        self.avis.compagnies_concernees.add(self.compagnie)
        self.assertEqual(self.avis.get_preavis_count(), 1)
