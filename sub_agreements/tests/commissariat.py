# coding: utf-8
import datetime

from django.urls import reverse
from django.test import TestCase

from administration.factories import CommissariatFactory
from administration.factories import DDSPFactory
from administrative_division.factories import ArrondissementFactory
from administrative_division.factories import CommuneFactory
from administrative_division.factories import DepartementFactory
from agreements.factories import DDSPAvisFactory
from authorizations.factories import ManifestationAuthorizationFactory
from events.factories import AutorisationNMFactory
from sub_agreements.tests.base import PreavisTestsBase
from ..factories import PreAvisCommissariatFactory


class PreAvisCommissariatTests(PreavisTestsBase):
    """ Tests préavis commissariat """

    # Configuration
    def setUp(self):
        super().setUp()
        self.commissariat = CommissariatFactory.create(commune=self.commune)
        self.avis = DDSPAvisFactory.create(authorization=self.authorization)

    # Tests
    def test_str(self):
        """ Tester les représentations chaîne """
        preavis = PreAvisCommissariatFactory.create(avis=self.avis, commissariat=self.commissariat)
        manifestation = preavis.avis.get_manifestation()
        self.assertEqual(str(preavis), ' - '.join([str(manifestation), str(preavis.commissariat)]))

    def test_access_url(self):
        """ Tester les URLs """
        preavis = PreAvisCommissariatFactory.create(avis=self.avis, commissariat=self.commissariat)
        self.assertEqual(preavis.get_absolute_url(), reverse('sub_agreements:commissariat_subagreement_detail', kwargs={'pk': preavis.pk}))

    def test_life_cycle(self):
        """ Tester l'état de rendu de l'avis """
        preavis = PreAvisCommissariatFactory.create(avis=self.avis, commissariat=self.commissariat)
        preavis.acknowledge()  # Rendre le préavis
        self.assertEqual(preavis.reply_date, datetime.date.today())  # Vérifier que le préavis a bien été rendu aujourd'hui

    def test_workflow(self):
        """ Tester la création du préavis """
        self.assertEqual(self.avis.get_preavis_count(), 0)  # L'avis ne possède pas de préavis, il faut sélectionner des CGD avant
        self.avis.commissariats_concernes.add(self.commissariat)
        self.assertEqual(self.avis.get_preavis_count(), 1)
