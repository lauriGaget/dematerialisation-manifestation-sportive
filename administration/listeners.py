from django.dispatch import receiver
from django.core.mail import send_mail
from django.conf import settings
from allauth.account.signals import email_confirmed

from core.models import User


@receiver(email_confirmed)
def email_confirmed_(request, email_address, **kwargs):
    user = email_address.user
    if not user.is_active:
        url = '/admin/core/user/' + str(user.pk) + '/change/'
        address = request.build_absolute_uri(url)
        sender_email = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
        adminstancelist = User.objects.filter(groups__name__icontains='Administrateurs d\'instance').filter(default_instance=user.default_instance).values_list('email', flat=True)
        if adminstancelist:
            send_mail(subject='[Manifestation Sportive] Création de compte',
                      message='Un nouveau compte a été créé (avec une adresse mail valide) : ' + user.username + chr(10) +
                              "Pour vérifier et valider ce compte, rendez-vous à l'adresse : " + chr(10) +
                              address,
                      from_email=sender_email,
                      recipient_list=list(adminstancelist))
        else:
            techlist = User.objects.filter(groups__name__icontains='Administrateurs techniques').values_list('email', flat=True)
            send_mail(subject='[Manifestation Sportive] Creation de compte',
                      message="Un nouveau compte a été créé avec une adresse mail valide." + chr(10) +
                              "Il n'y a pas d'administrateur d'instance pour valider ce compte !" + chr(10) +
                              "Pour vérifier et valider le compte de l'utilisateur " + user.username + ' ,' + chr(10) +
                              "rendez-vous à l'adresse : " + address,
                      from_email=sender_email,
                      recipient_list=list(techlist))
