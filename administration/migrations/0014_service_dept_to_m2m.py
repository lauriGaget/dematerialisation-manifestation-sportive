# Generated by Django 2.2.1 on 2020-02-04 13:39
from django.db import migrations


def dept_vers_depts(apps, schema_editor):
    """
    Copier les départements d'origine vers la table m2m
    """
    Service = apps.get_model('administration', 'Service')
    for service in Service.objects.all():
        service.departements.add(service.departement)


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0013_auto_20200204_1438'),
    ]

    operations = [
        migrations.RunPython(dept_vers_depts),
    ]
