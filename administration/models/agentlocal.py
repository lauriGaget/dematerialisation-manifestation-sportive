# coding: utf-8
from django.urls import reverse
from django.conf import settings
from django.db import models


class AgentQueryset(models.QuerySet):
    def get_by_natural_key(self, username):
        return self.get(user__username=username)


class AgentLocal(models.Model):
    """ Base agent local """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="agentlocal", verbose_name='utilisateur', on_delete=models.CASCADE)
    objects = AgentQueryset.as_manager()

    # Getter
    @staticmethod
    def users_from_agents(agents):
        """ Renvoyer une liste d'utilisateurs depuis un itérable """
        return [agent.user for agent in agents]

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:administration_' + type(self)._meta.object_name.lower() + '_change', args=(self.pk,))

    # Overrides
    def __str__(self):
        return self.user.get_full_name()

    def natural_key(self):
        return self.user.username,

    class Meta:
        verbose_name = "Agent local"
        verbose_name_plural = "Agents locaux"
        app_label = 'administration'
        default_related_name = 'agentslocaux'


class EDSRAgentLocal(AgentLocal):
    """ Agent EDSR local (assigné aux préavis uniquement) """

    edsr = models.ForeignKey('administration.edsr', verbose_name='EDSR', on_delete=models.CASCADE)
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='edsragentlocal', verbose_name="ID Agent Local", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "EDSR - Agent local"
        verbose_name_plural = "EDSR - Agents locaux"
        app_label = 'administration'
        default_related_name = 'edsragentslocaux'


class CGDAgentLocal(AgentLocal):
    """ Agent CGD (local) """

    cgd = models.ForeignKey('administration.cgd', verbose_name='CGD', on_delete=models.CASCADE)
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='cgdagentlocal', verbose_name="ID Agent Local", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "CGD - Agent local"
        verbose_name_plural = "CGD - Agents locaux"
        app_label = 'administration'
        default_related_name = 'ggdagentslocaux'


class CompagnieAgentLocal(AgentLocal):
    """ Agent local de Compagnie SDIS """
    compagnie = models.ForeignKey('administration.compagnie', verbose_name='compagnie/groupement', on_delete=models.CASCADE)
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='compagnieagentlocal', verbose_name="ID Agent Local", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "SDIS - Compagnie/Groupement - Agent local"
        verbose_name_plural = "SDIS - Compagnie/Groupement - Agents locaux"
        app_label = 'administration'
        default_related_name = 'compagnieagentslocaux'


class CommissariatAgentLocal(AgentLocal):
    """ Agent local de commissariat """

    commissariat = models.ForeignKey('administration.commissariat', verbose_name='commissariat', on_delete=models.CASCADE)
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='commissariatagentlocal', verbose_name="ID Agent Local", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "DDSP - Commissariat - Agent local"
        verbose_name_plural = "DDSP - Commissariat - Agents locaux"
        app_label = 'administration'
        default_related_name = 'commissariatagentslocaux'


class CGServiceAgentLocal(AgentLocal):
    """ Agent local d'un service du Conseil départemental """

    cg_service = models.ForeignKey('administration.cgservice', verbose_name='Service CD', on_delete=models.CASCADE)
    agentlocal = models.OneToOneField('administration.agentlocal', parent_link=True, related_name='cgserviceagentlocal', verbose_name="ID Agent Local", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "CD - Service - Agent local"
        verbose_name_plural = "CD - Service - Agents locaux"
        app_label = 'administration'
        default_related_name = 'cgserviceagentslocaux'
