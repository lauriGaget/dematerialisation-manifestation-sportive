# coding: utf-8
import random
import string

import factory
from factory.django import DjangoModelFactory

from administration.models.service import GGD, SDIS, CG, DDSP, EDSR, Prefecture, Service, Commissariat, CGD, Compagnie, CIS, CGService, CODIS, Brigade


class GGDFactory(DjangoModelFactory):
    """ Factory """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')

    class Meta:
        model = GGD


class SDISFactory(DjangoModelFactory):
    """ Factory """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')

    class Meta:
        model = SDIS


class CGFactory(DjangoModelFactory):
    """ Factory """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')
    email = factory.Sequence(lambda n: 'cg{0}@example.com'.format(n))

    class Meta:
        model = CG


class DDSPFactory(DjangoModelFactory):
    """ Factory """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')
    email = factory.Sequence(lambda n: 'ddsp{0}@example.com'.format(n))

    class Meta:
        model = DDSP


class EDSRFactory(DjangoModelFactory):
    """ Factory """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')

    class Meta:
        model = EDSR


class CODISFactory(DjangoModelFactory):
    """ Factory """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')

    class Meta:
        model = CODIS


class PrefectureFactory(DjangoModelFactory):
    """ Factory """

    sous_prefecture = True
    email = factory.Sequence(lambda n: 'pref{0}@loire.gouv.fr'.format(n))

    class Meta:
        model = Prefecture


class ServiceFactory(DjangoModelFactory):
    """ Factory """

    name = 'sncf'

    class Meta:
        model = Service

    @factory.post_generation
    def departements(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for dept in extracted:
                self.departements.add(dept)


class CommissariatFactory(DjangoModelFactory):
    """ Factory """

    commune = factory.SubFactory('administrative_division.factories.CommuneFactory')

    class Meta:
        model = Commissariat


class CGDFactory(DjangoModelFactory):
    """ Factory """

    commune = factory.SubFactory('administrative_division.factories.CommuneFactory')

    class Meta:
        model = CGD


class CompagnieFactory(DjangoModelFactory):
    """ Factory """

    sdis = factory.SubFactory('administration.factories.SDISFactory')
    number = factory.Sequence(lambda n: ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(3)))

    class Meta:
        model = Compagnie


class CISFactory(DjangoModelFactory):
    """ Factory """

    commune = factory.SubFactory('administrative_division.factories.CommuneFactory')
    compagnie = factory.SubFactory('administration.factories.CompagnieFactory')

    class Meta:
        model = CIS


class CGServiceFactory(DjangoModelFactory):
    """ Factory """

    cg = factory.SubFactory('administration.factories.CGFactory')
    name = factory.Sequence(lambda n: 'service_name.%d' % n)
    service_type = factory.Iterator(CGService.SERVICE_TYPE_CHOICES, getter=lambda c: c[0])

    class Meta:
        model = CGService


class BrigadeFactory(DjangoModelFactory):
    """ Factory """

    cgd = factory.SubFactory('administration.factories.CGDFactory')
    kind = factory.Iterator(Brigade.KIND_CHOICES, getter=lambda c: c[0])
    commune = factory.SubFactory('administrative_division.factories.CommuneFactory')

    class Meta:
        model = Brigade
