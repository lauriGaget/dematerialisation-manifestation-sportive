from django.test import TestCase
from django.core import mail
import sys

from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import CGServiceFactory, CGDFactory, CommissariatFactory, BrigadeFactory, CompagnieFactory, CISFactory, ServiceFactory
from emergencies.factories import Association1ersSecoursFactory
from sports.factories import FederationFactory
from core.forms import SignupAgentForm
from core.models.instance import Instance
from core.models import User


class InscriptionAgentTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        """
        Création des objets nécéssaires aux tests
        :return:
        """
        print()
        print('============ Inscription agent (Clt) =============')
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_EDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        cls.dep07 = DepartementFactory.create(name='07',
                                            instance__name="instance 07 de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory.create(name='Bard', arrondissement=arrondissement)
        cls.federation = FederationFactory.create()
        cls.cgserv = CGServiceFactory.create(cg=cls.dep.cg)
        cls.commissariat = CommissariatFactory.create(commune=cls.commune)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        cls.brigade = BrigadeFactory.create(cgd=cls.cgd, commune=cls.commune)
        cls.compagnie = CompagnieFactory.create(sdis=cls.dep.sdis)
        cls.cis = CISFactory.create(compagnie=cls.compagnie, commune=cls.commune)
        cls.service = ServiceFactory.create(departements=(cls.dep,))
        cls.secours = Association1ersSecoursFactory.create(departement=cls.dep)

    def Inscription(self, instance, service, sous_service):
        """
        Test de l'inscription d'un agent
        """
        sys.stdout.write('===> Inscription' + chr(13))
        reponse = self.client.get('/inscription/agent')
        self.assertContains(reponse, 'Inscription')
        self.assertContains(reponse, 'Création d\'un compte Agent')
        field_sous_service = None
        if service in SignupAgentForm.SERVICE_FIELD:
            field_sous_service = SignupAgentForm.SERVICE_FIELD[service]
        reponse = self.client.post('/inscription/agent',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': 'jandup',
                                    'email': 'ne-pas-repondre-42@manifestationsportive.fr',
                                    'password1': 'azeaz5646dfzdsf',
                                    'password2': 'azeaz5646dfzdsf',
                                    'instance': str(instance),
                                    'service': service,
                                    field_sous_service: str(sous_service),
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        return reponse

    def Verification(self, reponse):
        sys.stdout.write('===> Vérification' + chr(13))
        self.assertContains(reponse, 'Vérifiez votre adresse email', count=2)
        self.assertContains(reponse, 'E-mail de confirmation envoyé à ne-pas-repondre-42@manifestationsportive.fr')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, '[example.com] Confirmer l\'adresse email')
        self.assertEqual(mail.outbox[0].to, ['ne-pas-repondre-42@manifestationsportive.fr'])
        url = mail.outbox[0].body.split()[-1]
        return url

    def Confirmation(self, url):
        sys.stdout.write('===> Confirmation' + chr(13))
        reponse = self.client.get(url)
        self.assertContains(reponse, 'Confirmer une adresse email', count=2)
        self.assertContains(reponse, '>ne-pas-repondre-42@manifestationsportive.fr</a> est bien une adresse email de jandup.')
        reponse = self.client.post(url, follow=True,)
        self.assertContains(reponse, 'Adresse email confirmée', count=2)
        self.assertContains(reponse, 'Vous avez confirmé ne-pas-repondre-42@manifestationsportive.fr.')

    def ConnexionFail(self):
        sys.stdout.write('===> echec connexion' + chr(13))
        reponse = self.client.get('/accounts/login/')
        self.assertContains(reponse, 'Connexion', count=3)
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True)
        self.assertContains(reponse, 'Compte inactif', count=2)

    def ConnexionPass(self, roletest):
        sys.stdout.write('===> connexion réussie' + chr(13))
        user = User.objects.get()
        user.is_active = True
        user.save()

        reponse = self.client.get('/accounts/login/')
        self.assertContains(reponse, 'Connexion', count=3)
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True)
        self.assertContains(reponse, 'Connexion avec jandup réussie.')
        url = reponse.template_name[0].split('/')[0]
        if roletest == 'Observateur':
            self.assertEquals(url, 'core')
        else:
            self.assertEquals(url, 'instructions')
        role = user.get_friendly_role_names()[0]
        self.assertEquals(role, roletest)

    def Deconnexion(self):
        print('===> déconnexion')
        self.client.post('/accounts/logout/', follow=True)
        User.objects.get().delete()
        del mail.outbox[0]

    def test_1_Inscription(self):
        """
        Test de l'inscription d'un instructeur
        """
        print('**** test Inscription instructeur ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Prefecture', sous_service=self.prefecture.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Instructeur')
        self.Deconnexion()

        print('**** test Inscription agent mairie ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Commune', sous_service=self.commune.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent mairie')
        self.Deconnexion()

        print('**** test Inscription agent fédération ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Federation', sous_service=self.federation.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental fédération')
        self.Deconnexion()

        print('**** test Inscription agent CD ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CG', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental CD')
        self.Deconnexion()

        print('**** test Inscription agent CD n+1 ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CGn+1', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Supérieur CD')
        self.Deconnexion()

        print('**** test Inscription agent local CD ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CG_Service', sous_service=self.cgserv.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent local service CD')
        self.Deconnexion()

        print('**** test Inscription agent GGD ****')
        reponse = self.Inscription(instance=self.dep.pk, service='GGD', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental GGD')
        self.Deconnexion()

        print('**** test Inscription agent local CGD ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CGD', sous_service=self.dep.ggd.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent local CGD')
        self.Deconnexion()

        print('**** test Inscription agent EDSR ****')
        reponse = self.Inscription(instance=self.dep.pk, service='EDSR', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental EDSR')
        self.Deconnexion()

        print('**** test Inscription agent local EDSR ****')
        reponse = self.Inscription(instance=self.dep07.pk, service='EDSR', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent local EDSR')
        self.Deconnexion()

        print('**** test Inscription agent local Brigade ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Brigade', sous_service=self.brigade.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental brigade')
        self.Deconnexion()

        print('**** test Inscription agent DDSP ****')
        reponse = self.Inscription(instance=self.dep.pk, service='DDSP', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental DDSP')
        self.Deconnexion()

        print('**** test Inscription agent local commissariat ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Commissariat', sous_service=self.commissariat.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent local commissariat')
        self.Deconnexion()

        print('**** test Inscription agent SDIS ****')
        reponse = self.Inscription(instance=self.dep.pk, service='SDIS', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental SDIS')
        self.Deconnexion()

        print('**** test Inscription agent CODIS ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CODIS', sous_service=None)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental CODIS')
        self.Deconnexion()

        print('**** test Inscription agent local compagnie ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Compagnie', sous_service=self.compagnie.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent local compagnie')
        self.Deconnexion()

        print('**** test Inscription agent CIS ****')
        reponse = self.Inscription(instance=self.dep.pk, service='CIS', sous_service=self.cis.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental CIS')
        self.Deconnexion()

        print('**** test Inscription agent Secours ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Association1ersSecours', sous_service=self.secours.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Observateur')
        self.Deconnexion()

        print('**** test Inscription agent Service ****')
        reponse = self.Inscription(instance=self.dep.pk, service='Service', sous_service=self.service.pk)
        url = self.Verification(reponse)
        self.Confirmation(url)
        self.ConnexionFail()
        self.ConnexionPass('Agent départemental service')
        self.Deconnexion()
