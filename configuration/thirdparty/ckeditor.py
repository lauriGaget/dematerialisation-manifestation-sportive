# CKEditor configuration options
# https://github.com/django-ckeditor/django-ckeditor

CKEDITOR_IMAGE_BACKEND = 'pillow'
CKEDITOR_JQUERY_URL = '//code.jquery.com/jquery-2.1.1.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_RESTRICT_BY_USER = False
CKEDITOR_BROWSE_SHOW_DIRS = True
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Full': [
            ['Format'],
            ['PasteText'],
            ['Undo', 'Redo'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Table'],
            ['Anchor', 'Link', 'Unlink', 'Image'],
            ['HorizontalRule', 'SpecialChar'],
            ['Maximize','FontAwesome', 'ShowBlocks', 'Source'],
        ],
        'toolbar': 'Full',
        'extraPlugins': ','.join(
            [
                'fontawesome',
            ]
        ),
        "contentsCss": "/static/libs/fontawesome-pro/css/all.css",
        'width': '100%',
        'height': '150',
        'skin': 'bootstrapck',
        'removePlugins': 'stylesheetparser',
        'allowedContent': True,
    },
    'minimal': {
        'toolbar_Full': [
            ['Format'],
            ['PasteText'],
            ['Undo', 'Redo'],
            ['Bold', 'Italic'],
            ['NumberedList', 'BulletedList'],
            ['Link', 'Unlink'],
        ],
        'toolbar': 'Full',
        'width': '100%',
        'height': '80',
        'skin': 'bootstrapck',
    },
}
