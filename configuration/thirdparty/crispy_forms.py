# coding: utf-8

# DJANGO-CRISPY-FORMS SETTINGS
# ============================
# Sets default template pack for django-crispy-forms
CRISPY_TEMPLATE_PACK = 'bootstrap3'
