# coding: utf-8
import os
import yaml

from django.core.wsgi import get_wsgi_application
from raven.contrib.django.raven_compat.middleware.wsgi import Sentry

# Get the local settings of the server
server_settings = {}
SETTINGS_FILE = 'SETTINGS_FILE' in os.environ and os.environ['SETTINGS_FILE'] or '/etc/django/settings-manifestationsportive.yaml'
try:
    with open(SETTINGS_FILE, 'r') as f:
        server_settings = yaml.load(f, Loader=yaml.FullLoader)
except FileNotFoundError:
    print('No local settings.')
    pass


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "configuration.settings")

application = 'RAVEN_CONFIG_dsn' in server_settings and Sentry(get_wsgi_application()) or get_wsgi_application()
