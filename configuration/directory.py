# coding: utf-8
import sys
from os.path import dirname, exists, join

from django.utils import timezone
from django.utils.deconstruct import deconstructible


class Paths(object):
    """ Utilitaire pour accéder facilement aux répertoires du projet """

    @staticmethod
    def get_root_dir(*sublist):
        """ Renvoyer le répertoire du projet, contenant manage.py """
        current_dir = dirname(__file__)
        while not exists(join(current_dir, 'manage.py')):
            current_dir = dirname(current_dir)
        sublist += ('',)
        for item in sublist:
            current_dir = join(current_dir, item)
        return current_dir

    @staticmethod
    def get_python():
        """ Renvoyer le chemin de l'exécutable python """
        return sys.executable

    @staticmethod
    def is_isolated():
        """ Renvoyer si l'on est dans un environnement isolé, type virtualenv """
        return hasattr(sys, 'real_prefix')


@deconstructible
class UploadPath(object):
    """ Un callable qui permet de configurer le chemin d'upload des fichiers """

    def __init__(self, name):
        """ Initialiser avec le préfixe du chemin de sortie """
        self.prefix = name

    def __call__(self, instance, filename):
        """
        Résultat de la méthode

        :param instance: instance du modèle dans lequel se trouve le champ FileField
        :param filename: nom de fichier passé lors de l'upload
        """

        now = timezone.now()
        nowfmt = now.strftime
        if instance.__class__.__name__=="Manif":
            manif = instance
        elif hasattr(instance, 'manifestation_ptr'):
            manif = instance.manifestation_ptr
        elif hasattr(instance, 'manif'):
            manif = instance.manif
        elif hasattr(instance, 'avis') and instance.avis:
            manif = instance.avis.instruction.manif
        elif hasattr(instance, 'preavis') and instance.preavis:
            manif = instance.preavis.avis.instruction.manif
        elif hasattr(instance, 'instruction'):
            manif = instance.instruction.manif
        departement_number = manif.get_instance().get_departement_name()
        out_path = "{departement}/{manif}/{filename}"
        data = {'departement': departement_number, 'manif': str(manif.pk), 'filename': filename}
        out_path = out_path.format(**data)
        return out_path
