# coding: utf-8
import re

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.views import ChainedSelectChoicesView
from core.util.types import make_iterable


class CommuneAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les activités selon le champ parent (Discipline) sélectionné """
        # Une erreur ici indique un pb avec Apache2. lors d'une requête avec un objet
        # HttpRequest construit manuellement. Il faut lui appliquer manuellement les
        # méthodes process_request et process_response pour conserver la session.
        if hasattr(self.request, 'session'):
            request_departement = self.request.session.get('extradata')
        else:
            request_departement = ''
        self.parent_value = make_iterable(self.parent_value)
        if request_departement:
            try:
                # Si le formulaire contient un attribut extradata, l'utiliser dans la cascade
                departement_id = Departement.objects.get(name=request_departement).pk
                self.parent_value.append(departement_id)
            except Exception:
                pass
        if isinstance(self.parent_value, (list, tuple)):
            result = list(Commune.objects.filter(arrondissement__departement_id__in=self.parent_value).values_list('id', 'name'))
        else:
            result = []
        # Formater les résultats pour afficher les codes postaux
        # dans les sélections à plusieurs départements, plusieurs villes
        # peuvent avoir le même nom
        for ind, item in enumerate(result):
            pk = item[0]
            commune = Commune.objects.get(id=pk)
            name = commune.get_zipcoded_name()
            result[ind] = list(result[ind])
            result[ind][1] = name
        return result
