# coding: utf-8
from ajax_select.helpers import make_ajax_form
from django.contrib import admin

from ..models import ManifestationAutorisation


class EventAuthorizationInline(admin.StackedInline):
    """ Inline admin pour les autorisations """

    # Configuration
    model = ManifestationAutorisation
    extra = 0
    max_num = 1
    form = make_ajax_form(ManifestationAutorisation, {'concerned_cities': 'commune'})
    readonly_fields = ['creation_date']
