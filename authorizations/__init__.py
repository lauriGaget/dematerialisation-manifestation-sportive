# coding: utf-8
from django.apps import AppConfig


class AutorisationsConfig(AppConfig):
    """ Configuration de l'application des autorisations """
    name = "authorizations"
    verbose_name = "Autorisations"

    def ready(self):
        from authorizations import listeners


default_app_config = 'authorizations.AutorisationsConfig'
