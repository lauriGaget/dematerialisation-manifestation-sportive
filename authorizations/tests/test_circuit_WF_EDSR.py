from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.core import mail
import re

from core.models import Instance
from sub_agreements.models import PreAvisCGD, PreAvisCommissariat, PreAvisServiceCG, PreAvisCompagnie

from core.factories import UserFactory
from events.factories import AutorisationNMFactory
from authorizations.factories import ManifestationAuthorizationFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory,\
    CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory,\
    SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory, FederationFactory


class Circuit_EDSRTests(TestCase):
    """
    Test du circuit d'instance EDSR pour une autorisationNM
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ WF_EDSR (Clt) =============')
        # Création des objets sur le 42
        cls.dep = dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)
        cls.structure = StructureFactory(commune=cls.commune)

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=fede)
        cls.agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        GGDAgentFactory.create(user=cls.agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EDSRAgentFactory.create(user=cls.agent_edsr, edsr=dep.edsr)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        cls.agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        CGDAgentFactory.create(user=cls.agent_cgd, cgd=cls.cgd)
        cls.brigade = BrigadeFactory.create(kind='bta', cgd=cls.cgd, commune=cls.commune)
        cls.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        DDSPAgentFactory.create(user=cls.agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        cls.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance)
        CommissariatAgentFactory.create(user=cls.agent_commiss, commissariat=cls.commiss)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)
        cls.agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance)
        CGAgentFactory.create(user=cls.agent_cg, cg=dep.cg)
        cls.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        cls.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance)
        CGServiceAgentFactory.create(user=cls.agent_cgserv, cg_service=cls.cgserv)
        cls.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance)
        CGSuperieurFactory.create(user=cls.agent_cgsup, cg=dep.cg)
        cls.agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance)
        SDISAgentFactory.create(user=cls.agent_sdis, sdis=dep.sdis)
        cls.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        cls.agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance)
        GroupementAgentFactory.create(user=cls.agent_group, compagnie=cls.group)
        cls.agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance)
        CODISAgentFactory.create(user=cls.agent_codis, codis=dep.codis)
        cls.cis = CISFactory.create(name='CIS_test', compagnie=cls.group, commune=cls.commune)
        cls.agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance)
        CISAgentFactory.create(user=cls.agent_cis, cis=cls.cis)

        # Création de l'événement
        cls.manifestation = AutorisationNMFactory.create(departure_city=cls.commune,
                                                         structure=cls.structure,
                                                         name='Manifestation_Test',
                                                         crossed_cities=(cls.autrecommune,),
                                                         activite=activ)
        cls.avis_nb = 6

    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        def presence_avis(self, username, state):
            """
            Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
            :param self:
            :param username: agent considéré
            :param state: couleur de l'événement
            :return: reponse: la réponse http
            """
            # Connexion avec l'utilisateur
            self.assertTrue(self.client.login(username=username, password='123'))
            # sélection de la page adéquate
            if username == 'instructeur':
                template = '/authorizations/dashboard/'
            elif username == 'agent_commiss' or username == 'agent_cgd' or username == 'agent_cgserv' or username == 'agent_group':
                template = '/sub_agreements/dashboard/'
            else:
                template = '/agreements/dashboard/'
            # Appel de la page
            reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
            # print(reponse.content)
            #f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
            #f.write(str(reponse.content.decode('utf-8')).replace('\\n',""))
            #f.close()
            # Test du contenu
            if state == 'none':
                recherche = re.search('Aucun avis|Aucune demande', reponse.content.decode('utf-8'))
                self.assertTrue(recherche, msg='test pas d\'avis / préavis')
            elif username == 'instructeur':
                self.assertContains(reponse, self.authorization)
            else:
                self.assertContains(reponse, self.authorization.get_avis().first())
            #Test de la couleur affichée
            if state == 'danger':
                self.assertContains(reponse, 'list-group-item-danger', count=1)
                self.assertNotContains(reponse, 'list-group-item-warning')
                self.assertNotContains(reponse, 'list-group-item-success')
            if state == 'warning':
                self.assertNotContains(reponse, 'list-group-item-danger')
                self.assertContains(reponse, 'list-group-item-warning', count=1)
                self.assertNotContains(reponse, 'list-group-item-success')
            if state == 'success':
                self.assertNotContains(reponse, 'list-group-item-danger')
                self.assertNotContains(reponse, 'list-group-item-warning')
                self.assertContains(reponse, 'list-group-item-success', count=1)
            return reponse

        def vue_detail(reponse, type=0):
            """
            Appel de la vue de détail de la manifestation
            :param reponse: réponse précédente
            :param type: recherche différente suivant l'utilisateur
            :return: reponse suivante
            """
            if type == 1:
                detail = re.search('href="(?P<url>(/[^"]+)).+Manifestation_Test', reponse.content.decode('utf-8'))
            else:
                detail = re.search('href="(?P<url>(/[^"]+))"\\n>\\n.+Manifestation_Test', reponse.content.decode('utf-8'))
            if hasattr(detail, 'group'):
                reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
            self.assertContains(reponse, 'Détail de la manifestation<', count=1)
            return reponse

        def aucune_action(reponse):
            """
            Test auncune action affichée dans la zone action de la dashboard
            :param reponse: réponse précédente
            """
            action = re.search('Voici les actions.+<a href="(?P<url>(/[^"]+))".+</ul>', str(reponse.content))
            test_action = hasattr(action, 'group')
            self.assertFalse(test_action, msg='test aucune action')

        def affichage_avis(self):
            """
            Affichage des avis émis pour l'événement avec leur status
            :param self:
            """
            for avis in self.authorization.get_avis():
                titre = avis
                try:
                    titre = avis.ggdavis
                except:
                    pass
                try:
                    titre = avis.edsravis
                except:
                    pass
                try:
                    titre = avis.federationavis
                except:
                    pass
                try:
                    titre = avis.ddspavis
                except:
                    pass
                try:
                    titre = avis.mairieavis
                except:
                    pass
                try:
                    titre = avis.cgavis
                except:
                    pass
                try:
                    titre = avis.sdisavis
                except:
                    pass
                if avis.state != 'acknowledged':
                    print(titre, end=" ; ")
                    print(avis.state)

        print('**** test 1 creation manif ****')
        self.authorization = ManifestationAuthorizationFactory.create(manifestation=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.departure_city, end=" ; ")
        print(self.manifestation.departure_city.get_departement(), end=" ; ")
        print(self.manifestation.departure_city.get_departement().get_instance())
        print(self.manifestation.departure_city.get_departement().ggd, end=" ; ")
        print(self.manifestation.departure_city.get_departement().edsr, end=" ; ")
        print(self.manifestation.departure_city.get_departement().ddsp)
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first().name)
        affichage_avis(self)
        self.assertEqual(str(self.authorization), str(self.manifestation))
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(mail.outbox[0].subject, 'avis requis pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_fede.email])
        self.assertEqual(mail.outbox[1].subject, 'demande d\'autorisation envoyée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[1].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[2].subject, 'demande d\'autorisation envoyée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[2].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 2 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        presence_avis(self, 'agent_ggd', 'none')
        # EDSR
        presence_avis(self, 'agent_edsr', 'none')
        # CGD
        presence_avis(self, 'agent_cgd', 'none')
        # Mairie
        presence_avis(self, 'agent_mairie', 'none')
        # CG
        presence_avis(self, 'agent_cg', 'none')
        # CGSuperieur
        presence_avis(self, 'agent_cgsup', 'none')
        # SDIS
        presence_avis(self, 'agent_sdis', 'none')

        print('**** test 3 instructeur ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'instructeur', 'danger')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse, 1)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=1)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        edit_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(edit_form, 'group'):
            reponse = self.client.post(edit_form.group('url'),
                                       {'edsr_concerned': True,
                                        'ddsp_concerned': True,
                                        'sdis_concerned': True,
                                        'cg_concerned': True,
                                        'concerned_cities': [self.commune.pk]}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.authorization)
        affichage_avis(self)
        # Vérifier le passage en warning et le nombre d'avis manquants
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 8)
        self.assertEqual(mail.outbox[0].subject, 'début de l\'instruction pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.structure.organisateur.user.email])
        self.assertEqual(mail.outbox[1].subject, 'avis requis pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[1].to, [self.agent_edsr.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_sdis.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_ddsp.email])
        self.assertEqual(mail.outbox[4].to, [self.dep.ddsp.email])
        self.assertEqual(mail.outbox[5].to, [self.agent_cg.email])
        self.assertEqual(mail.outbox[6].to, [self.dep.cg.email])
        self.assertEqual(mail.outbox[7].to, [self.agent_mairie.email])
        del mail.outbox

        print('**** test 4 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_fede', 'danger')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ackno = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ackno, 'group'):
            reponse = self.client.post(ackno.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis(self)
        # Vérifier le passage en success
        presence_avis(self, 'agent_fede', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 5 vérification avis; 0 pour GGD et CGD ****')
        # Vérification des avis des divers agents
        # EDSR
        presence_avis(self, 'agent_ggd', 'none')
        # CGD
        presence_avis(self, 'agent_cgd', 'none')

        print('**** test 6 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_edsr', 'danger')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'concerned_cgd': self.cgd.pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisCGD.objects.first()
        print(preavis.preaviscgd, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérification du passage en warning
        reponse = presence_avis(self, 'agent_edsr', 'warning')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cgd.email])
        del mail.outbox

        print('**** test 7 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_cgd', 'danger')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les brigades', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les brigades', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'concerned_brigades': [self.brigade.pk]}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation', count=1)
        # Vérification du passage en warning
        reponse = presence_avis(self, 'agent_cgd', 'warning')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisCGD.objects.first()
        print(preavis.preaviscgd, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérification du passage en success
        presence_avis(self, 'agent_cgd', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_edsr.email])
        del mail.outbox

        print('**** test 8 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_edsr', 'warning')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Formater l'avis de l'événement avec l'url fournie et tester la redirection
        form_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(form_form, 'group'):
            reponse = self.client.post(form_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        affichage_avis(self)
        # vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_edsr', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'avis mis en forme pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_ggd.email])
        del mail.outbox

        print('**** test 9 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_ggd', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis(self)
        # vérification de la présence de l'événement en success
        presence_avis(self, 'agent_ggd', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 10 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        presence_avis(self, 'agent_commiss', 'none')

        print('**** test 11 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_ddsp', 'danger')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'commissariats_concernes': self.commiss.pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisCommissariat.objects.first()
        print(preavis.preaviscommissariat, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérifier le passage en warning
        reponse = presence_avis(self, 'agent_ddsp', 'warning')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_commiss.email])
        del mail.outbox

        print('**** test 12 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_commiss', 'danger')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisCommissariat.objects.first()
        print(preavis.preaviscommissariat, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérification du passage en success
        presence_avis(self, 'agent_commiss', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(mail.outbox[1].to, [self.dep.ddsp.email])
        del mail.outbox

        print('**** test 13 agent ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_ddsp', 'warning')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis(self)
        # Vérifier le passage en success
        presence_avis(self, 'agent_ddsp', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 14 agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_mairie', 'danger')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis(self)
        # Vérifier le passage en success
        presence_avis(self, 'agent_mairie', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 15 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        presence_avis(self, 'agent_cgserv', 'none')

        print('**** test 16 agent cg ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_cg', 'danger')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'concerned_services': self.cgserv.pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisServiceCG.objects.first()
        print(preavis.preavisservicecg, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérifier le passage en warning
        reponse = presence_avis(self, 'agent_cg', 'warning')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cgserv.email])
        del mail.outbox


        print('**** test 17 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_cgserv', 'danger')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisServiceCG.objects.first()
        print(preavis.preavisservicecg, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérification du passage en success
        presence_avis(self, 'agent_cgserv', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cg.email])
        self.assertEqual(mail.outbox[1].to, [self.dep.cg.email])
        del mail.outbox

        print('**** test 18 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        presence_avis(self, 'agent_cgsup', 'none')

        print('**** test 19 agent cg ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_cg', 'warning')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Mettre en forme l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        affichage_avis(self)
        # vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_cg', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'avis mis en forme pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cgsup.email])
        del mail.outbox

        print('**** test 20 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_cgsup', 'warning')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis(self)
        # Vérifier le passage en success
        presence_avis(self, 'agent_cgsup', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 21 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        presence_avis(self, 'agent_cis', 'none')
        # CODIS
        presence_avis(self, 'agent_codis', 'none')
        # SDIS groupement
        presence_avis(self, 'agent_group', 'none')

        print('**** test 22 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_sdis', 'danger')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'compagnies_concernees': self.group.pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisCompagnie.objects.first()
        print(preavis.preaviscompagnie, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérification du passage en warning
        reponse = presence_avis(self, 'agent_sdis', 'warning')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_group.email])
        del mail.outbox

        print('**** test 23 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en danger
        reponse = presence_avis(self, 'agent_group', 'danger')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvisCompagnie.objects.first()
        print(preavis.preaviscompagnie, end=" ; ")
        print(preavis.state)
        affichage_avis(self)
        # Vérification du passage en warning
        presence_avis(self, 'agent_group', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_sdis.email])
        del mail.outbox

        print('**** test 24 agent sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en warning
        reponse = presence_avis(self, 'agent_sdis', 'warning')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis(self)
        # Vérifier le passage en success
        presence_avis(self, 'agent_sdis', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis(self, 'instructeur', 'warning')
        self.assertNotContains(reponse, 'avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_codis.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_group.email])
        del mail.outbox

        print('**** test 25 avis codis ****')
        # Vérification de la présence de l'événement
        reponse = presence_avis(self, 'agent_codis', 'success')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Tester aucune action disponible
        aucune_action(reponse)

        print('**** test 26 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en success
        reponse = presence_avis(self, 'agent_group', 'success')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les CIS', count=1)
        # Informer les CIS de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les CIS', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'concerned_cis': self.cis.pk}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        # Vérification du passage en warning
        presence_avis(self, 'agent_group', 'success')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Prenez connaissance des informations pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cis.email])

        print('**** test 27 avis cis ****')
        # Vérification de la présence de l'événement
        reponse = presence_avis(self, 'agent_cis', 'success')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Tester aucune action disponible
        aucune_action(reponse)
