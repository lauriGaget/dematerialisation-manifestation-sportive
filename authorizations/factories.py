# coding: utf-8
import factory

from events.factories import AutorisationNMFactory
from .models import ManifestationAutorisation


class ManifestationAuthorizationFactory(factory.django.DjangoModelFactory):
    """ Factory pour les autorisations """

    # Champs
    manifestation = factory.SubFactory(AutorisationNMFactory)

    # Meta
    class Meta:
        model = ManifestationAutorisation
