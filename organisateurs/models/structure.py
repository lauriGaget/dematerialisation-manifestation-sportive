# coding: utf-8
import re

from django.urls import reverse
from django.db import models

from administrative_division.models import Commune


class StructureTypeQuerySet(models.QuerySet):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, type_of_structure):
        return self.get(type_of_structure=type_of_structure)


class StructureType(models.Model):
    """ Type de structure """

    # Champs
    type_of_structure = models.CharField("type de structure", max_length=255, unique=True)
    objects = StructureTypeQuerySet.as_manager()

    # Overrides
    def __str__(self):
        """ Représentation du type de structure """
        return re.sub(r'(\w+)', lambda w: w.group()[0].upper() + w.group()[1:], self.type_of_structure)

    def natural_key(self):
        return self.type_of_structure,

    # Méta
    class Meta:
        verbose_name = "type de structure"
        verbose_name_plural = "types de structure"
        default_related_name = "structuretypes"
        ordering = ['type_of_structure']
        app_label = "organisateurs"


class StructureQuerySet(models.QuerySet):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Structure(models.Model):
    """ Structure """

    # Constantes
    NAME_HELP = "Précisez le nom de la structure organisatrice (nom de l'association ou du club, de la société ou de la personne physique " \
                "s'il s'agit d'un particulier) de la manifestation"
    TYPE_HELP = "opérez votre choix parmi les statuts juridiques proposés"

    # Champs
    name = models.CharField("nom", help_text=NAME_HELP, max_length=200, unique=True)
    type_of_structure = models.ForeignKey(StructureType, verbose_name="forme juridique de la structure", help_text=TYPE_HELP, on_delete=models.CASCADE)
    address = models.CharField("adresse", max_length=255)
    commune = models.ForeignKey(Commune, verbose_name="commune", on_delete=models.CASCADE)
    phone = models.CharField("numéro de téléphone", max_length=14)
    website = models.URLField("site web", max_length=200, blank=True)
    organisateur = models.OneToOneField('organisateurs.organisateur', related_name='structure', verbose_name="organisateur", on_delete=models.CASCADE)
    objects = StructureQuerySet.as_manager()

    # Overrides
    def __str__(self):
        """ Représentation de l'objet """
        return self.name.title()

    def get_absolute_url(self):
        """ Renvoyer l'URL d'accès à l'objet """
        return reverse('events:dashboard')

    def natural_key(self):
        return self.name,

    # Getter
    def get_departement(self):
        return self.commune.get_departement()

    # Meta
    class Meta:
        verbose_name = "structure"
        default_related_name = "structures"
        app_label = "organisateurs"
