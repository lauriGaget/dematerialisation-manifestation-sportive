# coding: utf-8
from django.test import TestCase

from administration.factories.service import CompagnieFactory, CGServiceFactory, CGDFactory
from authorizations.factories import ManifestationAuthorizationFactory
from ..factories import OrganisateurFactory


class OrganisateurMethodTests(TestCase):
    """ Tests des organisateurs """

    def setUp(self):
        """ Configuration """
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.authorization = ManifestationAuthorizationFactory.create()
        self.manifestation = self.authorization.get_manifestation()
        self.commune = self.manifestation.departure_city
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.cg = self.departement.cg
        self.edsr = self.departement.edsr
        self.ddsp = self.departement.ddsp
        self.sdis = self.departement.sdis
        self.ggd = self.departement.ggd
        self.activite = self.manifestation.activite
        self.compagnie = CompagnieFactory.create(sdis=self.sdis)
        self.cgservice = CGServiceFactory.create(cg=self.cg)
        self.cgd = CGDFactory.create(commune=self.commune)
        self.structure = self.manifestation.structure
        self.organisateur = self.structure.organisateur

    def test_properties(self):
        """ Vérifier que les propriétés/méthodes renvoient les valeurs attendues """
        self.assertEqual(self.organisateur.get_departement(), self.structure.commune.arrondissement.departement)
