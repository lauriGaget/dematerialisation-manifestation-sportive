# coding: utf-8
from django.shortcuts import render, get_object_or_404

from core.util.permissions import require_role
from events.models.manifestation import Manifestation
from evenements.models.manifestation import Manif


def require_owner(function=None):
    """ Décorateur : limite l'accès aux organisateurs d'un événement """

    def decorator(view_func):
        @require_role('organisateur')
        def _wrapped_view(request, *args, **kwargs):
            if request.user == Manifestation.objects.get(pk=kwargs['pk']).structure.organisateur.user:  # noqa
                return view_func(request, *args, **kwargs)
            else:
                return render(request, "core/access_restricted.html",
                              {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_proprietaire(function=None):
    """ Décorateur : limite l'accès aux organisateurs d'un événement """

    def decorator(view_func):
        @require_role('organisateur')
        def _wrapped_view(request, *args, **kwargs):
            # Dans le cas d'une demande de complément d'information, le pk fourni est celui de la demande d'information
            if request.path.split('/')[-3] == 'doc_complementaire':
                manif = get_object_or_404(Manif, documentscomplementaires__pk=kwargs['pk'])
            # Dans le cas d'une édition d'évaluation n2k, le pk fourni est celui de l'évaluation'
            elif request.path.split('/')[-4] == 'natura2000evaluation' and request.path.split('/')[-2] == 'edit':
                manif = get_object_or_404(Manif, natura2000evaluations__pk=kwargs['pk'])
            elif request.path.split('/')[-4] == 'n2keval' and request.path.split('/')[-2] == 'edit':
                manif = get_object_or_404(Manif, n2kevaluation__pk=kwargs['pk'])
            # Dans le cas d'une édition d'évaluation rnr, le pk fourni est celui de l'évaluation'
            elif request.path.split('/')[-4] == 'rnrevaluation' and request.path.split('/')[-2] == 'edit':
                manif = get_object_or_404(Manif, rnrevaluations__pk=kwargs['pk'])
            elif request.path.split('/')[-4] == 'rnreval' and request.path.split('/')[-2] == 'edit':
                manif = get_object_or_404(Manif, rnrevaluation__pk=kwargs['pk'])
            else:
                # Cas général ou le pk est celui de la manif
                pk = kwargs.get('pk') or kwargs.get('manif_pk')
                manif = get_object_or_404(Manif, pk=pk)
            if request.user == manif.structure.organisateur.user:  # noqa
                return view_func(request, *args, **kwargs)
            else:
                return render(request, "core/access_restricted.html",
                              {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
