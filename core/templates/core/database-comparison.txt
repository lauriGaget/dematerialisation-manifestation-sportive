État des bases de données
==========================
{{ "Modèle" | ljust:40 }}...{% for name in names %}{{ name | rjust:7 }}{% endfor %}
{% for model, state in data.items %}
{{ model | ljust:40 }} : {% for val in state %}{{ val | rjust:7 }}{% endfor %}{% endfor %}
