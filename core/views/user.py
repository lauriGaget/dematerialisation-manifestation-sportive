# coding: utf-8
import logging
import html

from django.http import HttpResponse
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.views.generic import DetailView, TemplateView, View
from django.views.generic.edit import UpdateView
from allauth.account.views import SignupView, PasswordChangeView
from allauth.account.utils import send_email_confirmation
from allauth.account.forms import ResetPasswordForm
from django.template.loader import render_to_string
from django.core.mail import send_mail, get_connection
from django.contrib.auth import get_user_model

from configuration import settings
from core.models import User
from core.forms import UserUpdateForm, SignupAgentForm, SignupOrganisateurForm


mail_logger = logging.getLogger('smtp')


class RenvoiMailConfirm(View):
    """
    Vue utilisée par l'admin pour renvoyer un mail de confirmation
    """
    def get(self, request, pk):
        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
            user = get_object_or_404(User, pk=pk)
            send_email_confirmation(request, user)
            return redirect("admin:core_user_change", pk)
        else:
            return redirect("admin:core_user_change", pk)


class ProfileDetailView(DetailView):
    """ Vue du profil utilisateur """

    # Configuration
    model = get_user_model()

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class PasswordChangeCustomView(PasswordChangeView):
    """
    Evite de rester sur la même vue après un changement de mot de passe
    L'url est interceptée avant transmission à allauth
    """
    success_url = reverse_lazy('profile')


class PasswordExpired(View):
    """
    Si le mot de passe est perimé on l'efface
    """
    def get(self, request):
        # le mot de passe va être purgé et on va envoyé un mail de reset
        request.user.set_unusable_password()
        request.user.save()
        reset_password_form = ResetPasswordForm(data={'email': request.user.email})
        if reset_password_form.is_valid():
            reset_password_form.save(request=request)

            # bof Avertir les administrateurs d'instance
            instance = request.user.get_instance()
            admin_instance_list = User.objects.filter(
                groups__name__icontains='Administrateurs d\'instance').filter(
                default_instance=instance).values_list('email', flat=True)
            if admin_instance_list:
                try:
                    sender_email = request.user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    email_config = request.user.get_instance().get_email_settings()
                    connection = get_connection(use_tls=True, **email_config)
                    subject = "Mot de passe expiré"
                    msg = render_to_string('notifications/mail/password_expired.txt', {'username': request.user.username,
                                                                                       'email': request.user.email})
                    send_mail(subject=html.unescape(subject), message=html.unescape(msg),
                              from_email=sender_email,
                              recipient_list=list(admin_instance_list), connection=connection)
                except:
                    pass
            # eof Avertir les administrateurs d'instance

            logout(request)
            return render(request, "core/password_expired.html", status=400)
        else:
            return render(request, "403.html", status=400)


class UserUpdateView(UpdateView):
    """ Modification du profil utilisateur """

    # Configuration
    model = get_user_model()
    form_class = UserUpdateForm
    success_url = '/accounts/profile/'

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class SignupAgentView(SignupView):
    form_class = SignupAgentForm
    template_name = "account/signupagent.html"
    view_name = 'signupagentview'

    def get_context_data(self, **kwargs):
        ret = super(SignupAgentView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret


signupagentview = SignupAgentView.as_view()


class SignupOrganisateurView(SignupView):
    form_class = SignupOrganisateurForm
    view_name = 'signuporganisateurview'

    def get_context_data(self, **kwargs):
        ret = super(SignupOrganisateurView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret


signuporganisateurview = SignupOrganisateurView.as_view()

class EmailConfirmedView(TemplateView):
    template_name = "account/email_confirmed.html"

    def get_context_data(self, **kwargs):
        context = super(EmailConfirmedView, self).get_context_data(**kwargs)
        context['valid'] = True if self.request.GET.get('valid') == 'true' else False
        return context


@method_decorator(login_required, name='dispatch')
class NomPrenomChange(View):

    def post(self, request):
        last_name = request.POST['nom']
        first_name = request.POST['prenom']
        user = request.user
        user.last_name = last_name
        user.first_name = first_name
        user.save()
        return HttpResponse(200)
