import urllib

from django.http import HttpResponse
from django.http import HttpResponseForbidden

from django.contrib.contenttypes.models import ContentType

from evenements.models import Manif
from instructions.models import Instruction
from core.util.user import UserHelper
from core.models.instance import Instance


def media_access(request, path):
    """
    Fonction pour verifier le droit d'acces au fichier media
    - Fichier ckeditor ou tmp tout le monde
    - admin techniques : tout les droits
    - admin instance & instructeur & mairie agent : tous ceux de son instance
    - organisateur ceux de ses manif
    - autres selon le droit d'acces à la manif en question

    Nessecite de configurer Nginx avec le code suivant

    location /protected/
        {
            internal;
            alias /var/www/html/;
        }

    """
    # cas où ce sont des fichiers publics d'aide
    if path.split('/')[0]=="uploads" or path.split('/')[0]=="exportation":
        response = HttpResponse()
        # Content-type will be detected by nginx
        del response['Content-Type']
        response['X-Accel-Redirect'] = '/protected/media/' + path
        print('ok')
        return response

    # cas utilisateur non auth
    if not request.user.is_authenticated:
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")

    # cas où fichier de manifestations
    id = path.split("/")[1]
    if not id.isdigit():
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")

    manif = Manif.objects.filter(pk=id)
    if not manif:
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")
    manif = manif.first()
    user = request.user
    ok = False
    if user.has_group('Administrateurs techniques'):
        ok = True
    # Mairie agent ont accès même si ils sont dans le role agent consulté car on ne peut pas savoir
    elif hasattr(user, 'instructeur') or hasattr(user, 'maireagent') or user.has_group("Administrateurs d'instance") :
        if manif.get_instance() == user.get_instance():
            ok = True
    elif hasattr(user, 'organisateur') and user.organisateur == manif.get_organisateur():
        ok = True
    # ici on reprend ce qu'on avait dans le tableau de bord
    else:
        manifacceslibre = Instruction.objects.all().prefetch_related('manif__ville_depart__arrondissement__departement')
        filtre_specifique = None
        role = UserHelper.get_role_name(user)
        instance = user.get_instance()
        if not filtre_specifique and role in ['federationagent', 'serviceagent', 'ddspagent', 'cgagent',
                                              'mairieagent', 'cgsuperieuragent', 'sdisagent'] \
                and UserHelper.get_role_name(user) in ['federationagent', 'serviceagent', 'ddspagent', 'cgagent',
                                                       'mairieagent', 'cgsuperieuragent', 'sdisagent']:
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
            if user.has_role('cgsuperieuragent') and role == 'cgsuperieuragent':
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                         avis__etat__in=['formaté', 'rendu']).distinct()
        elif user.has_role('codisagent') and role == 'codisagent':
            service = user.get_departement().sdis
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                     avis__etat='rendu')
        elif user.has_role('ggdagent') and role == 'ggdagent':
            if instance.get_workflow_ggd() == Instance.WF_EDSR:
                service = user.get_departement().edsr
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                         avis__etat__in=['formaté', 'rendu']).distinct()
            else:
                service = user.get_service()
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
        elif user.has_role('edsragent') and role == 'edsragent':
            if instance.get_workflow_ggd() == Instance.WF_GGD_EDSR:
                service = user.get_departement().ggd
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                         avis__etat__in=['transmis', 'distribué', 'formaté',
                                                                         'rendu']).distinct()
            else:
                service = user.get_service()
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
        elif user.has_role('cisagent') and role == 'cisagent':
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__acces__isnull=False,
                                                     avis__acces__content_type=ct_service,
                                                     avis__acces__object_id=service.id,
                                                     avis__etat='rendu').distinct()
        elif user.has_role('brigadeagent') and role == 'brigadeagent':
            if instance.get_workflow_ggd() == Instance.WF_EDSR:
                manifacceslibre = manifacceslibre.filter(avis__service_concerne='edsr').par_instance(request=request)
            elif instance.get_workflow_ggd() in [Instance.WF_GGD_EDSR, Instance.WF_GGD_SUBEDSR]:
                manifacceslibre = manifacceslibre.filter(avis__service_concerne='ggd').par_instance(request=request)

        elif role in ['compagnieagentlocal', 'cgdagentlocal', 'commissariatagentlocal',
                      'cgserviceagentlocal', 'edsragentlocal'] \
                and UserHelper.get_role_name(user) in ['compagnieagentlocal', 'cgdagentlocal', 'commissariatagentlocal',
                                                       'cgserviceagentlocal', 'edsragentlocal']:
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__preavis__content_type=ct_service,
                                                     avis__preavis__object_id=service.id)

        if hasattr(manif, "instruction") and manif.instruction in manifacceslibre:
            ok = True

    if ok:
        response = HttpResponse()
        # Content-type will be detected by nginx
        del response['Content-Type']
        # gunicorn et nginx ne semble pas vouloir se comprendre cette ligne permet de mettre le bon encodage
        path = urllib.parse.quote(path)
        response['X-Accel-Redirect'] = '/protected/media/' + path
        return response
    else:
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")