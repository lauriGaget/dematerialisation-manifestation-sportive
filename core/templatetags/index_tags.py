from django import template

register = template.Library()


@register.filter
def index(liste, i):
    """ Renvoyer l'élément i d'une liste """
    if isinstance(liste, list):
        return liste[int(i)]
    else:
        return liste