# coding: utf-8

from django.contrib.auth.signals import user_logged_in
from django.dispatch.dispatcher import receiver

from core.models.user import User


@receiver(user_logged_in, sender=User)
def login_actions(sender, request, user, **kwargs):
    """ Traiter la connexion d'un utilisateur """
    # Si l'utilisateur se connecte, ce n'est plus la peine de le marquer comme ayant changé de login
    if user.status == User.USERNAME_CHANGED:
        user.status = User.NORMAL
        user.save()
