# coding: utf-8
import factory
from django.contrib.auth import get_user_model


class UserFactory(factory.django.DjangoModelFactory):
    """ Factory utilisateur """

    # Champs
    username = factory.Sequence(lambda n: 'jdoe{0}'.format(n))
    email = factory.Sequence(lambda n: 'ne-pas-repondre-42{0}@manifestationsportive.fr'.format(n))
    last_name = "Robert"
    first_name = "Bob"

    # Meta
    class Meta:
        model = get_user_model()
