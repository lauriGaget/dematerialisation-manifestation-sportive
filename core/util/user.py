# coding: utf-8
"""
Utilitaires pour les utilisateurs

UserHelper : cette classe permet plusieurs choses :
    - récupérer le type d'agent correspondant au rôle d'un utilisateur
    - récupérer le service attaché à un utilisateur. ex.: s'il est agent GGD, renvoyer son GGD
"""
from collections import OrderedDict
from operator import itemgetter


class UserHelper:
    """ Utilitaires liés aux utilisateurs """

    # Constantes
    ROLE_TYPES = ['ggdagent', 'edsragent', 'cgdagentlocal', 'brigadeagent', 'codisagent', 'sdisagent', 'compagnieagentlocal', 'cisagent',
                  'ddspagent', 'commissariatagentlocal', 'serviceagent', 'federationagent', 'mairieagent', 'cgagent',
                  'cgsuperieuragent', 'cgserviceagentlocal', 'organisateur', 'instructeur', 'edsragentlocal', 'secouriste', 'observateur']
    AGENT_TYPES = ['ggdagent', 'edsragent', 'brigadeagent', 'codisagent', 'sdisagent', 'cisagent',
                   'ddspagent', 'serviceagent', 'federationagent', 'mairieagent', 'cgagent', 'cgsuperieuragent']
    AGENTLOCAL_TYPES = ['cgdagentlocal', 'commissariatagentlocal', 'cgserviceagentlocal', 'edsragentlocal', 'compagnieagentlocal']
    OBSERVATEUR_TYPES = ['secouriste', ]
    DIRECT_TYPES = ['organisateur', 'instructeur', 'observateur']
    ROLE_CHOICES = [['ggdagent', "Agent départemental GGD"], ['edsragent', "Agent départemental EDSR"], ['cgdagentlocal', "Agent local CGD"],
                    ['brigadeagent', "Agent départemental brigade"], ['codisagent', "Agent départemental CODIS"], ['sdisagent', "Agent départemental SDIS"],
                    ['compagnieagentlocal', "Agent local compagnie"], ['cisagent', "Agent départemental CIS"], ['ddspagent', "Agent départemental DDSP"],
                    ['commissariatagentlocal', "Agent local commissariat"], ['serviceagent', "Agent départemental service"],
                    ['federationagent', "Agent départemental fédération"], ['mairieagent', "Agent mairie"], ['cgagent', "Agent départemental CD"],
                    ['cgsuperieuragent', "Supérieur CD"], ['cgserviceagentlocal', "Agent local service CD"], ['organisateur', "Organisateur"],
                    ['instructeur', "Instructeur"], ['edsragentlocal', "Agent local EDSR"], ['secouriste', "Secouriste"], ['observateur', "Observateur"]]

    SERVICES = {'ggdagent': 'ggd', 'edsragent': 'edsr', 'edsragentlocal': 'edsr', 'cgdagentlocal': 'cgd', 'brigadeagent': 'brigade', 'codisagent': 'codis',
                'sdisagent': 'sdis', 'compagnieagentlocal': 'compagnie', 'cisagent': 'cis', 'ddspagent': 'ddsp', 'commissariatagentlocal': 'commissariat',
                'serviceagent': 'service', 'federationagent': 'federation', 'mairieagent': 'commune', 'cgagent': 'cg', 'cgsuperieuragent': 'cg',
                'cgserviceagentlocal': 'cg_service', 'instructeur': 'prefecture', 'organisateur': 'structure'}
    ROLE_CHOICES = sorted(ROLE_CHOICES, key=itemgetter(1))
    ROLE_CHOICES_DICT = OrderedDict(ROLE_CHOICES)

    # Fonctions
    @staticmethod
    def get_role_names(user):
        """ Renvoie les chaînes correspondant aux rôles de l'utilisateur """
        roles = []
        for role in UserHelper.ROLE_TYPES:
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    roles.append(role)
            except AttributeError:
                pass
            # Tester le nom de modèle pour user.agent (uniquement avec parent_link=True)
            try:
                model_name = user.agent._meta.model_name
                if model_name in UserHelper.ROLE_TYPES:
                    roles.append(model_name)
            except AttributeError:
                pass
            # Tester le nom de modèle pour user.agentlocal (uniquement avec parent_link=True)
            try:
                model_name = user.agentlocal._meta.model_name
                if model_name in UserHelper.ROLE_TYPES:
                    roles.append(model_name)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agent (déprécie depuis parent_link)
            try:
                if getattr(user.agent, role) is not None:
                    roles.append(role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal (déprécié depuis parent_link)
            try:
                if getattr(user.agentlocal, role) is not None:
                    roles.append(role)
            except AttributeError:
                pass
        return roles

    @staticmethod
    def get_role_name(user):
        """ Renvoyer un nom de rôle pour l'utilisateur """
        roles = UserHelper.get_role_names(user)
        return roles[0] if roles else None

    @staticmethod
    def get_role_instance(user, role=None):
        """ Renvoyer l'objet de rôle pour l'utilisateur """
        if not role:
            for role in UserHelper.ROLE_TYPES:
                # Tester les rôles dont le modèle est lié directement à User.agent
                try:
                    if getattr(user.agent, role) is not None:
                        return getattr(user.agent, role)
                except AttributeError:
                    pass
                # Tester les rôles dont le modèle est lié directement à User.agentlocal
                try:
                    if getattr(user.agentlocal, role) is not None:
                        return getattr(user.agentlocal, role)
                except AttributeError:
                    pass
                # Tester les rôles dont le modèle est lié directement à User
                try:
                    if getattr(user, role) is not None:
                        return getattr(user, role)
                except AttributeError:
                    pass
        else:
            try:
                if getattr(user.agent, role) is not None:
                    return getattr(user.agent, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal
            try:
                if getattr(user.agentlocal, role) is not None:
                    return getattr(user.agentlocal, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    return getattr(user, role)
            except AttributeError:
                pass

        # Si les autres méthodes ne marchent pas, on suppose que parent_link a été utilisé
        try:
            return user.agentlocal
        except AttributeError:
            pass
        try:
            return user.agent
        except AttributeError:
            pass
        return None

    @staticmethod
    def has_role(user, role):
        """ Renvoyer si l'utilisateur possède un rôle, donné par son nom par UserHelper.ROLE_TYPES """
        # Tester les rôles dont le modèle est lié directement à User
        if user.is_anonymous:
            return False
        try:
            if getattr(user, role) is not None:
                return True
        except AttributeError:
            pass
        # Tester les rôles dont le modèle est lié directement à User.agent
        try:
            if getattr(user.agent, role) is not None:
                return True
        except AttributeError:
            pass
        # Tester les rôles dont le modèle est lié directement à User.agentlocal
        try:
            if getattr(user.agentlocal, role) is not None:
                return True
        except AttributeError:
            pass
        return False

    @staticmethod
    def get_service_instance(user):
        """ Renvoyer l'instance de service correspondant à l'utilisateur """
        role_instance = UserHelper.get_role_instance(user)
        role_field = UserHelper.get_role_name(user)
        service_field = UserHelper.SERVICES.get(role_field, None)
        try:
            if service_field is not None and role_instance is not None:
                return getattr(role_instance, service_field)
            else:
                return None
        except AttributeError:
            return None
