# coding: utf-8
from collections import OrderedDict
from os import path

from allauth.account.models import EmailAddress
from django.apps import apps
from django.conf import settings
from django.template.loader import render_to_string


def dump_diagnostics(filename, only=None):
    """
    Enregistrer sur disque l'état général des bases connues

    :param filename: Nom de fichier. Enregistré dans /tmp/
    :param only: Liste des noms de bases de données à exporter
    """
    models = apps.get_models()
    databases = only or settings.DATABASES.keys()
    data = dict()

    for database in databases:
        data[database] = dict()
        for model in models:
            data[database][model._meta.label_lower] = model.objects.using(database).count()

    output = render_to_string('core/database-diagnostics.txt', {'data': data})

    with open(path.join('/tmp/', filename), 'w', encoding='utf-8') as out:
        out.write(output)

    print("Données exportées avec succès dans /tmp/{name}".format(name=filename))


def dump_compared_diagnostics(filename):
    """
    Enregistrer sur disque un fichier de log

    contenant l'état général des bases connues, généralement après fusion de n bases
    (fusion effectuée via la commande full_merge)
    L'état est sous la forme d'un tableau de plusieurs lignes.
    Chaque ligne contient le nom de modèle, le nombre d'objets dans la table
    pour chacune des bases, ainsi qu'un total de toutes les bases sauf default.

    Ex. :
    core.user  200  50  100  50  200
    aide.faq    48  12   16  20   48

    :param filename: Nom de fichier. Enregistré dans /tmp/
    """
    models = apps.get_models()
    models = sorted(models, key=lambda x: x._meta.label_lower)
    databases = list(settings.DATABASES.keys())
    data = OrderedDict()

    for model in models:
        line = []
        for database in databases:
            line.append(model.objects.using(database).count())
        line.append(sum(line[1:]))
        data[model._meta.label_lower] = line.copy()

    output = render_to_string('core/database-comparison.txt', {'data': data, 'names': databases + ['total']})

    with open(path.join('/tmp/', filename), 'w', encoding='utf-8') as out:
        out.write(output)

    print("Récapitulatif d'exportation exporté avec succès dans /tmp/{name}".format(name=filename))


def dump_emails():
    output = "\n".join(EmailAddress.objects.values_list('email', flat=True).order_by('email'))
    with open('/tmp/post_emails.txt', 'w', encoding='utf-8') as out:
        out.write(output)
