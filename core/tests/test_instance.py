# coding: utf-8
from django.conf import settings
from django.http.request import HttpRequest
from django.test import TestCase

from administrative_division.factories import DepartementFactory
from core.factories.instance import InstanceAFactory, MasterInstanceFactory
from core.factories.user import UserFactory
from core.models.instance import Instance
from core.util.application import get_current_subdomain


class TestInstance(TestCase):

    # Tests
    def test_departement(self):
        """ Tester les liens département-instance """
        master = MasterInstanceFactory.build()
        instance = InstanceAFactory.build()
        self.assertEqual(instance.get_manifestation_anm_ldept_delay(), 47)
        self.assertNotEqual(instance.get_manifestation_anm_ndept_delay(), None)
        self.assertEqual(instance.get_manifestation_anm_ndept_delay(), master.get_manifestation_anm_ndept_delay())
        self.assertTrue(master.is_master())

    def test_domain(self):
        """ Tester les accès et noms de domaine """
        request = HttpRequest()
        # Test de sous-domaine
        request.META['HTTP_HOST'] = '42.test.com'
        subdomain = get_current_subdomain(request)
        self.assertEqual(subdomain, "42")
        # Test de domaine principal
        request.META['HTTP_HOST'] = 'www.test.com'
        subdomain = get_current_subdomain(request)
        self.assertEqual(subdomain, "www")
        # Vérifier qu'une requête sur un sous-domaine est bien liée à une instance
        departement = DepartementFactory.create()
        instance = departement.instance
        department_name = instance.departement.name
        request.META['HTTP_HOST'] = '{0}.test.com'.format(department_name)  # pour le sous-domaine A, on doit bien trouver l'instance du département A.
        self.assertEqual(Instance.objects.get_for_request(request), instance)
        request.META['HTTP_HOST'] = 'random.test.com'  # pour un sous-domaine dont le nom ne correspond pas à un nom de département, on renvoie master
        self.assertNotEqual(Instance.objects.get_for_request(request), instance)
        self.assertEqual(Instance.objects.get_for_request(request), Instance.objects.get_master())

    def test_user(self):
        """ Tester que les instances fonctionnent correctement pour les utilisateurs """
        master = MasterInstanceFactory.create()
        instance = InstanceAFactory.build()
        user_a = UserFactory.build(default_instance=instance)
        user_b = UserFactory.build()
        self.assertEqual(user_a.get_instance(), instance)
        self.assertEqual(user_b.get_instance(), master)

    def test_instance_mail_config(self):
        """ Tester les paramètres SMTP """
        instance = InstanceAFactory.build()
        config = instance.get_email_settings()
        self.assertTrue(bool(config))
        self.assertEqual(config['host'], settings.EMAIL_HOST)
        self.assertEqual(config['port'], settings.EMAIL_PORT)

        instance.email_settings = 'localhost;587;john@aa.aa;john'
        config = instance.get_email_settings()
        self.assertTrue(bool(config))
        self.assertEqual(config['host'], 'localhost')
        self.assertEqual(config['port'], 587)
        self.assertEqual(config['username'], 'john@aa.aa')
        self.assertEqual(config['password'], 'john')

    def test_instance_email_sending(self):
        """ Tester que les emails partent bien """
        instance = InstanceAFactory.build()
        instance.send_test_mail()
