from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.core import mail
import re

from core.models import Instance
from events.factories import AutorisationNMFactory
from authorizations.factories import ManifestationAuthorizationFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory
from core.factories import UserFactory
from administration.factories import InstructeurFactory, MairieAgentFactory
from sports.factories import ActiviteFactory


class InstructionANM(TestCase):
    """
    Test du circuit d'instruction d'une ANM
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= ANM 1 commune (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        structure = StructureFactory(commune=cls.commune)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)

        # Création de l'événement
        cls.manifestation = AutorisationNMFactory.create(departure_city=cls.commune,
                                                        structure=structure,
                                                        name='Manifestation_Test',
                                                        activite=activ)

    def test_Instruction_ANM(self):
        print('**** test 1 creation manif ****')
        self.declaration = ManifestationAuthorizationFactory.create(manifestation=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.departure_city, end=" ; ")
        print(self.manifestation.departure_city.get_departement(), end=" ; ")
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline)
        self.assertEqual(str(self.declaration.manifestation), str(self.manifestation))
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'demande d\'autorisation envoyée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_mairie.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])

        print('**** test 2 instruction préfecture ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        template = '/authorizations/dashboard/'
        reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        recherche = re.search('Aucune déclaration', reponse.content.decode('utf-8'))
        self.assertTrue(recherche, msg='test pas de déclaration')
        # Appel de la page
        template = '/declarations/dashboard/'
        reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.declaration.manifestation)
        self.assertContains(reponse, 'list-group-item-danger', count=1)
        self.assertNotContains(reponse, 'list-group-item-warning')
        self.assertNotContains(reponse, 'list-group-item-success')

        print('**** test 3 instruction mairie ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie', password='123'))
        template = '/declarations/dashboard/'
        # Appel de la page
        reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.declaration.manifestation)
        self.assertContains(reponse, 'list-group-item-danger', count=1)
        self.assertNotContains(reponse, 'list-group-item-warning')
        self.assertNotContains(reponse, 'list-group-item-success')


class InstructionANM2com(TestCase):
    """
    Test du circuit d'instruction d'une DNM avec 1 autre commune traversée
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= ANM 2 communes (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.depart = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.commune = CommuneFactory(name='Aboen', arrondissement=arrondissement)
        structure = StructureFactory(commune=cls.commune)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)

        # Création de l'événement
        cls.manifestation = AutorisationNMFactory.create(departure_city=cls.depart,
                                                        crossed_cities=[cls.commune,],
                                                        structure=structure,
                                                        name='Manifestation_Test',
                                                        activite=activ)

    def test_Instruction_ANM(self):
        print('**** test 1 creation manif ****')
        self.declaration = ManifestationAuthorizationFactory.create(manifestation=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.departure_city, end=" ; ")
        print(self.manifestation.departure_city.get_departement(), end=" ; ")
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline)
        self.assertEqual(str(self.declaration.manifestation), str(self.manifestation))
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'demande d\'autorisation envoyée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])

        print('**** test 2 instruction préfecture ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        template = '/declarations/dashboard/'
        reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content)
        recherche = re.search('Aucune déclaration', reponse.content.decode('utf-8'))
        self.assertTrue(recherche, msg='test pas d\'avis / préavis')
        # Appel de la page
        template = '/authorizations/dashboard/'
        reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.declaration.manifestation)
        self.assertContains(reponse, 'list-group-item-danger', count=1)
        self.assertNotContains(reponse, 'list-group-item-warning')
        self.assertNotContains(reponse, 'list-group-item-success')

        print('**** test 3 instruction mairie ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='agent_mairie', password='123'))
        template = '/declarations/dashboard/'
        # Appel de la page
        reponse = self.client.get(template, HTTP_HOST='127.0.0.1:8000')
        recherche = re.search('Aucune déclaration', reponse.content.decode('utf-8'))
        self.assertTrue(recherche, msg='test pas de déclaration')
