from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from core.models import Instance
from django.contrib.auth.hashers import make_password as make
from allauth.account.models import EmailAddress
from django.test import tag
import os, time

from core.factories import UserFactory
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from contacts.factories import ContactFactory
from events.models import Manifestation


class OrganisateurTestAVTMCIR(StaticLiveServerTestCase):
    """
    Test du circuit organisateur avec sélénium
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut

    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('======== Manisfestation AVTMCIR ========')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur',
                                              email='organisateur@example.com',
                                              password=make(123),
                                              default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.instructeur = UserFactory.create(username='instructeur',
                                             email='instructeur@example.com',
                                             password=make(123),
                                             default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)

        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create(discipline__motorise=True)
        cls.contact = ContactFactory.create()

        os.chdir("/tmp")
        for file in ('manifestation_rules', 'organisateur_commitment', 'safety_provisions', 'signalers_list',
                     'tech_organisateur_certificate', 'commissioners', 'hourly_itinerary', 'rounds_safety'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

        super(OrganisateurTestAVTMCIR, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Suppression des fichiers créés dans /tmp
            Arrêt du driver sélénium
        """
        os.chdir("/tmp")
        for file in ('manifestation_rules', 'organisateur_commitment', 'safety_provisions', 'signalers_list',
                     'tech_organisateur_certificate', 'commissioners', 'hourly_itinerary', 'rounds_safety'):
            os.remove(file+".txt")

        cls.selenium.quit()
        super(OrganisateurTestAVTMCIR, cls).tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    def chosen_select_multi(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("input").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def skip_test_autorisationVTMCIR(self):
        """
        Circuit organisateur pour une autorisation avec véhicules, manifestation sur circuit
        :return:
        """
        print('**** test déclaration manifestation ****')
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('organisateur')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Connexion avec organisateur réussie', self.selenium.page_source)

        self.selenium.find_element_by_link_text('Déclarer une nouvelle manifestation').click()
        self.selenium.find_element_by_link_text('42 - Loire').click()
        self.selenium.find_element_by_partial_link_text('Manifestation sportive avec').click()
        self.selenium.find_element_by_partial_link_text('Manifestation hors voie publique').click()
        self.assertIn('Organisation d\'une manifestation sportive', self.selenium.page_source)

        name = self.selenium.find_element_by_id('id_name')
        name.send_keys('La grosse course')

        description = self.selenium.find_element_by_id('id_description')
        description.send_keys('une grosse course qui déchire')
        time.sleep(self.DELAY)

        self.chosen_select('id_discipline_chosen', 'Discipline')
        time.sleep(self.DELAY)

        self.chosen_select('id_activite_chosen', 'Activite')
        time.sleep(self.DELAY)

        self.chosen_select('id_departure_city_chosen', 'Bard')
        time.sleep(self.DELAY)

        self.chosen_select_multi('id_crossed_cities_chosen', 'Roche')
        time.sleep(self.DELAY)

        parcours = self.selenium.find_element_by_id('id_route_descriptions')
        parcours.send_keys('parcours de 10Km')

        entries = self.selenium.find_element_by_id('id_number_of_entries')
        entries.send_keys('50')

        audience = self.selenium.find_element_by_id('id_max_audience')
        audience.send_keys('100')
        time.sleep(self.DELAY)

        self.chosen_select('id_support_chosen', 'circuit')
        time.sleep(self.DELAY)

        contact = self.selenium.find_element_by_xpath("//legend[text()='Personne à contacter sur place']//parent::fieldset")
        champs = contact.find_elements_by_tag_name("input")
        for champ in champs:
            name = champ.get_attribute("id").split('-')[-1]
            if name.lower() == name and name != '' and name != 'id':
                champ.send_keys(getattr(self.contact, name))
                time.sleep(self.DELAY * 1.5)

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        print('**** test étape 1 ****')
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # self.assertIn('La Grosse Course', self.selenium.page_source)
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')
        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape1 n\'est pas vert")
        try:
            boutons[1].find_element_by_class_name('fa-exclamation-triangle')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape2 n\'est pas rouge")

        files = self.selenium.find_elements_by_css_selector('.bg-primary li')
        for file in files:
            if file.text not in ('Réglement de la manifestation',
                                 'Déclaration et engagement de l\'organisateur',
                                 'Dispositions prises pour la sécurité', 'Cartographie de parcours'):
                self.assertTrue(False, msg="le fichier "+file.text+" n\'est pas dans la liste")

        self.selenium.find_element_by_partial_link_text('Joindre des documents').click()
        print('**** test ajout fichiers ****')
        self.assertIn('Fichiers attachés', self.selenium.page_source)

        manif = Manifestation.objects.get()
        manif.openrunner_route = (456132,)
        manif.save()

        file1 = self.selenium.find_element_by_id('id_manifestation_rules')
        file1.send_keys('/tmp/manifestation_rules.txt')
        file2 = self.selenium.find_element_by_id('id_organisateur_commitment')
        file2.send_keys('/tmp/organisateur_commitment.txt')
        file3 = self.selenium.find_element_by_id('id_safety_provisions')
        file3.send_keys('/tmp/safety_provisions.txt')
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()

        self.selenium.find_element_by_partial_link_text('Demander l\'autorisation').click()

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        print('**** test étape 2 ****')
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # self.assertIn('La Grosse Course', self.selenium.page_source)
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')
        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape1 n\'est pas vert")
        try:
            boutons[1].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape2 n\'est pas vert")

        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Déconnexion').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        print('**** test instructeur ****')
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('instructeur')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY*4)
        self.assertIn('Connexion avec instructeur réussie', self.selenium.page_source)
        declar = self.selenium.find_element_by_class_name('authorizations-requested')
        try:
            declar.find_element_by_partial_link_text('La Grosse Course')
            declar.find_element_by_class_name('list-group-item-danger')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en rouge")
