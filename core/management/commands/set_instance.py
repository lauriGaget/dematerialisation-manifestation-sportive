# coding: utf-8

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db.utils import DEFAULT_DB_ALIAS

from core.models.instance import Instance
from core.util.security import protect_code
from events.models.manifestation import Manifestation


class Command(BaseCommand):
    """
    Mettre à jour le champ d'instance des manifestations et des utilisateurs d'un serveur

    L'étape est nécessaire pour la fusion des données des différents serveurs déployés
    """
    args = ''
    help = "Définir l'instance des utilisateurs et manifestations d'un serveur"

    def add_arguments(self, parser):
        parser.add_argument('--database', action='store', dest='database', default=DEFAULT_DB_ALIAS,
                            help='Nominates a specific database to dump fixtures from. Defaults to the "default" database.')
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Demander une validation de l'utilisateur")
        parser.add_argument('--number', action='store', dest='departement', help='Département à assigner')

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        using = options.get('database')
        number = options.get('departement')
        interactive = options.get('interactive')
        instance = Instance.objects.db_manager(using).set_for_departement(number)
        reply = ''
        # Demander à confirmer l'action
        if interactive:
            print("Vous souhaitez assigner l'instance {num} aux entités de la base {base}".format(num=number, base=using))
            reply = input("Mettre à jour les utilisateurs et manifestations de cette base ? [oui/*] : ")

        if not interactive or reply.lower() == "oui":
            # Mettre à jour tous les utilisateurs
            updated1 = get_user_model().objects.using(using).filter(default_instance__isnull=True).update(default_instance=instance)
            print("{count} utilisateurs ont été mis à jour.".format(count=updated1))
            updated2 = Manifestation.objects.using(using).filter(instance__isnull=True).update(instance=instance)
            print("{count} manifestations ont été mises à jour.".format(count=updated2))
            if updated1 + updated2:
                print("L'opération a été effectuée avec succès. Terminé.")
                return
        print("Aucun élément de la base n'a été modifié. Terminé.")
        return
