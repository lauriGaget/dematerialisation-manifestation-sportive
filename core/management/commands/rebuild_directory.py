# coding: utf-8
"""
recontruit le système de fichier
"""
import os
import shutil
import random
from configuration import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from evenements.models import Manif


def deplacement(file, instance):
    if os.path.exists(file.path):
        # generation du chemin de la manif
        dir, base_filename = os.path.split(file.path)
        if instance.__class__.__name__=="Manif":
            manif = instance
        elif hasattr(instance, 'manifestation_ptr'):
            manif = instance.manifestation_ptr
        elif hasattr(instance, 'manif'):
            manif = instance.manif
        elif hasattr(instance, 'instruction'):
            manif = instance.instruction.manif
        departement_number = manif.get_instance().get_departement_name()
        if not os.path.exists(settings.MEDIA_ROOT+departement_number+'/'+str(manif.pk)):
            os.makedirs(settings.MEDIA_ROOT+departement_number+'/'+str(manif.pk))
        # si le fichier existe dejà on met un numero aléatoire devant le nom
        if (os.path.exists(settings.MEDIA_ROOT+departement_number+"/"+str(manif.pk)+'/'+base_filename)):
            rand = str(random.randint(1,99999))
            shutil.move(file.path, settings.MEDIA_ROOT+departement_number+"/"+str(manif.pk)+'/'+rand+base_filename)
            return departement_number+"/"+str(manif.pk)+'/'+rand+base_filename
        # sinon on le deplace
        else:
            shutil.move(file.path,
                        settings.MEDIA_ROOT + departement_number + "/" + str(manif.pk) + '/')
            return departement_number+"/"+str(manif.pk)+'/'+base_filename
    # cas où le fichier n'existe pas dans le repertoire on ne change rien
    else:
        return file.name


class Command(BaseCommand):



    def handle(self, *args, **options):
        """ Exécuter la commande """



        with transaction.atomic():
             for manif in Manif.objects.all():

                # pour chaque manif on va essayer de deplacer tout les fichiers
                cerfa = manif.cerfa
                cerfa.cartographie.name = deplacement(cerfa.cartographie, cerfa) if cerfa.cartographie else ''
                cerfa.reglement_manifestation.name = deplacement(cerfa.reglement_manifestation, cerfa)  if cerfa.reglement_manifestation else ''
                cerfa.disposition_securite.name = deplacement(cerfa.disposition_securite, cerfa) if cerfa.disposition_securite else ''
                cerfa.presence_docteur.name = deplacement(cerfa.presence_docteur, cerfa) if cerfa.presence_docteur else ''
                cerfa.docs_additionels.name = deplacement(cerfa.docs_additionels, cerfa) if cerfa.docs_additionels else ''
                cerfa.engagement_organisateur.name = deplacement(cerfa.engagement_organisateur, cerfa) if cerfa.engagement_organisateur else ''
                cerfa.certificat_assurance.name = deplacement(cerfa.certificat_assurance, cerfa) if cerfa.certificat_assurance else ''
                cerfa.topo_securite.name = deplacement(cerfa.topo_securite, cerfa) if cerfa.topo_securite else ''
                if hasattr(cerfa, 'dossier_tech_cycl') and cerfa.dossier_tech_cycl:
                    cerfa.dossier_tech_cycl.name = deplacement(cerfa.dossier_tech_cycl, cerfa)
                if hasattr(cerfa, "liste_signaleurs") and cerfa.liste_signaleurs:
                    cerfa.liste_signaleurs.name = deplacement(cerfa.liste_signaleurs, cerfa)
                if hasattr(cerfa, "itineraire_horaire") and cerfa.itineraire_horaire:
                    cerfa.itineraire_horaire.name = deplacement(cerfa.itineraire_horaire, cerfa)
                if hasattr(cerfa, 'avis_federation_delegataire') and cerfa.avis_federation_delegataire:
                    cerfa.avis_federation_delegataire.name = deplacement(cerfa.avis_federation_delegataire, cerfa)
                if hasattr(cerfa, "carte_zone_public") and cerfa.carte_zone_public:
                    cerfa.carte_zone_public.name = deplacement(cerfa.carte_zone_public, cerfa)
                if hasattr(cerfa, "commissaires") and cerfa.commissaires:
                    cerfa.commissaires.name = deplacement(cerfa.commissaires, cerfa)
                if hasattr(cerfa, "certificat_organisateur_tech") and cerfa.certificat_organisateur_tech:
                    cerfa.certificat_organisateur_tech.name = deplacement(cerfa.certificat_organisateur_tech, cerfa)
                if hasattr(cerfa, "participants") and cerfa.participants:
                    cerfa.participants.name = deplacement(cerfa.participants, cerfa)
                if hasattr(cerfa, 'plan_masse') and cerfa.plan_masse:
                    cerfa.plan_masse.name = deplacement(cerfa.plan_masse, cerfa)

                cerfa.save()
                if manif.documentscomplementaires.all():
                    for doc in manif.documentscomplementaires.all():
                        doc.document_attache.name = deplacement(doc.document_attache, doc) if doc.document_attache else ''
                        doc.save()
                if hasattr(manif, 'instruction'):
                    for doc in manif.instruction.documents.all():
                        doc.fichier.name = deplacement(doc.fichier, doc)
                        doc.save()

                if hasattr(manif, 'instruction'):
                    for avi in manif.instruction.avis.all():
                        avi.document_attache.name = deplacement(avi.document_attache, avi) if avi.document_attache else ''
                        avi.save()






