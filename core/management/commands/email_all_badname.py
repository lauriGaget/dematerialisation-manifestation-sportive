# coding: utf-8
"""
Avertir tout ceux qui ont à renseigner ou _ qu'il faut mettre à jour leur nom prenom
"""
from django.core.management.base import BaseCommand
from django.db import transaction
# coding: utf-8
import html
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail, get_connection




class Command(BaseCommand):
    help = 'Verifie les noms prenoms et les remplace par à renseigner _ si vide'
    def handle(self, *args, **options):
        """ Exécuter la commande """
        with transaction.atomic():
            nom = "à renseigner"
            prenom = "à renseigner"
            for user in get_user_model().objects.all():
                if user.last_name==nom or user.first_name==prenom:
                    sender_email =  settings.DEFAULT_FROM_EMAIL
                    values = "{host};{port};{user};{pwd}".format(host=settings.EMAIL_HOST, port=int(settings.EMAIL_PORT),
                                                                 user=settings.EMAIL_HOST_USER,
                                                                 pwd=settings.EMAIL_HOST_PASSWORD)
                    values = values.split(';')
                    config = {'host': values[0], 'port': int(values[1]), 'username': values[2], 'password': values[3]}
                    email_config = config
                    connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
                    message = "<p>Bonjour</p><p>Vous n'avez pas enregistré de Nom et de Prénom dans la plateforme. Pour de futures fonctionnalités ceux-ci ont besoin d'être renseignés.<br><br>"+\
                              "Merci de vous connecter à la plateforme et de les enregistrer dans votre profil utilisateur</p>"+\
                              "<p>Cordialement</p><p>Manifestation Sportive</p>"

                    send_mail(subject=html.unescape("Manifestation sportive"), message=html.unescape(message), from_email=sender_email,
                              recipient_list=[user.email], connection=connection)

            print("Great success !")
