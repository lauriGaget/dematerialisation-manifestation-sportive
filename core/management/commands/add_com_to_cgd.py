# coding: utf-8
from django.core.management.base import BaseCommand
from django.db import transaction

from administrative_division.models import Commune
from administration.models import CGD


class Command(BaseCommand):
    """
    Remplir le champ commune des CGDs en fonction de leur arrondissement
    Champ nouvellement créé pour passer les CGDs sur les communes
    """
    args = ''
    help = "Remplir le champ commune des CGDs"

    def add_arguments(self, parser):
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Ne pas demander de validation de l'utilisateur")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        interactive = options.get('interactive')
        reply = ''

        if interactive is True:
            reply = input("Cette commande va affecter la commune du même nom que l'arrondissement à chaque CGD'. Continuer ? [oui/*]\n")

        if reply.lower() in ["oui", "o"] or not interactive:
            with transaction.atomic():
                for cgd in CGD.objects.all():
                    com = Commune.objects.filter(arrondissement=cgd.arrondissement).get(name=cgd.arrondissement.name)
                    cgd.commune = com
                    cgd.save()
                    print('{} - {:<25} \t>> {} - {}'.format(cgd.arrondissement.pk, cgd.arrondissement.name, com.pk, com.name))
        else:
            print("Commande abandonnée.")
