# coding: utf-8
"""
Commande de test ne pas executer
"""
from django.contrib.auth.hashers import identify_hasher
from django.core.management.base import BaseCommand
from concurrent.futures import ThreadPoolExecutor
from core.models import User

tab =[]
count = 51
def test(users, trucatest, haser):
    tab = []
    for user in users:

        algorithm, iterations, salt, hash = user.password.split('$', 3)
        test = haser.encode(user.username, salt, int(iterations))

        if test == user.password:
            tab.append(user)
    return tab

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def rempli(future):
    global tab
    global count
    result=future.result()
    tab+=result
    count =count - 1
    if count==0:
        print(len(tab))


class Command(BaseCommand):


    def handle(self, *args, **options):
        """ la commande va créer un fichier json avec toutes les erreurs. """
        bad_passs= 0
        users = User.objects.all()
        size = int(len(users) / 50)
        haser = identify_hasher(users.last().password)
        result = []
        with ThreadPoolExecutor(max_workers=50) as executor:
            for page in list(chunks(users, size)):
                future = executor.submit(test, page, "123", haser)
                future.add_done_callback(rempli)

