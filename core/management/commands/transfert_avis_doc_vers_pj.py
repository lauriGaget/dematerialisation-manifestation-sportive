# coding: utf-8
"""
Transférer les documents attachés des avis vers le modèle PiecejointeAvis
Commande à faire avant la migration qui supprime le champ doc_attache du modèle Avis
Ne concerne pas l'ancienne réglementation
Utilisable uniquement en mode DEBUG.
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction

from core.util.security import protect_code
from instructions.models import Avis, PieceJointeAvis


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Transférer les documents attachés des avis vers le modèle PiecejointeAvis'

    def add_arguments(self, parser):
        parser.add_argument('weeks_nb', nargs='?', type=int, default=104)

    def handle(self, *args, **options):
        """ Exécuter la commande """
        nb_doc = len([1 for avis in Avis.objects.all() if avis.document_attache])
        nb_pj = PieceJointeAvis.objects.all().count()
        protect_code()
        print('Il y a ' + str(nb_doc) + ' documents attachés dans les avis de la DB.')
        print('Il y a ' + str(nb_pj) + ' pièces jointes d\'avis dans la DB.')
        print('Transférer les documents vers pièces jointes.')
        print('Entrez Y pour continuer.')
        if (input() == 'Y'):
            if settings.DEBUG:
                with transaction.atomic():
                    for avis in Avis.objects.all():
                        if avis.document_attache:
                            if avis.date_reponse:
                                PieceJointeAvis.objects.create(fichier=avis.document_attache, avis=avis, date_depot=avis.date_reponse)
                            else:
                                PieceJointeAvis.objects.create(fichier=avis.document_attache, avis=avis, date_depot=avis.date_demande)
                            avis.document_attache = None
                            avis.save()
                    nb_pj_n = PieceJointeAvis.objects.all().count()
                    print(str(nb_pj_n - nb_pj) + " pièces jointes d\'avis ont été créées.")
            else:
                print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
        else:
            print('Abandon')