# coding: utf-8
"""
verifier tout les liens mort des pages d'(aide
"""
from django.core.management.base import BaseCommand
from aide.models.help import HelpPage, HelpNote
from aide.models.helpaccordion import HelpAccordionPage, HelpAccordionPanel

import requests
import json
class Command(BaseCommand):



    def handle(self, *args, **options):
        """ la commande va créer un fichier json avec toutes les erreurs. """
        result = []
        resulttemp=[]
        # verification page d'aide
        for aide in HelpPage.objects.all():
            contenu = aide.content.split('href="')
            count = 0
            provi=[]
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0]=="/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code==200 and not response.status_code==403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpage": resulttemp})
        resulttemp=[]
        # note d'aide
        for aide in HelpNote.objects.all():
            contenu = aide.content.split('href="')
            count = 0
            provi=[]
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0]=="/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code==200 and not response.status_code==403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpnote": resulttemp})
        resulttemp=[]
        # aide accordion
        for aide in HelpAccordionPanel.objects.all():
            contenu = aide.content.split('href="')
            count = 0
            provi=[]
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0]=="/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code==200 and not response.status_code==403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpaccordionpanel": resulttemp})
        resulttemp=[]
        # aide accordion page
        for aide in HelpAccordionPage.objects.all():
            contenu = aide.contentbefore.split('href="')
            count = 0
            provi=[]
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0]=="/":
                        url = "http://manifestationsportive.fr"+deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code==200 and not response.status_code==403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            contenu = aide.contentafter.split('href="')
            count = 0
            provi = []
            for text in contenu:
                if count:
                    deb = text.split('"')[0]
                    url = ""
                    if deb[0] == "/":
                        url = "http://manifestationsportive.fr" + deb
                    elif deb.startswith('http'):
                        url = deb
                    if url:
                        print(url)
                        try:
                            response = requests.get(url)
                            print(response.status_code)
                            if not response.status_code == 200 and not response.status_code == 403:
                                meh = {"url": url, "statut": response.status_code}
                                provi.append(meh)
                        except:
                            meh = {"url": url, "statut": "erreur"}
                            provi.append(meh)
                count += 1
            if len(provi):
                aze = {"nom": aide.title, "result": provi}
                resulttemp.append(aze)
        result.append({"helpaccordionpage": resulttemp})
        resulttemp=[]

        f = open("result.json", "w+")
        jsonresult = json.dumps(result)
        f.write(str(jsonresult))
        f.close()

