# coding: utf-8
from django.core.management.base import BaseCommand

from sports.models import Discipline


class Command(BaseCommand):
    """
    Mettre à jour le champ motorisé des activités motorisées

    Après une migration depuis une base de données de prod, un nouveau champ
    'motorise' dans sports.Discipline est à False, même pour les disciplines
    motorisées. Cette commande le met à True pour les disciplines choisies.
    """
    args = ''
    help = 'Mettre à jour le champ motorisé des activités motorisées'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        motorised_names = ['sportautomobile', 'motocyclisme']
        disciplines = Discipline.objects.all()
        for discipline in disciplines:
            if discipline.name in motorised_names:
                discipline.motorise = True
                discipline.save()
