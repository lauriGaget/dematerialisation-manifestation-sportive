# coding: utf-8
from crispy_forms.helper import FormHelper
from django import forms


class GenericForm(forms.ModelForm):
    """ Formulaire de base """

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super(GenericForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'

    def ajouter_css_class_requis_aux_champs_requis(self):
        """ Ajoute une classe css nommée 'requis' à tous les champs obligatoires """
        for cle in self.instance.get_liste_champs_requis():
            field = self.fields[cle]
            css = field.widget.attrs.get('class', '')
            field.widget.attrs['class'] = 'requis ' + css
