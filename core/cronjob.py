import logging
import os
from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from django.core.mail import send_mail, get_connection, mail_admins
from django.conf import settings
from django.utils import timezone

from allauth.account.models import EmailAddress
from smtplib import SMTPException

from core.models import User


mail_logger = logging.getLogger('smtp')


class DeleteFlagFile(CronJobBase):
    """
    Preview Manager n'arrive parfois pas à traiter des fichiers, il laisse des fichiers .flag derrière lui
    Ce qui peut le faire planter au second passage et remplir inutilement le dossier.
    Ce cron nettoie donc le dossier media des fichiers flag qui s'y trouve.
    """
    RUN_EVERY_MINS = 60  # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.deleteflagfile'

    def do(self):
        os.system('cd ' + settings.MEDIA_ROOT + ' && find . -name "*.*_flag" -type f -delete')


class DeleteExportationFile(CronJobBase):
    """
    Ici on va supprimer le dossier des fichiers temporaires de l'exportations
    """
    RUN_EVERY_MINS = 60 # à lancer toutes les heures

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.deleteexportationfile'

    def do(self):
        os.system('rm ' + settings.MEDIA_ROOT + 'exportation/* -R')


class EmailAdresseValide(CronJobBase):
    RUN_EVERY_MINS = 1  # à lancer toutes les minutes

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'core.emailadressevalide'    # a unique code

    def do(self):
        # log_retour = ""
        # delay_avertissement = 20
        # delay_suppression = 30
        #
        # # Purger les logs sans message
        # CronJobLog.objects.filter(code="core.emailadressevalide").filter(is_success=True).filter(
        #     message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()
        #
        # # User avec email non validée
        # liste = EmailAddress.objects.filter(verified=False,
        #                                     user__date_joined__lte=timezone.now() - timezone.timedelta(days=delay_avertissement),
        #                                     user__date_joined__gt=timezone.now() - timezone.timedelta(days=delay_suppression))
        # for emailaddress in liste:
        #     user = emailaddress.user
        #     log_retour += "%s : L'utilisateur %s n'a pas validé son adresse !" % (user.default_instance, user.username) + chr(10)
        #     sender_email = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
        #     email_config = user.get_instance().get_email_settings()
        #     connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
        #     message = "L'utilisateur " + user.get_full_name() + \
        #               " n'a pas confirmer son adresse email depuis la création de son compte, il y a " + \
        #               str(delay_avertissement) + " jours."+ chr(10) + \
        #               "Vérifier l'adresse mail de l'utilisateur " + user.username + "." + chr(10) + \
        #               "Celui-ci sera supprimé dans " + str(delay_suppression - delay_avertissement) + " jours."
        #     destinataires = User.objects.filter(groups__name__contains="Administrateurs d'instance").filter(
        #         default_instance__departement=user.get_instance().departement)
        #     if not destinataires:
        #         destinataires = User.objects.filter(groups__name__contains="Administrateurs techniques")
        #         message += " " + user.default_instance
        #     try:
        #         send_mail(subject="Adresse email non validée", message=message, from_email=sender_email,
        #                   recipient_list=[destinataire.email for destinataire in destinataires], connection=connection)
        #     except SMTPException:
        #         mail_logger.exception("cronjob EmailAdresseValide : SMTPException")
        #         mail_admins("SMTPException", "Cronjob EmailAdresseValide : Une exception SMTP non gérée vient de se produire")
        #
        # liste = EmailAddress.objects.filter(verified=False,
        #                                     user__date_joined__lte=timezone.now() - timezone.timedelta(days=delay_suppression))
        # for emailaddress in liste:
        #     user = emailaddress.user
        #     log_retour += "%s : Adresse non validée, l'utilisateur %s est supprimé  !" % (user.default_instance, user.username) + chr(10)
        #     user.delete()
        #
        #
        # return log_retour
        return True
