# coding: utf-8

from django.urls import reverse
from django.utils.html import format_html
from ckeditor_uploader.fields import RichTextUploadingFormField
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from allauth.account.models import EmailAddress
from allauth.account.admin import EmailAddressAdmin
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm as BaseFlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _
from django_cron.models import CronJobLog
from django_cron.admin import CronJobLogAdmin

from core.admin.filters import RoleFilter
from core.models.user import User
from core.models.instance import Instance
from core.forms import CustomUserChangeForm, CustomUserCreateForm, UserAdminGroupForm, UserAdminGroupInstanceForm

class FlatpageForm(BaseFlatpageForm):
    """ Formulaire admin des flat pages """

    # Initialiser
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    # Champs
    content = RichTextUploadingFormField()


class FlatPageAdmin(FlatPageAdmin):
    """ Administration des flatpages """

    # Configuration
    form = FlatpageForm


class EmailInline(admin.TabularInline):
    model = EmailAddress

    fieldsets = (
        (None,
         {'fields': ('email', 'verified', 'primary')}),
    )

    def get_fieldsets(self, request, obj=None):
        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
           return  (
            (None,
                {'fields': ('email', 'verified', 'primary', "renvoi_mail")}),
            )
        else:
            return (
                (None,
                 {'fields': ('email', 'verified', 'primary')}),
            )

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
           return  ("renvoi_mail",)
        else:
            return ()

    # renvoi d'un mail de confirmation
    def renvoi_mail(self, obj):
        if obj.pk and not obj.verified:
            pk= obj.user.pk
            url = reverse("send_confirm_mail", args=[pk])
            return format_html('<a class="btn" href="{}">Renvoyer le mail de confirmation</a>', url)
        return '-'
    renvoi_mail.short_description = "Mail confirmation"

    def get_extra(self, request, obj=None, **kwargs):
        return 1




def supprimer_utilisateur_a_un_groupe(modeladmin, request, queryset):
    """ Ajoute une action pour supprimer des utilisateurs d'un groupe"""
    groupname = request.POST['nomGroupe']
    group = Group.objects.get(name=groupname)
    for user in queryset:
        group.user_set.remove(user)
        group.save()
supprimer_utilisateur_a_un_groupe.short_description = "Supprimer les utilisateurs sélectionnés d'un groupe..."

def ajouter_utilisateur_a_un_groupe(modeladmin, request, queryset):
    """ Ajoute une action pour ajouter des utilisateurs à un groupe"""
    groupname = request.POST['nomGroupe']
    group = Group.objects.get(name=groupname)
    for user in queryset:
        group.user_set.add(user)
        group.save()
ajouter_utilisateur_a_un_groupe.short_description = "Ajouter les utilisateurs sélectionnés dans un groupe..."

class UtilisateurAdmin(UserAdmin):
    """ Configuration de l'admin utilisateur """
    form = CustomUserChangeForm
    add_form = CustomUserCreateForm
    action_form = UserAdminGroupForm
    # Configuration
    list_display = ('agent_name', 'email', 'is_active', 'is_staff', 'date_joined', 'default_instance', 'get_role', 'get_service')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', 'default_instance', RoleFilter)
    readonly_fields = ('get_service', 'get_role',"last_change_password")
    actions = [ajouter_utilisateur_a_un_groupe, supprimer_utilisateur_a_un_groupe]
    ordering = ('-date_joined',)
    inlines = [EmailInline,]
    fieldsets = (
        (None, {'fields': ('username', 'password',"last_change_password",  ('default_instance', 'get_role', 'get_service'))}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_per_page = 25

    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.has_group("Administrateurs techniques"):
            self.action_form = UserAdminGroupForm
        elif request.user.has_group("Administrateurs d\'instance"):
            self.action_form = UserAdminGroupInstanceForm
        return actions
    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.is_superuser:
            queryset = queryset.filter(default_instance__departement=request.user.get_departement())
        return queryset

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('Administrateurs techniques'):
            return ('get_service', 'get_role')
        else:
            return ('get_service', 'get_role', "groups")

    def save_model(self, request, obj, form, change):
        """ Enregistrer le modèle """
        try:
            instance = request.user.get_instance()
            if not obj.default_instance and not instance.is_master():
                obj.default_instance = instance
        except (AttributeError, Exception):
            pass
        return super().save_model(request, obj, form, change)

    def delete_model(self, request, obj):
        # Si il y a cascade, c'est à dire que cette suppression va entrainer la suppression d'une ou plusieurs manifs,
        # et si l'utilisateur n'a pas les droits pour les suppressions en cascade, la méthode ne sera pas atteinte
        # Si il a les droits, alors il faudra en plus qu'il soit superuser ou du groupe "Administrateurs techniques
        if hasattr(obj, 'organisateur'):
            if obj.organisateur.structure.manifs.filter(instruction__isnull=False).exists():
                if not request.user.is_superuser and not "Administrateurs techniques" in request.user.groups.values_list('name', flat=True):
                    self.message_user(request, "Des manifestations vont être supprimées en cascade ! Vous n'avez pas les droits", level=messages.WARNING)
                    return
        if obj.actions.all():
            if not request.user.is_superuser and not "Administrateurs techniques" in request.user.groups.values_list('name', flat=True):
                self.message_user(request,
                                  "Cette opération n'est pas possible, car elle entrainerait des suppressions d'actions sur des dossiers de manifestation !",
                                  level=messages.WARNING)
                return

        super().delete_model(request, obj)

    def get_form(self, request, obj=None, **kwargs):
        """ Empêcher les non superuser de modifier les permissions des utilisateurs """
        if not request.user.is_superuser:
            # Vous ne voulez surtout pas donner accès à ces 3 champs
            self.exclude = ['user_permissions', 'is_superuser']
            self.fieldsets = (
                (None, {'fields': ('username', 'password', ('default_instance', 'get_role', 'get_service'))}),
                (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
                (_('Permissions'), {'fields': ('is_active', 'is_staff', 'groups')}),
                (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
            )
        form = super().get_form(request, obj=obj, **kwargs)
        if request.user.get_departement():
            if hasattr(form.base_fields, 'default_instance'):
                form.base_fields['default_instance'].queryset = Instance.objects.filter(departement=request.user.get_departement())
                form.base_fields['default_instance'].initial = request.user.get_instance()
        return form

    def agent_name(self, user_obj):
        return user_obj.get_full_name_and_username()
    agent_name.short_description = "Nom complet et pseudo"


class CustomCronJobLogAdmin(CronJobLogAdmin):
    list_display = ('code', 'start_time', 'end_time', 'humanize_duration', 'is_success', 'extrait')

    def extrait(self, obj):
        return obj.message[0:20]


admin.site.unregister(EmailAddress)

admin.site.unregister(CronJobLog)
admin.site.register(CronJobLog, CustomCronJobLogAdmin)

admin.site.unregister(FlatPage)

admin.site.register(User, UtilisateurAdmin)
admin.site.register(FlatPage, FlatPageAdmin)
