# coding: utf-8
from django.contrib import admin

from ..models import N2KEvaluation


class Natura2000EvaluationInline(admin.StackedInline):
    """ Inline d'évaluation N2K """

    # Configuration
    model = N2KEvaluation
    extra = 0
    readonly_fields = ['content_type', 'object_id', 'manifestation', 'manif']
