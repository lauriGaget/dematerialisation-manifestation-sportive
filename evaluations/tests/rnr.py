# coding: utf-8
from django.test import TestCase

from evaluations.factories import RNREvaluationFactory


class RNREvaluationMethodTests(TestCase):
    def test_str_(self):
        '''
        __str__() should return the name of the evaluation
        '''
        rnr_evaluation = RNREvaluationFactory.build()
        self.assertEqual(rnr_evaluation.__str__(), rnr_evaluation.manifestation.__str__())
