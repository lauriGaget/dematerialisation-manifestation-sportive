# coding: utf-8
from django.db import models
from django.utils.encoding import smart_text

from evaluations.models.evaluation import EvaluationManifestation


class RNREvaluation(EvaluationManifestation):
    """ Évalutation réserve naturelle régionale """

    # Constantes
    DURATION_CHOICES = (
        ('1', "demande d'autorisation pour 1 année"),
        ('3', "demande d'autorisation pour 3 années avec engagement de non modification des parcours et des conditions d'organisation"),
    )
    # Frequency
    request_duration = models.CharField("durée de la demande d'autorisation", max_length=1, choices=DURATION_CHOICES)
    # Localization
    sites = models.ManyToManyField('protected_areas.rnr', verbose_name="sites RNR concernés")
    # Attendance
    rnr_entries = models.PositiveIntegerField("participants concernés par le passage dans la RNR")
    rnr_audience = models.PositiveIntegerField("spectateurs concernés par le passage dans la RNR")
    # Provisions taken
    administrator_contact_date = models.DateField("date de contact avec les gestionnaires de la réserve")
    sensitization = models.CharField("dispositif de sensibilisation à l'environnement et à la réserve envisagé", max_length=255)
    forbidden_areas = models.CharField("dispositif de canalisation du public", max_length=255)
    administrator_attendance = models.BooleanField(
        "l’organisateur souhaite la présence du gestionnaire de la RNR lors de la "
        "manifestation",
        default=False
    )
    # Potential implications
    plane_attendance = models.BooleanField("il y aura présence d'engins aériens [À décrire dans le champ réservé]", default=False)
    plane_attendance_desc = models.TextField("dispositif pour la gestion des engins aériens", blank=True)
    trampling = models.BooleanField("la présence des participants et du public entraînera un piétinement des sols "
                                    "[À décrire dans le champ réservé]", default=False)
    trampling_desc = models.TextField("dispositif pour la gestion du piétinement", blank=True)
    markup = models.BooleanField("vous avez prévu un balisage, une signalisation spécifique à la manifestation "
                                 "[À décrire dans le champ réservé]", default=False)
    markup_desc = models.TextField("dispositif pour la gestion du balisage et de la signalisation", blank=True)
    # Conclusion
    regulatory_compliance = models.BooleanField(
        "le projet, la manifestation respecte bien la réglementation propre à la réserve",
        default=False,
        help_text="voir encadré ci-dessus"
    )
    rnr_conclusion = models.BooleanField("ce formulaire permet de conclure à l'absence d'incidence significative sur "
                                         "le ou les sites RNR concernés par la manifestation", default=False)

    # Overrides
    def __str__(self):
        if self.manifestation:
            return smart_text(self.manifestation.__str__())
        return smart_text(self.manif.__str__())

    # Méta
    class Meta(EvaluationManifestation.Meta):
        verbose_name = "évaluation RNR"
        verbose_name_plural = "évaluations RNR"
        default_related_name = "rnrevaluations"
        app_label = "evaluations"
