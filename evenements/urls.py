# coding: utf-8
from django.urls import path, re_path

from .views.dnm import DnmCreate, DnmDetail, DnmUpdate, DnmFilesUpdate, DnmDelete
from .views.dcnm import DcnmCreate, DcnmDetail, DcnmUpdate, DcnmFilesUpdate, DcnmDelete
from .views.dnmc import DnmcCreate, DnmcDetail, DnmcUpdate, DnmcFilesUpdate, DnmcDelete
from .views.dcnmc import DcnmcCreate, DcnmcDetail, DcnmcUpdate, DcnmcFilesUpdate, DcnmcDelete
from .views.dvtm import DvtmCreate, DvtmDetail, DvtmUpdate, DvtmFilesUpdate, DvtmDelete
from .views.avtm import AvtmCreate, AvtmDetail, AvtmUpdate, AvtmFilesUpdate, AvtmDelete
from .views.avtmcir import AvtmcirCreate, AvtmcirDetail, AvtmcirUpdate, AvtmcirFilesUpdate, AvtmcirDelete
from .views.dashboard import TableauDeBordOrganisateur
from .views.manif import ManifInstructeurUpdate, ManifPublicDetail, ManifCartoUpdate, ManifCreateAncien, ManifUrlGen
from .views.documentcomplementaire import (
    DocumentComplementaireProvideView,
    DocumentComplementaireRequestView,
    EnvoiDocumentComplementaire,
)
from .views.sansinstance import SansInstanceManifCreate
from .views.create import CreationEvenementView
from .views.exportation import ExportPourInstructeur, ExportPourOrganisateur, ExportPourAgentlocal, ExportPourAgent


app_name = 'evenements'
urlpatterns = [

    # Tableau de bord principal
    path('tableau-de-bord-organisateur/', TableauDeBordOrganisateur.as_view(), name="tableau-de-bord-organisateur"),

    path('evenement/creation/', CreationEvenementView.as_view(), name='creation_evenement'),
    path('evenement/creationancien/', ManifCreateAncien.as_view(), name='creation_ancien'),

    # Manifestation sans type (non supporté)
    path('sansinstance/add/', SansInstanceManifCreate.as_view(), name="manif_sansinstance_add"),

    # Modifications instructeur
    re_path('evenement/(?P<pk>\d+)/instruction/', ManifInstructeurUpdate.as_view(), name="manifestation_instructeur"),
    re_path('evenement/(?P<pk>\d+)/cartoedit/', ManifCartoUpdate.as_view(), name="manif_carto"),
    re_path('evenement/(?P<manif_pk>\d+)/doc_complementaire/add/', DocumentComplementaireRequestView.as_view(), name="demander_doc_complementaire"),

    path('evenement/<int:pk>/export/instructeur/', ExportPourInstructeur.as_view(), name="export_manif_instructeur"),
    path('evenement/<int:pk>/export/organisateur/', ExportPourOrganisateur.as_view(), name="export_manif_organisateur"),
    path('evenement/<int:pk>/export/agent/', ExportPourAgent.as_view(), name="export_manif_agent"),
    path('evenement/<int:pk>/export/agentlocal/', ExportPourAgentlocal.as_view(), name="export_manif_agentlocal"),

    path('doc_complementaire/<int:pk>/repondre/', EnvoiDocumentComplementaire.as_view(), name='repondre_doc_complementaire'),
    re_path('doc_complementaire/(?P<pk>\d+)/', DocumentComplementaireProvideView.as_view(), name="fournir_doc_complementaire"),
    path('doc_complementaire/new/<int:pk>', EnvoiDocumentComplementaire.as_view(), name='fournir_doc_complementaire_new'),
    re_path('evenement/(?P<pk>\d+)/', ManifPublicDetail.as_view(), name="manif_detail"),
    re_path('manifestation/(?P<pk>\d+)/', ManifUrlGen.as_view(), name="manif_url"),

    # Dnm urls
    path('Dnm/add/', DnmCreate.as_view(), name="dnm_add"),
    re_path('Dnm/(?P<pk>\d+)/edit/', DnmUpdate.as_view(), name="dnm_update"),
    re_path('Dnm/(?P<pk>\d+)/edit_files/', DnmFilesUpdate.as_view(), name="dnm_files_update"),
    re_path('Dnm/(?P<pk>\d+)/delete/', DnmDelete.as_view(), name="dnm_delete"),
    re_path('Dnm/(?P<pk>\d+)/', DnmDetail.as_view(), name="dnm_detail"),

    # Dnmc urls
    path('Dnmc/add/', DnmcCreate.as_view(), name="dnmc_add"),
    re_path('Dnmc/(?P<pk>\d+)/edit/', DnmcUpdate.as_view(), name="dnmc_update"),
    re_path('Dnmc/(?P<pk>\d+)/edit_files/', DnmcFilesUpdate.as_view(), name="dnmc_files_update"),
    re_path('Dnmc/(?P<pk>\d+)/delete/', DnmcDelete.as_view(), name="dnmc_delete"),
    re_path('Dnmc/(?P<pk>\d+)/', DnmcDetail.as_view(), name="dnmc_detail"),

    # Dcnm urls
    path('Dcnm/add/', DcnmCreate.as_view(), name="dcnm_add"),
    re_path('Dcnm/(?P<pk>\d+)/edit/', DcnmUpdate.as_view(), name="dcnm_update"),
    re_path('Dcnm/(?P<pk>\d+)/edit_files/', DcnmFilesUpdate.as_view(), name="dcnm_files_update"),
    re_path('Dcnm/(?P<pk>\d+)/delete/', DcnmDelete.as_view(), name="dcnm_delete"),
    re_path('Dcnm/(?P<pk>\d+)/', DcnmDetail.as_view(), name="dcnm_detail"),

    # Dcnmc urls
    path('Dcnmc/add/', DcnmcCreate.as_view(), name="dcnmc_add"),
    re_path('Dcnmc/(?P<pk>\d+)/edit/', DcnmcUpdate.as_view(), name="dcnmc_update"),
    re_path('Dcnmc/(?P<pk>\d+)/edit_files/', DcnmcFilesUpdate.as_view(), name="dcnmc_files_update"),
    re_path('Dcnmc/(?P<pk>\d+)/delete/', DcnmcDelete.as_view(), name="dcnmc_delete"),
    re_path('Dcnmc/(?P<pk>\d+)/', DcnmcDetail.as_view(), name="dcnmc_detail"),

    # Dvtm urls
    path('Dvtm/add/', DvtmCreate.as_view(), name="dvtm_add"),
    re_path('Dvtm/(?P<pk>\d+)/edit/', DvtmUpdate.as_view(), name="dvtm_update"),
    re_path('Dvtm/(?P<pk>\d+)/edit_files/', DvtmFilesUpdate.as_view(), name="dvtm_files_update"),
    re_path('Dvtm/(?P<pk>\d+)/delete/', DvtmDelete.as_view(), name="dvtm_delete"),
    re_path('Dvtm/(?P<pk>\d+)/', DvtmDetail.as_view(), name="dvtm_detail"),

    # Avtm urls
    path('Avtm/add/', AvtmCreate.as_view(), name="avtm_add"),
    re_path('Avtm/(?P<pk>\d+)/edit/', AvtmUpdate.as_view(), name="avtm_update"),
    re_path('Avtm/(?P<pk>\d+)/edit_files/', AvtmFilesUpdate.as_view(), name="avtm_files_update"),
    re_path('Avtm/(?P<pk>\d+)/delete/', AvtmDelete.as_view(), name="avtm_delete"),
    re_path('Avtm/(?P<pk>\d+)/', AvtmDetail.as_view(), name="avtm_detail"),

    # Avtmcir urls
    path('Avtmcir/add/', AvtmcirCreate.as_view(), name="avtmcir_add"),
    re_path('Avtmcir/(?P<pk>\d+)/edit/', AvtmcirUpdate.as_view(), name="avtmcir_update"),
    re_path('Avtmcir/(?P<pk>\d+)/edit_files/', AvtmcirFilesUpdate.as_view(), name="avtmcir_files_update"),
    re_path('Avtmcir/(?P<pk>\d+)/delete/', AvtmcirDelete.as_view(), name="avtmcir_delete"),
    re_path('Avtmcir/(?P<pk>\d+)/', AvtmcirDetail.as_view(), name="avtmcir_detail"),

]
