# coding: utf-8
from django.core.files.storage import default_storage
from django.db import models

from configuration.directory import UploadPath
from evenements.models.manifestation import Manif, ManifRelatedModel
from notifications.models.action import Action
from notifications.models.notification import Notification


class DocumentComplementaire(ManifRelatedModel):
    """ Document supplémentaire pour une manifestation """

    # Champs
    manif = models.ForeignKey(Manif, on_delete=models.CASCADE)
    information_requise = models.TextField("information requise")
    document_attache = models.FileField(upload_to=UploadPath('additional_data'), blank=True, null=True, verbose_name="Document", max_length=512)

    # Override
    def __str__(self):
        """ Renvoyer la représentation texte de l'objet """
        return self.information_requise

    # Action
    def log_doc_fourni(self):
        """ Enregistrer dans le journal la provision de données """
        action = "Documents complémentaires envoyés"
        Action.objects.log(self.manif.structure.organisateur, action, self.manif)

    def log_doc_demande(self):
        """ Enregistrer dans le journal la demande de document """
        action = "Documents complémentaires requis"
        manifestation = getattr(self.manif, 'manifestation_ptr', self.manif)
        Action.objects.log(self.manif.instruction.referent, action, manifestation)

    def notifier_doc_fourni(self, prefecture):
        """ Notifier la provision de document """
        subject = "Documents complémentaires envoyés"
        recipients = self.manif.liste_instructeurs()
        for avis in self.manif.instruction.get_tous_avis():
            recipients += avis.get_agents()
            for preavis in avis.get_tous_preavis():
                recipients += preavis.get_agents()
        Notification.objects.notify_and_mail(recipients, subject, self.manif.structure, self.manif)

    def notifier_doc_demande(self):
        """ Notifier la demande de document """
        subject = "Documents complémentaires requis "
        recipients = self.manif.structure.organisateur
        origine = self.manif.instruction.referent.get_service()
        manifestation = getattr(self.manif, 'manifestation_ptr', self.manif)
        Notification.objects.notify_and_mail(recipients, subject, origine, manifestation)

    def demande_doc(self):
        """ Consigner et notifier """
        self.notifier_doc_demande()
        self.log_doc_demande()

    def doc_fourni(self):
        """ Consigner et notifier """
        prefecture = self.get_prefecture()
        self.notifier_doc_fourni(prefecture)
        self.log_doc_fourni()

    # Getter
    def get_prefecture(self):
        """ Renvoyer la préfecture de la manifestation liée au document """
        return self.manif.get_prefecture_concernee()

    def existe(self):
        """ Renvoyer si le fichier de la pièce jointe existe sur le système de fichiers """
        field_populated = self.id and self.document_attache and self.document_attache.path
        file_exists = default_storage.exists(self.document_attache.name)
        return field_populated and file_exists

    # Meta
    class Meta:
        verbose_name = "document complémentaire"
        verbose_name_plural = "documents complémentaires"
        default_related_name = "documentscomplementaires"
        app_label = "evenements"
