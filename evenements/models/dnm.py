from configuration.directory import UploadPath
from django.db import models

from . import Manif
from .instructionconfig import AbstractInstructionConfig


class AbstractDnm(AbstractInstructionConfig):
    """ Manifestation sans véhicule motorisé """
    AIDE_SIGNALEURS = "un modèle de fichier prêt à remplir est disponible <a target='_blank' href='/media/uploads/2018/07/02/tableau_signaleurs.ods'>ici</a>"
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0 + ['nb_vehicules_accompagnement', 'nb_signaleurs']

    # Champs
    depart_groupe = models.BooleanField("départ groupé des participants", default=False)
    circul_groupe = models.BooleanField("circulation groupée des participants", default=False)
    nb_vehicules_accompagnement = models.PositiveIntegerField("nombre de véhicules d'accompagnement", blank=True, null=True, help_text="nombre de véhicules d'accompagnement")
    # Ajout de default dans les CharField et TextField pour la DB existante
    securite_nom = models.CharField("nom de famille", max_length=200, blank=True, default='')
    securite_prenom = models.CharField("prénom", max_length=200, blank=True, default='')
    securite_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    securite_email = models.EmailField("adresse email", max_length=200, blank=True, default='')

    vehicule_ouverture = models.BooleanField('présence d\'un véhicule d\'ouverture (véhicule "pilote")', default=False)
    vehicule_debut = models.BooleanField('présence d\'un véhicule de début de course (véhicule "tête de course")', default=False)
    vehicule_fin = models.BooleanField('présence d\'un véhicule de fin de course', default=False)
    vehicule_organisation = models.BooleanField('présence d\'autres véhicules d\'organisation (auto ou moto)', default=False)
    nb_signaleurs = models.PositiveSmallIntegerField("nombre de signaleurs", blank=True, null=True)
    nb_signaleurs_fixes = models.PositiveSmallIntegerField("en postes fixes", default=0)
    nb_signaleurs_autos = models.PositiveSmallIntegerField("mobiles en voitures", default=0)
    nb_signaleurs_motos = models.PositiveSmallIntegerField("mobiles en motos", default=0)
    police_municipale = models.BooleanField('Disposerez-vous d\'un encadrement de la police municipale ?', default=False)
    detail_police_municipale = models.TextField("Précisez les moyens affectés", blank=True, default='')
    police_nationale = models.BooleanField('Avez-vous passez une convention avec la police nationale ou la gendarmerie ?', default=False)
    detail_police_nationale = models.TextField("Précisez les moyens affectés", blank=True, default='', help_text="joignez, dans la mesure du possible la convention")

    respect_code_route = models.BooleanField('respect code de la route', default=True)
    priorite_passage = models.BooleanField('priorité de passage', default=False)
    usage_temporaire = models.BooleanField('usage exclusif temporaire de la chaussée', default=False)
    usage_privatif = models.BooleanField('usage privatif de la chaussée', default=False)
    respect_code_route_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")
    priorite_passage_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")
    usage_temporaire_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")
    usage_privatif_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")

    itineraire_horaire = models.FileField(upload_to=UploadPath('itineraires_horaires'), blank=True, null=True,
                                          verbose_name="itinéraire horaire", max_length=512)

    # Meta
    class Meta:
        abstract = True


class Dnm(AbstractDnm, Manif):
    """ Manifestation sans véhicule motorisé hors cyclisme hors compétition """

    # Constantes
    refCerfa = "cerfa 15825-02"
    consultFederation = False
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    delaiDepot = 30
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['itineraire_horaire']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='dnm', on_delete=models.CASCADE)

    # Meta
    class Meta:
        verbose_name = "manifestation sans véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations sans véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "dnm"
        app_label = "evenements"


class Dnmc(AbstractDnm, Manif):
    """ Manifestation cycliste sans véhicule motorisé hors compétition """

    # Constantes
    refCerfa = "cerfa 15826-01"
    consultFederation = False
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    delaiDepot = 30
    delaiDepot_ndept = 30
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['dossier_tech_cycl', 'itineraire_horaire']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='dnmc', on_delete=models.CASCADE)
    dossier_tech_cycl = models.FileField(upload_to=UploadPath('dossier_technique_cycle'), blank=True, null=True,
                                        help_text="Le formulaire vierge est disponible <a href='https://www.ffc.fr/wp-content/uploads/2018/01/Dossier-technique-manifestation-cycliste-2018.pdf' target='_blank'>ici</a>",
                                        verbose_name="dossier technique cyclisme", max_length=512)

    # Meta
    class Meta:
        verbose_name = "manifestation de cyclisme sans véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations de cyclisme sans véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "dnmc"
        app_label = "evenements"


class Dcnm(AbstractDnm, Manif):
    """ Manifestation compétitive sans véhicule motorisé hors cyclisme """

    # Constantes
    refCerfa = "cerfa 15824-03"
    consultFederation = True
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    # delaiDepot = defaut = 60
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['itineraire_horaire']
    LISTE_FICHIERS_ETAPE_1 = ['liste_signaleurs']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='dcnm', on_delete=models.CASCADE)
    federation_multisport = models.ForeignKey('sports.federation', verbose_name="fédération multi-sports",
                                              limit_choices_to={'multisports': True}, blank=True, null=True, on_delete=models.SET_NULL,
                                              help_text="Si votre manifestation est organisée sous la tutelle d'une fédération multisports, sélectionnez là ici.")
    affiliation = models.BooleanField("Affiliation à une fédération délégataire", default=False, help_text="Êtes-vous affilié à la fédération délégataire de la discipline pratiquée lors de la manifestation ?")
    liste_signaleurs = models.FileField(upload_to=UploadPath('listes_signaleurs'), blank=True, null=True, verbose_name="liste des signaleurs", max_length=512, help_text=AbstractDnm.AIDE_SIGNALEURS)

    def get_delai_legal(self):
        """ Renvoyer le délai légal pour l'obtention d'une autorisation """
        if self.departements_traverses.count() == 0:
            return self.delaiDepot
        else:
            return self.delaiDepot_ndept

    # Meta
    class Meta:
        verbose_name = "manifestation compétitive sans véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations compétitives sans véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "dcnm"
        app_label = "evenements"


class Dcnmc(AbstractDnm, Manif):
    """ Manifestation cycliste compétitive sans véhicule motorisé """

    # Constantes
    refCerfa = "cerfa 15827-01"
    consultFederation = True
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    # delaiDepot = defaut = 60
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['dossier_tech_cycl', 'itineraire_horaire']
    LISTE_FICHIERS_ETAPE_1 = ['liste_signaleurs']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='dcnmc', on_delete=models.CASCADE)
    liste_signaleurs = models.FileField(upload_to=UploadPath('listes_signaleurs'), blank=True, null=True, verbose_name="liste des signaleurs", max_length=512, help_text=AbstractDnm.AIDE_SIGNALEURS)
    dossier_tech_cycl = models.FileField(upload_to=UploadPath('dossier_technique_cycle'), blank=True, null=True,
                                        help_text="Le formulaire vierge est disponible <a href='https://www.ffc.fr/app/uploads/sites/3/2019/12/Dossier-technique-manifestation-cycliste-v-2019-03-09.pdf' target='_blank'>ici</a>",
                                        verbose_name="dossier technique cyclisme", max_length=512)

    def get_delai_legal(self):
        """ Renvoyer le délai légal pour l'obtention d'une autorisation """
        if self.departements_traverses.count() == 0:
            return self.delaiDepot
        else:
            return self.delaiDepot_ndept

    # Meta
    class Meta:
        verbose_name = "manifestation compétitive de cyclisme sans véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations compétitives de cyclisme sans véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "dcnmc"
        app_label = "evenements"
