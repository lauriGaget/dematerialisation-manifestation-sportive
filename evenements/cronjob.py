from django_cron import CronJobBase, Schedule
from django_cron.models import CronJobLog
from smtplib import SMTPRecipientsRefused, SMTPException
from django.core.mail import send_mail, get_connection
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from django.conf import settings
from django.utils import timezone
import datetime, logging, html

from instructions.models.instruction import Instruction
from notifications.models.notification import Notification
from core.util.types import one_line


mail_logger = logging.getLogger('smtp')


class CompletudeDossier(CronJobBase):
    """
    Job cron destiné à rappeler aux organisateurs les pièces à joindre en cours d'instruction.
    C'sst à dire aux deux dates clés : J-21 et J-6
    """
    RUN_EVERY_MINS = 1  # à lancer toutes les minutes
    # Il est possible de lancer ce cron toute les minutes mais c'est le système qui gère le cadencement dans la crontab
    # Afin de ne pas être limité dans la crontab, la variable est au minimum

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'evenements.conpletudedossier'    # a unique code

    # Purger les logs sans message
    CronJobLog.objects.filter(code="evenements.conpletudedossier").filter(is_success=True).filter(
        message__isnull=True).filter(end_time__lt=timezone.now() - timezone.timedelta(days=30)).delete()

    def do(self):
        log_retour = ""

        # Dossier en instruction dont la date n'est pas dépassée
        liste = Instruction.objects.filter(manif__date_fin__gte=timezone.now()).exclude(etat="annulée")
        for dossier in liste:
            manifestation = dossier.manif
            # A ce stade, seul certaines pièces jointes peuvent manquer
            if not manifestation.get_cerfa().dossier_complet():
                # Détermination de la date limite de l'étape en cours
                if manifestation.etape_en_cours() == 'etape 1':
                    date_etape = manifestation.get_date_etape_1()
                elif dossier.manif.etape_en_cours() == 'etape 2':
                    date_etape = manifestation.get_date_etape_2()
                else:
                    date_etape = 0
                if date_etape:
                    if datetime.date.today() >= date_etape.date() - datetime.timedelta(days=5):
                        delai = manifestation.get_cerfa().delai_en_cours()
                        nb_jours = (date_etape.date() - datetime.date.today()).days
                        log_retour += '%s ; delai : %sj, échéance : %s ; jours restants : %s' % (manifestation.nom, delai, date_etape.date(), nb_jours)
                        envoyer_alerte_dossier(nb_jours, dossier.manif)
        return log_retour


def envoyer_alerte_dossier(jour, manifestation):
    """
    Ajouter une notification pour l'organisateur et envoyer un mail

    :param jour: nombre de jours restants avant de changer d'étape
    :param manifestation: manifestation ciblée par la notification
    """
    if jour == 0:
        subject = 'Alerte, fichiers manquants'
    else:
        subject = 'Rappel, fichiers manquants'
    data = {'subject': subject, 'jour': jour, 'manifestation': manifestation, 'url': settings.MAIN_URL}
    data['liste'] = manifestation.get_cerfa().liste_manquants()
    subject = one_line(render_to_string('notifications/mail/subject.txt', data))
    message = render_to_string('notifications/mail/message_organisateur_fichiers_manquants.txt', data)

    recipient = manifestation.structure.organisateur
    recipient = getattr(recipient, 'user', recipient)
    # Envoyer un email si le champ email est disponible sur le destinataire
    if hasattr(recipient, 'email'):
        try:
            sender_email = manifestation.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
            email_config = manifestation.get_instance().get_email_settings()
            connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
            send_mail(subject=html.unescape(subject), message=html.unescape(message), from_email=sender_email,
                      recipient_list=[recipient.email], connection=connection)

            Notification.objects.create(user=recipient, manif=manifestation, subject=subject,
                                        content_object=Site.objects.get_current())

        except SMTPRecipientsRefused:
            # En cas d'échec où l'adresse n'existe pas (ou similaire), notifier les instructeurs du dossier
            # À noter que l'échec SMTP ne peut se produire qu'en situation de production
            # donc à surveiller.
            mail_logger.exception("notifications.notification.notify_and_mail: SMTP Recipients refused, trying to send to instructors")
            if not recipient.has_role('instructeur'):  # évite d'éventuels problèmes de récursivité infinie
                Notification.objects.notify_and_mail(manifestation.liste_instructeurs(), "problème d'email",
                                                     recipient, manifestation,
                                                     template='notifications/mail/message-mailerror.txt')
        except SMTPException:
            # Si le serveur SMTP pose un autre type de problème, logger l'erreur
            exception_text = """
            notifications.notification.notify_and_mail: Une exception SMTP non gérée vient de se produire
            instance: {instance}
            destinataires: {recipients}
            manifestation: {manifestation}
            cible: {target}
            """
            mail_logger.exception(exception_text.format(instance=manifestation.liste_instructeurs(),
                                                        recipients=recipient, manifestation=manifestation,
                                                        target=manifestation.structure))
