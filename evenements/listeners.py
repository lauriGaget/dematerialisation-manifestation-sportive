# coding: utf-8

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings

from evenements.models.documentcomplementaire import DocumentComplementaire
from evenements.models.dnm import Dnm, Dnmc, Dcnm, Dcnmc
from evenements.models.vtm import Avtm, Avtmcir, Dvtm


if not settings.DISABLE_SIGNALS:
    @receiver(post_save, sender=DocumentComplementaire)
    def log_creation_demande_document(created, instance, **kwargs):
        if created:
            instance.demande_doc()


    @receiver(post_save, sender=Dnm)
    @receiver(post_save, sender=Dnmc)
    @receiver(post_save, sender=Dcnm)
    @receiver(post_save, sender=Dcnmc)
    @receiver(post_save, sender=Dvtm)
    @receiver(post_save, sender=Avtm)
    @receiver(post_save, sender=Avtmcir)
    def log_manif_creation(sender, created=None, instance=None, raw=None, **kwargs):
        # Ne pas activer les logs avec loaddata
        if not raw:
            if created:
                instance.actions.create(user=instance.structure.organisateur.user, action="description de la manifestation")
