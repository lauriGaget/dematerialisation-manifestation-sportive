/**
 * Created by knt on 22/01/17.
 */
$(document).ready(function () {

    $("textarea").autogrow({flickering: false, horizontal: false});

    // BOF Modification pour "joindre des documents"
    // Pour les champs FileField disabled, n'afficher que le lien vers le fichier chargé
    $('.clearablefileinput').each(function () {
        if ($(this).prop("disabled")) {
            var a = $(this).parent().contents().filter("a");
            $(this).parent().html('Actuellement: ').append(a);
        }
    });
    // EOF

    // BOF Modification pour l'édition de manifestation
    // Marquer les champs de classe "requis"
    $('.requis').each(function () {
        $(this).parents('.form-group').append('<div class="col-sm-3 text-center completer"></div>');
        if ($(this).val() == '') {
            $(this).parents('.form-group').children('.completer').children().remove();
            $(this).parents('.form-group').children('.completer').append('<div><i class="a-completer"></i> à completer</div>');

        }
    });
AfficherIcones();
    // Gérer le marquage en fonction du remplissage
    $('.requis').on('change', function () {
        if ($(this).val() != '') {
            $(this).parents('.form-group').children('.completer').empty();
        } else {
            $(this).parents('.form-group').children('.completer').children().remove();
            $(this).parents('.form-group').children('.completer').append('<div><i class="a-completer"></i> à completer</div>')
        }
    AfficherIcones();
    });
    // Gérer le marquage en fonction du remplissage pour les champs 'date'
    $('.requis').on('dp.change', function () {
        if ($(this).val() != '') {
            $(this).parents('.form-group').children('.completer').empty();
        } else {
            $(this).parents('.form-group').children('.completer').children().remove();
            $(this).parents('.form-group').children('.completer').append('<div><i class="a-completer"></i> à completer</div>')
        }
    AfficherIcones();
    });
    // EOF

    // BOF Ajout du champ "Dossier technique cyclisme"
    $('#id_activite').parent().parent().after('<div id="id_cycling" class="form-group">' +
        '<label class="control-label col-sm-3">Dossier technique cyclisme</label>' +
        '<div class="controls col-sm-6" style="padding-top: 7px">' +
        'Veuillez d\'ores et déjà ' +
        '<a href="https://www.ffc.fr/wp-content/uploads/2018/01/Dossier-technique-manifestation-cycliste-2018.pdf" ' +
        'target="_blank"> télécharger le document vierge</a>' +
        '. Vous devrez le remplir et le joindre à votre demande.</div></div>');
    // On cache le champ par défaut
    $('#id_cycling').hide();

    // Cas d'édition
    if ($('#id_discipline option:selected').text() == 'Cyclisme') {
        $('#id_cycling').show();
    }
    // Cas de création
    $('#id_discipline').change(function () {
        if ($('#id_discipline option:selected').text() == 'Cyclisme'){
            $('#id_cycling').show(400);
        } else {
            $('#id_cycling').hide(200);
        }
    });
    // EOF

    // BOF Régime de circulation
    function check_traffic() {
        // Renvoie la liste des switches commutés
        var list = [false,false,false,false];
        if ($('[name="respect_code_route"]').is(':checked')) { list[0] = true; }
        if ($('[name="priorite_passage"]').is(':checked')) { list[1] = true; }
        if ($('[name="usage_temporaire"]').is(':checked')) { list[2] = true; }
        if ($('[name="usage_privatif"]').is(':checked')) { list[3] = true; }
        return list;
    }
    function set_detail(list) {
        // Affichage les champs "détails" pour chaque switch
        if (list[0]) {
            $('#div_id_respect_code_route_detail').show(400);
            $('#div_id_respect_code_route_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_respect_code_route_detail').hide(400);
            $('#id_respect_code_route_detail').text('');
        }
        if (list[1]) {
            $('#div_id_priorite_passage_detail').show(400);
            $('#div_id_priorite_passage_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_priorite_passage_detail').hide(400);
            $('#id_priorite_passage_detail').text('');
        }
        if (list[2]) {
            $('#div_id_usage_temporaire_detail').show(400);
            $('#div_id_usage_temporaire_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_usage_temporaire_detail').hide(400);
            $('#id_usage_temporaire_detail').text('');
        }
        if (list[3]) {
            $('#div_id_usage_privatif_detail').show(400);
            $('#div_id_usage_privatif_detail').autogrow({flickering: false, horizontal: false});
        } else {
            $('#div_id_usage_privatif_detail').hide(400);
            $('#id_usage_privatif_detail').text('');
        }
    }
    function which_traffic(id) {
        // Renvoie le switch qui a communté parmi les quatre
        var list = [false,false,false,false];
        if ( id === 'id_respect_code_route') { list[0] = true; }
        if ( id === 'id_priorite_passage') { list[1] = true; }
        if ( id === 'id_usage_temporaire') { list[2] = true; }
        if ( id === 'id_usage_privatif') { list[3] = true; }
        return list
    }
    function set_traffic(list) {
        // Communte les switches
        if (list[0]) { $('#id_respect_code_route').click(); }
        if (list[1]) { $('#id_priorite_passage').click(); }
        if (list[2]) { $('#id_usage_temporaire').click(); }
        if (list[3]) { $('#id_usage_privatif').click(); }
    }
    // Gérer l'affichage initial
    if ($('[name="respect_code_route_detail"]').text() != '') {
        $('#div_id_respect_code_route_detail').show();
        $('#div_id_respect_code_route_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_respect_code_route_detail').hide();
    }
    if ($('[name="priorite_passage_detail"]').text() != '') {
        $('#div_id_priorite_passage_detail').show();
        $('#div_id_priorite_passage_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_priorite_passage_detail').hide();
    }
    if ($('[name="usage_temporaire_detail"]').text() != '') {
        $('#div_id_usage_temporaire_detail').show();
        $('#div_id_usage_temporaire_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_usage_temporaire_detail').hide();
    }
    if ($('[name="usage_privatif_detail"]').text() != '') {
        $('#div_id_usage_privatif_detail').show();
        $('#div_id_usage_privatif_detail').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_usage_privatif_detail').hide();
    }
    // Gérer l'affichage quand les switches sont commutés
    $('.traffic').click(function() {
        var previous = which_traffic($(this).attr('id'));
        var etat = check_traffic();
        if ( !(etat[0] | etat[1] | etat[2] | etat[3]) ) {
            // pas de switch, on communte le suivant
            set_traffic([previous[3], previous[0], previous[1], previous[2]]);
            // alert('Vous devez sélectionner au moins un régime');
        } else {
            var prev = etat[0];
            var xor = true;
            for (var i = 1; i < etat.length; i++) {
                if (prev == 1 & etat[i] == 1) {
                    // plus de un switch, on affiche le détail pour chaque
                    xor = false;
                    set_detail(etat);
                    break;
                }
                prev = prev | etat[i];
            }
            // un seul switch, pas de détails
            if (xor) { set_detail([0, 0, 0, 0]); }
        }
    });
    // EOF

    // BOF Dispositif de sécurité
    // Gérer l'affichage du champ "moyens affectés" si le switch est true initialement
    if ($('[name="police_municipale"]').is(':checked')) {
        $('#div_id_detail_police_municipale').show();
        $('#div_id_detail_police_municipale').autogrow({flickering: false, horizontal: false});
    } else {
        $('#div_id_detail_police_municipale').hide();
    }
    if ($('[name="police_nationale"]').is(':checked')) {
        $('#div_id_detail_police_nationale').show();
        $('#div_id_detail_police_nationale').autogrow({flickering: false, horizontal: false});
        $('#id_detail_police_nationale').attr("placeholder", "Vous pouvez inclure votre convention dans les pièces jointes");
    } else {
        $('#div_id_detail_police_nationale').hide();
    }
    // Gérer l'affichage du champ "moyens affectés" si le switch est commuté
    $('#id_police_municipale').click(function() {
        $('#div_id_detail_police_municipale').toggle(400);
        $('#div_id_detail_police_municipale').autogrow({flickering: false, horizontal: false});
    });
    $('#id_police_nationale').click(function() {
        $('#div_id_detail_police_nationale').toggle(400);
        $('#div_id_detail_police_nationale').autogrow({flickering: false, horizontal: false});
        $('#id_detail_police_nationale').attr("placeholder", "Vous pouvez inclure votre convention dans les pièces jointes");
    });
    // Gérer l'affichage des signaleurs si le nombre est rempli initialement
    if ($('[name="nb_signaleurs"]').val() != '' && $('[name="nb_signaleurs"]').val() != 0) {
        $('#div_id_nb_signaleurs_fixes').show();
        $('#div_id_nb_signaleurs_autos').show();
        $('#div_id_nb_signaleurs_motos').show();
    } else {
        $('#div_id_nb_signaleurs_fixes').hide();
        $('#div_id_nb_signaleurs_autos').hide();
        $('#div_id_nb_signaleurs_motos').hide();
    }
    // Gérer l'affichage des signaleurs si le nombre est rempli
    $('[name="nb_signaleurs"]').on("change", function () {
        if ($('[name="nb_signaleurs"]').val() != '' && $('[name="nb_signaleurs"]').val() != 0){
            $('#div_id_nb_signaleurs_fixes').show(400);
            $('#div_id_nb_signaleurs_autos').show(400);
            $('#div_id_nb_signaleurs_motos').show(400);
        } else {
            $('#div_id_nb_signaleurs_fixes').hide(400);
            $('#div_id_nb_signaleurs_autos').hide(400);
            $('#div_id_nb_signaleurs_motos').hide(400);
        }
    });
    // EOF


    $('#id_departements_traverses').trigger("change");  // Provoquer la mise à jour du champ Communes traversées
    $('#id_departements_traverses').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    $("#manifestation-form").liveValidate('/validate/manif/', {event: 'blur'});

});
