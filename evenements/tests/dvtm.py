# coding: utf-8

from instructions.factories import InstructionFactory
from .base import EvenementsTestsBase
from ..factories.manif import DvtmFactory
from evaluation_incidence.factories import N2kSiteConfigFactory


class DvtmTests(EvenementsTestsBase):
    """ Tests Dvtm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation compétitive
        sportive motorisée soumise à déclaration.
        """
        self.instruction = InstructionFactory.create(manif=DvtmFactory.create())
        super().setUp()
        self.n2ksiteconfig = N2kSiteConfigFactory.create()
        self.n2ksite = self.n2ksiteconfig.n2ksite
        self.n2ksite.departements.add(self.departement)
        self.n2ksiteconfig.vtm_seuil_participants = 700
        self.n2ksiteconfig.vtm_seuil_vehicules = 40
        self.n2ksiteconfig.vtm_seuil_total = 6000
        self.n2ksiteconfig.save()

    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = DvtmFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/Dvtm/4/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critères nationaux
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        # Critères de site sans formulaire ciblé
        self.manifestation.sites_natura2000.add(self.n2ksite)
        self.n2ksiteconfig.vtm_seuil_participants = 200
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.vtm_seuil_participants = 0
        self.n2ksiteconfig.vtm_seuil_vehicules = 25
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.vtm_seuil_vehicules = 0
        self.n2ksiteconfig.vtm_seuil_total = 5000
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères de site avec formulaire ciblé, condition sur seuil total
        self.n2ksiteconfig.vtm_formulaire = ['avtm']                         # mauvais formulaire
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.vtm_formulaire = ['dvtm', 'avtm']                 # bon formulaire
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères de site avec charte de dispense, condition sur seuil total
        self.manifestation.signature_charte_dispense_site_n2k = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.charte_dispense_acceptee = True
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.vtm_formulaire = []                              # sans formulaire
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.signature_charte_dispense_site_n2k = False
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay_for_declared(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)
