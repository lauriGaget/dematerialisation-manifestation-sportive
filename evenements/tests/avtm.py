# coding: utf-8

from instructions.factories import InstructionFactory
from .base import EvenementsTestsBase
from ..factories.manif import AvtmFactory
from evaluation_incidence.factories import N2kDepConfigFactory, N2kSiteFactory


class AvtmTests(EvenementsTestsBase):
    """ Tests Avtm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation sportive motorisée.
        """
        self.instruction = InstructionFactory.create(manif=AvtmFactory.create())
        super().setUp()
        self.n2ksite = N2kSiteFactory.create()
        self.n2ksite.departements.add(self.departement)
        self.n2kdepconfig = N2kDepConfigFactory.create(instance=self.instance)
        self.n2kdepconfig.vtm_seuil_participants = 700
        self.n2kdepconfig.vtm_seuil_vehicules = 40
        self.n2kdepconfig.vtm_seuil_total = 6000
        self.n2kdepconfig.save()

    # Tests
    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = AvtmFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/Avtm/4/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critères nationaux
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        # Critères departementaux sans formulaire ciblé
        self.n2kdepconfig.vtm_seuil_participants = 200
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.vtm_seuil_participants = 0
        self.n2kdepconfig.vtm_seuil_vehicules = 25
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.vtm_seuil_vehicules = 0
        self.n2kdepconfig.vtm_seuil_total = 5000
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères departementaux avec formulaire ciblé, condition sur seuil total
        self.n2kdepconfig.vtm_formulaire = ['dvtm']                         # mauvais formulaire
        self.n2kdepconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.vtm_formulaire = ['dvtm', 'avtm']                 # bon formulaire
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.vtm_sur_siten2k = True                            # sur_site_n2k sans site n2k
        self.n2kdepconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.sites_natura2000.add(self.n2ksite)               # sur_site_n2k avec site n2k
        self.manifestation.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.vtm_formulaire = []                               # sans formulaire mais sur_site avec site
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.sites_natura2000.clear()                         # sans formulaire et sur_site sans site
        self.manifestation.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 90)
