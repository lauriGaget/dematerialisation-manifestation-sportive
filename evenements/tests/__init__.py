from .dnm import DnmTests
from .dcnm import DcnmTests
from .dvtm import DvtmTests
from .avtm import AvtmTests
from .avtmcir import AvtmcirTests
from .test_aiguillage_organisateur import Aiguillage_OrganisateurTests
from .test_selenium_Dnm import TestAiguillageDnm
from .test_selenium_Dnmc import TestAiguillageDnmc
from .test_selenium_Dcnm import TestAiguillageDcnm
from .test_selenium_Dcnmc import TestAiguillageDcnmc
from .test_selenium_Dvtm_concentration import TestAiguillageDvtm1
from .test_selenium_Avtmcir_circuit_homologue import TestAiguillageAvtmcir
from .test_selenium_Avtm_compet_vp import TestAiguillageAvtm1
from .test_selenium_Avtm_circuit_non_perm import TestAiguillageAvtm2
from .test_selenium_Avtm_circuit_non_homologue import TestAiguillageAvtm3
