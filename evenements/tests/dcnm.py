# coding: utf-8
import datetime
from django.utils.timezone import utc

from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory
from .base import EvenementsTestsBase
from notifications.models import Action
from ..factories.manif import DcnmFactory
from evaluation_incidence.factories import N2kDepConfigFactory, N2kSiteFactory


class DcnmTests(EvenementsTestsBase):
    """ Tests Dcnm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation compétitive
        sportive non motorisée soumise à déclaration.
        """
        self.instruction = InstructionFactory.create(manif=DcnmFactory.create())
        super().setUp()
        self.n2ksite = N2kSiteFactory.create()
        self.n2ksite.departements.add(self.departement)
        self.n2kdepconfig = N2kDepConfigFactory.create(instance=self.instance)
        self.n2kdepconfig.nm_seuil_participants = 700
        self.n2kdepconfig.nm_seuil_vehicules = 40
        self.n2kdepconfig.nm_seuil_total = 6000
        self.n2kdepconfig.save()

    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = DcnmFactory.build(pk=3)
        self.assertEqual(manifestation.get_absolute_url(), '/Dcnm/3/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critères nationaux
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        # Critères departementaux sans formulaire ciblé
        self.n2kdepconfig.nm_seuil_participants = 200
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.nm_seuil_participants = 0
        self.n2kdepconfig.nm_hors_circulation = True
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.nm_hors_circulation = False
        self.n2kdepconfig.nm_seuil_total = 5000
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères departementaux avec formulaire ciblé, condition sur seuil total
        self.n2kdepconfig.nm_formulaire = ['dnm']                         # mauvais formulaire
        self.n2kdepconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.nm_formulaire = ['dnm', 'dcnm']                 # bon formulaire
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.nm_sur_siten2k = True                            # sur_site_n2k sans site n2k
        self.n2kdepconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.sites_natura2000.add(self.n2ksite)               # sur_site_n2k avec site n2k
        self.manifestation.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2kdepconfig.nm_formulaire = []                               # sans formulaire mais sur_site avec site
        self.n2kdepconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.sites_natura2000.clear()                         # sans formulaire et sur_site sans site
        self.manifestation.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())

    def test_manifestation_log_creation(self):
        action_count = Action.objects.count()
        action = Action.objects.last()
        self.assertEqual(action.user, self.manifestation.structure.organisateur.user)
        self.assertEqual(action.manif, self.manifestation.manifestation_ptr)
        self.assertEqual(action.action, "description de la manifestation")
        # Vérifier qu'aucune nouvelle action n'a été créée
        self.manifestation.save()
        self.assertEqual(action_count, Action.objects.count())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)

    def test_legal_delay_2(self):
        DepartementFactory.create()
        self.manifestation.departements_traverses.add(DepartementFactory.create())
        self.assertEqual(self.manifestation.get_delai_legal(), 90)

    def test_get_limit_date(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc)
        self.assertEqual(
            self.manifestation.get_date_limite(),
            (self.manifestation.date_debut - datetime.timedelta(days=60)).replace(hour=23, minute=59)
        )

    def test_delay_exceeded(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=59)
        self.assertTrue(self.manifestation.delai_depasse())

    def test_delay_not_exceeded(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=61)
        self.assertFalse(self.manifestation.delai_depasse())

    def test_not_two_weeks_left(self):
        self.manifestation.date_debut = datetime.datetime.today() + datetime.timedelta(days=60) +\
                                        datetime.timedelta(weeks=2)
        self.assertFalse(self.manifestation.deux_semaines_restantes())

    def test_two_weeks_left(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc) +\
                                        datetime.timedelta(days=60) + datetime.timedelta(weeks=1)
        self.assertTrue(self.manifestation.deux_semaines_restantes())
