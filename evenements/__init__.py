from django.apps import AppConfig


class EvenementsConfig(AppConfig):
    name = 'evenements'
    verbose_name = 'Manifestations sportives'

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from evenements import listeners


default_app_config = 'evenements.EvenementsConfig'
