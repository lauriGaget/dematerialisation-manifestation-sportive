# coding: utf-8
from crispy_forms.layout import Layout, Fieldset, HTML

from core.forms.base import GenericForm
from ..models.vtm import Avtmcir
from .manif import ManifForm, FIELDS_INITIALS, FIELDS_MANIFESTATION, FIELDS_FILES, FIELDS_MARKUP, FIELDS_NATURA2000, FORM_WIDGETS, FIELDS_CONTACT


class AvtmcirForm(ManifForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(FIELDS_INITIALS)),
                                    Fieldset("Organisateur Technique", 'technique_nom', 'technique_prenom', 'technique_tel', 'technique_email'),
                                    Fieldset(*(FIELDS_MANIFESTATION + ['vehicules', 'type_support'])),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(FIELDS_NATURA2000[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Avtmcir
        exclude = ('descriptions_parcours', 'parcours_openrunner', 'structure', 'demande_homologation', 'instance')
        widgets = FORM_WIDGETS


class AvtmcirFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES + ['plan_masse', 'certificat_organisateur_tech', 'participants'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = Avtmcir
        fields = FIELDS_FILES[1:] + ['plan_masse', 'certificat_organisateur_tech', 'participants']
