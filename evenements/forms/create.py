from django import forms
from django.forms import ModelChoiceField
from clever_selects.forms import ChainedChoicesForm
from clever_selects.form_fields import ChainedModelChoiceField
from django.urls import reverse_lazy
from ajax_select.fields import AutoCompleteField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML, ButtonHolder
from crispy_forms.bootstrap import FormActions, StrictButton

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from sports.models import Discipline
from sports.models.sport import Activite


# Textes d'aide
HELP_EXTRA_ACTIVITE = "recherchez votre activité sportive dans ce champ. Cliquez ensuite sur l'activité pour remplir automatiquement les champs ci-dessous."
BLOC_INFORMATION = "<div class='col-10 mx-auto mt-3'>" \
                   "<div class='card'>" \
                   "<div class='card-header bg-secondary text-white text-center'>Information</div>" \
                   "<div class='card-body'>" \
                   "<p id='info-cerfa' class='card-text text-justify'>Veuillez remplir le formulaire ci-dessus afin de rechercher le formulaire adapté à votre manifestation...</p>"


class CreationEvenementForm(ChainedChoicesForm):

    extra_activite = AutoCompleteField('activite', label="Recherche d'activité sportive", required=False, help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)
    discipline = ModelChoiceField(required=False, queryset=Discipline.objects.all())
    activite = ChainedModelChoiceField('discipline', reverse_lazy('sports:activite_widget'), Activite, required=False)
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    ville_depart = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Commune de départ", required=False)
    competition = forms.BooleanField(label="Classement, chronométrage, horaire fixé à l'avance", required=False, initial=True)
    voiepublique = forms.BooleanField(label='Se déroule en totalité ou en partie sur la voie publique ou ouverte à la circulation', required=False, initial=True)
    circuitnonperm = forms.BooleanField(label='Circuit non permanent, terrains ou parcours', required=False)
    homologue = forms.BooleanField(label='Circuit permanent homologué dans la discipline de la manifestation', help_text="Sélectionnez 'Non' si circuit homologué dans une discipline différente ou non homologué", required=False)
    nb_participants = forms.IntegerField(label='Nombre de participants', required=False, help_text="En cas d'incertitude, prévoyez \"large\" car vous ne pourrez plus modifier cette information et elle influe directement sur le formulaire nécessaire.")
    formulaire = forms.CharField(max_length=20, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        """ Initialiser le formulaire """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.helper.layout = Layout(
            Fieldset("", 'extra_activite', 'discipline', 'activite', 'departement', 'ville_depart', 'competition',
                     'voiepublique', 'circuitnonperm', 'homologue', 'nb_participants', 'formulaire'),
            StrictButton('Rechercher le formulaire', css_class="btn-primary btn-block", id="searchform"),
            HTML(BLOC_INFORMATION),
            ButtonHolder(Submit('submit','Accéder au formulaire', css_class='btn-success btn-block')),
            HTML('</div></div></div>'),

        )

    def clean_nb_participants(self):
        data = self.cleaned_data['nb_participants']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        if data == 0:
            raise forms.ValidationError("Ce champ ne doit pas être égal à 0 !")
        return data

    def clean_ville_depart(self):
        data = self.cleaned_data['ville_depart']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean_departement(self):
        data = self.cleaned_data['departement']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean_activite(self):
        data = self.cleaned_data['activite']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data
