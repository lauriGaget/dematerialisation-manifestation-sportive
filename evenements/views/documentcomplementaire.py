# coding: utf-8
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404,redirect, render, reverse
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, View
from django.urls import reverse_lazy
from django.forms import modelformset_factory
from django.http import HttpResponse

from core.util.permissions import require_role
from organisateurs.decorators import verifier_proprietaire
from instructions.decorators import verifier_secteur_instruction
from evenements.forms.documentcomplementaire import DocumentComplementaireRequestForm, DocumentComplementaireProvideForm
from evenements.models import *


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class DocumentComplementaireRequestView(SuccessMessageMixin, CreateView):
    """ Demande de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireRequestForm
    success_message = "Demande de documents complémentaires envoyée avec succès"

    def form_valid(self, form):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        form.instance.manif = manif
        manif.instruction.referent = self.request.user
        manif.instruction.save()
        return super(DocumentComplementaireRequestView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(DocumentComplementaireRequestView, self).get_context_data(**kwargs)
        context['manif'] = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        return context

    def get_success_url(self):
        return reverse('instructions:instruction_detail', kwargs={'pk': self.object.manif.instruction.pk})


# TODO : classe inutilisée, à supprimer
@method_decorator(verifier_proprietaire, name='dispatch')
class DocumentComplementaireProvideView(SuccessMessageMixin, UpdateView):
    """ Renseignement de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireProvideForm
    success_message = "Documents complémentaires envoyés avec succès"
    success_url = reverse_lazy('evenements:tableau-de-bord-organisateur')

    # Overrides
    def form_valid(self, form):
        self.object = form.save()
        self.object.doc_fourni()
        return super(DocumentComplementaireProvideView, self).form_valid(form)


@method_decorator(verifier_proprietaire, name='dispatch')
class EnvoiDocumentComplementaire(View):
    """Affichage et envoi des documents complémentaires par l'organisateur"""

    # Affichage du formulaire
    def get(self,request,pk):
        manif_pk = pk

        doc_formset = modelformset_factory(DocumentComplementaire, fields=('information_requise','document_attache'), extra=0 )
        docs = DocumentComplementaire.objects.filter(manif=manif_pk).order_by('-pk')
        done = any([False for doc in docs if not doc.document_attache] or [True,])
        forms = doc_formset(queryset=docs)
        context = {
            'pk': pk,
            'forms': forms,
            'done': done
        }
        return render(request, 'evenements/documentcomplementaire_form_new.html', context)

    # envoi du formulaire
    def post(self,request,pk):
        doc_formset = modelformset_factory(DocumentComplementaire, fields=('information_requise','document_attache'), extra=0)
        forms = doc_formset(request.POST, request.FILES)
        doc = None
        if forms.is_valid():
            for form in forms:
                instances = form.save()
                doc = DocumentComplementaire.objects.get(pk=instances.pk)
            if doc:
                doc.doc_fourni()
            return redirect('evenements:tableau-de-bord-organisateur')
        else:
            return HttpResponse(forms.errors)