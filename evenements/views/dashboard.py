# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from core.util.permissions import require_role
from evenements.models import Manif
from organisateurs.models.structure import Structure


class TableauDeBordOrganisateur(ListView):
    """ Tableau de bord organisateur """

    # Configuration
    model = Manif
    template_name = 'evenements/dashboard_organisateur.html'

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les organisateurs uniquement """
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        """ Renvoyer les manifestations à afficher dans le tableau de bord """
        try:
            user_structure = self.request.user.organisateur.structure
        except Structure.DoesNotExist:
            user_structure = None
        return Manif.objects.filter(structure=user_structure).order_by('-pk')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
