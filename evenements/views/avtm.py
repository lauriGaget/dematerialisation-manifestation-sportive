# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.avtm import AvtmForm, AvtmFilesForm
from ..models.vtm import Avtm


class AvtmDetail(ManifDetail):
    """ Détail de manifestation """

    # Confugration
    model = Avtm


class AvtmCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Avtm
    form_class = AvtmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtm'
        return context


class AvtmUpdate(ManifUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Avtm
    form_class = AvtmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtm'
        return context


class AvtmFilesUpdate(ManifFilesUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Avtm
    form_class = AvtmFilesForm


class AvtmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Avtm