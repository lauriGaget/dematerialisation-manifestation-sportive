# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.dnm import DnmForm, DnmFilesForm
from ..models.dnm import Dnm


class DnmDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dnm


class DnmCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dnm
    form_class = DnmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnm'
        return context


class DnmUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dnm
    form_class = DnmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnm'
        return context


class DnmFilesUpdate(ManifFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = Dnm
    form_class = DnmFilesForm


class DnmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dnm