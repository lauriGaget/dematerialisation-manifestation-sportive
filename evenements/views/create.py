from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.utils.decorators import method_decorator
from django.shortcuts import redirect

from sports.models.sport import Discipline
from administrative_division.models.departement import Departement
from core.util.permissions import require_role
from ..forms.create import CreationEvenementForm
from ..models.manifestation import Manif


class CreationEvenementView(FormView):
    template_name = 'evenements/recherche_creation_formulaire.html'
    form_class = CreationEvenementForm
    success_url = '/thanks/'

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        dept = Departement.objects.get(id=int(form['departement'].data))
        self.request.session['commune'] = form['ville_depart'].data
        self.request.session['discipline'] = form['discipline'].data
        self.request.session['activite'] = form['activite'].data
        self.request.session['nb_participants'] = form['nb_participants'].data
        self.request.session['voie_publique'] = form['voiepublique'].data
        self.request.session['competition'] = form['competition'].data
        self.request.session['circuit_non_permanent'] = form['circuitnonperm'].data
        self.request.session['circuit_homologue'] = form['homologue'].data
        if form['formulaire'].data in ['Avtm', 'Avtmcir']:
            self.request.session['vtm_hors_circulation'] = not form['voiepublique'].data
        url = '/' + form['formulaire'].data + '/add/?dept=' + dept.name
        if dept not in Departement.objects.configured():
            url = '/sansinstance/add/?dept=' + dept.name
            return redirect(url)
        else:
            return redirect(url)

    def get_context_data(self, **kwargs):
        self.request.session['extradata'] = ''
        context = super().get_context_data(**kwargs)
        context['ancien_manifs'] = Manif.objects.filter(structure=self.request.user.organisateur.structure)
        context['vtm'] = list(Discipline.objects.motorise().values_list('id', flat=True))
        return context
