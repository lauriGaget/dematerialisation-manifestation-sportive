# Generated by Django 2.2.1 on 2019-11-21 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evenements', '0010_auto_20191115_0918'),
    ]

    operations = [
        migrations.AddField(
            model_name='manif',
            name='delai_cours',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Delai en cours'),
        ),
    ]
