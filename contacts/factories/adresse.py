# coding: utf-8
import factory

from administrative_division.factories import CommuneFactory
from ..models import Adresse


class AdresseFactory(factory.django.DjangoModelFactory):
    """ Factory des adresses """

    # Champs
    address = '42 rue John Doe'
    commune = factory.SubFactory(CommuneFactory)

    # Meta
    class Meta:
        model = Adresse
