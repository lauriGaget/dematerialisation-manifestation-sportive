# coding: utf-8
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models


class Contact(models.Model):
    """ Personne à contacter pour une manifestation """

    # Champs
    first_name = models.CharField("prénom", max_length=200)
    last_name = models.CharField("nom de famille", max_length=200)
    phone = models.CharField("numéro de téléphone", max_length=14)
    content_type = models.ForeignKey('contenttypes.contenttype', null=True, on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    # Overrides
    def __str__(self):
        """ Renvoyer le texte par défaut pour le contact """
        return ' '.join([self.first_name.capitalize(), self.last_name.capitalize()])

    def natural_key(self):
        return self.first_name, self.last_name, self.phone

    # Meta
    class Meta:
        verbose_name = "contact"
        verbose_name_plural = "contacts"
        app_label = 'contacts'
