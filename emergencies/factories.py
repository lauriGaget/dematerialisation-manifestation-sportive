# coding: utf-8
import factory

from administrative_division.factories import DepartementFactory
from .models import Association1ersSecours, SecoursPublics


class Association1ersSecoursFactory(factory.django.DjangoModelFactory):

    # Champs
    name = 'Croix Rouge'
    departement = factory.SubFactory(DepartementFactory)

    # Méta
    class Meta:
        model = Association1ersSecours


class SecoursPublicsFactory(factory.django.DjangoModelFactory):

    # Champs
    name = ''
    departement = factory.SubFactory(DepartementFactory)

    # Méta
    class Meta:
        model = SecoursPublics
