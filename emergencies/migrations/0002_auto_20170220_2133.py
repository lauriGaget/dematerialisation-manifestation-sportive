# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-02-20 20:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('administrative_division', '0001_initial'),
        ('emergencies', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='association1erssecours',
            name='departement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='administrative_division.Departement', verbose_name='département'),
        ),
        migrations.AddField(
            model_name='secourspublics',
            name='departement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='administrative_division.Departement', verbose_name='département'),
        ),
    ]
