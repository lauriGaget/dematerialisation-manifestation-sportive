# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from contacts.admin import AddressInline, ContactInline
from core.util.admin import RelationOnlyFieldListFilter
from ..models import Association1ersSecours


@admin.register(Association1ersSecours)
class Association1ersSecoursAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Admin des associations de premiers secours """

    list_display = ['pk', 'name', 'email', 'get_departement']
    list_filter = [('departement', RelationOnlyFieldListFilter)]

    # Configuration
    inlines = [AddressInline, ContactInline]

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset
