# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import RelationOnlyFieldListFilter
from ..models import SecoursPublics


@admin.register(SecoursPublics)
class SecoursPublicsAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Admin des secours publics """

    list_display = ['pk', 'name', 'get_departement']
    list_filter = [('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset
