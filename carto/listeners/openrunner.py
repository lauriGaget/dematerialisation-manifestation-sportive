# coding: utf-8
from django.db.models.signals import post_save
from django.dispatch import receiver

from organisateurs.models import *


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=Structure)
    def create_openrunner_user(sender, instance, raw, created, **kwargs):
        """ Créer un utilisateur OpenRunner pour l'organisateur de chaque nouvelle structure """
        if created:
            instance.organisateur.create_openrunner_user()
