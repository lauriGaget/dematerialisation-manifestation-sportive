# -*- coding: utf-8 -*-
# Generated by Django 1.9.10 on 2016-10-20 23:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('events', '0005_change_apps'),
    ]

    state_operations = [
        migrations.CreateModel(
            name='CompteOpenRunner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.BooleanField(default=False, verbose_name='créé')),
                ('creation_date', models.DateTimeField(blank=True, null=True, verbose_name='date de création')),
                ('password', models.CharField(blank=True, max_length=255, verbose_name='mot de passe')),
                ('error_message', models.CharField(blank=True, max_length=255, verbose_name="message d'erreur")),
            ],
            options={
                'verbose_name': 'compte OpenRunner',
                'verbose_name_plural': 'comptes OpenRunner',
                'default_related_name': 'comptesopenrunner',
            },
        ),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(state_operations=state_operations)
    ]
