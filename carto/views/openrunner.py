# coding: utf-8
from django.http.response import HttpResponseRedirect
from furl.furl import furl

from carto.consumer.openrunner import openrunner_api
from configuration import OPENRUNNER_ROUTE_DISPLAY


def route_create(request, role=None):
    """
    Redirige vers la page de création de parcours dans Openrunner

    :param request: requête
    :param role: rôle de l'utilisateur, en principe 'organizer'
    :type role: str
    """
    user = request.user
    if user.is_authenticated:
        openrunner_api.create_from_user(request.user)  # Toujours tenter de créer l'utilisateur OR en premier
        target_url = openrunner_api.get_url_with_login(None, user.username, role=role)
        if user.groups.filter(name="beta-carto"):
            target_url+='&beta=true'
        else:
            beta = ""
        return HttpResponseRedirect(str(target_url))
    return HttpResponseRedirect('')


def route_view(request, route_ids, role=None):
    """
    Redirige vers la page de visualisation de parcours dans Openrunner

    Permet normalement de consulter simplement le trajet, pour les invités
    et pour les organisateurs.

    :param request: requête
    :param route_ids: id de parcours Openrunner, séparés par des tirets
    :param role: rôle de l'utilisateur, en principe 'organizer'
    :type role: str
    :type route_ids: str
    """
    view_ids = route_ids.split('-')
    user = request.user
    if user.is_authenticated:
        openrunner_api.create_from_user(request.user)
        target_url = furl(openrunner_api.get_url_with_login(None, user.username, role=role))
        target_url.args['id'] = view_ids[0] if len(view_ids) == 1 else view_ids
        target_url.args['token'] = openrunner_api.get_user_login_token(user.username, role=role)
        if user.groups.filter(name="beta-carto"):
            beta='&beta=true'
        else:
            beta = ""
        return HttpResponseRedirect(str(target_url)+beta)
    return HttpResponseRedirect('')


def route_view_instructor(request, route_ids, role=None):
    """
    Redirige vers la page de visualisation de parcours dans Openrunner

    Permet normalement de consulter simplement un ou plusieurs trajets
    pour les instructeurs et pour les security_officers (agents SDIS).

    :param request: requête
    :param route_ids: id de parcours Openrunner
    :param role: rôle de l'utilisateur, en principe 'organizer'
    :type role: str
    """
    view_ids = route_ids.split('-')
    user = request.user
    if user.is_authenticated:
        openrunner_api.create_from_user(request.user)
        target_url = furl(openrunner_api.get_url_with_login(None, user.username, role=role))
        target_url.join(OPENRUNNER_ROUTE_DISPLAY)
        target_url.args['id'] = ",".join(view_ids)
        target_url.args['token'] = openrunner_api.get_user_login_token(user.username, role=role)
        if user.groups.filter(name="beta-carto"):
            beta='&beta=true'
        else:
            beta = ""
        return HttpResponseRedirect(str(target_url)+beta)
    return HttpResponseRedirect('')
