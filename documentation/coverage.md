#Coverage

##Commande à effectuer
**coverage erase** =>
*effacer les anciennes données*

**corevage run** =>
*lancer les tests de la ligne de commande de la configuration*

**coverage report > ../rapport.txt** =>
*produire le rapport de texte dans le fichier de sortie*

**coverage html** =>
*produire le rapport html avec les détails des lignes parcourues*

On peut surcharger le fichier de config en ajoutant à la commande, la ligne désirée modifiée (*coverage html -d ailleurs/rapport*)


##Fichier de configuration
Le fichier de configuration s'appelle **.coveragerc** et est placé à la racine du projet.

Il comporte plusieurs parties correspondant à chaque commande *run*, *report*, *html* ...

####source :
Le fait de ne pas spécifier de source modifie la prise en compte des fichiers pour le résultat.
On peut voir que sans la ligne **source**, les fichiers vides et les fichiers non testés sont exclus du résultat :

Différence :

    aide/forms/faq.py                       0      0   100%
    aide/middleware/context.py             22     22     0%   2-42
    api/admin.py                            1      1     0%   1
    api/apps.py                             3      3     0%   1-5
    api/models.py                           1      1     0%   1
    configuration/wsgi.py                  13     13     0%   2-20
    core/cronjob.py                        23     23     0%   1-83
    core/util/diagnose.py                  37     37     0%   2-74
    core/util/security.py                   6      6     0%   2-10
    emergencies/admin/premierssecours.py   16     16     0%   2-26
    emergencies/admin/secourspublics.py    15     15     0%   2-23
    evenements/cronjob.py                  60     60     0%   1-108
    instructions/cronjob.py                23     23     0%   1-38
    TOTAL                               11048   3584    68%
    ---
    TOTAL                               10828   3364    69%

####branch :
Le flag **branche** modifie le résultat dans la prise en compte des branches non parcourues.

Souvent des **if** mais aussi des **while** qui ne seraient jamais exécutés en totalité.

Deux colonnes sont ajoutées dans le rapport, le nombre de branches trouvées et le nombre de branches non parcourues

##Résultat
Le 27-4-20

        Stmts   miss    branch  part    cover
                     
        11048   3584                    68%     avec source
        10828   3364                    69%     sans source
        10828   3364    2876    305     63%     + branch
        10828   2985    2876    312     67%     + tests sélénium

##Pycharm
Pycharm propose le coverage des tests avec soit une version incorporée, soit une version de l'environnement.

Il faut cliquer droit dans le fenêtre **Project** sur le projet, l'application ou le test individuel et sélectionner **Run xxx with Coverage**.

On peut aussi créer une configuration de tests.
Par exemple, pour lancer les tests complets sans Sélénium, donner un nom, laisser target vide et remplir options avec **--exclude-tag=selenium**.
Il faut ensuite la sélectionner dans le menu déroulant et choisir dans les boutons à droite **Run with Coverage**.

Le fichier de configuration tient compte apparement du réglage **branch** mais la présentation des résultats n'est pas conforme au réglage du fichier.
Par contre, les fichiers en édition sont colorés en fonction des manques observés par Coverage.
