Dépendances Python et Django
===

### beautifulsoup4
https://beautiful-soup-4.readthedocs.io/en/latest/

Librairie qui permet de parser une page html et ainsi de pourvoir accéder à n'importe quel élément du DOM sous la forme d'un dictionnaire.

Utilisée dans les apps aide et nouveauté

### Django
https://docs.djangoproject.com

On ne le présente plus... Tutoriel disponible

### django-admin-sortable2
https://django-admin-sortable2.readthedocs.io/en/latest/

Fournit des classes mixin qui permettent, dans l'admin, de pourvoir changer l'ordre des inlines ou des items dans les listes.

Utilisée dans l'appli "aide" pour maitriser l'ordre des onglets et des panneaux dans les pages de type "accordion".

### django-ajax-selects
https://django-ajax-selects.readthedocs.io/en/latest/

Permet d'éditer des champs ForeignKey, ManyToManyField et CharField en utilisant jQuery UI AutoComplete.<br>
Il faut définir un fichier lookups pour enregistrer un canal, ensuite, grâce à la méthode "make_ajax_form", un formulaire est créé avec les champs où la librairie ajoute l'autocomplètion.

Utilisée essentiellement dans l'admin des applis "events", "evenements", "instructions" et "authorizations"

### django-allauth
https://github.com/pennersr/django-allauth

Application django gérant les processus d'authentification (validation de l'adresse email, 	changement d'adresse email, etc...)

### django-annoying
https://github.com/skorokithakis/django-annoying/blob/master/README.md

Librairie qui fournit des fonctionnalités qui réduisent les lignes de code ou reprennent certaines fonctions pour ajouter un plus.

Utilisée dans les applis "aide" (render_to) et "nouveauté" (AutoOneToOneField).

### django-bootstrap-datepicker-plus
https://github.com/monim67/django-bootstrap-datepicker-plus

Fournit un widget basé sur bootstrap pour les champs DateTimeField.

Utilisée dans les formulaires où un champ date est requis et dans les filtres de l'API.

### django-braces
https://github.com/brack3t/django-braces

Application django qui fournit des mixins pour les vues basées sur les classes.

Utilisée dans l'appli "events" avec les mixins LoginRequiredMixin et SuperuserRequiredMixin

### django-ckeditor
https://github.com/Openscop/django-ckeditor

Application permettant d'intégrer l'éditeur wysiwyg ckeditor

### django-clever-selects
https://github.com/PragmaticMates/django-clever-selects

Application permettant de chainer des SelecBox entre elles. Typiquement, chaine la sélection d'un département avec la sélection d'un service consulté.

Utilisation généralisée dans tout le projet.

### django-crispy-forms
https://github.com/maraujop/django-crispy-forms/

Application permettant de générer plus facilement des formulaires avec les thèmes Bootstrap

### django-cron
https://django-cron.readthedocs.io/en/latest/introduction.html

Application qui permet d'exécuter du code Django/Python de façon récurente.

### django-extensions
https://github.com/django-extensions/django-extensions

Divers utilitaires pour aider au développement d'applis django :
    • python manage.py runserver_plus 
    • python manage.py shell_plus  --- Shell interactif avec import automatique des modèles de données  

### django-extra-views
https://github.com/AndrewIngram/django-extra-views

Permet de mixer des vues Update et Create avec des formulaires Inlines

### django-filter
https://django-filter.readthedocs.io/en/master/index.html

Librairie utilisée avec DjangoRestFramework pour définir les filtres de l'API.
Attention, déclarée "django-filters" dans settings.INSTALLED_APPS

### django-fixture-magic
https://github.com/davedash/django-fixture-magic

Librairie qui ajoute 4 nouvelles commandes à manage.py.
Ce sont : "dump_object", "merge_fixtrures", "reorder_fixtures" et "custom_dump".

### django-fsm
https://github.com/kmmbvnr/django-fsm

Application Django permettant de mettre simplement en place une machine à état sur un modèle.

### django-import-export
https://django-import-export.readthedocs.io/en/latest/

C'est une application Django qui permet l'importation ou l'exportation des données d'un modèle.

### django-jenkins
https://github.com/kmmbvnr/django-jenkins/

Application permettant de lancer les tests unitaires ainsi que divers outils d'analyse du code :
- pep8 : conventions d'écriture python 
- pylint : idem un peu plus haut niveau 
- coverage : analyse de la couverture du code par les tests unitaires. Essayer de toujours rester au dessus de 80% de couverture

### django-model-utils
https://github.com/carljm/django-model-utils

Permet d'utiliser InheritanceManager qui facilite les queryset portant sur des classes héritées


### django-multiselectfield
https://github.com/goinnn/django-multiselectfield

Un modèle de champ à selection multiple sans relation m2m

### django-related-admin
https://github.com/PetrDlouhy/django-related-admin

Permet, au niveau de l'administration, d'introduire dans les list-display des relations traversantes sur des clés étrangères avec le double underscore.

Utilisation généralisée dans admin

### django-simple-captcha
https://django-simple-captcha.readthedocs.io/en/latest/index.html

Permet d'ajouter un champ captcha dans un formulaire.

Utilisation dans le formulaire de contact de l'appli "aide"

### djangorestframework
https://www.django-rest-framework.org/

Toolkit permettant de construire une Web API.

Utilisation dans l'appli "aide"

### djangorestframework-jwt
https://getblimp.github.io/django-rest-framework-jwt/

Support de DjangoRestFramework pour l'authentification par Json Web Token.

Non utilisée

### dnspython
http://www.dnspython.org/

DNS toolkit pour python. Permet notamment d'interroger la réponse à un domaine


### factory_boy
https://github.com/rbarrois/factory_boy

Application pour une mise en oeuvre simple des fixtures pour les tests unitaires

### furl
https://github.com/gruns/furl

Librairie python qui permet de parser et manipuler des urls facilement

Utilisée dans l'appli "carto"

### markdown
https://python-markdown.github.io/

Librairie markdown pour python.

### psycopg2
https://pypi.python.org/pypi/psycopg2

Pilote postgreSQL pour python

### python-memcached
https://pypi.org/project/python-memcached/

Gére la mise en cache.

### PyYAML
https://github.com/yaml/pyyaml

Permet la lecture des fichiers Yaml.
Utilisé pour lire la configuration locale dans /etc/django

### raven
https://github.com/getsentry/raven-python

Logge les exceptions générées en prod à travers la plateforme https://app.getsentry.com/

### selenium
https://www.seleniumhq.org/docs/03_webdriver.jsp

Toolkit pour la création de tests sur navigateur.

Prerequis : geckodriver en version > 0.26.0

### simplejson
https://simplejson.readthedocs.io/en/latest/

Permet la manipulation d'objet Json.

Utilisée dans l'appli "core".

### slumber
https://github.com/samgiles/slumber

Permet d'adresser des apis simplement.
Basée sur la librairie "requests"

Utilisée dans l'appli "carto"

### unidecode
https://pypi.org/project/Unidecode/

Fournit une représentation ASCII d'un caractère codé en unicode.

### preview-generator
https://github.com/algoo/preview-generator

Librairie pour la génération des vignettes des fichiers

### Pillow
https://pillow.readthedocs.io/en/5.3.x/

C'est la librairie python de manipulation des images,
elle est requise pour les champ image.

### xhtml2pdf
https://github.com/xhtml2pdf/xhtml2pdf

Librairie django pour créer des pdf à partir de template html.

### func_timeout
https://github.com/kata198/func_timeout

Librairie qui permet de mettre un timeout sur n'importe quelle fonction python.

### MarkupPy, xlrd, xlwt, odfpy, openpyxl

Dépendances à ajouter pour utiliser un pyenv séparé du système