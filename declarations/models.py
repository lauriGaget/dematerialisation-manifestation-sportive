# coding: utf-8
from datetime import date

from django.urls import reverse
from django.db import models
from django.utils import timezone
from django_fsm import FSMField, transition

from administration.models import Prefecture, Instructeur
from configuration.directory import UploadPath
from events.models.manifestation import ManifestationRelatedModel
from notifications.models.action import Action
from notifications.models.notification import Notification


class ManifestationDeclarationQuerySet(models.QuerySet):
    """ Queryset pour les déclarations """

    def by_instructeur(self, instructeur):
        """ Renvoyer les déclarations pour l'instructeur passé (celles de son arrondissement) """
        from core.models import Instance
        config_instructeur = instructeur.user.get_instance().get_instruction_mode()
        if config_instructeur == Instance.IM_ARRONDISSEMENT:
            return self.filter(manifestation__departure_city__arrondissement=instructeur.get_prefecture().arrondissement)
        else:  # Département
            return self.filter(manifestation__departure_city__arrondissement__departement=instructeur.get_prefecture().arrondissement.departement)

    def display_pref(self, instructeur):
        """ Renvoyer les déclarations qui traversent une autre commune """
        result1 = self.by_instructeur(instructeur).filter(manifestation__crossed_cities__isnull=False)
        result2 = self.by_instructeur(instructeur).filter(manifestation__crossed_cities__isnull=True).filter(manifestation__motorizedconcentration__isnull=False)
        return (result1 | result2).distinct()

    def display_mairie(self, instructeur):
        """ Renvoyer les déclarations qui ne traversent pas une autre commune et qui ne sont pas motorisées """
        if type(instructeur) == Instructeur:
            result = self.by_instructeur(instructeur)
        else:
            commune = instructeur.agent.mairieagent.commune
            result = self.filter(manifestation__departure_city=commune)
        return result.filter(manifestation__crossed_cities__isnull=True).filter(manifestation__motorizedconcentration__isnull=True).distinct()

    def to_process(self):
        """ Renvoyer les déclarations dont l'événement n'est pas encore passé """
        return self.filter(manifestation__end_date__gte=timezone.now())

    def finished(self):
        """ Renvoyer les déclarations pour lesquelles l'événement prévu est terminé """
        return self.filter(manifestation__end_date__lt=timezone.now())

    def closest_first(self):
        """ Trier par date de départ de la manifestation """
        return self.order_by('manifestation__begin_date')

    def last_first(self):
        """ Renvoyer les déclarations, triées par date de l'événement décroissante """
        return self.order_by('-manifestation__begin_date')


class ManifestationDeclaration(ManifestationRelatedModel):
    """
    Déclaration de manifestation sportive

    Lorsqu'une association veut organiser un événement occupant temporairement
    le domaine public, elle doit déclarer préalablement la manifestation envisagée
    aux autorités compétentes. (source: service-public.fr)
    +
    Pour les manifestations sportives non motorisées,
    Si l'événement n'est pas une compétition (sans classement),
    en l'absence de circuit ou de parcours, une déclaration à l'aide du formulaire
    Cerfa n°13447*03, au moins 1 mois avant la manifestation

    Ce modèle représente le dossier de déclaration.
    Pour les manifestations nécessitant une demande d'autorisation, se reférer
    au modèle authorizations.manifestationautorisation
    """

    # Champs
    state = FSMField(default='created', verbose_name="état de la déclaration")
    creation_date = models.DateField("date de création", default=date.today)
    dispatch_date = models.DateField("date d'envoi", blank=True, null=True)
    manifestation = models.OneToOneField('events.manifestation', related_name='manifestationdeclaration', verbose_name='manifestation associée', on_delete=models.CASCADE)
    receipt = models.FileField(upload_to=UploadPath('recepisses'), blank=True, null=True,
                               verbose_name="récépissé de déclaration", max_length=512)
    objects = ManifestationDeclarationQuerySet.as_manager()

    # Getter
    @staticmethod
    def get_absolute_url():
        """ Renvoyer l'URL d'accès à la déclaration """
        return reverse('authorizations:dashboard')

    def get_concerned_prefecture(self):
        """ Renvoyer la préfecture concernée """
        return self.manifestation.departure_city.arrondissement.prefecture

    def get_instance(self):
        """
        Renvoyer l'instance de la déclaration

        (la méthode est aussi utilisée de façon générique pour l'upload de fichiers)
        """
        return self.manifestation.get_instance()

    # Action
    def log_creation(self):
        """ Consigner l'envoi de la déclaration """
        recipients = [self.manifestation.structure.organisateur]
        Action.objects.log(recipients, "déclaration envoyée", self.manifestation)

    def log_publish(self, prefecture):
        """ Consigner la publication de récépissé de déclaration """
        Action.objects.log(prefecture.get_instructeurs(), "récépissé de déclaration publié", self.manifestation)

    def notify_creation(self):
        """ Notifier de la réception d'une nouvelle déclaration """
        try:
            prefecture = self.get_concerned_prefecture()
            if not self.manifestation.crossed_cities.all() and hasattr(self.manifestation, 'declarationnm'):
                # Instruction par la mairie
                commune = self.manifestation.departure_city
                recipients = list(commune.get_mairieagents()) + [prefecture]
            else:
                # Instruction par la préfecture
                recipients = list(prefecture.get_instructeurs()) + [prefecture]
            Notification.objects.notify_and_mail(recipients, "déclaration reçue", self.manifestation.structure, self.manifestation)
        except Prefecture.DoesNotExist:
            pass

    def notify_publish(self, prefecture):
        """ Notifier de la publication du récépissé de déclaration """
        recipients = [self.manifestation.structure.organisateur]
        Notification.objects.notify_and_mail(recipients, "récépissé de déclaration publié", prefecture, self.manifestation)

    @transition(field=state, source='created', target='published')
    def publish(self):
        try:
            prefecture = self.get_concerned_prefecture()
            self.notify_publish(prefecture)
            self.log_publish(prefecture)
        except Prefecture.DoesNotExist:
            pass

    # Metadonnées
    class Meta:
        verbose_name = "déclaration de manifestation sportive"
        verbose_name_plural = "déclarations de manifestations sportives"
        app_label = 'declarations'
