# coding: utf-8
from django.urls import path, re_path

from instructions.views.instruction import *
from instructions.views.avis import *
from instructions.views.preavis import *
from instructions.views.tableaudebord import *


app_name = 'instructions'
urlpatterns = [
    # Dashboard
    path('tableaudebord/', TableauDeBord.as_view(), name="tableaudebord"),
    path('tableaudebord/ajax', TableauDeBordAjax.as_view(), name="tableaudebordajax"),
    path('tableaudebord/<role>', TableauDeBord.as_view(), name="tableaudebord"),

    # Preavis
    re_path('preavis/(?P<pk>\d+)/notifier_brigades/', NotifierBrigadesView.as_view(), name='notifier_brigades'),
    re_path('preavis/(?P<pk>\d+)/notifier_cis/', NotifierCISView.as_view(), name='notifier_cis'),
    re_path('preavis/(?P<pk>\d+)/resend/', PreAvisResend.as_view(), name='preavis_resend'),
    re_path('preavis/(?P<pk>\d+)/acknowledge/', PreAvisAcknowledge.as_view(), name='preavis_acknowledge'),
    path('preavis/<int:pk>/addfile/', PreAvisAddFile.as_view(), name='preavis_ajout_fichier'),
    path('preavis/<int:pk>/del/', PreAvisRemove.as_view(), name="prevavis_remove"),
    re_path('preavis/(?P<pk>\d+)/', PreAvisDetail.as_view(), name='preavis_detail'),

    # Avis
    re_path('avis/(?P<pk>\d+)/dispatch/', AvisDispatch.as_view(), name='avis_dispatch'),
    re_path('avis/(?P<pk>\d+)/pass/', AvisPass.as_view(), name='avis_pass'),
    re_path('avis/(?P<pk>\d+)/format/', AvisFormat.as_view(), name='avis_format'),
    re_path('avis/(?P<pk>\d+)/acknowledge/', AvisAcknowledge.as_view(), name='avis_acknowledge'),
    re_path('avis/(?P<pk>\d+)/resend/', AvisResend.as_view(), name='avis_resend'),
    path('avis/<int:pk>/addfile/', AvisAddFile.as_view(), name='avis_ajout_fichier'),
    path('avis/<int:pk>/getpreavis/', AvisGetPreavisAjax.as_view(), name='avis_get_preavis'),
    path(r'avis/<int:pk>/del/', AvisRemove.as_view(), name='avis_remove'),
    re_path('avis/(?P<pk>\d+)/', AvisDetail.as_view(), name='avis_detail'),

    # Autres vues
    re_path('add/(?P<manif_pk>\d+)', InstructionCreateView.as_view(), name='instruction_add'),
    re_path('(?P<pk>\d+)/valider/', InstructionValidDocView.as_view(), name='valider_documents'),
    re_path('(?P<pk>\d+)/invalider/', InstructionInValidDocView.as_view(), name='invalider_documents'),
    re_path('(?P<pk>\d+)/publish/', InstructionPublishBylawView.as_view(), name='instruction_publish'),
    path('<int:pk>/publier_pj/', InstructionTransfertPjVersDocOfficiel.as_view(), name='instruction_transfert_pj_vers_doc_officiel'),
    re_path('(?P<pk>\d+)/relance/', RelanceAvis.as_view(), name="relance_avis"),
    re_path('(?P<pk>\d+)/edit/', InstructionUpdateView.as_view(), name="instruction_update"),
    re_path('(?P<pk>\d+)/', InstructionDetailView.as_view(), name="instruction_detail"),


]
