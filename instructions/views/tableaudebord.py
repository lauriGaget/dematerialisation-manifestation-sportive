# coding: utf-8
from instructions.models import Instruction
from django.views.generic import ListView
from django.utils.decorators import method_decorator
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.db.models import Q

from core.util.permissions import require_role
from core.models.instance import Instance
from core.util.user import UserHelper
from administrative_division.models import Arrondissement


class TableauDeBord(ListView):

    model = Instruction
    template_name = 'instructions/tableaudebord.html'

    @method_decorator(require_role(['agent', 'agentlocal', 'instructeur']))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        """
        expliquer la temporalité avec les valeurs attendues :
            - "a_traiter" :
            - "en_retard" :
            - "archive" :
        expliquer le filtre_specifique avec les valeurs attendues :
            - manif qui permet de considérer les agents mairies en tant qu'instructeurs
        """

        user = self.request.user

        if 'role' in self.kwargs:
            role = self.kwargs['role']
        else:
            role = UserHelper.get_role_name(user)

        instance = self.request.user.get_instance()

        if self.request.method == 'GET':
            temporalite = self.request.GET.get('temporalite', default='a_traiter')
            filtre_specifique = self.request.GET.get('filtre_specifique', default=None)
        else:
            return None

        # Filtrer les manifs en fonctions des actions à mener

        manifacceslibre = Instruction.objects.all().prefetch_related('manif__ville_depart__arrondissement__departement')
        if temporalite == "a_traiter":  # cas général
            manifacceslibre = manifacceslibre.filter(manif__date_fin__gte=timezone.now())
        elif temporalite == "en_retard":  # todo : à supprimer ou à développer
            manifacceslibre = manifacceslibre
        elif temporalite == "archive":
            manifacceslibre = manifacceslibre.filter(manif__date_fin__lt=timezone.now())

        # Filtrer les manifs en fonctions du rôle de l'utilisateur
        if not filtre_specifique and role in ['federationagent', 'serviceagent', 'ddspagent', 'cgagent',
                                              'mairieagent', 'cgsuperieuragent', 'sdisagent']\
                and UserHelper.get_role_name(user) in ['federationagent', 'serviceagent', 'ddspagent', 'cgagent',
                                                       'mairieagent', 'cgsuperieuragent', 'sdisagent']:
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
            if user.has_role('cgsuperieuragent') and role == 'cgsuperieuragent':
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                         avis__etat__in=['formaté', 'rendu']).distinct()
        elif user.has_role('codisagent') and role == 'codisagent':
            service = user.get_departement().sdis
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                     avis__etat='rendu')
        elif user.has_role('ggdagent') and role == 'ggdagent':
            if instance.get_workflow_ggd() == Instance.WF_EDSR:
                service = user.get_departement().edsr
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                         avis__etat__in=['formaté', 'rendu']).distinct()
            else:
                service = user.get_service()
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
        elif user.has_role('edsragent') and role == 'edsragent':
            if instance.get_workflow_ggd() == Instance.WF_GGD_EDSR:
                service = user.get_departement().ggd
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                         avis__etat__in=['transmis', 'distribué', 'formaté', 'rendu']).distinct()
            else:
                service = user.get_service()
                ct_service = ContentType.objects.get_for_model(service)
                manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
        elif user.has_role('cisagent') and role == 'cisagent':
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__acces__isnull=False,
                                                     avis__acces__content_type=ct_service,
                                                     avis__acces__object_id=service.id,
                                                     avis__etat='rendu').distinct()
        elif user.has_role('brigadeagent') and role == 'brigadeagent':
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__acces__isnull=False,
                                                     avis__acces__content_type=ct_service,
                                                     avis__acces__object_id=service.id).distinct()

        elif role in ['compagnieagentlocal', 'cgdagentlocal', 'commissariatagentlocal',
                      'cgserviceagentlocal', 'edsragentlocal']\
                and UserHelper.get_role_name(user) in ['compagnieagentlocal', 'cgdagentlocal', 'commissariatagentlocal',
                                                       'cgserviceagentlocal', 'edsragentlocal']:
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__preavis__content_type=ct_service,
                                                     avis__preavis__object_id=service.id)

        elif user.has_role('instructeur') and role == 'instructeur':
            config_instructeur = user.get_instance().get_instruction_mode()
            if config_instructeur == Instance.IM_ARRONDISSEMENT:
                manifacceslibre = manifacceslibre.filter(manif__ville_depart__arrondissement=user.instructeur.get_prefecture().arrondissement)
            else:  # Département
                manifacceslibre = manifacceslibre.filter(manif__ville_depart__arrondissement__departement=user.instructeur.get_prefecture().arrondissement.departement)
            if filtre_specifique == 'instructionmairie':  # Instruction en mairie
                manifacceslibre = manifacceslibre.filter(Q(manif__villes_traversees__isnull=True) &
                                                         Q(manif__dvtm__isnull=True) & Q(manif__avtm__isnull=True) & Q(manif__avtmcir__isnull=True))
            else:  # Instruction en préfecture
                result1 = manifacceslibre.filter(manif__villes_traversees__isnull=False)
                result2 = manifacceslibre.filter(Q(manif__villes_traversees__isnull=True) &
                                                 Q(manif__dvtm__isnull=False) | Q( manif__avtm__isnull=False) | Q(manif__avtmcir__isnull=False))
                manifacceslibre = (result1 | result2).distinct()

        elif user.has_role('mairieagent') and role == 'mairieagent' and filtre_specifique == 'instructionmairie':  # agent mairie en tant qu'instructeur
            commune = user.agent.mairieagent.commune
            manifacceslibre = manifacceslibre.filter(Q(manif__ville_depart=commune) & Q(manif__villes_traversees__isnull=True) &
                                                     Q(manif__dvtm__isnull=True) & Q(manif__avtm__isnull=True) & Q(manif__avtmcir__isnull=True))
        else:
            return Instruction.objects.none()

        # MesManifAvecAutorisationAccess = Manif.objects.filter()  # avec la classe AutorisationAccess

        return manifacceslibre
        # return manifacceslibre.union(MesManifAvecAutorisationAccess).distinct()
        # voir pour éliminer les éventuels doublons

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        # Utilisés pour les liens des boutons du bandeau titre
        if 'role' in context['view'].kwargs:
            context['role'] = context['view'].kwargs['role']
        else:
            context['role'] = UserHelper.get_role_name(self.request.user)
        context['user_is_instructeur'] = bool(
            user.has_role('instructeur') and context['role'] == 'instructeur' or
            user.has_role('mairieagent') and self.request.GET.get('filtre_specifique', default=None) == 'instructionmairie')
        context['archive'] = bool(self.request.GET.get('temporalite', default=None) == 'archive')
        context['instruction_mairie'] = bool(self.request.GET.get('filtre_specifique', default=None) == 'instructionmairie')
        if user.has_role('serviceagent'):
            service_dept = self.request.user.get_service().departements.all()
            context['onglet_service'] = bool(len(service_dept) > 1)
            context['liste_dept'] = service_dept
        if user.has_role('instructeur'):
            context['onglet'] = bool(
                user.get_instance().acces_arrondissement and not
                user.get_instance().instruction_mode and not
                self.request.GET.get('temporalite', default=None) == 'archive')
            context['user_arrondis'] = user.instructeur.get_prefecture().arrondissement.code
            context['liste_arrondis'] = Arrondissement.objects.filter(departement=user.get_instance().departement)
        # Todo :     reprendre ici la logique métier d'affichage des boutons qui se trouve dans le titre h1 du template /instructions/templates/instructions/tableaudebord.html
        return context


class TableauDeBordAjax(ListView):

    model = Instruction
    template_name = 'instructions/tbody_TdB.html'

    @method_decorator(require_role(['instructeur', 'serviceagent']))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        if user.has_role('instructeur'):
            arrondis_code = int(self.request.GET.get('arrondis', default='0'))
            filtre_specifique = self.request.GET.get('filtre_specifique', default=None)
            manifacceslibre = Instruction.objects.all().prefetch_related('manif__ville_depart__arrondissement__departement')
            manifacceslibre = manifacceslibre.filter(manif__date_fin__gte=timezone.now())
            config_instructeur = user.get_instance().get_instruction_mode()
            if config_instructeur == Instance.IM_ARRONDISSEMENT and user.get_instance().acces_arrondissement:
                manifacceslibre = manifacceslibre.filter(manif__ville_depart__arrondissement__code=arrondis_code)
                if filtre_specifique == 'instructionmairie':  # Instruction en mairie
                    manifacceslibre = manifacceslibre.filter(Q(manif__villes_traversees__isnull=True) &
                                                             Q(manif__dvtm__isnull=True) & Q(manif__avtm__isnull=True) & Q(manif__avtmcir__isnull=True))
                else:  # Instruction en préfecture
                    result1 = manifacceslibre.filter(manif__villes_traversees__isnull=False)
                    result2 = manifacceslibre.filter(Q(manif__villes_traversees__isnull=True) &
                                                     Q(manif__dvtm__isnull=False) | Q( manif__avtm__isnull=False) | Q(manif__avtmcir__isnull=False))
                    manifacceslibre = (result1 | result2).distinct()
            else:
                manifacceslibre = manifacceslibre.none()
        elif user.has_role('serviceagent'):
            dept = self.request.GET.get('dept', default=None)
            manifacceslibre = Instruction.objects.all().prefetch_related('manif__ville_depart__arrondissement__departement')
            manifacceslibre = manifacceslibre.filter(manif__date_fin__gte=timezone.now())
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
            manifacceslibre = manifacceslibre.filter(manif__ville_depart__arrondissement__departement__name=dept)
        else:
            manifacceslibre = Instruction.objects.none()
        return manifacceslibre

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.request.user
        context['user_is_instructeur'] = bool(user.has_role('instructeur'))
        return context
