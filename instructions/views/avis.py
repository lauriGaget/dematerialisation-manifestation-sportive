# coding: utf-8
from django.utils import timezone
import datetime
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView

from administration.models.service import CGD, Commissariat, Compagnie, CGService, EDSR
from instructions.decorators import verifier_service_avis, verifier_secteur_instruction
from core.util.permissions import require_role
from core.util.user import UserHelper
from core.models.instance import Instance
from ..models import Avis, PreAvis, PieceJointeAvis
from ..forms.avis import AvisDispatchForm, AvisFormatForm, AvisPassForm, AvisAcknowledgeForm

liste_decorateur_avispass = [require_role('ggdagent'), verifier_service_avis]
liste_decorateur_avisformat = [require_role(['edsragent', 'cgagent']), verifier_service_avis]
liste_decorateur_avisdispatch = [require_role(['edsragent', 'ggdagent', 'cgagent', 'ddspagent', 'sdisagent']), verifier_service_avis]


@method_decorator(verifier_service_avis, name='dispatch')
class AvisDetail(DetailView):
    """
    Vue de détail d'un avis
    La vue est protégée par le décorateur "verifier_service_avis" qui intervient dans la méthode "dispatch"
    """

    # Configuration
    model = Avis

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        context['type'] = self.object.instruction.manif.get_type_manif()
        if not self.object.instruction.manif.date_depassee():
            wf_ggd = self.object.get_instance().get_workflow_ggd()
            context['demander_preavis'] = bool(
                self.object.etat == 'demandé' and (
                        self.object.service_concerne == 'ddsp' or self.object.service_concerne == 'sdis' or
                        self.object.service_concerne == 'cg' and hasattr(self.request.user.agent, 'cgagent') or
                        self.object.service_concerne == 'edsr' and hasattr(self.request.user.agent, 'edsragent') or
                        self.object.service_concerne == 'ggd' and hasattr(self.request.user.agent, 'edsragent') and wf_ggd == Instance.WF_GGD_EDSR or
                        self.object.service_concerne == 'ggd' and hasattr(self.request.user.agent, 'ggdagent') and wf_ggd == Instance.WF_GGD_SUBEDSR) or
                self.object.etat == 'transmis' and hasattr(self.request.user.agent, 'edsragent'))
            context['rendre_avis'] = bool(
                self.object.etat == 'demandé' and (
                        self.object.service_concerne == 'mairie' or self.object.service_concerne == 'service' or
                        self.object.service_concerne == 'federation') or
                self.object.etat == 'distribué' and (
                        self.object.service_concerne == 'ddsp' and self.object.tous_preavis_rendus() or
                        self.object.service_concerne == 'sdis' and self.object.tous_preavis_rendus() or
                        self.object.service_concerne == 'ggd' and self.object.tous_preavis_rendus() and wf_ggd == Instance.WF_GGD_SUBEDSR) or
                self.object.etat == 'formaté' and (
                        self.object.service_concerne == 'cg' and hasattr(self.request.user.agent, 'cgsuperieuragent') or
                        self.object.service_concerne == 'edsr' and hasattr(self.request.user.agent, 'ggdagent') or
                        self.object.service_concerne == 'ggd' and hasattr(self.request.user.agent, 'ggdagent') and wf_ggd == Instance.WF_GGD_EDSR))
            context['passer_avis'] = bool(
                self.object.etat == 'demandé' and (
                        self.object.service_concerne == 'ggd' and hasattr(self.request.user.agent, 'ggdagent') and wf_ggd == Instance.WF_GGD_EDSR))
            context['mettre_en_forme'] = bool(
                self.object.etat == 'distribué' and (
                        self.object.service_concerne == 'cg' and hasattr(self.request.user.agent, 'cgagent') and self.object.tous_preavis_rendus() or
                        self.object.service_concerne == 'edsr' and hasattr(self.request.user.agent, 'edsragent') and self.object.tous_preavis_rendus() or
                        self.object.service_concerne == 'ggd' and hasattr(self.request.user.agent, 'edsragent') and self.object.tous_preavis_rendus()))
            context['transmis_message'] = bool(self.object.etat == 'transmis' and not hasattr(self.request.user.agent, 'edsragent'))
            context['distribué_message'] = bool(self.object.etat == 'distribué' and not context['mettre_en_forme'] and not context['rendre_avis'])
            context['ggdagent_et_wf_ggd_edsr'] = bool(hasattr(self.request.user.agent, 'ggdagent') and wf_ggd == Instance.WF_GGD_EDSR)
            context['formaté_message'] = bool(self.object.etat == 'formaté' and not context['rendre_avis'])
            context['rendu_message'] = bool(self.object.etat == 'rendu')
        else:
            context['depasse'] = True
        return context


@method_decorator(liste_decorateur_avispass, name='dispatch')
class AvisPass(UpdateView):
    """ Vue de passage de l'avis à l'EDSR (juste renseigner l'EDSR en fait) """

    # Configuration
    model = Avis
    form_class = AvisPassForm

    # Overrides
    def get_form(self, form_class=None):
        """ Renvoyer le formulaire """
        form = super().get_form(form_class=form_class)
        # Filtrer les choix à ceux possibles selon la manifestation de l'avis
        # TODO : actuellement, le formulaire peut proposer plusieurs EDSR mais un seul peut être sélectionné puis traité
        # Question : que faire un cas d'instruction sur plusieurs département
        form.fields['edsr_concerne'].queryset = EDSR.objects.filter(departement__in=self.object.get_manifestation().get_departements_traverses())
        return form

    def form_valid(self, form):
        if form.instance.etat != 'demandé':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été transmis.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            edsr = form.cleaned_data['edsr_concerne']
            if edsr is not None:
                form.instance.agent = self.request.user
                avis = form.instance
                avis.notifier_passage_avis(self.request.user, edsr.get_edsr_agents())
            return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pass'] = True
        return context


@method_decorator(liste_decorateur_avisdispatch, name='dispatch')
class AvisDispatch(UpdateView):
    """ Vue d'envoi des demandes de preavis """

    # Configuration
    model = Avis
    form_class = AvisDispatchForm

    # Overrides
    def get_form(self, form_class=None):
        """ Renvoyer le formulaire """
        form = super().get_form(form_class=form_class)
        # Filtrer les choix à ceux possibles selon la manifestation de l'avis
        if self.object.service_concerne == 'ggd':
            form.fields['cgd_concerne'].queryset = CGD.objects.filter(commune__arrondissement__departement__in=self.object.get_manifestation().get_departements_traverses())
            form.fields['edsr_concerne'].queryset = EDSR.objects.filter(departement__in=self.object.get_manifestation().get_departements_traverses())
            if Instance.objects.get_for_request(self.request).get_workflow_ggd() == Instance.WF_GGD_EDSR:
                # Dans le workflow GGD avec agent EDSR, cacher le choix d'EDSR à l'agent EDSR
                if UserHelper.get_role_name(self.request.user) == 'edsragent':
                    form.fields.pop('edsr_concerne')
                # Dans le workflow GGD avec agent EDSR, cacher le choix de CGD à l'agent GGD
                # Inutile car un ggd n'accede pas a dispatch en WF_GGD_EDSR
                # elif UserHelper.get_role_name(self.request.user) == 'ggdagent':
                #     form.fields.pop('cgd_concerne')
            form.fields.pop('commissariats_concernes')
            form.fields.pop('services_concernes')
            form.fields.pop('compagnies_concernees')
        if self.object.service_concerne == 'ddsp':
            form.fields['commissariats_concernes'].queryset = Commissariat.objects.filter(commune__arrondissement__departement__in=self.object.get_manifestation().get_departements_traverses())
            form.fields.pop('services_concernes')
            form.fields.pop('compagnies_concernees')
            form.fields.pop('cgd_concerne')
            form.fields.pop('edsr_concerne')
        if self.object.service_concerne == 'edsr':
            form.fields['cgd_concerne'].queryset = CGD.objects.filter(commune__arrondissement__departement__in=self.object.get_manifestation().get_departements_traverses())
            form.fields.pop('commissariats_concernes')
            form.fields.pop('services_concernes')
            form.fields.pop('compagnies_concernees')
            form.fields.pop('edsr_concerne')
        if self.object.service_concerne == 'sdis':
            form.fields['compagnies_concernees'].queryset = Compagnie.objects.filter(sdis__departement__in=self.object.get_manifestation().get_departements_traverses())
            form.fields.pop('commissariats_concernes')
            form.fields.pop('services_concernes')
            form.fields.pop('edsr_concerne')
            form.fields.pop('cgd_concerne')
        if self.object.service_concerne == 'cg':
            form.fields['services_concernes'].queryset = CGService.objects.filter(cg__departement__in=self.object.get_manifestation().get_departements_traverses())
            form.fields.pop('commissariats_concernes')
            form.fields.pop('compagnies_concernees')
            form.fields.pop('edsr_concerne')
            form.fields.pop('cgd_concerne')
        return form

    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.etat not in ['transmis', 'demandé']:
            messages.error(self.request, "Aucune action effectuée : les demandes de pré-avis ont déjà été envoyées.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            if self.object.service_concerne in ['ggd', 'edsr']:
                # Création des pré-avis
                if form.cleaned_data['cgd_concerne']:
                    for cgd in form.cleaned_data['cgd_concerne']:
                        if cgd not in [preavis.destination_object for preavis in PreAvis.objects.filter(service_concerne='cgd').filter(avis=form.instance)]:
                            preavis = PreAvis.objects.create(service_concerne='cgd', avis=form.instance, destination_object=cgd)
                            preavis.notifier_creation_preavis(agents=preavis.get_agents(), content_object=preavis.get_services_sup())
                if 'edsr_concerne' in form.cleaned_data and form.cleaned_data['edsr_concerne']:
                    edsr = form.cleaned_data['edsr_concerne']
                    if edsr not in [preavis.destination_object for preavis in PreAvis.objects.filter(service_concerne='edsr').filter(avis=form.instance)]:
                        preavis = PreAvis.objects.create(service_concerne='edsr', avis=form.instance, destination_object=edsr)
                        preavis.notifier_creation_preavis(agents=preavis.get_agents(), content_object=preavis.get_services_sup())
            elif self.object.service_concerne == 'sdis':
                # Création des pré-avis
                if form.cleaned_data['compagnies_concernees']:
                    for compagnie in form.cleaned_data['compagnies_concernees']:
                        if compagnie not in [preavis.destination_object for preavis in PreAvis.objects.filter(service_concerne='compagnies').filter(avis=form.instance)]:
                            preavis = PreAvis.objects.create(service_concerne='compagnies', avis=form.instance, destination_object=compagnie)
                            preavis.notifier_creation_preavis(agents=preavis.get_agents(), content_object=preavis.get_services_sup())
            elif self.object.service_concerne == 'ddsp':
                # Création des pré-avis
                if form.cleaned_data['commissariats_concernes']:
                    for commissariat in form.cleaned_data['commissariats_concernes']:
                        if commissariat not in [preavis.destination_object for preavis in PreAvis.objects.filter(service_concerne='commissariats').filter(avis=form.instance)]:
                            preavis = PreAvis.objects.create(service_concerne='commissariats', avis=form.instance, destination_object=commissariat)
                            preavis.notifier_creation_preavis(agents=preavis.get_agents(), content_object=preavis.get_services_sup())
            elif self.object.service_concerne == 'cg':
                # Création des pré-avis
                if form.cleaned_data['services_concernes']:
                    for service in form.cleaned_data['services_concernes']:
                        if service not in [preavis.destination_object for preavis in PreAvis.objects.filter(service_concerne='services').filter(avis=form.instance)]:
                            preavis = PreAvis.objects.create(service_concerne='services', avis=form.instance, destination_object=service)
                            preavis.notifier_creation_preavis(agents=preavis.get_agents(), content_object=preavis.get_services_sup())
            else:
                pass
            form.instance.agent = self.request.user
            form.instance.log_distribution(self.request.user)
            return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dispatch'] = True
        return context


@method_decorator(liste_decorateur_avisformat, name='dispatch')
class AvisFormat(UpdateView):
    """ Vue de mise en forme de l'avis GGD """

    # Configuration
    model = Avis
    form_class = AvisFormatForm

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.etat == 'distribué':
            if form.instance.tous_preavis_rendus():
                form.instance.formaterAvis(self.request.user)
                form.instance.agent = self.request.user
                return super().form_valid(form)
            else:
                messages.error(self.request, "Aucune action effectuée : les pré-avis ne sont pas tous rendus.")
                return HttpResponseRedirect(self.get_success_url())
        else:
            messages.error(self.request, "Aucune action effectuée : cet avis n'est pas à l'état distribué.")
            return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['format'] = True
        return context


@method_decorator(verifier_service_avis, name='dispatch')
class AvisAcknowledge(UpdateView):
    """ Vue de rendu d'avis GGD """

    # Configuration
    model = Avis
    form_class = AvisAcknowledgeForm

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.etat == 'rendu':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été validé.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            if not form.instance.tous_preavis_rendus():
                messages.error(self.request, "Aucune action effectuée : les pré-avis ne sont pas tous rendus.")
                return HttpResponseRedirect(self.get_success_url())
            if form.instance.etat != 'formaté' and (
                    form.instance.service_concerne in ['edsr', 'cg'] or (
                    form.instance.service_concerne == 'ggd' and form.instance.get_instance().get_workflow_ggd() == Instance.WF_GGD_EDSR)):
                messages.error(self.request, "Aucune action effectuée : cet avis n'est pas à l'état formaté.")
                return HttpResponseRedirect(self.get_success_url())
            if form.instance.etat != 'distribué' and form.instance.service_concerne in ['sdis', 'ddsp']:
                messages.error(self.request, "Aucune action effectuée : cet avis n'est pas à l'état distribué.")
                return HttpResponseRedirect(self.get_success_url())
            form.instance.date_reponse = datetime.date.today()
            form.instance.agent = self.request.user
            form.instance.rendreAvis(self.request.user)
            return super().form_valid(form)

    def get_initial(self):
        """ Renvoyer les valeurs de base du formulaire """
        initial = super().get_initial()
        initial['favorable'] = True
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rendre'] = bool(self.object.service_concerne != 'mairie' and
                                 self.object.service_concerne != 'service' and
                                 self.object.service_concerne != 'federation')
        return context


@method_decorator(verifier_secteur_instruction(arr_wrt=True), name='dispatch')
class AvisResend(SingleObjectMixin, View):
    """ Vue de renvoi des demandes d'avis """

    # Configuration
    model = Avis

    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notifier_creation_avis(origine=self.request.user.get_service(), agents=instance.get_agents())
        instance.log_resend(self.request.user, instance.get_service().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('instructions:instruction_detail', kwargs={'pk': instance.instruction.id}))


@method_decorator(verifier_service_avis, name='dispatch')
class AvisGetPreavisAjax(SingleObjectMixin, View):
    """
    Renvoyer la synthèse des pré-avis pour pré-remplir la prescription de l'avis
    """
    # Configuration
    model = Avis

    # Ajouter une pièce jointe
    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        liste_preavis = avis.get_tous_preavis()
        return render(request, "instructions/avis_prescription.html", {'liste_preavis': liste_preavis})


@method_decorator(verifier_service_avis, name='dispatch')
class AvisAddFile(SingleObjectMixin, View):

    """ class d'ajout des avis """

    # Configuration
    model = Avis

    # Ajouter une pièce jointe
    def post(self, request, *args, **kwargs):
        avis = self.get_object()
        if 'fichier' in request.FILES and request.FILES['fichier']:
            PieceJointeAvis.objects.create(fichier=request.FILES['fichier'], avis=avis, date_depot=timezone.now())
            avis.log_ajout_pj(self.request.user)
            if avis.etat == 'rendu':
                avis.notifier_ajout_pj(self.request.user)
            return redirect('instructions:avis_detail', pk=avis.pk)
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Données POST incorrectes !"},
                          status=403)

    # Transférer une pièce jointe sur un avis
    def get(self, request, *args, **kwargs):
        preavis = PreAvis.objects.get(id=kwargs.get('pk', ''))
        avis = preavis.avis
        if 'doc' in request.GET and request.GET['doc']:
            pj = PieceJointeAvis.objects.get(id=request.GET['doc'])
            pj.avis = avis
            pj.save()
            avis.log_ajout_pj(self.request.user)
            if avis.etat == 'rendu':
                avis.notifier_ajout_pj(self.request.user)
            return redirect('instructions:avis_detail', pk=avis.pk)
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Données GET incorrectes !"},
                          status=403)


@method_decorator(verifier_secteur_instruction, name='dispatch')
class AvisRemove(SingleObjectMixin, View):
    """ Vue d'annulation d'une demande d'avis """

    # Configuration
    model = Avis

    # Overrides
    def get(self, request, *args, **kwargs):
        avis = self.get_object()
        instruction = avis.instruction
        if avis.etat == 'demandé':

            avis.log_delete(self.request.user, avis.get_service().__str__())
            avis.notifier_suppression_avis(origine=self.request.user.get_service(), agents=avis.get_agents())

            if avis.service_concerne == 'sdis':
                instruction.sdis_concerne = False
            if avis.service_concerne == 'ggd':
                instruction.ggd_concerne = False
            if avis.service_concerne == 'edsr':
                instruction.edsr_concerne = False
            if avis.service_concerne == "cg":
                instruction.cg_concerne = False
            if avis.service_concerne == 'ddsp':
                instruction.ddsp_concerne = False
            if avis.service_concerne == "mairie":
                instruction.villes_concernees.remove(avis.destination_object)
            if avis.service_concerne == 'service':
                instruction.services_concernes.remove(avis.destination_object)
            avis.delete()
            if not instruction.avis.all():
                instruction.etat = 'demandée'
            instruction.save()
            return redirect(reverse('instructions:instruction_detail', kwargs={'pk': instruction.pk}))
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous ne pouvez plus supprimer cet avis. Il n'est plus dans l'état initial !"}, status=403)
