# coding: utf-8
import datetime
from django.urls import reverse
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import View, DetailView
from django.views.generic.edit import UpdateView
from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponseRedirect

from notifications.models.action import Action
from notifications.models.notification import Notification
from core.util.permissions import require_role
from instructions.decorators import verifier_service_preavis, verifier_service_avis
from ..forms.preavis import PreAvisForm, NotifyBrigadesForm, NotifyCISForm
from ..models import AutorisationAcces, PieceJointeAvis, PreAvis, AutorisationAcces


liste_decorateur_preavisnotifbrigade = [require_role('cgdagentlocal'), verifier_service_preavis]
liste_decorateur_preavisnotifcis = [require_role('compagnieagentlocal'), verifier_service_preavis]


@method_decorator(verifier_service_preavis, name='dispatch')
class PreAvisDetail(DetailView):
    """ Détail d'un préavis """

    # Configuration
    model = PreAvis

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        context['type'] = self.object.avis.instruction.manif.get_type_manif()
        if not self.object.avis.instruction.manif.date_depassee():
            context['rendre_preavis'] = bool(
                self.object.etat == 'demandé' and (
                        self.object.service_concerne == 'services' or
                        self.object.service_concerne == 'compagnies' or
                        self.object.service_concerne == 'edsr' or
                        self.object.service_concerne == 'commissariats') or
                self.object.etat == 'notifié')
            context['informer_brigades'] = bool(
                self.object.etat == 'demandé' and self.object.service_concerne == 'cgd' or
                self.object.etat == 'notifié')
            context['informer_cis'] = bool(
                self.object.service_concerne == 'compagnies' and
                self.object.etat == 'rendu' and
                self.object.avis.etat == 'rendu' and
                AutorisationAcces.objects.filter(avis_id=self.object.avis.id).count() == 0)
            context['rendu_message'] = bool(self.object.etat == 'rendu' and not context['informer_cis'])
        else:
            context['depasse'] = True
        return context


@method_decorator(verifier_service_avis, name='dispatch')
class PreAvisRemove(SingleObjectMixin, View):
    """ View pour supprimer un préavis """

    # Configuration
    model = PreAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        preavis = self.get_object()
        avis = preavis.avis
        if preavis.etat == 'demandé':
            preavis.notifier_suppression_preavis(agents=preavis.get_agents(), content_object=preavis.get_services_sup())
            preavis.log_suppression(self.request.user)
            preavis.delete()
            if not avis.preavis.all():
                avis.etat = 'demandé'
                avis.save()
            return redirect(reverse('instructions:avis_detail', kwargs={'pk': avis.pk}))
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Vous ne pouvez plus supprimer ce préavis. Il a été rendu ou notifié !"}, status=403)


@method_decorator(verifier_service_preavis, name='dispatch')
class PreAvisAcknowledge(UpdateView):
    """ Rendu préavis CG """

    # Configuration
    model = PreAvis
    form_class = PreAvisForm

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.etat == 'rendu':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été validé.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.date_reponse = datetime.date.today()
            form.instance.agentlocal = self.request.user
            form.instance.rendrePreavis(self.request.user)
            return super().form_valid(form)

    def get_initial(self):
        """ Renvoyer les valeurs de base du formulaire """
        initial = super().get_initial()
        initial['favorable'] = True
        return initial


@method_decorator(verifier_service_preavis, name='dispatch')
class PreAvisAddFile(SingleObjectMixin, View):
    """ class d'ajout des avis """

    # Configuration
    model = PreAvis

    def post(self, request, *args, **kwargs):
        preavis = self.get_object()
        if 'fichier' in request.FILES and request.FILES['fichier']:
            if preavis.etat == "rendu":
                PieceJointeAvis.objects.create(fichier=request.FILES['fichier'], preavis=preavis)
                preavis.log_ajout_pj(self.request.user)
                preavis.notifier_ajout_pj()
                return redirect('instructions:preavis_detail', pk=preavis.pk)
            else:
                return render(request, "core/access_restricted.html",
                              {'message': "Vous ne pouvez pas ajouter de pièce jointe à ce préavis. Il n'est pas encore rendu !"},
                              status=403)
        else:
            return render(request, "core/access_restricted.html",
                          {'message': "Données POST incorrectes !"},
                          status=403)


@method_decorator(verifier_service_avis, name='dispatch')
class PreAvisResend(SingleObjectMixin, View):
    """ Vue de renvoi des demandes de préavis """

    # Configuration
    model = PreAvis

    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notifier_creation_preavis(content_object=self.request.user.get_service(), agents=instance.get_agents())
        instance.log_resend(self.request.user, instance.__str__())
        messages.success(request, "Demande de préavis relancée avec succès")
        return redirect('instructions:avis_detail', pk=instance.avis.id)


@method_decorator(liste_decorateur_preavisnotifbrigade, name='dispatch')
class NotifierBrigadesView(UpdateView):
    """ Notification des brigades """

    # Configuration
    model = PreAvis
    form_class = NotifyBrigadesForm

    # Overrides
    def form_valid(self, form):
        if form.cleaned_data['brigades_concernees']:
            preavis = form.instance
            preavis.agentlocal = self.request.user
            brg_liste = []
            deja_informe = False
            for brg in form.cleaned_data['brigades_concernees']:
                ct_brg = ContentType.objects.get_for_model(brg)
                if not AutorisationAcces.objects.filter(content_type=ct_brg, object_id=brg.id, avis_id=preavis.avis.id).exists():
                    brg_liste.append(brg)
                    AutorisationAcces.objects.create(referent=self.request.user, service_object=brg,
                                                     manif=preavis.avis.get_manifestation(), avis=preavis.avis)
                else:
                    deja_informe = True
            if brg_liste and not deja_informe:
                messages.success(self.request, "Les brigades ont été informées.")
            if brg_liste and deja_informe:
                messages.success(self.request, "Les brigades ont été informées, certaines l'étaient déjà !")
            if  not brg_liste and deja_informe:
                messages.warning(self.request, "La ou les brigades ont déjà été informées !")
            preavis.notifier_brigades(brg_liste, self.request.user)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['brigade'] = True
        return context


@method_decorator(liste_decorateur_preavisnotifcis, name='dispatch')
class NotifierCISView(UpdateView):
    """ Notification des agents CIS """

    # Configuration
    model = PreAvis
    form_class = NotifyCISForm

    # Overrides
    def form_valid(self, form):
        if form.instance.etat != 'rendu' or form.instance.avis.etat != 'rendu' or AutorisationAcces.objects.filter(avis_id=form.instance.avis.id).count() != 0:
            messages.error(self.request, "Aucune action effectuée : cet avis / préavis n'a pas été rendu ou des CIS ont déjà été informé.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            messages.success(self.request, "Les CIS ont été informés.")
            if form.cleaned_data['cis_concernes']:
                preavis = form.instance
                recipients = [agent for cis in form.cleaned_data['cis_concernes'] for agent in cis.cisagents.all()]
                Notification.objects.notify_and_mail(recipients, "Prenez connaissance des informations",
                                                     preavis.destination_object, preavis.avis.get_manifestation())
                Action.objects.log(self.request.user, "CIS informé", preavis.avis.get_manifestation())
                for service in form.cleaned_data['cis_concernes']:
                    AutorisationAcces.objects.create(referent=self.request.user, service_object=service,
                                                     manif=preavis.avis.get_manifestation(), avis=preavis.avis)
            return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cis'] = True
        return context
