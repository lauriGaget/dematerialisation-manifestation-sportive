import factory

from evenements.factories import DcnmFactory
from .models.instruction import Instruction
from .models.avis import Avis


class InstructionFactory(factory.django.DjangoModelFactory):
    """ Factory pour les instructions """

    # Champs
    manif = factory.SubFactory(DcnmFactory)

    # Meta
    class Meta:
        model = Instruction


class AvisInstructionFactory(factory.django.DjangoModelFactory):
    """ Factory des avis """

    instruction = factory.SubFactory(InstructionFactory)

    # Meta
    class Meta:
        model = Avis
