from django_cron import CronJobBase, Schedule
from django.db.models import Q
from django.utils import timezone

from instructions.models import Avis

import pytz



class RelanceAvis(CronJobBase):
    RUN_EVERY_MINS = 60*24  # à lancer toutes lees jour
    """
    Code pour envoyer des relances d'avis automatiques.
    La fréquence du Cron est géré par Crontab. Elle devrait être de 24h.
    Le delai de relance d'un avis est mis dans les paramètres d'instance.
    """

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'instructions.relanceavis'    # a unique code

    def do(self):

        avis = Avis.objects.en_cours().filter(~Q(etat='rendu'))
        for avi in avis:
            instance = avi.get_instance()
            utc = pytz.UTC
            debutlimite = avi.get_date_limite() - timezone.timedelta(days=instance.avis_delai_relance)
            # Comme on ne peut pas comparer un date et un datetime, on le localize le premier et on lui ajout une heure
            debutlimite = utc.localize(timezone.datetime.combine(debutlimite, timezone.datetime.min.time()))
            finlimite = utc.localize(timezone.datetime.combine(avi.get_date_limite(), timezone.datetime.min.time()))

            if timezone.now() < finlimite and timezone.now() > debutlimite:
                avi.notifier_creation_avis(origine=avi.get_service(), agents=avi.get_agents())
                if avi.instruction.referent:
                    avi.log_resend(avi.instruction.referent, avi.get_service().__str__())
                print(str(avi))
        print("cron job completude")


