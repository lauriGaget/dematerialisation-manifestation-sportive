# coding: utf-8
from django.db import models
from datetime import date
from django.utils import timezone
from django.urls import reverse
from django_fsm import FSMField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_fsm import transition
from django.conf import settings

from administration.models.agentlocal import AgentLocal
from notifications.models.action import Action
from notifications.models.notification import Notification
from core.models import Instance
from administration.models import EDSR


class PreAvisQuerySet(models.QuerySet):
    """ Queryset de base des préavis """

    # Getter
    def en_cours(self):
        """ Renvoyer les préavis dont la date de manifestation n'est pas encore passée """
        return self.filter(avis__instruction__manif__date_fin__gte=timezone.now())

    def termine(self):
        """ Renvoyer les préavis dont la date de manifestation est  passée """
        return self.filter(avis__instruction__manif__date_fin__lt=timezone.now()).order_by('-avis__instruction__manif__date_debut')

    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(avis__instruction__manif__instance_id=request.user.get_instance().pk)
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(avis__instruction__manif__instance__in=instances)
        else:
            return self


class PreAvis(models.Model):
    """ Modèle de base des préavis """

    LIST_SERVICES = [('cgd', 'cgd'), ('services', 'services'), ('compagnies', 'compagnies'),
                     ('commissariats', 'commissariats'), ('edsr', 'edsr')]
    LIST_SERVICES_SUP = {'cgd': 'edsr', 'services': 'cg', 'compagnies': 'sdis', 'commissariats': 'ddsp', 'edsr': 'ggd'}

    # Champs
    etat = FSMField(default='demandé')  # Voir liste des états dans : documentation/Version-4_description.md
    service_concerne = models.CharField(max_length=20, choices=LIST_SERVICES)
    date_demande = models.DateField("Date de demande", default=date.today)
    date_reponse = models.DateField("Date de retour", blank=True, null=True)
    favorable = models.BooleanField("Avis favorable ?", default=False, help_text="Si coché, l'avis rendu est considéré favorable")
    prescriptions = models.TextField("prescriptions", blank=True)

    avis = models.ForeignKey("instructions.avis", verbose_name="avis", on_delete=models.CASCADE)
    agentlocal = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Dernier intervenant", on_delete=models.SET_NULL, null=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    destination_object = GenericForeignKey()

    objects = PreAvisQuerySet.as_manager()

    # Overrides
    def __str__(self):
        service = self.service_concerne.capitalize()
        if self.destination_object:
            return ' - '.join([service, str(self.destination_object)])
        else:
            return '{0} - *** PAS DE SERVICE RATTACHE ***'.format(service)

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('instructions:preavis_detail', kwargs={'pk': self.pk})

    def get_services_sup(self):
        """ Retrouver le service qui a demandé le préavis """
        config = self.avis.get_instance().get_workflow_ggd()
        # Si c'est un préavis CGD, dans le cas du WF_GGD_EDSR, c'est l'EDSR qui a demandé le préavis
        if config == Instance.WF_GGD_EDSR and self.service_concerne == 'cgd':
            return self.avis.get_instance().get_departement().edsr
        return self.avis.get_service()

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        # Si un service n'existe plus, le préavis n'est plus relié par "destination_object"
        if self.destination_object is None:
            return []
        if self.service_concerne == "cgd":
            #  ggdagentslocaux correspond au related_name des cgd
            return self.destination_object.ggdagentslocaux.all()
        elif self.service_concerne == "services":
            return self.destination_object.cgserviceagentslocaux.all()
        elif self.service_concerne == "compagnies":
            return self.destination_object.compagnieagentslocaux.all()
        elif self.service_concerne == "commissariats":
            return self.destination_object.commissariatagentslocaux.all()
        elif self.service_concerne == "edsr":
            return self.destination_object.edsragentslocaux.all()
        else:
            return []

    def get_agents_sup(self):
        """ Renvoyer les agents concernés par l'avis du préavis """
        if self.service_concerne == "commissariats":
            return self.get_services_sup().ddspagents.all()
        elif self.service_concerne == "compagnies":
            return self.get_services_sup().sdisagents.all()
        elif self.service_concerne == "cgd":
            if type(self.get_services_sup()) is EDSR:
                return self.get_services_sup().edsragents.all()
            else:
                return self.get_services_sup().ggdagents.all()
        elif self.service_concerne == "services":
            return self.get_services_sup().cgagents.all()
        elif self.service_concerne == "edsr":
            return self.get_services_sup().ggdagents.all()
        else:
            return []

    # Action
    @transition(field='etat', source=['demandé', 'notifié'], target='rendu')
    def rendrePreavis(self, origine):
        """ Rendre le préavis """
        non_agent = []
        if self.service_concerne in ['commissariats', 'services']:
            non_agent.append(self.get_services_sup())
        self.notifier_preavis_rendu(agents=self.get_agents_sup(), content_object=self.destination_object, non_agent_recipient=non_agent)
        # Enregistrer l'action préavis rendu
        Action.objects.log(origine, "pré-avis rendu", self.avis.get_manifestation())

    def log_suppression(self, origine):
        """
        Consigner dans les logs la suppresion du préavis
        :param origine:
        :return:
        """
        Action.objects.log(origine, "demandes de pré-avis supprimées", self.avis.get_manifestation())

    def log_resend(self, origine, recipient):
        """
        Consigner l'événement lorsque qu'une demande de préavis est relancée

        Les agents notifiés sont les instructeurs attachés à la préfecture de l'autorisation
        :param recipient: TODO: Documenter
        """
        Action.objects.log(origine, "demande d'avis relancée : {recipient}".format(recipient=recipient), self.avis.get_manifestation())

    # TODO : A traiter dans branche "messagerie"
    def log_ajout_pj(self, origine):
        """ Consigner dans les logs l'ajout d'une pièce jointe """
        Action.objects.log(origine, "Ajout de pièce jointe", self.avis.get_manifestation())

    @transition(field='etat', source=['demandé', 'notifié'], target='notifié')
    def notifier_brigades(self, brigades, origine):
        """ Notifier tous les agents des brigades concernées par le préavis """
        recipients = [agent for brigade in brigades for agent in brigade.brigadeagents.all()]
        # IMPORTANT texte utilisé pour des querysets
        Notification.objects.notify_and_mail(recipients, "Prenez connaissance des informations", self.destination_object, self.avis.get_manifestation())
        Action.objects.log(origine, "Brigades informées", self.avis.get_manifestation())

    def notifier_creation_preavis(self, agents, content_object):
        """ Notifier de la création du préavis """
        recipients = AgentLocal.users_from_agents(agents)
        Notification.objects.notify_and_mail(recipients, "pré-avis demandés", content_object, self.avis.get_manifestation())

    def notifier_suppression_preavis(self, agents, content_object):
        """
        Notifier de la suppresion du préavis
        :param agents:
        :param content_object:
        :return:
        """
        recipients = AgentLocal.users_from_agents(agents)
        Notification.objects.notify_and_mail(recipients, "pré-avis supprimés", content_object, self.avis.get_manifestation())

    def notifier_preavis_rendu(self, agents, content_object, non_agent_recipient=None):
        """ Notifier du rendu du préavis """
        recipients = AgentLocal.users_from_agents(agents) + non_agent_recipient
        Notification.objects.notify_and_mail(recipients, "pré-avis rendu", content_object, self.avis.get_manifestation())

    # TODO : A traiter dans branche "messagerie"
    def notifier_ajout_pj(self):
        """ Notifier de l'ajout d'une pièce jointe """
        non_agent = []
        if self.service_concerne in ['commissariats', 'services']:
            non_agent.append(self.get_services_sup())
        agents = self.get_agents_sup()
        recipients = AgentLocal.users_from_agents(agents) + [non_agent]
        Notification.objects.notify_and_mail(recipients, "Pièce jointe ajoutée", self.destination_object, self.avis.get_manifestation())

    # Meta
    class Meta:
        verbose_name = 'pré-avis'
        verbose_name_plural = 'pré-avis'
        default_related_name = "preavis"
        app_label = "instructions"
