# coding: utf-8
import pytz

from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone
from django_fsm import FSMField
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_fsm import transition
from django.conf import settings

from administration.models.agent import Agent
from configuration.directory import UploadPath
from notifications.models.action import Action
from notifications.models.notification import Notification
from core.models.instance import Instance


class AvisQuerySet(models.QuerySet):
    """ Queryset par défaut pour les avis """

    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(instruction__manif__instance_id=request.user.get_instance().pk)
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(instruction__manif__instance__in=instances)
        else:
            return self

    def en_cours(self):
        """ Renvoyer les avis dont la manifestation n'est pas terminée """
        return self.filter(instruction__manif__date_fin__gte=timezone.now())

    def termine(self):
        """ Renvoyer les avis pour lesquels l'événement prévu est terminé """
        return self.filter(instruction__manif__date_fin__lt=timezone.now()).order_by('-instruction__manif__date_debut')

    def rendu(self):
        """
        Renvoyer les avis rendus

        L'état acknowledged indique qu'un avis est rendu.
        """
        return self.filter(etat='rendu')

    def envoye(self):
        """ Renvoyer les avis pour lesquels les demandes de préavis ont été envoyées  """
        return self.filter(etat='distribué')

    def a_formatter(self):
        """
        Renvoyer les avis qui peuvent être formatés

        Avant d'être rendu, un agent autorisé peut formater un avis
        en ajoutant des informations sensibles pour l'agent en charge
        de valider l'avis ou de ne pas trancher positivement.
        """
        return self.filter(etat__in=['demandé', 'distribué'])

    def a_rendre(self):
        """
        Renvoyer les avis qui peuvent être rendus

        Les avis qui peuvent être (re)rendus sont des avis qui sont
        soit à l'état formaté (donc prêts à soumettre à l'agent probateur),
        soit à l'état rendu (dans ce cas, on peut à nouveau rendre l'avis.
        """
        return self.filter(etat__in=['formaté', 'rendu'])

    def a_envoye(self):
        """
        Renvoyer les avis qui peuvent être dispatchés

        On peut redispatcher un avis rendu, ou qui a déjà été dispatché (rappel aux agents)
        """
        return self.filter(etat__in=['rendu', 'distribué'])

    def pour_federation(self, federation):
        """ Renvoyer tous les avis pour la fédération passée """
        return self.filter(service_concerne='federation').filter(instruction__manif__activite__discipline__federations=federation)

    def sans_edsr(self):
        """ Renvoyer les avis GGD sans sélection d'EDSR """
        return self.filter(service_concerne='ggd').filter(object_id__isnull=True)

    def avec_edsr(self):
        """ Renvoyer les avis GGD sans sélection d'EDSR """
        return self.filter(service_concerne='ggd').filter(object_id__isnull=False)


class Avis(models.Model):
    """
    Avis

    """

    LIST_SERVICES = [('cg', 'cg'), ('ddsp', 'ddsp'), ('edsr', 'edsr'), ('federation', 'federation'),
                     ('ggd', 'ggd'), ('mairie', 'mairie'), ('sdis', 'sdis'), ('service', 'service')]

    # Champs
    etat = FSMField(default='demandé')  # Voir liste des états dans : documentation/Version-4_description.md
    service_concerne = models.CharField(max_length=10, choices=LIST_SERVICES)
    date_demande = models.DateField("Date de demande", default=timezone.now)
    date_reponse = models.DateField("Date de retour", blank=True, null=True)
    favorable = models.BooleanField("Avis favorable ?", default=False, help_text="Si coché, l'avis rendu est considéré favorable")
    prescriptions = models.TextField("prescriptions", blank=True)

    instruction = models.ForeignKey('instructions.instruction', verbose_name="Instruction", on_delete=models.CASCADE)
    agent = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name="Dernier intervenant", on_delete=models.SET_NULL, null=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    destination_object = GenericForeignKey()

    objects = AvisQuerySet.as_manager()

    # Overrides
    def __str__(self):
        manifestation = self.get_manifestation()
        service = self.service_concerne.upper()
        departement = manifestation.ville_depart.get_departement()
        if self.service_concerne == "federation":
            return 'Fédération : {0}'.format(str(manifestation.get_federation()).capitalize())
        if self.service_concerne in ("mairie", "service"):
            if self.destination_object and hasattr(self.destination_object, 'name'):
                return '{0} : {1}'.format(self.service_concerne.capitalize(), self.destination_object.name)
            else:
                return '{0} : *** PAS DE SERVICE RATTACHE ***'.format(self.service_concerne.capitalize())
        return service

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('instructions:avis_detail', kwargs={'pk': self.pk})

    def get_service(self):
        """ Renvoyer le service concerné """
        if self.service_concerne == "federation":
            return self.get_manifestation().get_federation()
        if self.service_concerne in ("mairie", "service"):
            return self.destination_object
        return getattr(self.get_manifestation().ville_depart.get_departement(), self.service_concerne)

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        # Si un service n'existe plus, l'avis n'est plus relié par "destination_object"
        if self.get_service() is None:
            return []
        if self.service_concerne == "cg": return self.get_service().get_cgagents()
        elif self.service_concerne == "ddsp": return self.get_service().get_ddspagents()
        elif self.service_concerne == "edsr": return self.get_service().get_edsr_agents()
        elif self.service_concerne == "sdis": return self.get_service().get_sdisagents()
        elif self.service_concerne == "service": return self.get_service().get_serviceagents()
        elif self.service_concerne == "mairie": return self.get_service().get_mairieagents()
        elif self.service_concerne == "federation": return self.get_service().federationagents.all()
        elif self.service_concerne == "ggd":
            edsr_agents = []
            if self.etat == 'transmis':
                edsr_agents = self.get_instance().get_departement().edsr.get_edsr_agents()
            ggd_agents = self.get_service().get_ggdagents()
            return list(ggd_agents) + list(edsr_agents)
        return []

    def get_date_limite(self):
        """ Renvoyer la date maximum de rendu de l'avis selon le type d'avis """
        instance = self.get_instance()
        delays = {'federation': timezone.timedelta(days=instance.avis_delai_federation)}
        default_delay = timezone.timedelta(days=instance.avis_delai_service)
        if self.date_demande.__class__.__name__=="date":
            date_demande = pytz.UTC.localize(timezone.datetime.combine(self.date_demande, timezone.datetime.min.time()))
        else:
            date_demande = self.date_demande
        for related_field in delays:
            if self.service_concerne == related_field:
                return date_demande + delays[related_field]
        return date_demande + default_delay

    def get_jours_restants_avant_limite(self):
        """ Renvoyer le nombre de jours restant avant la deadline d'instruction """
        now = pytz.UTC.localize(timezone.datetime.combine(timezone.now(), timezone.datetime.min.time()))
        return self.get_date_limite() - now

    def delai_expire(self):
        """ Renvoyer si le délai légal d'instruction a expiré """
        now = pytz.UTC.localize(timezone.datetime.combine(timezone.now(), timezone.datetime.min.time()))
        return now > self.get_date_limite()

    def get_tous_preavis(self):
        """ Renvoyer les préavis de l'avis """
        return self.preavis.all()

    def get_preavis_user(self, user):
        """ Renvoyer les préavis de l'avis """
        service = user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        if self.preavis.filter(content_type=ct_service, object_id=service.id).exists():
            return self.preavis.get(content_type=ct_service, object_id=service.id)
        return None

    def get_preavis_rendus(self):
        """ Renvoyer les préavis validés/rendus """
        return self.preavis.filter(etat='rendu')

    def tous_preavis_rendus(self):
        """ Renvoyer si tous les préavis en cours sont rendus """
        validated = self.get_preavis_rendus().count()
        return validated == self.preavis.all().count()

    def get_nb_preavis(self):
        """ Renvoyer le nombre de préavis générés """
        return self.preavis.all().count()

    def get_nb_preavis_a_rendre(self):
        """ Renvoyer le nombre de préavis non rendus/validés """
        return self.get_nb_preavis() - self.get_preavis_rendus().count()

    def get_manifestation(self):
        """ Renvoyer l'événement sportif pour cet avis """
        return self.instruction.manif

    def get_instance(self):
        """ Renvoyer l'instance du département de la manifestation de l'instruction """
        return self.get_manifestation().get_instance()

    # Action
    @transition(field='etat', source='*', target='rendu')
    def rendreAvis(self, origine):
        """ Rendre l'avis """
        # Notifier les instructeurs et les agents
        if self.service_concerne == 'edsr' or self.service_concerne == 'ggd':
            ggd = self.get_manifestation().ville_depart.get_departement().ggd
            self.notify_ack(content_object=ggd)
        else:
            self.notify_ack(content_object=self.get_service())
        # Consigner l'événement lorsque l'avis est rendu
        Action.objects.log(origine, "avis rendu", self.get_manifestation())

    @transition(field='etat', source=['demandé', 'transmis'], target='distribué')
    def log_distribution(self, origine):
        """ Distribuer les pré-avis """
        # Consigner l'événement lorsque les demandes de préavis sont envoyées
        Action.objects.log(origine, "demandes de pré-avis envoyées", self.get_manifestation())

    @transition(field='etat', source='distribué', target='formaté')
    def formaterAvis(self, origine):
        """ Notifier l'action Mettre en forme l'avis """
        service = self.get_service()
        agents = []
        if self.service_concerne == 'cg':
            agents += service.get_cgsuperieurs()
        if self.service_concerne == 'edsr' or self.service_concerne == 'ggd':
            departement = self.get_manifestation().ville_depart.get_departement()
            agents += departement.ggd.get_ggdagents()
            service = getattr(departement, 'edsr')
        for preavis in self.get_tous_preavis():
            agents += preavis.get_agents()
        # Consigner et envoyer un mail lorsque l'avis est formaté
        recipients = Agent.users_from_agents(agents)
        Notification.objects.notify_and_mail(recipients, "avis mis en forme", service, self.get_manifestation())
        # Consigner l'événement lorsque l'avis est formaté/mis en forme
        Action.objects.log(origine, "avis mis en forme", self.get_manifestation())

    def log_resend(self, origine, recipient):
        """
        Consigner l'événement lorsque qu'une demande d'avis est relancée

        Les agents notifiés sont les instructeurs attachés à la préfecture de l'autorisation
        :param origine: l'utilisateur qui demande
        :param recipient: le service visé
        """

        Action.objects.log(origine, "demande d'avis relancée : {recipient}".format(recipient=recipient), self.get_manifestation())

    def log_delete(self, origine, recipient):
        """
        Consigner l'évenement lors d'une suppression d'avis.
        :param origine: l'utilisateur qui demande
        :param recipient: le service visé
        :return:
        """
        Action.objects.log(origine, "Demande d'avis supprimée : {recipient}".format(recipient=recipient), self.get_manifestation())

    # TODO : A traiter dans branche "messagerie"
    def log_ajout_pj(self, origine):
        """ Consigner dans les logs l'ajout d'une pièce jointe """
        Action.objects.log(origine, "Ajout de pièce jointe", self.get_manifestation())

    @transition(field='etat', source='demandé', target='transmis')
    def notifier_passage_avis(self, origine, agents):
        """
        Consigner l'événement et envoyer un mail aux agents lorsque l'avis est passé

        :param origine: un utilisateur, en principe, un ggd
        :param agents: agents qui reçoivent le mail et sont notifiés
        """
        Action.objects.log(origine, "avis transmis", self.get_manifestation())
        try:
            service = origine.get_service()
            recipients = Agent.users_from_agents(agents)
            Notification.objects.notify_and_mail(recipients, "avis requis", service, self.get_manifestation())
        except ObjectDoesNotExist:
            raise

    def notifier_creation_avis(self, origine, agents, non_agent_recipient=None):
        """
        Consigner l'événement et envoyer un mail aux agents lorsque l'avis est créé

        :param origine: service initiateur
        :param agents: agents qui reçoivent le mail et sont notifiés
        :param non_agent_recipient: un objet avec un champ email, ex. Préfecture etc. qui sera notifié par mail
        """
        try:
            recipients = Agent.users_from_agents(agents) + [non_agent_recipient]
            Notification.objects.notify_and_mail(recipients, "avis requis", origine, self.get_manifestation())
        except ObjectDoesNotExist:
            raise

    def notifier_suppression_avis(self, origine, agents, non_agent_recipient=None):
        """
        Envoyer un mail lors de la suppression d'un avis
        :param origine:
        :param agents:
        :param non_agent_recipient:
        :return:
        """
        try:
            recipients= Agent.users_from_agents(agents) + [non_agent_recipient]
            Notification.objects.notify_and_mail(recipients, "Suppression de la demande d'avis", origine, self.get_manifestation())
        except ObjectDoesNotExist:
            raise

    def notify_ack(self, content_object):
        """
        Consigner l'événement et envoyer un mail aux instructeurs lorsqu'un avis est rendu

        Envoie également un mail à la préfecture (institutional)
        :param content_object: objet d'intérêt pour la notification
        """
        recipients = self.instruction.manif.liste_instructeurs()
        if self.service_concerne == 'sdis':
            # Notifier les agents CODIS
            recipients += self.get_manifestation().ville_depart.get_departement().codis.get_codisagents()
            # Notifier tous les agents de toutes les compagnies concernées
            agents_compagnies = [agent for preavis in self.preavis.all() for agent in preavis.destination_object.get_compagnieagentslocaux()]
            Notification.objects.notify_and_mail(agents_compagnies, "avis rendu - CIS à notifier", content_object, self.get_manifestation())
        if self.service_concerne in ['edsr', 'cg']:
            # Notifier le n quand le n+1 a rendu l'avis
            recipients += self.get_agents()
        if self.service_concerne == 'ggd' and self.get_instance().get_workflow_ggd() == Instance.WF_GGD_EDSR:
            # Notifier le n-1 quand le ggd a rendu l'avis
            recipients += self.get_instance().departement.edsr.get_edsr_agents()
        if not self.service_concerne == 'sdis':
            for preavis in self.get_tous_preavis():
                recipients += preavis.get_agents()

        Notification.objects.notify_and_mail(recipients, "avis rendu", content_object, self.get_manifestation())

    # TODO : A traiter dans branche "messagerie"
    def notifier_ajout_pj(self, user):
        """ Notifier de l'ajout d'une pièce jointe """
        recipients = self.instruction.manif.liste_instructeurs()
        Notification.objects.notify_and_mail(recipients, "Pièce jointe ajoutée", user.get_service(), self.get_manifestation())

    def exist_document_officiel(self):
        from instructions.models.instruction import DocumentOfficiel
        avis = self
        if avis.document_attache and avis.etat == 'rendu':
            return DocumentOfficiel.objects.filter(fichier=avis.document_attache).exists()
        else:
            return False

    # Meta
    class Meta:
        verbose_name = "Avis"
        verbose_name_plural = "Avis"
        ordering = ["-date_reponse"]
        app_label = 'instructions'
        default_related_name = 'avis'


class PieceJointeAvis(models.Model):
    """
    Docuements complémentaires des avis
    """
    fichier = models.FileField(upload_to=UploadPath('avis'), verbose_name='Pièce jointe avis', max_length=512)
    avis = models.ForeignKey("Avis", verbose_name='avis', on_delete=models.CASCADE, blank=True, null=True)
    preavis = models.ForeignKey("Preavis", verbose_name='preavis', on_delete=models.CASCADE, blank=True, null=True)
    # auto_now_add n'est pas adapté quand des entrées existent dans la DB
    # Pour lancer makemigrations
    date_depot = models.DateTimeField("date de dépot", null=True, editable=False, blank=True)
    #date_depot = models.DateTimeField("date de dépot", auto_now_add=True)

    def __str__(self):
        return self.fichier.name.split('/')[-1] + " déposé pour l'avis : " + self.avis.__str__()

    def get_instance(self):
        """ Renvoyer l'instance du département de la manifestation de l'instruction """
        if self.avis:
            return self.avis.get_manifestation().get_instance()
        return self.preavis.avis.get_manifestation().get_instance()

    def exist_document_officiel(self):
        from instructions.models.instruction import DocumentOfficiel
        return DocumentOfficiel.objects.filter(fichier=self.fichier).exists()

    def exist_piece_jointe(self):
        if self.preavis:
            if self.avis:
                return True
        return False

    class Meta:
        verbose_name = 'Document complémentaire d\'un avis'
        verbose_name_plural = "Documents complémentaires d\'un avis"
        app_label = 'instructions'
        default_related_name = 'docs'
