from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from django.core import mail
import re, os

from core.models import Instance
from instructions.models.preavis import PreAvis

from core.factories import UserFactory
from evenements.factories import DcnmFactory
from instructions.models import Instruction
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory,\
    CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory,\
    SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory, BrigadeAgentFactory
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from sports.factories import ActiviteFactory, FederationFactory


class Circuit_EDSRTests(TestCase):
    """
    Test du circuit d'instance EDSR pour une Dnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ WF_EDSR (Clt) =============')
        # Création des objets sur le 42
        cls.dep = dep = DepartementFactory.create(name='42',
                                                  instance__name="instance de test",
                                                  instance__workflow_ggd=Instance.WF_EDSR,
                                                  instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance, email='orga@test.fr')
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance, email='inst@test.fr')
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance, email='fede@test.fr')
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=fede)
        cls.agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance, email='ggd@test.fr')
        GGDAgentFactory.create(user=cls.agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance, email='edsr@test.fr')
        EDSRAgentFactory.create(user=cls.agent_edsr, edsr=dep.edsr)
        cls.cgd1 = CGDFactory.create(commune=cls.commune)
        cls.agent_cgd1 = UserFactory.create(username='agent_cgd1', password=make(123), default_instance=dep.instance, email='cgd1@test.fr')
        CGDAgentFactory.create(user=cls.agent_cgd1, cgd=cls.cgd1)
        cls.cgd2 = CGDFactory.create(commune=cls.autrecommune)
        cls.agent_cgd2 = UserFactory.create(username='agent_cgd2', password=make(123), default_instance=dep.instance, email='cgd2@test.fr')
        CGDAgentFactory.create(user=cls.agent_cgd2, cgd=cls.cgd2)
        cls.brigade1 = BrigadeFactory.create(kind='bta', cgd=cls.cgd1, commune=cls.commune)
        cls.agent_brg1 = UserFactory.create(username='agent_brg1', password=make(123), default_instance=dep.instance, email='brg1@test.fr')
        BrigadeAgentFactory.create(user=cls.agent_brg1, brigade=cls.brigade1)
        cls.brigade2 = BrigadeFactory.create(kind='bta', cgd=cls.cgd2, commune=cls.commune)
        cls.agent_brg2 = UserFactory.create(username='agent_brg2', password=make(123), default_instance=dep.instance, email='brg2@test.fr')
        BrigadeAgentFactory.create(user=cls.agent_brg2, brigade=cls.brigade2)
        cls.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance, email='ddsp@test.fr')
        DDSPAgentFactory.create(user=cls.agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        cls.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance, email='comm@test.fr')
        CommissariatAgentFactory.create(user=cls.agent_commiss, commissariat=cls.commiss)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance, email='mair@test.fr')
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)
        cls.agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance, email='cg@test.fr')
        CGAgentFactory.create(user=cls.agent_cg, cg=dep.cg)
        cls.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        cls.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance, email='cgserv@test.fr')
        CGServiceAgentFactory.create(user=cls.agent_cgserv, cg_service=cls.cgserv)
        cls.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance, email='cgsup@test.fr')
        CGSuperieurFactory.create(user=cls.agent_cgsup, cg=dep.cg)
        cls.agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance, email='sdis@test.fr')
        SDISAgentFactory.create(user=cls.agent_sdis, sdis=dep.sdis)
        cls.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        cls.agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance, email='group@test.fr')
        GroupementAgentFactory.create(user=cls.agent_group, compagnie=cls.group)
        cls.agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance, email='codis@test.fr')
        CODISAgentFactory.create(user=cls.agent_codis, codis=dep.codis)
        cls.cis = CISFactory.create(name='CIS_test', compagnie=cls.group, commune=cls.commune)
        cls.agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance, email='cis@test.fr')
        CISAgentFactory.create(user=cls.agent_cis, cis=cls.cis)

        # Création de l'événement
        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=cls.structure, activite=activ,
                                               nom='Manifestation_Test', villes_traversees=(cls.autrecommune,))
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'parcours de 10Km'
        cls.manifestation.nb_participants = 1
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 6
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire', 'recepisse_declaration'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire', 'recepisse_declaration'):
            os.remove(file+".txt")
        super(Circuit_EDSRTests, cls).tearDownClass()

    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        def presence_avis(username, state):
            """
            Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            :return: retour: la réponse http
            """
            # Connexion avec l'utilisateur
            self.assertTrue(self.client.login(username=username, password='123'))
            # Appel de la page
            retour = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000')
            # print(reponse.content.decode('utf-8'))
            # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
            # f.write(str(retour.content.decode('utf-8')).replace('\\n',""))
            # f.close()
            # Test du contenu
            if state == 'none':
                recherche = re.search('Aucun avis|Aucune demande', retour.content.decode('utf-8'))
                self.assertTrue(recherche, msg='test pas d\'avis / préavis')
            else:
                self.assertContains(retour, self.instruction)
            # Test de la couleur affichée
            if state == 'danger':
                self.assertContains(retour, 'table-danger', count=1)
                self.assertNotContains(retour, 'table-warning')
                self.assertNotContains(retour, 'table-success')
                self.assertNotContains(retour, 'table-info')
            if state == 'warning':
                self.assertNotContains(retour, 'table-danger')
                self.assertContains(retour, 'table-warning', count=1)
                self.assertNotContains(retour, 'table-success')
                self.assertNotContains(retour, 'table-info')
            if state == 'success':
                self.assertNotContains(retour, 'table-danger')
                self.assertNotContains(retour, 'table-warning')
                self.assertContains(retour, 'table-success', count=1)
                self.assertNotContains(retour, 'table-info')
            if state == 'info':
                self.assertNotContains(retour, 'table-danger')
                self.assertNotContains(retour, 'table-warning')
                self.assertNotContains(retour, 'table-success')
                self.assertContains(retour, 'table-info', count=1)
            return retour

        def vue_detail(page):
            """
            Appel de la vue de détail de la manifestation
            :param page: réponse précédente
            :return: reponse suivante
            """
            detail = re.search('data-href=\'(?P<url>(/[^"]+))', page.content.decode('utf-8'))
            if hasattr(detail, 'group'):
                page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
            # self.assertContains(retour, 'Détail de la manifestation<', count=1)
            # print(page.content.decode('utf-8'))
            self.assertContains(page, 'Manifestation_Test')
            return page

        def aucune_action(page):
            """
            Test auncune action affichée dans la zone action de la dashboard
            :param page: réponse précédente
            """
            action = re.search('Voici les actions.+<a href="(?P<url>(/[^"]+))".+</ul>', str(page.content))
            test_action = hasattr(action, 'group')
            self.assertFalse(test_action, msg='test aucune action')

        def affichage_avis():
            """
            Affichage des avis émis pour l'événement avec leur status
            """
            for avis in self.instruction.get_tous_avis():
                if avis.etat != 'rendu':
                    print(avis, end=" ; ")
                    print(avis.etat)

        print('**** test 1 creation manif ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(declar, 'group'):
            self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.ville_depart.get_departement().ggd, end=" ; ")
        print(self.manifestation.ville_depart.get_departement().edsr, end=" ; ")
        print(self.manifestation.ville_depart.get_departement().ddsp)
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first().name)
        affichage_avis()
        self.assertEqual(str(self.instruction), str(self.manifestation))
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(mail.outbox[0].subject, 'avis requis pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_fede.email])
        self.assertEqual(mail.outbox[1].subject, 'demande d\'instruction reçue pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[1].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[2].subject, 'demande d\'instruction reçue pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[2].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[3].subject, 'Accusé de réception : Manifestation_Test')
        self.assertEqual(mail.outbox[3].to, [self.organisateur.email])
        del mail.outbox

        print('**** test 2 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        presence_avis('agent_ggd', 'none')
        # EDSR
        presence_avis('agent_edsr', 'none')
        # Mairie
        presence_avis('agent_mairie', 'none')
        # CG
        presence_avis('agent_cg', 'none')
        # CGSuperieur
        presence_avis('agent_cgsup', 'none')
        # SDIS
        presence_avis('agent_sdis', 'none')

        print('**** test 3 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en warning
        reponse = presence_avis('instructeur', 'warning')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        edit_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(edit_form, 'group'):
            reponse = self.client.post(edit_form.group('url'),
                                       {'edsr_concerne': True,
                                        'ddsp_concerne': True,
                                        'sdis_concerne': True,
                                        'cg_concerne': True,
                                        'villes_concernees': [self.commune.pk]}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        affichage_avis()
        # Vérifier le passage en info et le nombre d'avis manquants
        reponse = presence_avis('instructeur', 'info')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 8)
        mailist = [self.agent_edsr.email, self.agent_sdis.email, self.agent_ddsp.email, self.dep.ddsp.email,
                   self.agent_cg.email, self.dep.cg.email, self.agent_mairie.email, self.structure.organisateur.user.email]
        self.assertEqual(mail.outbox[0].subject, 'début de l\'instruction pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[1].subject, 'avis requis pour la manifestation "Manifestation_Test"')
        for email in mail.outbox:
            self.assertIn(email.to[0], mailist)
        del mail.outbox

        print('**** test 4 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_fede', 'warning')
        self.assertContains(reponse, 'Délai : 28 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ackno = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ackno, 'group'):
            reponse = self.client.post(ackno.group('url'), {'favorable': True}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en success
        presence_avis('agent_fede', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'info')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 5 vérification avis; 0 pour GGD, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        presence_avis('agent_ggd', 'none')
        # CGD
        presence_avis('agent_cgd1', 'none')
        # BRG
        presence_avis('agent_brg1', 'none')

        print('**** test 6 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_edsr', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'cgd_concerne': [self.cgd1.pk, self.cgd2.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.filter(service_concerne='cgd')
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        affichage_avis()
        # Vérification du passage en info
        reponse = presence_avis('agent_edsr', 'info')
        self.assertContains(reponse, '2 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertIn(mail.outbox[0].to[0], [self.agent_cgd1.email, self.agent_cgd2.email])
        self.assertIn(mail.outbox[1].to[0], [self.agent_cgd1.email, self.agent_cgd2.email])
        del mail.outbox

        print('**** test 7 preavis cgd1 ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_cgd1', 'warning')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les brigades', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les brigades', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'brigades_concernees': [self.brigade1.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation', count=1)
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Prenez connaissance des informations pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_brg1.email])
        del mail.outbox
        # Vérification de la présence de la manif chez la brigade1 et pas chez la brigade2
        presence_avis('agent_brg1', 'info')
        presence_avis('agent_brg2', 'none')
        print('\t >>> Rendre préavis')
        # Vérification du passage en info
        reponse = presence_avis('agent_cgd1', 'info')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.filter(service_concerne='cgd')
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        affichage_avis()
        # Vérification du passage en success
        presence_avis('agent_cgd1', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_edsr.email])
        del mail.outbox

        print('**** test 7 preavis cgd2 ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_cgd2', 'warning')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les brigades', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les brigades', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'brigades_concernees': [self.brigade2.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation', count=1)
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Prenez connaissance des informations pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_brg2.email])
        del mail.outbox
        # Vérification de la présence de la manif chez la brigade2
        presence_avis('agent_brg2', 'info')
        print('\t >>> Rendre préavis')
        # Vérification du passage en info
        reponse = presence_avis('agent_cgd2', 'info')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.filter(service_concerne='cgd')
        for pr in preavis:
            print(pr, end=" ; ")
            print(pr.etat)
        affichage_avis()
        # Vérification du passage en success
        presence_avis('agent_cgd2', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_edsr.email])
        del mail.outbox

        print('**** test 8 avis edsr - formattage ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en info
        reponse = presence_avis('agent_edsr', 'info')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Formater l'avis de l'événement avec l'url fournie et tester la redirection
        form_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(form_form, 'group'):
            reponse = self.client.post(form_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        affichage_avis()
        # vérification de la présence de l'événement en info
        reponse = presence_avis('agent_edsr', 'info')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(mail.outbox[0].subject, 'avis mis en forme pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_ggd.email])
        self.assertEqual(mail.outbox[1].to, [self.agent_cgd1.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_cgd2.email])
        del mail.outbox

        print('**** test 9 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en info
        reponse = presence_avis('agent_ggd', 'info')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # vérification de la présence de l'événement en success
        presence_avis('agent_ggd', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'info')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 5)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_edsr.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_cgd1.email])
        self.assertEqual(mail.outbox[4].to, [self.agent_cgd2.email])
        del mail.outbox

        print('**** test 10 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        presence_avis('agent_commiss', 'none')

        print('**** test 11 avis ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_ddsp', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'commissariats_concernes': self.commiss.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='commissariats')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérifier le passage en info
        reponse = presence_avis('agent_ddsp', 'info')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_commiss.email])
        del mail.outbox

        print('**** test 12 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_commiss', 'warning')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='commissariats')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en success
        presence_avis('agent_commiss', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_ddsp.email])
        self.assertEqual(mail.outbox[1].to, [self.dep.ddsp.email])
        del mail.outbox

        print('**** test 13 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en info
        reponse = presence_avis('agent_ddsp', 'info')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en success
        presence_avis('agent_ddsp', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'info')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 3)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_commiss.email])
        del mail.outbox

        print('**** test 14 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_mairie', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Valider l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en success
        presence_avis('agent_mairie', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'info')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 15 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        presence_avis('agent_cgserv', 'none')

        print('**** test 16 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_cg', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'services_concernes': self.cgserv.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='services')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérifier le passage en info
        reponse = presence_avis('agent_cg', 'info')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cgserv.email])
        del mail.outbox

        print('**** test 17 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_cgserv', 'warning')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='services')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en success
        presence_avis('agent_cgserv', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cg.email])
        self.assertEqual(mail.outbox[1].to, [self.dep.cg.email])
        del mail.outbox

        print('**** test 18 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        presence_avis('agent_cgsup', 'none')

        print('**** test 19 avis cg - formattage ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en info
        reponse = presence_avis('agent_cg', 'info')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Mettre en forme l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        affichage_avis()
        # vérification de la présence de l'événement en info
        reponse = presence_avis('agent_cg', 'info')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'avis mis en forme pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cgsup.email])
        self.assertEqual(mail.outbox[1].to, [self.agent_cgserv.email])
        del mail.outbox

        print('**** test 20 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en info
        reponse = presence_avis('agent_cgsup', 'info')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en success
        presence_avis('agent_cgsup', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'info')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_cg.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_cgserv.email])
        del mail.outbox

        print('**** test 21 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        presence_avis('agent_cis', 'none')
        # CODIS
        presence_avis('agent_codis', 'none')
        # SDIS groupement
        presence_avis('agent_group', 'none')

        print('**** test 22 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_sdis', 'warning')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'compagnies_concernees': self.group.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='compagnies')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en info
        reponse = presence_avis('agent_sdis', 'info')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis demandés pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_group.email])
        del mail.outbox

        print('**** test 23 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en warning
        reponse = presence_avis('agent_group', 'warning')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        preavis = PreAvis.objects.get(service_concerne='compagnies')
        print(preavis, end=" ; ")
        print(preavis.etat)
        affichage_avis()
        # Vérification du passage en warning
        presence_avis('agent_group', 'success')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'pré-avis rendu pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_sdis.email])
        del mail.outbox

        print('**** test 24 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en info
        reponse = presence_avis('agent_sdis', 'info')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        self.avis_nb -= 1
        affichage_avis()
        # Vérifier le passage en success
        presence_avis('agent_sdis', 'success')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        reponse = presence_avis('instructeur', 'info')
        self.assertNotContains(reponse, 'avis manquants')
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 4)
        self.assertEqual(mail.outbox[0].subject, 'avis rendu - CIS à notifier pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[1].subject, 'avis rendu pour la manifestation "Manifestation_Test"')
        mailist = [self.instructeur.email, self.prefecture.email, self.agent_codis.email, self.agent_group.email]
        for email in mail.outbox:
            self.assertIn(email.to[0], mailist)
        del mail.outbox

        print('**** test 25 avis codis ****')
        # Vérification de la présence de l'événement
        reponse = presence_avis('agent_codis', 'success')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Tester aucune action disponible
        aucune_action(reponse)

        print('**** test 26 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en success
        reponse = presence_avis('agent_group', 'success')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les CIS', count=1)
        # Informer les CIS de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les CIS', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'cis_concernes': self.cis.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Tester aucune action disponible
        aucune_action(reponse)
        # Vérification du passage en warning
        presence_avis('agent_group', 'success')
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Prenez connaissance des informations pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_cis.email])
        del mail.outbox

        print('**** test 27 avis cis ****')
        # Vérification de la présence de l'événement
        reponse = presence_avis('agent_cis', 'success')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Tester aucune action disponible
        aucune_action(reponse)

        print('**** test 28 instructeur - autorisation ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en warning
        reponse = presence_avis('instructeur', 'info')
        self.assertNotContains(reponse, 'avis manquants')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Publier un document officiel', count=1)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        publish_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Publier un document officiel', reponse.content.decode('utf-8'))
        if hasattr(publish_form, 'group'):
            with open('/tmp/recepisse_declaration.txt') as file1:
                reponse = self.client.post(publish_form.group('url'),
                                           {'nature': '3', 'fichier': file1},
                                           follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info et le nombre d'avis manquants
        self.assertContains(reponse, 'table-success', count=1)
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 21)
        # for email in mail.outbox:
        #     print(email.to)
        mailist = [self.agent_edsr.email, self.agent_ggd.email, self.agent_cgd1.email, self.agent_cgd2.email,
                   self.agent_brg1.email, self.agent_brg2.email,
                   self.agent_ddsp.email, self.dep.ddsp.email, self.agent_commiss.email, self.agent_fede.email,
                   self.agent_cg.email, self.dep.cg.email, self.agent_cgserv.email, self.agent_cgsup.email,
                   self.agent_sdis.email, self.agent_codis.email, self.agent_group.email, self.agent_cis.email,
                   self.organisateur.email, self.agent_mairie.email, self.prefecture.email]
        self.assertEqual(mail.outbox[0].subject, 'récépissé de déclaration publié pour la manifestation "Manifestation_Test"')
        for email in mail.outbox:
            self.assertIn(email.to[0], mailist)
