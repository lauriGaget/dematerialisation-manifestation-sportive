# coding: utf-8
from django.utils import timezone
from django.urls import reverse
from django.test import TestCase

from core.models.instance import Instance
from sports.factories import FederationFactory
from ..factories import *


class AvisTests(TestCase):
    """ Tests de la base des avis """

    def setUp(self):
        """ Configuration """
        self.avis = AvisInstructionFactory.create()

    def test_dateLimite(self):
        """ Tester la date limite pour l'avis """
        expected_deadline = timezone.now() + Instance.get_avis_delay()
        self.assertEqual(self.avis.get_date_limite() - timezone.timedelta(microseconds=self.avis.get_date_limite().microsecond),
                         expected_deadline - timezone.timedelta(microseconds=expected_deadline.microsecond))
        self.assertEqual(self.avis.delai_expire(), False)
        self.avis.service_concerne = 'federation'
        expected_deadline = timezone.now() + Instance.get_federation_avis_delay()
        self.assertEqual(
            self.avis.get_date_limite() - timezone.timedelta(microseconds=self.avis.get_date_limite().microsecond),
            expected_deadline - timezone.timedelta(microseconds=expected_deadline.microsecond))
        self.assertEqual(self.avis.delai_expire(), False)

    def test_methods(self):
        """ Tester que les méthodes ne renvoient pas d'erreur d'emblée """
        try:
            # Test pour le commit 82969cc
            manifestation = self.avis.instruction.manif
            Avis.objects.pour_federation(manifestation.get_federation())
        except:
            self.fail("La méthode a provoqué une erreur")

    def test_dateLimite_depassee(self):
        expirable_date = timezone.now() - Instance.get_avis_delay(extra_days=2)
        self.avis.date_demande = expirable_date
        self.assertEqual(self.avis.delai_expire(), True)

    def test_preavis(self):
        """ Tester les méthodes concernant les préavis """
        self.assertEqual(self.avis.get_nb_preavis(), 0)  # Aucun préavis par défaut
        self.assertEqual(self.avis.get_nb_preavis_a_rendre(), 0)  # Donc aucun préavis en attente
        self.assertTrue(self.avis.tous_preavis_rendus())  # Et tous les préavis sont rendus

    def test_access_url(self):
        """ Tester l'URL d'accès """
        self.assertEqual(self.avis.get_absolute_url(), reverse('instructions:avis_detail', kwargs={'pk': self.avis.pk}))
