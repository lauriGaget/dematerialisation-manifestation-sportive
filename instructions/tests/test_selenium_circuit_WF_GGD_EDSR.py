from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.firefox.options import Options
from core.util.application import in_container
from selenium.common.exceptions import NoSuchElementException
from django.contrib.auth.hashers import make_password as make
from allauth.account.models import EmailAddress
from django.test import tag
import re, time, os
from core.models import Instance

from core.factories import UserFactory
from evenements.factories import DcnmFactory
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory,\
    CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory,\
    SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory, BrigadeAgentFactory
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from sports.factories import ActiviteFactory, FederationFactory


class InstructionCircuit_GGD_EDSRTests(StaticLiveServerTestCase):
    """
    Test du circuit d'instance GGD_EDSR avec sélénium pour une Dnm
        workflow_GGD : Avis GGD
        instruction par departement
        openrunner false par défaut
        limité à l'avis GGD pour le temps de test (pas d'avis mairie, sdis, cg et ddsp)
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 78
            Création des utilisateurs
            Création de l'événement
        """
        print()
        print('========== WF_GGD_EDSR (Sel) ===========')
        docker = in_container()
        if (docker == True):
            options = Options()
            options.headless = True
            cls.selenium = WebDriver(options=options)

        else:
            cls.selenium = WebDriver()

        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(800, 800)

        dep = DepartementFactory.create(name='78',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_GGD_EDSR,
                                        instance__instruction_mode=Instance.IM_DEPARTEMENT)
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=instructeur, prefecture=prefecture)
        agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_fede, email='agent_fede@example.com', primary=True, verified=True)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=agent_fede, federation=fede)
        agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_ggd, email='agent_ggd@example.com', primary=True, verified=True)
        GGDAgentFactory.create(user=agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_edsr, email='agent_edsr@example.com', primary=True, verified=True)
        EDSRAgentFactory.create(user=agent_edsr, edsr=dep.edsr)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgd, email='agent_cgd@example.com', primary=True, verified=True)
        CGDAgentFactory.create(user=agent_cgd, cgd=cls.cgd)
        cls.brigade = BrigadeFactory.create(kind='bta', cgd=cls.cgd, commune=cls.commune)
        agent_brg = UserFactory.create(username='agent_brg', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_brg, email='agent_brg@example.com', primary=True, verified=True)
        BrigadeAgentFactory.create(user=agent_brg, brigade=cls.brigade)

        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=structure, nom='Manifestation_Test',
                                               villes_traversees=(cls.autrecommune,), activite=activ)
        cls.manifestation.nb_participants = 1
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'un gros parcours'
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 2
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()
        super(InstructionCircuit_GGD_EDSRTests, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie'):
            os.remove(file+".txt")
        super(InstructionCircuit_GGD_EDSRTests, cls).tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_Circuit_GGD_EDSR(self):
        """
        Test des différentes étapes du circuit GGD_EDSR pour une autorisationNM
        """

        def connexion(username):
            """ Connexion de l'utilisateur 'username' """
            self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
            pseudo_input = self.selenium.find_element_by_name("login")
            pseudo_input.send_keys(username)
            time.sleep(self.DELAY)
            password_input = self.selenium.find_element_by_name("password")
            password_input.send_keys('123')
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
            time.sleep(self.DELAY * 4)
            self.assertIn('Connexion avec '+username+' réussie', self.selenium.page_source)

        def presence_avis(username, state):
            """
            Test de la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            """
            declar = self.selenium.find_element_by_tag_name('tbody')
            if state == 'none':
                if username == 'agent_commiss' or username == 'agent_cgd' or username == 'agent_cgserv' or username == 'agent_group':
                    self.assertIn('Aucune demande', self.selenium.page_source)
                else:
                    self.assertIn('Aucun avis', self.selenium.page_source)
            else:
                self.assertIn('Manifestation_Test', self.selenium.page_source)
            # Test de la couleur affichée
            if state == 'danger':
                try:
                    declar.find_element_by_class_name('table-danger')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en rouge")
            if state == 'warning':
                try:
                    declar.find_element_by_class_name('table-warning')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en jaune")
            if state == 'info':
                try:
                    declar.find_element_by_class_name('table-info')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en bleu")
            if state == 'success':
                try:
                    declar.find_element_by_class_name('table-success')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en vert")

        def deconnexion():
            """ Deconnexion de l'utilisateur """
            self.selenium.find_element_by_class_name('navbar-toggler').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_class_name('deconnecter').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        def vue_detail():
            """
            Appel de la vue de détail de la manifestation
            """
            self.selenium.find_element_by_xpath('//td/span[text()="Manifestation_Test"]').click()
            time.sleep(self.DELAY)
            self.assertIn('Détail de la manifestation', self.selenium.page_source)

        def aucune_action():
            """
            Test auncune action affichée dans la zone action de la dashboard
            """
            action = re.search('Voici les actions.+<a href="(?P<url>(/[^"]+))".+</ul>', self.selenium.page_source)
            test_action = hasattr(action, 'group')
            self.assertFalse(test_action, msg='test aucune action')

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'none')
        deconnexion()
        # EDSR
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'none')
        deconnexion()

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en danger
        connexion('instructeur')
        presence_avis('instructeur', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis GGD requis')]/..").click()
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY * 6)
        # Vérifier le passage en warning et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 900)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en warning
        connexion('agent_fede')
        presence_avis('agent_fede', 'warning')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_fede', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 4 vérification avis; 0 pour EDSR, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'none')
        deconnexion()
        # CGD
        connexion('agent_cgd')
        presence_avis('agent_cgd', 'none')
        deconnexion()
        # Brigade
        connexion('agent_brg')
        presence_avis('agent_brg', 'none')
        deconnexion()

        print('**** test 5 avis ggd - transmission ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en warning
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Choisir l\'EDSR', self.selenium.page_source)
        # Passer l'événement à l'EDSR
        self.selenium.find_element_by_partial_link_text('Choisir l\'EDSR').click()
        time.sleep(self.DELAY)
        self.chosen_select('id_edsr_concerne_chosen', 'EDSR 78')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # vérification de la présence de l'événement en success
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_ggd', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        deconnexion()

        print('**** test 6 vérification avis; 0 pour CGD ****')
        # Vérification des avis des divers agents
        # CGD
        connexion('agent_cgd')
        presence_avis('agent_cgd', 'none')
        deconnexion()

        print('**** test 7 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en warning
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_cgd_concerne_chosen', self.cgd.__str__())
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 8 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en warning
        connexion('agent_cgd')
        presence_avis('agent_cgd', 'warning')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les brigades', self.selenium.page_source)
        # informer les brigades de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les brigades').click()
        time.sleep(self.DELAY)
        self.assertIn('Choisissez les brigades concernées', self.selenium.page_source)
        self.chosen_select('id_brigades_concernees_chosen', 'BTA - Plaisir')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgd', 'info')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        print('\t >>> Rendre préavis')
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgd', 'success')
        deconnexion()
        print('\t >>> Vérifier brigade')
        connexion('agent_brg')
        presence_avis('agent_brg', 'info')
        deconnexion()

        print('**** test 9 avis edsr - formattage ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en info
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Formater l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        # self.selenium.find_element_by_id('id_favorable').click()
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # vérification de la présence de l'événement en warning
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        deconnexion()

        print('**** test 10 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en info
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en success
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_ggd', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        deconnexion()
