import re, time, os

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make
from django.test import tag
from django.core import mail

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.firefox.options import Options
from core.util.application import in_container
from selenium.common.exceptions import NoSuchElementException
from allauth.account.models import EmailAddress

from core.models import Instance
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from organisateurs.factories import OrganisateurFactory
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory, FederationFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import (InstructeurFactory, FederationAgentFactory, ServiceAgentFactory,
                                      MairieAgentFactory, ServiceFactory)


class InstructionAjoutPjTests(StaticLiveServerTestCase):
    """
    Test d'ajout de PJ avec sélénium pour une Dcnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services simples avec transfert des PJ en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        print()
        print('============ Pièces Jointes (Sel) =============')
        docker = in_container()
        if (docker == True):
            options = Options()
            options.headless = True
            cls.selenium = WebDriver(options=options)

        else:
            cls.selenium = WebDriver()


        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(800, 800)

        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance, email='orga@test.fr')
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance, email='inst@test.fr')
        EmailAddress.objects.create(user=cls.instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance, email='fede@test.fr')
        EmailAddress.objects.create(user=cls.agent_fede, email='agent_fede@example.com', primary=True, verified=True)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=fede)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance, email='mairie@test.fr')
        EmailAddress.objects.create(user=cls.agent_mairie, email='agent_mairie@example.com', primary=True, verified=True)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)
        sncf = ServiceFactory.create(departements=(dep,))
        cls.agent_sncf = UserFactory.create(username='agent_sncf', password=make(123), default_instance=dep.instance, email='sncf@test.fr')
        EmailAddress.objects.create(user=cls.agent_sncf, email='agent_sncf@example.com', primary=True, verified=True)
        ServiceAgentFactory.create(user=cls.agent_sncf, service=sncf)

        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=structure, nom='Manifestation_Test',
                                               villes_traversees=(cls.autrecommune,), activite=activ)
        cls.manifestation.nb_participants = 1
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'un gros parcours'
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 3
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie', 'pj'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test des pièces jointes")
            mon_fichier.close()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie', 'pj'):
            os.remove(file+".txt")
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """

        def connexion(username):
            """ Connexion de l'utilisateur 'username' """
            self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
            pseudo_input = self.selenium.find_element_by_name("login")
            pseudo_input.send_keys(username)
            time.sleep(self.DELAY)
            password_input = self.selenium.find_element_by_name("password")
            password_input.send_keys('123')
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
            time.sleep(self.DELAY * 4)
            self.assertIn('Connexion avec '+username+' réussie', self.selenium.page_source)

        def presence_avis(username, state):
            """
            Test de la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            """
            declar = self.selenium.find_element_by_tag_name('tbody')
            if state == 'none':
                if username == 'agent_commiss' or username == 'agent_cgd' or username == 'agent_cgserv' or username == 'agent_group':
                    self.assertIn('Aucune demande', self.selenium.page_source)
                else:
                    self.assertIn('Aucun avis', self.selenium.page_source)
            else:
                self.assertIn('Manifestation_Test', self.selenium.page_source)
            # Test de la couleur affichée
            if state == 'danger':
                try:
                    declar.find_element_by_class_name('table-danger')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en rouge")
            if state == 'warning':
                try:
                    declar.find_element_by_class_name('table-warning')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en jaune")
            if state == 'info':
                try:
                    declar.find_element_by_class_name('table-info')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en bleu")
            if state == 'success':
                try:
                    declar.find_element_by_class_name('table-success')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en vert")
            if state == 'annul':
                try:
                    declar.find_element_by_class_name('table-secondary')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en gris")

        def deconnexion():
            """ Deconnexion de l'utilisateur """
            self.selenium.find_element_by_class_name('navbar-toggler').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_class_name('deconnecter').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        def vue_detail():
            """
            Appel de la vue de détail de la manifestation
            """
            self.selenium.find_element_by_xpath('//td/span[text()="Manifestation_Test"]').click()
            time.sleep(self.DELAY)
            self.assertIn('Détail de la manifestation', self.selenium.page_source)

        def aucune_action():
            """
            Test auncune action affichée dans la zone action de la dashboard
            """
            action = re.search('Voici les actions.+<a href="(?P<url>(/[^"]+))".+</ul>', self.selenium.page_source)
            test_action = hasattr(action, 'group')
            self.assertFalse(test_action, msg='test aucune action')

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en danger
        connexion('instructeur')
        presence_avis('instructeur', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.chosen_select('id_villes_concernees_chosen', 'Bard')
        self.chosen_select('id_services_concernes_chosen', 'sncf')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY * 6)

        # Vérifier le passage en warning et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 3 agent fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en warning
        connexion('agent_fede')
        presence_avis('agent_fede', 'warning')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        del mail.outbox
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element_by_partial_link_text('.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_fede', 'success')
        deconnexion()
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'Pièce jointe ajoutée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 4 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 200)")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element_by_partial_link_text('Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath("//select[@name='nature']/option[text()='Arrêté de circulation']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (1)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Arrêté de circulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        deconnexion()
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 5)
        self.assertEqual(mail.outbox[0].subject, 'arrêté de circulation publié pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.organisateur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_fede.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_mairie.email])
        self.assertEqual(mail.outbox[4].to, [self.agent_sncf.email])
        del mail.outbox

        print('**** test 5  agent mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en warning
        connexion('agent_mairie')
        presence_avis('agent_mairie', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        del mail.outbox
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element_by_partial_link_text('.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_mairie', 'success')
        deconnexion()
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'Pièce jointe ajoutée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 6 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 200)")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']/following::*")
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element_by_partial_link_text('Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath("//select[@name='nature']/option[text()='Arrêté de circulation']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Mairie')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (2)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (2)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Arrêté de circulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        deconnexion()
        del mail.outbox

        print('**** test 7  agent sncf ****')
        # Instruction de l'avis par le service SNCF, vérification de la présence de l'événement en warning
        connexion('agent_sncf')
        presence_avis('agent_sncf', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        del mail.outbox
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence de la pièce jointe
        pj = self.selenium.find_element_by_partial_link_text('.txt')
        nom_pj = pj.get_attribute('text')
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_sncf', 'success')
        deconnexion()
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'Pièce jointe ajoutée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox

        print('**** test 8 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        vue_detail()
        # Avis de la fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 200)")
        # Déplier l'avis et transférer en document officiel
        avis.click()
        self.selenium.execute_script("window.scroll(0, 800)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']/following::*")
        # Tester le bouton de transfert
        self.assertIn('Transférer en document officiel', carte.text)
        self.selenium.find_element_by_partial_link_text('Transférer en document officiel').click()
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_xpath('//select[@name="nature"]/option[text()="Déclaration d\'annulation"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 200)")
        time.sleep(self.DELAY)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Service')][@data-toggle='collapse']/following::*")
        self.assertNotIn('Transférer en document officiel', carte.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (3)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (3)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Déclaration d\'annulation', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        presence_avis('instructeur', 'annul')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        deconnexion()
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 5)
        self.assertEqual(mail.outbox[0].subject, 'déclaration d\'annulation publiée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.organisateur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_fede.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_mairie.email])
        self.assertEqual(mail.outbox[4].to, [self.agent_sncf.email])
