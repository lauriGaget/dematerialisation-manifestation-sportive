from django.test import TestCase
from django.utils.timezone import timedelta, now
from django.contrib.auth.hashers import make_password as make
import re

from core.models import Instance
from evenements.factories import DnmFactory
from evenements.models import Manif
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory
from core.factories import UserFactory
from administration.factories import InstructeurFactory, MairieAgentFactory
from sports.factories import ActiviteFactory


class ConflitPossible(TestCase):
    """
    Test de la détection de conflit entre deux manifestations
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= DNM conflit de parcours (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.commune2 = CommuneFactory(name='Boen', arrondissement=arrondissement)
        structure = StructureFactory(commune=cls.commune1)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune1)

        # Création de l'événement 1
        cls.manifestation1 = DnmFactory.create(ville_depart=cls.commune1, structure=structure,
                                               date_debut=now() + timedelta(days=100),
                                              nom='Manifestation_Test_1', activite=activ)

        # Création de l'événement 2
        cls.manifestation2 = DnmFactory.create(ville_depart=cls.commune2, structure=structure,
                                               villes_traversees=[cls.commune1,],
                                               date_debut=now() + timedelta(days=100),
                                               nom='Manifestation_Test_2', activite=activ)

    def test_Conflit_1(self):
        print('-- test conflit de parcours sans cartographie --')
        print()

        print('**** test 1 creation instruction ****')
        manif = Manif.objects.get(nom='Manifestation_Test_1')
        self.instruction1 = InstructionFactory.create(manif=manif)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation1.ville_depart, end=" ; ")
        print(self.manifestation1.ville_depart.get_departement(), end=" ; ")
        print(list(self.manifestation1.villes_traversees.all()), end=" ; ")
        print(self.manifestation1.date_debut)
        self.assertEqual(str(self.instruction1.manif), str(self.manifestation1))

        manif = Manif.objects.get(nom='Manifestation_Test_2')
        self.instruction2 = InstructionFactory.create(manif=manif)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation2.ville_depart, end=" ; ")
        print(self.manifestation2.ville_depart.get_departement(), end=" ; ")
        print(list(self.manifestation2.villes_traversees.all()), end=" ; ")
        print(self.manifestation2.date_debut)
        self.assertEqual(str(self.instruction2.manif), str(self.manifestation2))


        print('**** test 2 présence instruction ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, 'table-danger')
        self.assertContains(reponse, 'table-warning', count=1)
        self.assertNotContains(reponse, 'table-success')
        self.assertNotContains(reponse, 'table-info')

        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, 'table-danger')
        self.assertContains(reponse, 'table-warning', count=1)
        self.assertNotContains(reponse, 'table-success')
        self.assertNotContains(reponse, 'table-info')


        print('**** test 3 conflit instruction ****')
        # Appel de la page détail
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        page = ''
        if hasattr(detail, 'group'):
            page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')

        # print(page.content.decode('utf-8'))
        self.assertContains(page, 'Manifestation_Test_2')
        self.assertContains(page, 'Possibles conflits de parcours')
        self.assertContains(page, 'qui n\'ont pas de cartographie de parcours')
        conflit = re.search(r'<li><strong>(?P<nom>(.*))</strong>', page.content.decode('utf-8'))
        if not hasattr(conflit, 'group'):
            self.assertIsNotNone(conflit)
        self.assertEqual(conflit.group('nom'), 'Manifestation_Test_1')
        lien = re.search('href="(?P<url>(/[^"]+)).*\\n.*accès au dossier', page.content.decode('utf-8'))
        if not hasattr(lien, 'group'):
            self.assertIsNotNone(lien)
        self.assertEqual(lien.group('url'), '/instructions/' + str(self.instruction1.id) + '/')

        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Appel de la page détail
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test_1')
        self.assertContains(page, 'Possibles conflits de parcours')
        conflit = re.search('<li><strong>(?P<nom>(.*))</strong>', page.content.decode('utf-8'))
        if not hasattr(conflit, 'group'):
            self.assertIsNotNone(conflit)
        self.assertEqual(conflit.group('nom'), 'Manifestation_Test_2')
        lien = re.search('href="(?P<url>(/[^"]+)).*\\n.*accès au dossier', page.content.decode('utf-8'))
        if not hasattr(lien, 'group'):
            self.assertIsNotNone(lien)
        self.assertEqual(lien.group('url'), '/instructions/' + str(self.instruction2.id) + '/')


    def test_Conflit_2(self):
        print()
        print('-- test conflit de parcours avec cartographie --')
        print()

        print('**** test 1 creation instruction ****')
        self.manifestation1.parcours_openrunner = "45,56"
        self.manifestation1.save()
        self.instruction1 = InstructionFactory.create(manif=self.manifestation1)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation1.ville_depart, end=" ; ")
        print(self.manifestation1.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation1.parcours_openrunner, end=" ; ")
        print(list(self.manifestation1.villes_traversees.all()), end=" ; ")
        print(self.manifestation1.date_debut)
        self.assertEqual(str(self.instruction1.manif), str(self.manifestation1))

        self.manifestation2.parcours_openrunner = "23,34"
        self.manifestation2.save()
        self.instruction2 = InstructionFactory.create(manif=self.manifestation2)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation2.ville_depart, end=" ; ")
        print(self.manifestation2.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation2.parcours_openrunner, end=" ; ")
        print(list(self.manifestation2.villes_traversees.all()), end=" ; ")
        print(self.manifestation2.date_debut)
        self.assertEqual(str(self.instruction2.manif), str(self.manifestation2))


        print('**** test 2 présence instruction ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, 'table-danger')
        self.assertContains(reponse, 'table-warning', count=1)
        self.assertNotContains(reponse, 'table-success')
        self.assertNotContains(reponse, 'table-info')

        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, 'table-danger')
        self.assertContains(reponse, 'table-warning', count=1)
        self.assertNotContains(reponse, 'table-success')
        self.assertNotContains(reponse, 'table-info')


        print('**** test 3 conflit instruction ****')
        # Appel de la page détail
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        page = ''
        if hasattr(detail, 'group'):
            page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')

        self.assertContains(page, 'Manifestation_Test_2')
        self.assertContains(page, 'Possibles conflits de parcours')
        self.assertNotContains(page, 'qui n\'ont pas de cartographie de parcours')
        conflit = re.search(r'<li><strong>(?P<nom>(.*))</strong>', page.content.decode('utf-8'))
        if not hasattr(conflit, 'group'):
            self.assertIsNotNone(conflit)
        self.assertEqual(conflit.group('nom'), 'Manifestation_Test_1')
        lien = re.search('href="(?P<url>(/[^"]+)).*\\n.*accès au dossier', page.content.decode('utf-8'))
        if not hasattr(lien, 'group'):
            self.assertIsNotNone(lien)
        self.assertEqual(lien.group('url'), '/instructions/' + str(self.instruction1.id) + '/')

        # Appel de la page
        url = '/instructions/tableaudebord/?filtre_specifique=instructionmairie'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        # Appel de la page détail
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(page, 'Manifestation_Test_1')
        self.assertContains(page, 'Possibles conflits de parcours')
        conflit = re.search(r'<li><strong>(?P<nom>(.*))</strong>', page.content.decode('utf-8'))
        if not hasattr(conflit, 'group'):
            self.assertIsNotNone(conflit)
        self.assertEqual(conflit.group('nom'), 'Manifestation_Test_2')
        lien = re.search('href="(?P<url>(/[^"]+)).*\\n.*accès au dossier', page.content.decode('utf-8'))
        if not hasattr(lien, 'group'):
            self.assertIsNotNone(lien)
        self.assertEqual(lien.group('url'), '/instructions/' + str(self.instruction2.id) + '/')
