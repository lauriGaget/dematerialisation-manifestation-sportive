from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.firefox.options import Options
from core.util.application import in_container
from selenium.common.exceptions import NoSuchElementException
from django.contrib.auth.hashers import make_password as make
from allauth.account.models import EmailAddress
from django.test import tag
import re, time, os

from core.models import Instance

from core.factories import UserFactory
from evenements.factories import DcnmFactory
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory,\
    CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory,\
    SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory, BrigadeAgentFactory
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from sports.factories import ActiviteFactory, FederationFactory


class InstructionCircuit_EDSRTests(StaticLiveServerTestCase):
    """
    Test du circuit d'instance EDSR avec sélénium pour une Dnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        print()
        print('============ WF_EDSR (Sel) =============')
        docker = in_container()
        if (docker == True):
            options = Options()
            options.headless = True
            cls.selenium = WebDriver(options=options)

        else:
            cls.selenium = WebDriver()

        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(800, 800)

        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=instructeur, prefecture=prefecture)
        agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_fede, email='agent_fede@example.com', primary=True, verified=True)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=agent_fede, federation=fede)
        agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_ggd, email='agent_ggd@example.com', primary=True, verified=True)
        GGDAgentFactory.create(user=agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_edsr, email='agent_edsr@example.com', primary=True, verified=True)
        EDSRAgentFactory.create(user=agent_edsr, edsr=dep.edsr)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgd, email='agent_cgd@example.com', primary=True, verified=True)
        CGDAgentFactory.create(user=agent_cgd, cgd=cls.cgd)
        cls.brigade = BrigadeFactory.create(kind='bta', cgd=cls.cgd, commune=cls.commune)
        agent_brg = UserFactory.create(username='agent_brg', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_brg, email='agent_brg@example.com', primary=True, verified=True)
        BrigadeAgentFactory.create(user=agent_brg, brigade=cls.brigade)
        agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_ddsp, email='agent_ddsp@example.com', primary=True, verified=True)
        DDSPAgentFactory.create(user=agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_commiss, email='agent_commiss@example.com', primary=True, verified=True)
        CommissariatAgentFactory.create(user=agent_commiss, commissariat=cls.commiss)
        agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_mairie, email='agent_mairie@example.com', primary=True, verified=True)
        MairieAgentFactory.create(user=agent_mairie, commune=cls.commune)
        agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cg, email='agent_cg@example.com', primary=True, verified=True)
        CGAgentFactory.create(user=agent_cg, cg=dep.cg)
        cls.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgserv, email='agent_cgserv@example.com', primary=True, verified=True)
        CGServiceAgentFactory.create(user=agent_cgserv, cg_service=cls.cgserv)
        agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgsup, email='agent_cgsup@example.com', primary=True, verified=True)
        CGSuperieurFactory.create(user=agent_cgsup, cg=dep.cg)
        agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_sdis, email='agent_sdis@example.com', primary=True, verified=True)
        SDISAgentFactory.create(user=agent_sdis, sdis=dep.sdis)
        cls.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_group, email='agent_group@example.com', primary=True, verified=True)
        GroupementAgentFactory.create(user=agent_group, compagnie=cls.group)
        agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_codis, email='agent_codis@example.com', primary=True, verified=True)
        CODISAgentFactory.create(user=agent_codis, codis=dep.codis)
        cls.cis = CISFactory.create(name='CIS_test', compagnie=cls.group, commune=cls.commune)
        agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cis, email='agent_cis@example.com', primary=True, verified=True)
        CISAgentFactory.create(user=agent_cis, cis=cls.cis)

        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=structure, nom='Manifestation_Test',
                                               villes_traversees=(cls.autrecommune,), activite=activ)
        cls.manifestation.nb_participants = 1
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'un gros parcours'
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 6
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()
        super(InstructionCircuit_EDSRTests, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie'):
            os.remove(file+".txt")
        super(InstructionCircuit_EDSRTests, cls).tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_Circuit_EDSR(self):
        """
        Test des différentes étapes du circuit EDSR pour une autorisationNM
        """

        def connexion(username):
            """ Connexion de l'utilisateur 'username' """
            self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
            pseudo_input = self.selenium.find_element_by_name("login")
            pseudo_input.send_keys(username)
            time.sleep(self.DELAY)
            password_input = self.selenium.find_element_by_name("password")
            password_input.send_keys('123')
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
            time.sleep(self.DELAY * 4)
            self.assertIn('Connexion avec '+username+' réussie', self.selenium.page_source)

        def presence_avis(username, state):
            """
            Test de la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            """
            declar = self.selenium.find_element_by_tag_name('tbody')
            if state == 'none':
                if username == 'agent_commiss' or username == 'agent_cgd' or username == 'agent_cgserv' or username == 'agent_group':
                    self.assertIn('Aucune demande', self.selenium.page_source)
                else:
                    self.assertIn('Aucun avis', self.selenium.page_source)
            else:
                self.assertIn('Manifestation_Test', self.selenium.page_source)
            # Test de la couleur affichée
            if state == 'danger':
                try:
                    declar.find_element_by_class_name('table-danger')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en rouge")
            if state == 'warning':
                try:
                    declar.find_element_by_class_name('table-warning')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en jaune")
            if state == 'info':
                try:
                    declar.find_element_by_class_name('table-info')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en bleu")
            if state == 'success':
                try:
                    declar.find_element_by_class_name('table-success')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en vert")

        def deconnexion():
            """ Deconnexion de l'utilisateur """
            self.selenium.find_element_by_class_name('navbar-toggler').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_class_name('deconnecter').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        def vue_detail():
            """
            Appel de la vue de détail de la manifestation
            """
            self.selenium.find_element_by_xpath('//td/span[text()="Manifestation_Test"]').click()
            time.sleep(self.DELAY)
            self.assertIn('Détail de la manifestation', self.selenium.page_source)

        def aucune_action():
            """
            Test auncune action affichée dans la zone action de la dashboard
            """
            action = re.search('Voici les actions.+<a href="(?P<url>(/[^"]+))".+</ul>', self.selenium.page_source)
            test_action = hasattr(action, 'group')
            self.assertFalse(test_action, msg='test aucune action')

        print('**** test 0 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 1 vérification avis; 0 pour tous sauf la fédération ****')
        # Vérification des avis des divers agents
        # GGD
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'none')
        deconnexion()
        # EDSR
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'none')
        deconnexion()
        # Mairie
        connexion('agent_mairie')
        presence_avis('agent_mairie', 'none')
        deconnexion()
        # CG
        connexion('agent_cg')
        presence_avis('agent_cg', 'none')
        deconnexion()
        # CGSuperieur
        connexion('agent_cgsup')
        presence_avis('agent_cgsup', 'none')
        deconnexion()
        # SDIS
        connexion('agent_sdis')
        presence_avis('agent_sdis', 'none')
        deconnexion()

        print('**** test 2 instructeur - distribution ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en danger
        connexion('instructeur')
        presence_avis('instructeur', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis DDSP requis')]/..").click()
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis EDSR requis')]/..").click()
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis SDIS requis')]/..").click()
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis Conseil Dép')]/..").click()
        # time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.chosen_select('id_villes_concernees_chosen', 'Bard')
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY * 6)

        # Vérifier le passage en warning et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 3 avis fede ****')
        # Instruction de l'avis par la fédération, vérification de la présence de l'événement en warning
        connexion('agent_fede')
        presence_avis('agent_fede', 'warning')
        self.assertIn('Délai : 28 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_fede', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 4 vérification avis; 0 pour GGD, CGD et Brigade ****')
        # Vérification des avis des divers agents
        # EDSR
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'none')
        deconnexion()
        # CGD
        connexion('agent_cgd')
        presence_avis('agent_cgd', 'none')
        deconnexion()
        # Brigade
        connexion('agent_brg')
        presence_avis('agent_brg', 'none')
        deconnexion()

        print('**** test 5 avis edsr - distribution ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en warning
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_cgd_concerne_chosen', self.cgd.__str__())
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 6 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en warning
        connexion('agent_cgd')
        presence_avis('agent_cgd', 'warning')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les brigades', self.selenium.page_source)
        # informer les brigades de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les brigades').click()
        time.sleep(self.DELAY)
        self.assertIn('Choisissez les brigades concernées', self.selenium.page_source)
        self.chosen_select('id_brigades_concernees_chosen', 'BTA - Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgd', 'info')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        print('\t >>> Rendre préavis')
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgd', 'success')
        deconnexion()
        print('\t >>> Vérifier brigade')
        connexion('agent_brg')
        presence_avis('agent_brg', 'info')
        deconnexion()

        print('**** test 7 avis edsr - formattage ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en info
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Formater l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        # self.selenium.find_element_by_id('id_favorable').click()
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # vérification de la présence de l'événement en warning
        self.selenium.execute_script("window.scroll(0, 600)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        deconnexion()

        print('**** test 8 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en info
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_ggd', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 9 vérification avis; 0 pour Commissariat ****')
        # Vérification des avis des divers agents
        # CGD
        connexion('agent_commiss')
        presence_avis('agent_commiss', 'none')
        deconnexion()

        print('**** test 10 avis ddsp - distribution ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en warning
        connexion('agent_ddsp')
        presence_avis('agent_ddsp', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_commissariats_concernes_chosen', 'Commissariat Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérifier le passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_ddsp', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 11 preavis commissariat ****')
        # Instruction du préavis par le commissairiat, vérification de la présence de l'événement en warning
        connexion('agent_commiss')
        presence_avis('agent_commiss', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_commiss', 'success')
        deconnexion()

        print('**** test 12 avis ddsp ****')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en info
        connexion('agent_ddsp')
        presence_avis('agent_ddsp', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_ddsp', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 13 avis mairie ****')
        # Instruction de l'avis par la mairie, vérification de la présence de l'événement en warning
        connexion('agent_mairie')
        presence_avis('agent_mairie', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Valider l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_mairie', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 14 vérification avis; 0 pour CGService ****')
        # Vérification des avis des divers agents
        # CGService
        connexion('agent_cgserv')
        presence_avis('agent_cgserv', 'none')
        deconnexion()

        print('**** test 15 avis cg - distribution ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en warning
        connexion('agent_cg')
        presence_avis('agent_cg', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_services_concernes_chosen', 'STD - STD_test')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérifier le passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cg', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 16 preavis cgservice ****')
        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en warning
        connexion('agent_cgserv')
        presence_avis('agent_cgserv', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgserv', 'success')
        deconnexion()

        print('**** test 17 vérification avis; 0 pour CGSupérieur ****')
        # Vérification des avis des divers agents
        # CGSuperieur
        connexion('agent_cgsup')
        presence_avis('agent_cgsup', 'none')
        deconnexion()

        print('**** test 18 avis cg - formattage ****')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en info
        connexion('agent_cg')
        presence_avis('agent_cg', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Mettre en forme l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        # self.selenium.find_element_by_id('id_favorable').click()
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # vérification de la présence de l'événement en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cg', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        deconnexion()

        print('**** test 19 agent cgsup ****')
        # Instruction de l'avis par le cgsup, vérification de la présence de l'événement en info
        connexion('agent_cgsup')
        presence_avis('agent_cgsup', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événementn
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgsup', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 20 vérification avis; 0 pour Groupement, codis et cis ****')
        # Vérification des avis des divers agents
        # CIS
        connexion('agent_cis')
        presence_avis('agent_cis', 'none')
        deconnexion()
        # CODIS
        connexion('agent_codis')
        presence_avis('agent_codis', 'none')
        deconnexion()
        # SDIS groupement
        connexion('agent_group')
        presence_avis('agent_group', 'none')
        deconnexion()

        print('**** test 21 avis sdis - distribution ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en warning
        connexion('agent_sdis')
        presence_avis('agent_sdis', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_compagnies_concernees_chosen', 'Compagnie/groupement 98')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_sdis', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 22 preavis groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en warning
        connexion('agent_group')
        presence_avis('agent_group', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_group', 'success')
        deconnexion()

        print('**** test 23 avis sdis ****')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en info
        connexion('agent_sdis')
        presence_avis('agent_sdis', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_sdis', 'success')
        deconnexion()
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 24 avis codis ****')
        # Vérification de la présence de l'événement
        connexion('agent_codis')
        presence_avis('agent_codis', 'success')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Tester aucune action disponible
        aucune_action()
        deconnexion()

        print('**** test 25 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en success
        connexion('agent_group')
        presence_avis('agent_group', 'success')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les CIS', self.selenium.page_source)
        # Informer les CIS de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les CIS').click()
        time.sleep(self.DELAY)
        self.assertIn('Choisissez les CIS concernés', self.selenium.page_source)
        self.chosen_select('id_cis_concernes_chosen', 'CIS Bard CIS_test')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_group', 'success')
        deconnexion()

        print('**** test 26 avis cis ****')
        # Vérification de la présence de l'événement
        connexion('agent_cis')
        presence_avis('agent_cis', 'success')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Tester aucune action disponible
        aucune_action()
        deconnexion()
