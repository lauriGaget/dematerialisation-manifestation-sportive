import re, time, os, platform

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.hashers import make_password as make
from django.test import tag
from django.core import mail

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.firefox.options import Options
from core.util.application import in_container
from selenium.common.exceptions import NoSuchElementException
from allauth.account.models import EmailAddress

from core.models import Instance
from core.factories import UserFactory
from evenements.factories import DcnmFactory
from organisateurs.factories import OrganisateurFactory
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory, FederationFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import (InstructeurFactory, FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory,
                                      CGDAgentFactory, BrigadeFactory, CGDFactory)


class InstructionAjoutPjTests(StaticLiveServerTestCase):
    """
    Test d'ajout de PJ du circuit d'instance EDSR avec sélénium pour une Dcnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services complexes avec transfert des PJ du préavis dans l'avis et
    transfert des PJ d'avis en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        print()
        print('============ Pièces Jointes (Sel) =============')
        docker = in_container()
        if (docker == True):
            options = Options()
            options.headless = True
            cls.selenium = WebDriver(options=options)

        else:
            cls.selenium = WebDriver()

        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(800, 800)

        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=cls.instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_fede, email='agent_fede@example.com', primary=True, verified=True)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=agent_fede, federation=fede)
        cls.agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=cls.agent_ggd, email='agent_ggd@example.com', primary=True, verified=True)
        GGDAgentFactory.create(user=cls.agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=cls.agent_edsr, email='agent_edsr@example.com', primary=True, verified=True)
        EDSRAgentFactory.create(user=cls.agent_edsr, edsr=dep.edsr)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        cls.agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=cls.agent_cgd, email='agent_cgd@example.com', primary=True, verified=True)
        CGDAgentFactory.create(user=cls.agent_cgd, cgd=cls.cgd)
        cls.brigade = BrigadeFactory.create(kind='bta', cgd=cls.cgd, commune=cls.commune)

        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=structure, nom='Manifestation_Test',
                                               villes_traversees=(cls.autrecommune,), activite=activ)
        cls.manifestation.nb_participants = 1
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'un gros parcours'
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 1
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie', 'pj_1', 'pj_2', 'pj_3', 'pj_4'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test des pièces jointes")
            mon_fichier.close()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """
        cls.selenium.quit()
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie', 'pj_1', 'pj_2', 'pj_3', 'pj_4'):
            os.remove(file+".txt")
        super().tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """

        def connexion(username):
            """ Connexion de l'utilisateur 'username' """
            self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
            pseudo_input = self.selenium.find_element_by_name("login")
            pseudo_input.send_keys(username)
            time.sleep(self.DELAY)
            password_input = self.selenium.find_element_by_name("password")
            password_input.send_keys('123')
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
            time.sleep(self.DELAY * 4)
            self.assertIn('Connexion avec '+username+' réussie', self.selenium.page_source)

        def presence_avis(username, state):
            """
            Test de la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            """
            declar = self.selenium.find_element_by_tag_name('tbody')
            if state == 'none':
                if username == 'agent_commiss' or username == 'agent_cgd' or username == 'agent_cgserv' or username == 'agent_group':
                    self.assertIn('Aucune demande', self.selenium.page_source)
                else:
                    self.assertIn('Aucun avis', self.selenium.page_source)
            else:
                self.assertIn('Manifestation_Test', self.selenium.page_source)
            # Test de la couleur affichée
            if state == 'danger':
                try:
                    declar.find_element_by_class_name('table-danger')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en rouge")
            if state == 'warning':
                try:
                    declar.find_element_by_class_name('table-warning')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en jaune")
            if state == 'info':
                try:
                    declar.find_element_by_class_name('table-info')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en bleu")
            if state == 'success':
                try:
                    declar.find_element_by_class_name('table-success')
                except NoSuchElementException:
                    self.assertTrue(False, msg="pas de demande d\'autorisation de manifestation en vert")

        def deconnexion():
            """ Deconnexion de l'utilisateur """
            self.selenium.find_element_by_class_name('navbar-toggler').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_class_name('deconnecter').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        def vue_detail():
            """
            Appel de la vue de détail de la manifestation
            """
            self.selenium.find_element_by_xpath('//td/span[text()="Manifestation_Test"]').click()
            time.sleep(self.DELAY)
            self.assertIn('Détail de la manifestation', self.selenium.page_source)

        def aucune_action():
            """
            Test auncune action affichée dans la zone action de la dashboard
            """
            action = re.search('Voici les actions.+<a href="(?P<url>(/[^"]+))".+</ul>', self.selenium.page_source)
            test_action = hasattr(action, 'group')
            self.assertFalse(test_action, msg='test aucune action')

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en danger
        connexion('instructeur')
        presence_avis('instructeur', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis EDSR requis')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY * 6)

        # Annuler la demande d'avis fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        carte.find_element_by_partial_link_text('Confirmer').click()

        # Vérifier le passage en warning et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('instructeur', 'info')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 3 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en warning
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'warning')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_cgd_concerne_chosen', self.cgd.__str__())
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Test d ela partie pièces jointes
        self.assertIn('Aucune', self.selenium.page_source)
        self.assertIn('Ajouter une pièce jointe', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        deconnexion()

        print('**** test 4 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en warning
        connexion('agent_cgd')
        presence_avis('agent_cgd', 'warning')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les brigades', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # informer les brigades de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les brigades').click()
        time.sleep(self.DELAY)
        self.assertIn('Choisissez les brigades concernées', self.selenium.page_source)
        self.chosen_select('id_brigades_concernees_chosen', 'BTA - Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgd', 'info')
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        del mail.outbox
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter deux pièces jointes
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_2.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence des pièces jointes
        pan = self.selenium.find_element_by_xpath("//div[@id='subagreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        # Tester aucune action disponible
        aucune_action()
        # Vérification du passage en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_cgd', 'success')
        deconnexion()
        # Vérifier les emails envoyés
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'Pièce jointe ajoutée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.agent_edsr.email])
        del mail.outbox

        print('**** test 5 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en info
        connexion('agent_edsr')
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Vérifier affichage
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('Aucune', pan.text)  # Pièces jointes
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        icones = preavis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            preavis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien2.text)
        # Transférer la PJ sur l'avis
        lien1.click()
        # Tester aucune notifications
        self.assertFalse(hasattr(mail, 'outbox'))
        # Tester la présence de la PJ sur l'avis
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertNotIn('Aucune', pan.text)
        self.assertIn('pj_1', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester l'absence du bouton de transfert pour PJ_1
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien2.text)
        self.selenium.execute_script("window.scroll(0, 400)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_3.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_3', pan.text)

        # Formater l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        # self.selenium.find_element_by_id('id_favorable').click()
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        aucune_action()
        del mail.outbox
        # vérification de la présence de l'événement en warning
        self.selenium.execute_script("window.scroll(0, 600)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_edsr', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        deconnexion()

        print('**** test 6 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en info
        connexion('agent_ggd')
        presence_avis('agent_ggd', 'info')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Vérifier affichage des PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_3', pan.text)
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        icones = preavis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            preavis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        self.assertIn('pj_1', carte.text)
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien2.text)
        # Transférer la PJ sur l'avis
        lien2.click()
        # Tester aucune notifications
        self.assertFalse(hasattr(mail, 'outbox'))
        # Tester la présence des PJs sur l'avis
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.assertIn('pj_3', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester l'absence du bouton de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        item1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item1.text)
        item2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/parent::*")
        self.assertNotIn('Transférer le document sur', item2.text)
        self.selenium.execute_script("window.scroll(0, 400)")
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        del mail.outbox
        # Ajouter une pièce jointe
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_4.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.assertIn('pj_3', pan.text)
        self.assertIn('pj_4', pan.text)
        # Tester les notifications
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, 'Pièce jointe ajoutée pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.instructeur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        del mail.outbox
        # Tester aucune action disponible
        aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en success
        self.selenium.execute_script("window.scroll(0, 500)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        presence_avis('agent_ggd', 'success')
        deconnexion()

        print('**** test 7 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        connexion('instructeur')
        presence_avis('instructeur', 'info')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        vue_detail()
        # Avis EDSR
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 600)")
        # Déplier l'avis
        avis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.assertIn('pj_3', carte.text)
        lien3 = carte.find_element_by_xpath("//a[contains(text(), 'pj_3')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien3.text)
        self.assertIn('pj_4', carte.text)
        lien4 = carte.find_element_by_xpath("//a[contains(text(), 'pj_4')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien4.text)
        pj = self.selenium.find_element_by_partial_link_text('pj_4')
        # Transférer la PJ en document officiel
        lien4.click()
        nom_pj = pj.get_attribute('text')
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath("//select[@name='nature']/option[text()='Récépissé de déclaration']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.assertIn('pj_3', carte.text)
        lien3 = carte.find_element_by_xpath("//a[contains(text(), 'pj_3')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien3.text)
        self.assertIn('pj_4', carte.text)
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_4')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (1)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Récépissé de déclaration', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier les emails envoyés
        # for em in mail.outbox:
        #     print(em.subject)
        #     print(em.to)
        self.assertEqual(len(mail.outbox), 5)
        self.assertEqual(mail.outbox[0].subject, 'récépissé de déclaration publié pour la manifestation "Manifestation_Test"')
        self.assertEqual(mail.outbox[0].to, [self.organisateur.email])
        self.assertEqual(mail.outbox[1].to, [self.prefecture.email])
        self.assertEqual(mail.outbox[2].to, [self.agent_cgd.email])
        self.assertEqual(mail.outbox[3].to, [self.agent_edsr.email])
        self.assertEqual(mail.outbox[4].to, [self.agent_ggd.email])
        # Vérifier le passage en success
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        presence_avis('instructeur', 'success')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        deconnexion()

