# coding: utf-8
import html

from django.test import TestCase

from administration.factories import InstructeurFactory
from administration.factories.service import CGServiceFactory, CompagnieFactory, CGDFactory
from notifications.models import Action
from notifications.models import Notification
from instructions.factories import InstructionFactory, AvisInstructionFactory
from instructions.models import Instruction


class InstructionTests(TestCase):
    """ Tests des instructions """

    # Configuration
    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'instruction

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée.
        """
        self.instruction = InstructionFactory.create()
        self.manifestation = self.instruction.manif
        self.commune = self.manifestation.ville_depart
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.instructeur = InstructeurFactory.create(prefecture=self.prefecture)
        self.cg = self.departement.cg
        self.edsr = self.departement.edsr
        self.ddsp = self.departement.ddsp
        self.sdis = self.departement.sdis
        self.ggd = self.departement.ggd
        self.activite = self.manifestation.activite
        self.federation = self.manifestation.get_federation()
        self.compagnie = CompagnieFactory.create(sdis=self.sdis)
        self.cgservice = CGServiceFactory.create(cg=self.cg)
        self.cgd = CGDFactory.create(commune=self.commune)

    # Tests
    def test_par_instructeur(self):
        """ Vérifier que l'instructeur est bien en charge de la délivrance de l'instruction """
        self.assertEqual(Instruction.objects.par_instructeur(self.instructeur)[0].pk, self.instruction.pk)

    def test_str(self):
        """ Tester la représentation texte de l'instruction """
        self.assertEqual(str(self.instruction), str(self.manifestation))

    def test_prefecture(self):
        """ Tester que la préfecture de l'instruction est correcte """
        self.assertEqual(self.instruction.get_prefecture_concernee(), self.prefecture)

    def test_aucun_avis(self):
        """ Tester la validation des avis """
        self.assertEqual(self.instruction.get_nb_avis(), 0)
        self.assertTrue(self.instruction.avis_tous_rendus())

    def test_avis(self):
        """ Tester la validation des avis """
        AvisInstructionFactory.create(instruction=self.instruction, service_concerne='federation')
        self.assertEqual(self.instruction.get_nb_avis(), 1)
        self.assertFalse(self.instruction.avis_tous_rendus())  # L'avis fédération (tjs créé par défaut) n'est pas validé
        avis = self.instruction.get_tous_avis().first()  # on sait que c'est un avis federation
        avis.etat = 'rendu'
        avis.save()
        self.assertEqual(avis.etat, 'rendu')
        self.assertEqual(self.instruction.get_nb_avis(), 1)
        self.assertTrue(self.instruction.avis_tous_rendus())  # L'avis fédération est désormais validé

    def test_action_log(self):
        """ Tester les logs d'actions """
        action = Action.objects.last()  # action loggée lorsque la manifestation est créée (voir evenements.listeners)
        action_count = Action.objects.count()
        self.assertEqual(action.user, self.manifestation.structure.organisateur.user)
        self.assertEqual(action.manif, self.manifestation.manifestation_ptr)
        self.assertTrue("description de la manifestation" in html.unescape(action.action))  # s'assurer que l'action contient cette chaîne

        self.instruction.save()  # on va vérifier qu'aucune nouvelle action n'est loggée
        self.assertEqual(action_count, Action.objects.count())

    def test_notification_log(self):
        """ Tester les notifications """
        notification = Notification.objects.first()  # instruction + avis fédération
        # pas de notification instructeur, ni de notification fédération : les agents sont créés après l'instruction
        self.assertIsNone(notification)

    def test_notify_instruction_creation_with_no_prefecture(self):
        instruction = InstructionFactory.create()
        self.assertEqual(Notification.objects.filter(manif=instruction.manif).count(), 0)
