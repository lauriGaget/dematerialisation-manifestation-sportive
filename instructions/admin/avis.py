# coding: utf-8
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from instructions.models import Avis, PreAvis


class PreAvisInline(admin.StackedInline):
    """ Inline Préavis """

    # Configuration
    model = PreAvis
    extra = 0
    view_on_site = False
    readonly_fields = ['date_demande', 'service_concerne', 'agentlocal_name']
    fieldsets = (
        (None, {'fields': ('service_concerne', 'etat', ('date_demande', 'date_reponse'), 'favorable', 'prescriptions',
                           'agentlocal_name')}),
    )

    def has_add_permission(self, request, obj=None):
        return False

    def agentlocal_name(self, obj):
        if obj.agentlocal:
            return obj.agentlocal.get_full_name()
        return '-'
    agentlocal_name.short_description = "Dernier intervenant"


class AvisInline(admin.StackedInline):
    """ Inline avis """

    # Configuration
    model = Avis
    extra = 0
    view_on_site = False
    readonly_fields = ['date_demande', 'service_concerne', 'agent_name', 'edition_avis']
    fieldsets = (
        (None, {'fields': (('service_concerne', 'etat', 'edition_avis'), ('date_demande', 'date_reponse'), 'favorable',
                           'prescriptions', 'agent_name')}),
    )

    def edition_avis(self, obj):
        if obj.pk:
            if Avis.objects.get(pk=obj.pk).preavis.all():
                url = reverse("admin:instructions_avis_change", args=[obj.pk])
                return format_html('<button><a href="{}">Avis</a></button>', url)
        return 'Pas de préavis'
    edition_avis.short_description = "Voir les préavis"

    def agent_name(self, obj):
        if obj.agent:
            return obj.agent.get_full_name()
        return '-'
    agent_name.short_description = "Dernier intervenant"

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(Avis)
class AvisAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin de l'avis GGD """
    # TODO : Remplacer le champ "document_attaché" par PieceJointeAvis quand la branche master_DTA_avis sera mergée
    # Configuration de base
    model = Avis
    inlines = [PreAvisInline]
    readonly_fields = ['instruction', 'service_concerne', 'agent_name']
    fieldsets = (
        (None, {'fields': ('etat', ('instruction', 'service_concerne'), ('date_demande', 'date_reponse'), 'favorable',
                           'prescriptions', 'agent_name')}),
    )

    def has_module_permission(self, request):
        return False

    def agent_name(self, obj):
        if obj.agent:
            return obj.agent.get_full_name()
        return '-'
    agent_name.short_description = "Dernier intervenant"

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(instruction__manif__ville_depart__arrondissement__departement=request.user.get_departement())
        return queryset
