# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout
from django import forms

from instructions.models.avis import Avis
from core.forms.base import GenericForm
from administration.models.service import EDSR


class AvisAcknowledgeForm(GenericForm):
    """ Formulaire de rendu d'avis (acknowledge) """

    prescriptions = forms.CharField(label="Contenu", required=False,
                                    widget=forms.Textarea(attrs={'placeholder': 'Précisez ici vos commentaires, prescriptions, recommandations...'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(Submit('save', "rendre l'avis".capitalize()))
        )

    class Meta:
        model = Avis
        fields = ['favorable', 'prescriptions']


class AvisFormatForm(GenericForm):
    """ Formulaire de mise en forme d'avis """

    prescriptions = forms.CharField(label="Contenu", required=False,
                                    widget = forms.Textarea(attrs={'placeholder': 'Précisez ici vos commentaires, prescriptions, recommandations...'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'favorable',
            'prescriptions',
            FormActions(
                Submit('save', "mettre en forme l'avis".capitalize())
            )
        )

    class Meta:
        model = Avis
        fields = ('favorable', 'prescriptions')


class AvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    services_concernes = forms.ModelMultipleChoiceField(queryset=None, label="Services concernés")
    commissariats_concernes = forms.ModelMultipleChoiceField(queryset=None, label="Commissariats concernés")
    compagnies_concernees = forms.ModelMultipleChoiceField(queryset=None, label="Compagnies concernées")
    cgd_concerne = forms.ModelMultipleChoiceField(queryset=None, label="CGD concernées")
    edsr_concerne = forms.ModelChoiceField(queryset=None, empty_label= "Choisissez l'EDSR concerné", label="EDSR concerné")

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cgd_concerne'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs CGD concernées"}
        self.fields['services_concernes'].widget.attrs = {'data-placeholder': "Choisissez un ou plusieurs services concernés"}
        self.fields['commissariats_concernes'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs communes concernées"}
        self.fields['compagnies_concernees'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs compagnies concernées"}
        self.helper.layout = Layout(
            'cgd_concerne',
            'edsr_concerne',
            'compagnies_concernees',
            'services_concernes',
            'commissariats_concernes',
            FormActions(Submit('save', "Envoyer les demandes de pré-avis"))
        )

    # Meta
    class Meta:
        model = Avis
        fields = ['cgd_concerne', 'edsr_concerne', 'commissariats_concernes', 'compagnies_concernees', 'services_concernes']
        exclude = []


class AvisPassForm(GenericForm):
    """ Formulaire de modification d'avis en vue de le passer à l'agent EDSR """

    edsr_concerne = forms.ModelChoiceField(queryset=None, empty_label= "Choisissez l'EDSR concerné", label="EDSR concerné")

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            'edsr_concerne',
            FormActions(Submit('save', "Envoyer l'avis à l'EDSR"))
        )

    # Meta
    class Meta:
        model = Avis
        fields = ['edsr_concerne']
        exclude = []
