# Generated by Django 2.2.1 on 2019-10-02 13:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instructions', '0016_piecejointeavis'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='avis',
            options={'default_related_name': 'avis', 'ordering': ['-date_reponse'], 'verbose_name': 'Avis', 'verbose_name_plural': 'Avis'},
        ),
        migrations.RemoveField(
            model_name='avis',
            name='document_attache',
        ),
    ]
