# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2017-09-22 07:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0005_auto_20160822_1925'),
        ('events', '0017_auto_20170322_1657'),
        ('sports', '0012_remove_discipline_federation'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='federationmultisport',
            name='federation_ptr',
        ),
        migrations.DeleteModel(
            name='FederationMultiSport',
        ),
    ]
