# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-04-05 14:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sports', '0013_auto_20170922_0915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='federation',
            name='departement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='administrative_division.Departement', verbose_name='Département'),
        ),
    ]
