# coding: utf-8
from django.test import TestCase

from administrative_division.factories import DepartementFactory
from sports.factories import FederationFactory, DisciplineFactory
from sports.models.federation import Federation


class FederationTests(TestCase):
    """ Tests des fédérations """

    def setUp(self):
        """ Configurer les tests """
        self.federation = FederationFactory.create()

    # Tests
    def test_default_federation(self):
        """ Vérifier qu'en l'absence de fédération, on se replie vers une fédération fourre-tout """
        discipline = DisciplineFactory.create()
        departement = DepartementFactory.create()
        base_federation = Federation.objects.get_for_departement(discipline, departement)
        self.assertIsInstance(base_federation, Federation)
        self.assertEqual(base_federation.short_name, Federation.DEFAULT_FEDERATION_NAME)
