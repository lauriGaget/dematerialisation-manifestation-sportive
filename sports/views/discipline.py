from django.db.models import Q, Count
from django.views.generic.list import ListView
from django.shortcuts import render

from sports.models.sport import Discipline
from sports.models.federation import Federation
from administrative_division.models.departement import Departement, LISTE_DEPARTEMENT
from core.util.user import UserHelper


class DisciplinesActivitesView(ListView):

    model = Discipline
    template_name = "sports/disciplinesactivites.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class DisciplinesFederationsView(ListView):

    model = Discipline
    template_name = "sports/disciplinesfederations.html"

    def dispatch(self, request, *args, **kwargs):
        if UserHelper.has_role(request.user, 'instructeur') or \
                "Administrateurs d'instance" in request.user.groups.values_list('name', flat=True):
            return super().dispatch(request, *args, **kwargs)
        return render(request, 'core/access_restricted.html', status=403)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['federations'] = Federation.objects.annotate(
            nb_user_actif=Count('federationagents', filter=Q(federationagents__user__is_active=True))).annotate(
            nb_user_nonactif=Count('federationagents', filter=~Q(federationagents__user__is_active=True)))

        liste_dept = []
        for d in Departement.objects.configured():
            liste_dept.append((d.name, d))
        context['liste_dept'] = liste_dept

        dept = self.request.GET.get('dept')
        if self.request.user.get_departement():
            dept = self.request.user.get_departement().name
            context['dept_user'] = self.request.user.get_departement()
        if dept:
            regional = Federation.objects.none()
            for departement in LISTE_DEPARTEMENT:
                if departement[0] ==dept:
                    regional = Federation.objects.filter(region=departement[2])
            departemental = Federation.objects.filter(departement__name=dept)
            context['liste_federations'] = departemental.union(regional)
        else:
            context['liste_federations'] = Federation.objects.all()
        return context
