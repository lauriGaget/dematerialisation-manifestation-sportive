# coding: utf-8
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions, AppendedText
from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from django.conf import settings
from django.template.loader import render_to_string

from core.forms.base import GenericForm
from ..models import EvaluationN2K
from django import forms


IMPACTS_HELP = HTML(render_to_string('evaluation_incidence/forms/help/impacts_help.html', {'target': settings.DDT_SITE}))

N2K_INFORMATIONS_FIELDSET = [
    'diurne',
    'nocturne',
    HTML('<strong>Fréquence de la manifestation :</strong>'),
    'annuel',
    'premiere_edition',
    'autre_frequence',
    HTML('<strong>Budget de la manifestation :</strong>'),
    'cout',
]

N2K_LOCALISATION_FIELDSET = [
    HTML("{% if sites %}"
         "<div class='row' id='accordion'><div class='col-sm-3'>Rappel des sites Natura 2000 traversés :</div>"
         "<div class='col-sm-6 sitesn2k'><ul>{% for site in sites %}<li class='mb-1' id='zone_{{ site.id }}'>{{ site  }}"
         "<a class='float-right' role='button' href='#collapse{{ site.id }}' data-toggle='collapse' data-target='#collapse{{ site.id }}' aria-expanded='false'"
         "aria-controls='collapse{{ site.id }}'>&nbsp;&nbsp;&nbsp;<i class='rechercher'></i> voir l'opérateur</a></li>{% endfor %}</ul></div>"
         "{% for site in sites %}<table class='collapse w-100 mb-3' id='collapse{{ site.id }}' aria-labelledby='site_{{ site.id }}' data-parent='#accordion'>"
         "<thead><tr class='table-secondary'><th>Administrateur</th><th>Adresse</th><th>Commune</th><th>Conservateur</th><th>E-mail</th></tr></thead>"
         "<tbody><tr class='table-active'><td>{{ site.operateursiten2k.nom }}</td><td>{{ site.operateursiten2k.adresse|default:'' }}</td>"
         "<td>{{ zone.operateursiten2k.commune|default:'' }}</td><td>{{ zone.operateursiten2k.contacts|linebreaksbr }}</td>"
         "<td>{{ zone.operateursiten2k.email|urlize }}</td></tr></tbody></table>{% endfor %}</div>"
         "{% endif %}"),
    'terrestre',
    'nautique',
    'aerien',
    'sensibilite',
    'lieu',
    AppendedText('Longueur_totale_parcours', 'km'),
    AppendedText('distance_site', 'km'),
    'impact_calcule',
]

N2K_INCIDENCES_FIELDSET = [
    'pietinement', 'mesures_pietinement',
    'emissions_sonores', 'mesures_emissions_sonores',
    'emissions_lumiere', 'mesures_emissions_lumiere',
    'pollution_eau', 'mesures_pollution_eau',
    'pollution_terre', 'mesures_pollution_terre',
    'sensibilisation', 'mesures_sensibilisation',
    'emprise_amenagement', 'mesures_emprise_amenagement',
    'public', 'mesures_public',
    'parking', 'mesures_parking',
    'engins_aeriens', 'mesures_engins_aeriens',
]

N2K_CONCLUSIONS_FIELDSET = [
    'impact_estime',
    'conclusion_natura_2000',
]
FORM_WIDGETS = {
    'mesures_pietinement': forms.Textarea(attrs={'rows': 1}),
    'mesures_emissions_sonores': forms.Textarea(attrs={'rows': 1}),
    'mesures_emissions_lumiere': forms.Textarea(attrs={'rows': 1}),
    'mesures_pollution_eau': forms.Textarea(attrs={'rows': 1}),
    'mesures_pollution_terre': forms.Textarea(attrs={'rows': 1}),
    'mesures_sensibilisation': forms.Textarea(attrs={'rows': 1}),
    'mesures_emprise_amenagement': forms.Textarea(attrs={'rows': 1}),
    'mesures_public': forms.Textarea(attrs={'rows': 1}),
    'mesures_parking': forms.Textarea(attrs={'rows': 1}),
    'mesures_engins_aeriens': forms.Textarea(attrs={'rows': 1}),
}


class EvaluationN2kForm(GenericForm):
    """ Formulaire d'évaluation N2K """

    impact_calcule = forms.CharField(label="Impact calculé", disabled=True, required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Informations générales", *N2K_INFORMATIONS_FIELDSET),
            Fieldset("Localisation de la manifestation", *N2K_LOCALISATION_FIELDSET),
            Fieldset("Incidences potentielles", *N2K_INCIDENCES_FIELDSET, css_class="offset_remove"),
            Fieldset("Conclusions", *N2K_CONCLUSIONS_FIELDSET),
            FormActions(
                Submit('save', "soumettre".capitalize()),
                HTML('<a class="btn btn-danger", href="javascript:history.back()">Annuler</a>'),
            )
        )
        self.ajouter_css_class_requis_aux_champs_requis()

    # Meta
    class Meta:
        model = EvaluationN2K
        exclude = ('manif',)
        widgets = FORM_WIDGETS


class DemandeN2kForm(forms.ModelForm):
    """ Formulaire de demande d'évaluation N2k par l'instructeur du dossier """
    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.form_id = 'demandereval'
        self.helper.label_class = 'col-sm-12'
        self.helper.field_class = 'col-sm-12 overflow-auto'
        self.helper.layout = Layout(
            Fieldset(
                "Motivation de la demande".capitalize(),
                'pourquoi_instructeur', 'pourquoi_organisateur'
            ),
            FormActions(
                Submit('save', "soumettre".capitalize())
            )
        )

    # Meta
    class Meta:
        model = EvaluationN2K
        fields = ['pourquoi_instructeur', 'pourquoi_organisateur']
        widgets = {'pourquoi_instructeur': forms.Textarea(attrs={'rows': 1}),
                   'pourquoi_organisateur': forms.Textarea(attrs={'rows': 1})}