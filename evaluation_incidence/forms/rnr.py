# coding: utf-8

from bootstrap_datepicker_plus import DateTimePickerInput
from crispy_forms.bootstrap import FormActions, AppendedText
from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from django import forms

from core.forms.base import GenericForm
from ..models import EvaluationRnr


INFORMATIONS_FIELDSET = [
    'duree',
    'nocturne',
    HTML('<strong>Fréquence de la manifestation :</strong>'),
    'annuel',
    'premiere_edition',
    'autre_frequence',
    HTML('<strong>Budget de la manifestation :</strong>'),
    'cout',
]

LOCALIZATION_FIELDSET = (
    HTML("{% if sites %}"
         "<div class='row' id='accordion'><div class='col-sm-3'>Rappel des zones RNR traversés :</div>"
         "<div class='col-sm-6'><ul>{% for zone in zones %}<li class='mb-1' id='zone_{{ zone.id }}'>{{ zone  }}"
         "<a class='float-right' role='button' href='#collapse{{ zone.id }}' data-toggle='collapse' data-target='#collapse{{ zone.id }}' aria-expanded='false' aria-controls='collapse{{ zone.id }}'>"
         "&nbsp;&nbsp;&nbsp;<i class='rechercher'></i> voir l'opérateur</a></li>{% endfor %}</ul></div>"
         "{% for zone in zones %}<table class='collapse w-100 mb-3' id='collapse{{ zone.id }}' aria-labelledby='zone_{{ zone.id }}' data-parent='#accordion'>"
         "<thead><tr class='table-secondary'><th>Administrateur</th><th>Adresse</th><th>Commune</th><th>Conservateur</th><th>E-mail</th></tr></thead>"
         "<tbody><tr class='table-active'><td>{{ zone.administrateurrnr.nom }}</td><td>{{ zone.administrateurrnr.adresse|default:'' }}</td>"
         "<td>{{ zone.administrateurrnr.commune|default:'' }}</td><td>{{ zone.administrateurrnr.contacts|linebreaksbr }}</td>"
         "<td>{{ zone.administrateurrnr.email|urlize }}</td></tr></tbody></table>{% endfor %}</div>"
         "{% endif %}"),
    'lieu',
    AppendedText('Longueur_totale_parcours', 'km'),
)

ATTENDANCE_FIELDSET = (
    AppendedText('participants', 'personnes'),
    AppendedText('spectateurs', 'personnes'),
)

PROVISIONS_FIELDSET = ('date_contact_administrateur', 'sensibilisation', 'canalisation_public', 'presence_administrateur',)

FACILITIES_FIELDSET_RNR = (
    'eclairage_nocturne',
    'description_eclairage_nocturne',
    'balisage',
    'description_balisage',
    'autre_amenagement',
    'description_autre_amenagement',
)

RNR_IMPLICATIONS_FIELDSET = (
    HTML(
        "<div class='well'>"
        "<p>{paragraph}</p>"
        "<ul>"
        "<li>{parking}</li>"
        "<li>{trash}</li>"
        "<li>{picking}</li>"
        "<li>{light}</li>"
        "<li>{noise}</li>"
        "<li>{animals}</li>"
        "</ul>"
        "</div>".format(
            paragraph='sont interdits sur la réserve naturelle : '.capitalize(),
            parking='le stationnement et passage de participants et du public en dehors des '
                    'sentiers autorisés'.capitalize(),
            trash="l'abandon de déchets ou produits".capitalize(),
            picking='la cueillette de végétaux'.capitalize(),
            light="d'utiliser un éclairage artificiel (en dehors de ceux utilisés par les "
                  "services publics de secours)".capitalize(),
            noise="de troubler le calme et la tranquillité des lieux et des animaux, par des "
                  "cris ou bruits divers, par l'utilisation d'un appareil radiophonique, ou "
                  "tout autre instrument sonore".capitalize(),
            animals="les animaux domestiques non tenus en laisse et la divagation de ces mêmes "
                    "animaux".capitalize(),
        )
    ),
    'emissions_sonores',
    'gestion_dechets',
    'description_gestion_dechets',
    'prise_conscience',
    'reduction_impact',
    'description_reduction_impact',
    'engins_aeriens',
    'description_engins',
    'pietinement',
    'description_pietinement',
    'gestion_balisage',
    'description_gestion_balisage',
)

RNR_CONCLUSIONS_FIELDSET = (
    'respect_reglement',
    'conclusion_rnr',
    'impact_estime',
)

FORM_WIDGETS = {
    'description_pietinement': forms.Textarea(attrs={'rows': 1}),
    'description_balisage': forms.Textarea(attrs={'rows': 1}),
    'description_engins': forms.Textarea(attrs={'rows': 1}),
    'description_eclairage_nocturne': forms.Textarea(attrs={'rows': 1}),
    'description_autre_amenagement': forms.Textarea(attrs={'rows': 1}),
    'description_gestion_dechets': forms.Textarea(attrs={'rows': 1}),
    'description_gestion_balisage': forms.Textarea(attrs={'rows': 1}),
    'description_reduction_impact': forms.Textarea(attrs={'rows': 1}),
    'date_contact_administrateur': DateTimePickerInput(options={"format": "DD/MM/YYYY", 'locale': 'fr'}),
}


class EvaluationRNRForm(GenericForm):
    """ Formulaire d'évaluation des incidences sur une RNR """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Informations générales", *INFORMATIONS_FIELDSET),
            Fieldset("Localisation de la manifestation par rapport aux sites RNR", *LOCALIZATION_FIELDSET),
            Fieldset("fréquentation".capitalize(), *ATTENDANCE_FIELDSET),
            Fieldset("prise en compte de l'environnement".capitalize(), *PROVISIONS_FIELDSET),
            Fieldset("présence d'aménagements connexes".capitalize(), *FACILITIES_FIELDSET_RNR, css_class="offset_remove"),
            Fieldset("Incidences potentielles sur RNR", *RNR_IMPLICATIONS_FIELDSET, css_class="offset_remove"),
            Fieldset("conclusion".capitalize(), *RNR_CONCLUSIONS_FIELDSET),
            FormActions(
                Submit('save', "soumettre".capitalize()),
                HTML('<a class="btn btn-danger", href="javascript:history.back()">Annuler</a>'),
            )
        )
        self.ajouter_css_class_requis_aux_champs_requis()

    # Meta
    class Meta:
        model = EvaluationRnr
        exclude = ('manif',)
        labels = {'Longueur_totale_parcours': "longueur totale des parcours en RNR"}
        widgets = FORM_WIDGETS
