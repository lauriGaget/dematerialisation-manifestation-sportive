from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.conf import settings
from clever_selects.form_fields import ChainedModelChoiceField
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField

from core.forms.base import GenericForm
from core.models import User
from administrative_division.models import Commune, Departement
from evaluation_incidence.models import Natura2kSiteConfig, Natura2kDepartementConfig, N2kOperateurSite, RnrAdministrateur


class Natura2kConfigForm(GenericForm):
    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nm_formulaire'].widget.attrs = {'class': 'multi-inline'}
        self.fields['nm_formulaire'].help_text = "<a href='https://manifestationsportive.fr/aide/types-formulaires'" \
                                                 "target='_blank'>voir la page d'aide sur les types de manifestation</a>"
        self.fields['vtm_formulaire'].widget.attrs = {'class': 'multi-inline'}
        self.fields['vtm_formulaire'].help_text = "<a href='https://manifestationsportive.fr/aide/types-formulaires'" \
                                                  "target='_blank'>voir la page d'aide sur les types de manifestation</a>"
        self.helper.field_class = 'col-sm-8'
        self.helper.form_tag = False

    def clean(self):
        cleaned_data = super().clean()
        if self.changed_data:
            liste_dept = []
            if self.instance.pk:
                if type(self.instance) is Natura2kSiteConfig:
                    liste_dept = self.instance.n2ksite.departements.all()
                elif type(self.instance) is Natura2kDepartementConfig:
                    liste_dept = [self.instance.instance.get_departement()]
                for dept in liste_dept:
                    adminstancelist = User.objects.filter(
                        groups__name__icontains='Administrateurs d\'instance').filter(
                        default_instance=dept.get_instance()).values_list('email', flat=True)
                    url = '/admin/evaluation_incidence/' + type(self.instance).__name__ + '/' + str(
                        self.instance.pk) + '/change/'
                    address = 'https://' + Site.objects.get_current().domain + url
                    if adminstancelist:
                        send_mail(subject='[Manifestation Sportive] Modification de configuration Natura 2000',
                                  message='Une modification a été effectuée sur une configuration Natura 2000 : ' +
                                          str(self.instance) + chr(10) +
                                          "Pour les champs suivants : " + " - ".join([field for field in self.changed_data]) + chr(10) +
                                          "Pour visualiser cette configuration, rendez-vous à l'adresse : " + chr(10) +
                                          address,
                                  from_email=settings.DEFAULT_FROM_EMAIL,
                                  recipient_list=list(adminstancelist))
            else:
                obj = None
                if 'instance' in cleaned_data:
                    liste_dept = [cleaned_data['instance'].get_departement()]
                    obj = cleaned_data['instance']
                elif 'n2ksite' in cleaned_data:
                    liste_dept = cleaned_data['n2ksite'].departements.all()
                    obj = cleaned_data['n2ksite']
                for dept in liste_dept:
                    adminstancelist = User.objects.filter(
                        groups__name__icontains='Administrateurs d\'instance').filter(
                        default_instance=dept.get_instance()).values_list('email', flat=True)
                    if adminstancelist:
                        send_mail(subject='[Manifestation Sportive] Création de configuration Natura 2000',
                                  message='Une configuration Natura 2000 vient d\'être créée: ' +
                                          type(self.instance).__name__ + chr(10) +
                                          "Pour le département ou le site suivant : " + str(obj),
                                  from_email=settings.DEFAULT_FROM_EMAIL,
                                  recipient_list=list(adminstancelist))
        return cleaned_data

    class Meta:
        exclude = []


class N2kOperateurSiteAdminForm(GenericForm):
    """ Formulaire des opérateurs de sites N2k """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'),
                                      Commune, label="Commune")
    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk and self.instance.commune:
            self.fields['departement'].initial = self.instance.commune.get_departement()
            self.fields['commune'].queryset = Commune.objects.by_departement(self.instance.commune.get_departement())

    # Meta
    class Meta:
        model = N2kOperateurSite
        fields = ['nom', 'departement', 'commune', 'adresse', 'email', 'contacts']


class RnrConfigForm(GenericForm):
    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['formulaire'].widget.attrs = {'class': 'multi-inline'}
        self.fields['formulaire'].help_text = "<a href='https://manifestationsportive.fr/aide/types-formulaires'" \
                                              "target='_blank'>voir la page d'aide sur les types de manifestation</a>"
        self.helper.field_class = 'col-sm-8'
        self.helper.form_tag = False

    def clean(self):
        cleaned_data = super().clean()
        if self.changed_data:
            if self.instance.pk:
                liste_dept = self.instance.rnrzone.departements.all()
                for dept in liste_dept:
                    adminstancelist = User.objects.filter(
                        groups__name__icontains='Administrateurs d\'instance').filter(
                        default_instance=dept.get_instance()).values_list('email', flat=True)
                    url = '/admin/evaluation_incidence/' + type(self.instance).__name__ + '/' + str(self.instance.pk) + '/change/'
                    address = 'https://' + Site.objects.get_current().domain + url
                    if adminstancelist:
                        send_mail(subject='[Manifestation Sportive] Modification de configuration de zone RNR',
                                  message='Une modification a été effectuée sur une configuration de zone RNR : ' +
                                          str(self.instance) + chr(10) +
                                          "Pour les champs suivants : " + " - ".join([field for field in self.changed_data]) + chr(10) +
                                          "Pour visualiser cette configuration, rendez-vous à l'adresse : " + chr(10) +
                                          address,
                                  from_email=settings.DEFAULT_FROM_EMAIL,
                                  recipient_list=list(adminstancelist))
            else:
                liste_dept = cleaned_data['rnrzone'].departements.all()
                obj = cleaned_data['rnrzone']
                for dept in liste_dept:
                    adminstancelist = User.objects.filter(
                        groups__name__icontains='Administrateurs d\'instance').filter(
                        default_instance=dept.get_instance()).values_list('email', flat=True)
                    if adminstancelist:
                        send_mail(subject='[Manifestation Sportive] Création de configuration RNR',
                                  message='Une configuration RNR vient d\'être créée: ' +
                                          type(self.instance).__name__ + chr(10) +
                                          "Pour la zone suivante : " + str(obj),
                                  from_email=settings.DEFAULT_FROM_EMAIL,
                                  recipient_list=list(adminstancelist))
        return cleaned_data

    class Meta:
        exclude = []


class RnrAdministrateurAdminForm(GenericForm):
    """ Formulaire des opérateurs de sites N2k """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'),
                                      Commune, label="Commune")
    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk and self.instance.commune:
            self.fields['departement'].initial = self.instance.commune.get_departement()
            self.fields['commune'].queryset = Commune.objects.by_departement(self.instance.commune.get_departement())

    # Meta
    class Meta:
        model = RnrAdministrateur
        fields = ['nom_court', 'nom', 'departement', 'commune', 'adresse', 'email', 'contacts']
