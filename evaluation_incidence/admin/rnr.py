# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from evaluation_incidence.models import EvaluationRnr, RnrAdministrateur, RnrZone, RnrZoneConfig
from core.util.admin import RelationOnlyFieldListFilter
from .filters import ConfigRnrFilter, OperateurRnrFilter
from .forms import RnrConfigForm, RnrAdministrateurAdminForm

class RNREvalInline(admin.StackedInline):
    """ Inline d'évaluation RNR """

    # Configuration
    model = EvaluationRnr
    extra = 0
    readonly_fields = ['manif']


class RnrAdministrateurAdminInline(admin.StackedInline):
    """ Inline de l'opérateur du site """

    # Configuration
    model = RnrAdministrateur
    form = RnrAdministrateurAdminForm


class RNRZoneConfigInline(admin.StackedInline):
    """ Inline d'évaluation RNR """

    # Configuration
    model = RnrZoneConfig
    extra = 0
    form = RnrConfigForm
    fieldsets = (
        (None, {'fields': ('formulaire', 'seuil_participants', 'seuil_total'),
                'classes': ('textadmin',),
                'description': "Si vous cochez un ou plusieurs types de formulaire,"
                               " les critères seront appliqués uniquement sur les manifestations correspondant à ce type.<br>"
                               "Sinon, les critères s'appliqueront pour toutes les manifestations.<br>"
                               "<br>Si un seuil est laissé à zéro, il n'aura pas d'effet."}),
    )


@admin.register(RnrZone)
class RNRAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration RNR """

    list_display = ['code', 'nom', 'operateur', 'config_enregistree', 'departements_concernes']
    list_filter = [('departements', RelationOnlyFieldListFilter), ConfigRnrFilter, OperateurRnrFilter]
    search_fields = ['nom__unaccent', 'code', 'administrateurrnr__nom__unaccent']
    inlines = [RNRZoneConfigInline, RnrAdministrateurAdminInline]
    list_per_page = 25
    list_display_links = ['code', 'nom']

    def departements_concernes(self, obj):
        deps = obj.departements.all()
        list_dep = ''
        for dep in deps:
            if list_dep == '':
                list_dep += dep.name
            else:
                list_dep += ' - ' + dep.name
        return list_dep

    def operateur(self, obj):
        if obj.administrateurrnr:
            return True
        return False
    operateur.short_description = "Opérateur enregistrée"
    operateur.boolean = True


    def config_enregistree(self, obj):
        if obj.rnrzoneconfig:
            return True
        return False
    config_enregistree.short_description = "Configuration enregistrée"
    config_enregistree.boolean = True

    # Overrides
    # Les utilisateurs ayant les droits voient leur département et les sites sans département affecté
    # Les sites ont été incorporés sans département, il faut que les gestionnaires les affectent
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(Q(departements=request.user.get_departement()) | Q(departements__isnull=True))
        return queryset
