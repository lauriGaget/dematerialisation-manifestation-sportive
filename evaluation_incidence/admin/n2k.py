# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from evaluation_incidence.models import EvaluationN2K, N2kOperateurSite, N2kSite, Natura2kSiteConfig, Natura2kDepartementConfig
from core.util.admin import RelationOnlyFieldListFilter
from core.models import Instance
from .filters import ConfigN2kFilter, OperateurN2kFilter
from .forms import Natura2kConfigForm, N2kOperateurSiteAdminForm


class N2kEvalInline(admin.StackedInline):
    """ Inline d'évaluation N2K """

    # Configuration
    model = EvaluationN2K
    extra = 0
    readonly_fields = ['manif']


class N2kOperateurSiteAdminInline(admin.StackedInline):
    """ Inline de l'opérateur du site """

    # Configuration
    model = N2kOperateurSite
    form = N2kOperateurSiteAdminForm


class N2kSiteConfigInline(admin.StackedInline):
    """ Inline d'évaluation N2K """

    # Configuration
    model = Natura2kSiteConfig
    extra = 0
    form = Natura2kConfigForm
    exclude = ['vtm_sur_siten2k', 'nm_sur_siten2k']
    fieldsets = (
        ('Paramètres généraux', {'fields': ('n2ksite', 'charte_dispense_acceptee',),
                                 'classes': ('textadmin',),
                                 'description': "Chaque condition de seuil est suffisante pour déclencher une évaluation d'incidence."}),
        ('Paramètres pour manifestation VTM', {'fields': ('vtm_formulaire', 'vtm_seuil_participants',
                                                          'vtm_seuil_vehicules', 'vtm_seuil_total'),
                                               'classes': ('textadmin',),
                                               'description': "Si vous cochez un ou plusieurs types de formulaire,"
                                                              " les critères seront appliqués uniquement sur les manifestations correspondant à ce type.<br>"
                                                              "Sinon, les critères s'appliqueront pour toutes les manifestations motorisées.<br>"
                                                              "<br>Si un seuil est laissé à zéro, il n'aura pas d'effet."}),
        ('Paramètres pour manifestation NM', {'fields': ('nm_formulaire', 'nm_seuil_participants',
                                                         'nm_seuil_total', 'nm_hors_circulation'),
                                              'classes': ('textadmin',),
                                              'description': "Si vous cochez un ou plusieurs types de formulaire,"
                                                             " les critères seront appliqués uniquement sur les manifestations correspondant à ce type.<br>"
                                                             "Sinon, les critères s'appliqueront pour toutes les manifestations non motorisées.<br>"
                                                             "<br>Si un seuil est laissé à zéro, il n'aura pas d'effet.<br>"
                                                             "La case à cocher << hors voie ouverte à la circulation >> est sans effet"
                                                             " car les manifestations non motorisées hors voie ouverte à la circulation ne nécessitent pas de déclaration."})
    )


@admin.register(N2kSite)
class N2KAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration Natura 2000 """

    list_display = ['index', 'nom', 'site_type', 'operateur', 'config_enregistree', 'config_signature', 'departements_concernes']
    list_filter = [('departements', RelationOnlyFieldListFilter),
                   'natura2ksiteconfig__charte_dispense_acceptee', ConfigN2kFilter, OperateurN2kFilter]
    search_fields = ['nom__unaccent', 'index', 'operateursiten2k__nom__unaccent']
    inlines = [N2kSiteConfigInline, N2kOperateurSiteAdminInline]
    list_per_page = 25
    list_display_links = ['index', 'nom']

    def departements_concernes(self, obj):
        deps = obj.departements.all()
        list_dep = ''
        for dep in deps:
            if list_dep == '':
                list_dep += dep.name
            else:
                list_dep += ' - ' + dep.name
        return list_dep

    def operateur(self, obj):
        if obj.operateursiten2k:
            return True
        return False
    operateur.short_description = "Opérateur enregistrée"
    operateur.boolean = True

    def config_enregistree(self, obj):
        if obj.natura2ksiteconfig:
            return True
        return False
    config_enregistree.short_description = "Configuration enregistrée"
    config_enregistree.boolean = True

    def config_signature(self, obj):
        if obj.natura2ksiteconfig:
            if obj.natura2ksiteconfig.charte_dispense_acceptee:
                return True
        return False
    config_signature.short_description = "Charte de dispense acceptée"
    config_signature.boolean = True

    # Overrides
    # Les utilisateurs ayant les droits voient leur département et les sites sans département affecté
    # Les sites ont été incorporés sans département, il faut que les gestionnaires les affectent
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(Q(departements=request.user.get_departement()) | Q(departements__isnull=True))
        return queryset


@admin.register(Natura2kDepartementConfig)
class Natura2kDepartementConfigAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['instance', 'pdesi']
    list_filter = ['pdesi']
    search_fields = ['instance__departement__name']
    list_per_page = 25
    form = Natura2kConfigForm
    fieldsets = (
        ('Paramètres généraux', {'fields': ('instance', 'pdesi',),
                                 'classes': ('textadmin',),
                                 'description': "Chaque condition de seuil est suffisante pour déclencher une évaluation d'incidence."}),
        ('Paramètres pour manifestation VTM', {'fields': ('vtm_formulaire', 'vtm_sur_siten2k', 'vtm_seuil_participants',
                                                          'vtm_seuil_vehicules', 'vtm_seuil_total'),
                                               'classes': ('textadmin',),
                                               'description': "Si vous cochez un ou plusieurs types de formulaire,"
                                                              " les critères seront appliqués uniquement sur les manifestations correspondant à ce type.<br>"
                                                              "Sinon, les critères s'appliqueront pour toutes les manifestations motorisées.<br>"
                                                              "<br>Si vous cochez sur << Manifestation sur un site Natura 2000 >>, les critères ne seront appliqués que dans ce cas.<br>"
                                                              "<br>Si un seuil est laissé à zéro, il n'aura pas d'effet."}),
        ('Paramètres pour manifestation NM', {'fields': ('nm_formulaire', 'nm_sur_siten2k', 'nm_seuil_participants',
                                                         'nm_seuil_total', 'nm_hors_circulation'),
                                              'classes': ('textadmin',),
                                              'description': "Si vous cochez un ou plusieurs types de formulaire,"
                                                             " les critères seront appliqués uniquement sur les manifestations correspondant à ce type.<br>"
                                                             "Sinon, les critères s'appliqueront pour toutes les manifestations non motorisées.<br>"
                                                             "<br>Si vous cochez sur << Manifestation sur un site Natura 2000 >>, les critères ne seront appliqués que dans ce cas.<br>"
                                                             "<br>Si un seuil est laissé à zéro, il n'aura pas d'effet.<br>"
                                                             "La case à cocher << hors voie ouverte à la circulation >> est sans effet"
                                                             " car les manifestations non motorisées hors voie ouverte à la circulation ne nécessitent pas de déclaration."})
    )

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(instance__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['instance'].queryset = Instance.objects.filter(departement=request.user.get_departement())
        return form
