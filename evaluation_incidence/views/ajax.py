# coding: utf-8
from django.views.generic import CreateView
from django.shortcuts import get_object_or_404, render
from django.utils.decorators import method_decorator
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from clever_selects.views import ChainedSelectChoicesView

from core.util.types import make_iterable
from evaluation_incidence.models import RnrZone, N2kSite, EvaluationN2K, PdesiLieu
from evaluation_incidence.forms import DemandeN2kForm
from administrative_division.models.departement import Departement
from evenements.models import Manif


class N2kEvalAjaxCreate(CreateView):
    """ Création d'évaluation N2K """

    # Configuration
    model = EvaluationN2K
    form_class = DemandeN2kForm
    template_name = 'evaluation_incidence/ajaxevaln2k_form.html'

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Si la déclaration existe déjà pour la manifestation, renvoyer une reponse vide
        if EvaluationN2K.objects.filter(manif=self.manif).exists():
            return HttpResponse()
        if self.origine() == 'instructeur':
            if request.user.has_role('instructeur'):
                from core.models import Instance
                config_instructeur = request.user.get_instance().get_instruction_mode()
                if config_instructeur == Instance.IM_ARRONDISSEMENT:
                    user_place = request.user.instructeur.get_prefecture().arrondissement
                    object_place = self.manif.ville_depart.arrondissement
                elif config_instructeur == Instance.IM_DEPARTEMENT:
                    user_place = request.user.get_instance().get_departement()
                    object_place = self.manif.ville_depart.get_departement()
                if user_place == object_place:
                    return super().dispatch(request, *args, **kwargs)
        elif self.origine() == 'organisateur':
            if request.user == self.manif.structure.organisateur.user:
                return super().dispatch(request, *args, **kwargs)
        return render(request, "core/access_restricted.html",
                      {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

    def get_success_url(self):
        referer = self.request.META.get('HTTP_REFERER')
        if self.origine() == 'instructeur':
            return self.manif.instruction.get_absolute_url()
        return self.manif.get_absolute_url()

    def get_form(self, form_class=None):
        form = super().get_form()
        if self.origine() == 'instructeur':
            del form.fields['pourquoi_organisateur']
        else:
            del form.fields['pourquoi_instructeur']
        return form

    def origine(self):
        referer = self.request.META.get('HTTP_REFERER')
        if referer and referer.split('/')[-3] == 'instructions':
            return 'instructeur'
        if referer:
            return 'organisateur'
        return ''

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manif = self.manif
        self.object.Longueur_totale_parcours = 0
        self.object.distance_site = 0
        return super().form_valid(form)


class SitesN2kAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les sites Natura 2000 selon le champ parent "autres département" sélectionné """
        # Sur la base de CommuneAjaxView de administrative_division
        if hasattr(self.request, 'session'):
            request_departement = self.request.session.get('extradata')
        else:
            request_departement = ''
        self.parent_value = make_iterable(self.parent_value)
        if request_departement:
            try:
                # Si le formulaire contient un attribut extradata, l'utiliser dans la cascade
                departement_id = Departement.objects.get(name=request_departement).pk
                self.parent_value.append(departement_id)
            except Exception:
                pass
        if isinstance(self.parent_value, (list, tuple)):
            result = []
            liste = N2kSite.objects.filter(departements__in=self.parent_value)
            for site in liste:
                result.append((str(site.id), site.__str__()))
        else:
            result = []
        return result


class ZonesRnrAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les zones RNR selon le champ parent "autres département" sélectionné """
        # Sur la base de CommuneAjaxView de administrative_division
        if hasattr(self.request, 'session'):
            request_departement = self.request.session.get('extradata')
        else:
            request_departement = ''
        self.parent_value = make_iterable(self.parent_value)
        if request_departement:
            try:
                # Si le formulaire contient un attribut extradata, l'utiliser dans la cascade
                departement_id = Departement.objects.get(name=request_departement).pk
                self.parent_value.append(departement_id)
            except Exception:
                pass
        if isinstance(self.parent_value, (list, tuple)):
            result = []
            liste = RnrZone.objects.filter(departements__in=self.parent_value)
            for site in liste:
                result.append((str(site.id), site.__str__()))
        else:
            result = []
        return result


class LieuxPdesiAJAXView(ChainedSelectChoicesView):
    """ Vue pour Ajax clever selects """

    def get_choices(self):
        """ Renvoyer les lieux PDESI selon le champ parent "autres département" sélectionné """
        # Sur la base de CommuneAjaxView de administrative_division
        if hasattr(self.request, 'session'):
            request_departement = self.request.session.get('extradata')
        else:
            request_departement = ''
        self.parent_value = make_iterable(self.parent_value)
        if request_departement:
            try:
                # Si le formulaire contient un attribut extradata, l'utiliser dans la cascade
                departement_id = Departement.objects.get(name=request_departement).pk
                self.parent_value.append(departement_id)
            except Exception:
                pass
        if isinstance(self.parent_value, (list, tuple)):
            result = []
            liste = PdesiLieu.objects.filter(departement__in=self.parent_value)
            for lieu in liste:
                result.append((str(lieu.id), lieu.__str__()))
        else:
            result = []
        return result
