# coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator

from evaluation_incidence.models import RnrZone, EvaluationRnr
from evaluation_incidence.forms import EvaluationRNRForm
from administrative_division.models.departement import Departement
from organisateurs.decorators import verifier_proprietaire
from evenements.models import Manif
from core.models import Instance


class RNRListe(ListView):
    """ Afficher la liste des réserves naturelles régionales """

    # Configuration
    model = RnrZone

    def get_queryset(self):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            return RnrZone.objects.filter(departements__name=departement).order_by('nom')
        return RnrZone.objects.all().order_by('nom')

    def get_context_data(self, **kwargs):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        context = super().get_context_data(**kwargs)
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            context['current_departement'] = departement
        context['departements'] = Departement.objects.all()
        return context


class RNREvalCreate(CreateView):
    """ Création d'évaluation RNR """

    # Configuration
    model = EvaluationRnr
    form_class = EvaluationRNRForm

    # Overrides
    @method_decorator(verifier_proprietaire())
    def dispatch(self, *args, **kwargs):
        self.manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if EvaluationRnr.objects.filter(manif=self.manif).exists():
            return redirect(self.manif.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dept'] = self.manif.ville_depart.get_departement().name
        context['zones'] = self.manif.zones_rnr.all()
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manif = self.manif
        return super().form_valid(form)


@method_decorator(verifier_proprietaire(), name='dispatch')
class RNREvalUpdate(UpdateView):
    """ Modification d'évaluation RNR """

    # Configuration
    model = EvaluationRnr
    form_class = EvaluationRNRForm

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['dept'] = self.get_object().manif.ville_depart.get_departement().name
        context['zones'] = self.get_object().manif.zones_rnr.all()
        return context
