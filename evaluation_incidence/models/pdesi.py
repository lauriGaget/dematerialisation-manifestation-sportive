# coding: utf-8
from django.db import models


class PdesiLieu(models.Model):
    """ Lieux inscrits dans le plan PDESI """

    # Champs
    nom = models.CharField("nom", max_length=255, unique=True)
    departement = models.ForeignKey('administrative_division.departement', verbose_name='département', on_delete=models.CASCADE)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.nom

    # Meta
    class Meta:
        verbose_name = "lieu inscrit au PDESI"
        verbose_name_plural = "lieux inscrits au PDESI"
        default_related_name = "lieupdesi"
        app_label = "evaluation_incidence"

