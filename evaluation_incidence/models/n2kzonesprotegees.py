# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import DepartementableMixin


class N2kSite(models.Model):
    """ Site Natura 2000 """

    # Choix
    TYPE_CHOICES = (('s', 'SIC'), ('z', 'ZPS'))

    # Champs
    nom = models.CharField("nom", max_length=255, unique=True)
    departements = models.ManyToManyField(to='administrative_division.Departement', verbose_name='départements concernés')
    site_type = models.CharField("type", max_length=1, choices=TYPE_CHOICES)
    index = models.CharField("fiche", max_length=9, unique=True)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.nom

    # Méta
    class Meta:
        verbose_name = "site natura 2000"
        verbose_name_plural = "sites natura 2000"
        default_related_name = "n2ksites"
        app_label = "evaluation_incidence"


class N2kOperateurSite(DepartementableMixin):
    """ Opérateur de site Natura 2000 """

    # Champs
    nom = models.CharField("nom", max_length=255)
    adresse = models.CharField("adresse", max_length=255, null=True, blank=True)
    commune = models.ForeignKey('administrative_division.commune', verbose_name="commune", on_delete=models.CASCADE, null=True, blank=True)
    email = models.EmailField('e-mail', max_length=200)
    # chargé de mission
    contacts = models.TextField(null=True, blank=True, help_text="prénom nom numéro de téléphone. Une ligne par contact")
    site = models.OneToOneField(N2kSite, related_name="operateursiten2k", verbose_name="site natura 2000", on_delete=models.CASCADE)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.nom

    # Meta
    class Meta:
        verbose_name = "opérateur de site natura 2000"
        verbose_name_plural = "opérateurs de site natura 2000"
        default_related_name = "n2koperateurssites"
        app_label = "evaluation_incidence"
