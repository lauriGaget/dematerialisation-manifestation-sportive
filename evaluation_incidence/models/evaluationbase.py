# coding: utf-8
from django.db import models

from evenements.models import Manif
from ..models.n2kconfiguration import Natura2kDepartementConfig, Natura2kSiteConfig
from ..models.rnrconfiguration import RnrZoneConfig


class EvaluationManif(models.Model):
    """ Évaluation de l'impact environnemental d'une manifestation """

    # Constantes
    COST_CHOICES = (('0', "moins de 5 000 €"), ('1', "entre 5 000 et 20 000 €"), ('2', "entre 20 000 et 100 000 €"), ('3', "plus de 100 000 €"))
    IMPACT_CHOICES = (('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'))

    # Champs
    manif = models.OneToOneField(Manif, verbose_name='manif', on_delete=models.CASCADE, null=True)
    # Description
    lieu = models.CharField("lieu-dit", max_length=255, blank=True)
    # Fréquence de la manifestation
    annuel = models.BooleanField("la manifestation a lieu chaque année", default=False)
    premiere_edition = models.BooleanField("première édition de la manifestation", default=False)
    autre_frequence = models.CharField("autre fréquence", max_length=255, blank=True)
    # Budget
    cout = models.CharField("coût de la manifestation", max_length=1, choices=COST_CHOICES, blank=True)
    # Localization
    Longueur_totale_parcours = models.PositiveIntegerField("longueur totale des parcours (km)", blank=True, null=True)
    # Conclusions
    impact_estime = models.CharField(
        "impact estimé de la manifestation",
        max_length=1,
        blank=True,
        choices=IMPACT_CHOICES,
        help_text="D'après les éléments mis en évidence dans ce formulaire, évaluer l'impact de "
                  "la manifestation en donnant un chiffre de 0 à 5 (0 correspondant à une "
                  "absence d'incidences et 5 à une incidence significative sur le ou les sites "
                  "concernés)",
    )

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de consultation du contenu """
        return Manif.objects.get_subclass(id=self.manif.id).get_absolute_url()

    @classmethod
    def evaluation_rnr_requise(cls, manif):
        """
        Renvoyer si l'encart d'évaluation RNR doit être affiché
        :param manif:
        :return: bool
        """
        # Si une évaluation est déjà créée
        fait = hasattr(manif, 'rnrevaluations') or hasattr(manif, 'rnrevaluation')
        # Critères de site
        condition_site = False
        for zone in manif.zones_rnr.all():                                                       # La manif passe sur un site rnr
            if RnrZoneConfig.objects.filter(rnrzone=zone).exists():                                # Est-ce qu'une config existe?
                config = RnrZoneConfig.objects.get(rnrzone=zone)
                if config.formulaire:
                    appliquer = False
                    for type in config.formulaire:
                        if type == manif.get_type_manif():
                            appliquer = True
                else:
                    appliquer = True
                if appliquer:
                    if config.seuil_participants and manif.nb_participants:
                        condition_site |= manif.nb_participants >= config.seuil_participants
                    if config.seuil_total and manif.nb_participants and manif.nb_organisateurs is not None and manif.nb_spectateurs is not None:
                        condition_site |= manif.nb_organisateurs + manif.nb_participants + manif.nb_spectateurs >= config.seuil_total

        return condition_site and not fait

    @classmethod
    def evaluation_n2k_requise(cls, manif, cause):
        """
        Renvoyer si l'encart d'évaluation Natura2000 doit être affiché
        :param manif:
        :return: bool
        """

        def application_criteres(type):
            condition = False
            if not type:
                return condition
            if type == 'vtm':
                if config.vtm_seuil_participants and manif.nb_participants:
                    condition |= manif.nb_participants >= config.vtm_seuil_participants
                if config.vtm_seuil_vehicules and manif.vehicules:
                    condition |= manif.vehicules >= config.vtm_seuil_vehicules
                if config.vtm_seuil_total and manif.nb_participants and manif.nb_organisateurs is not None and manif.nb_spectateurs is not None:
                    condition |= manif.nb_organisateurs + manif.nb_participants + manif.nb_spectateurs >= config.vtm_seuil_total
                return condition
            if type == 'nm':
                if config.nm_seuil_participants and manif.nb_participants:
                    condition |= manif.nb_participants >= config.nm_seuil_participants
                if config.nm_seuil_total and manif.nb_participants and manif.nb_organisateurs is not None and manif.nb_spectateurs is not None:
                    condition |= manif.nb_organisateurs + manif.nb_participants + manif.nb_spectateurs >= config.nm_seuil_total
                if config.nm_hors_circulation:
                    condition |= not manif.voie_publique
                return condition

        def decision():
            if manif.get_type_manif() in ["dvtm", "avtm", "avtmcir"]:   # manifestation VTM
                if config.vtm_formulaire:                               # Les critères VTM s'appliquent à un ou plusieurs types de manif
                    for type in config.vtm_formulaire:                  # sinon les critères s'appliquent
                        if type == manif.get_type_manif():              # la manif est dans la liste ?
                            if config.vtm_sur_siten2k:                  # Les critères VTM s'appliquent si la manif passe sur un site n2k
                                if not manif.sites_natura2000.all():
                                    return                              # la manif n'est pas sur un site n2k => next
                            return 'vtm'
                    return                                              # la manif n'est pas dans la liste => next
                if config.vtm_sur_siten2k:                              # Les critères VTM s'appliquent si la manif passe sur un site n2k
                    if not manif.sites_natura2000.all():
                        return                                          # la manif n'est pas sur un site n2k => next
                return 'vtm'
            else:
                if config.nm_formulaire:                                # manifestation NM, même logique
                    for type in config.nm_formulaire:
                        if type == manif.get_type_manif():
                            if config.nm_sur_siten2k:
                                if not manif.sites_natura2000.all():
                                    return
                            return 'nm'
                    return
                if config.nm_sur_siten2k:
                    if not manif.sites_natura2000.all():
                        return
                return 'nm'

        # Si une évaluation est déjà créée
        fait = hasattr(manif, 'natura2000evaluations') or hasattr(manif, 'n2kevaluation')
        # Critères nationaux, on teste tout
        # Demande_homologation n'est pas affiché dans les formulaires et est conservé pour une éventuelle utilisation
        condition_nat = manif.gros_budget or manif.lucratif or manif.delivrance_titre or \
                        manif.vtm_hors_circulation or manif.demande_homologation
        # Critères départementaux
        liste_dept = manif.get_departements_traverses()
        condition_dep = False
        origine = 'Critères nationaux'
        for dept in liste_dept:
            if  Natura2kDepartementConfig.objects.filter(instance=dept.get_instance()).exists():        # Est-ce qu'une config existe ?
                config = Natura2kDepartementConfig.objects.get(instance=dept.get_instance())
                if application_criteres(decision()):
                    origine = str(config)
                    condition_dep = True

        # Critères de site
        condition_site = False
        for site in manif.sites_natura2000.all():                                                       # La manif passe sur un site n2k
            if Natura2kSiteConfig.objects.filter(n2ksite=site).exists():                                # Est-ce qu'une config existe?
                config = Natura2kSiteConfig.objects.get(n2ksite=site)
                if not (config.charte_dispense_acceptee and manif.signature_charte_dispense_site_n2k):
                    if application_criteres(decision()):
                        origine = str(config)
                        condition_site = True

        if (condition_site or condition_dep or condition_nat) and not fait:
            if cause:
                return origine
            return True
        return False

    # Méta
    class Meta:
        abstract = True
        verbose_name = "évaluation d'une manifestation"
        verbose_name_plural = "évaluations de manifestations"
        app_label = 'evaluation_incidence'
