# coding: utf-8
from datetime import date

from django.db import models
from django_fsm import FSMField


class AvisBase(models.Model):
    """ Mixin pour les avis et préavis (+5 champs) """

    state = FSMField(default='created')  # Tout (pré)avis est à l'état 'created" par défaut
    request_date = models.DateField("Date de demande", default=date.today)
    reply_date = models.DateField("Date de retour", blank=True, null=True)
    favorable = models.BooleanField("Avis favorable ?", default=False, help_text="Si coché, l'avis rendu est considéré favorable")
    prescriptions = models.TextField("prescriptions", blank=True)

    # Meta
    class Meta:
        abstract = True
