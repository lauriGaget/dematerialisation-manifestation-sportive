# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django.db.models.aggregates import Count
from django_fsm import transition

from agreements.models.avis import AvisQuerySet, Avis
from core.models.instance import Instance


class GGDAvisQuerySet(AvisQuerySet):
    """ Queryset spécifique aux avis GGD """

    # Getter
    def without_edsr(self):
        """ Renvoyer les avis GGD sans sélection d'EDSR """
        return self.filter(concerned_edsr__isnull=True)

    def with_edsr(self):
        """ Renvoyer les avis GGD sans sélection d'EDSR """
        return self.filter(concerned_edsr__isnull=False)

    def without_cgd(self):
        """ Renvoyer les avis GGD où aucun CGD n'est sélectionné """
        return self.annotate(cgd_count=Count('concerned_cgd')).filter(cgd_count=0)


class GGDAvis(Avis):
    """ Avis GGD """

    # Champs
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True, related_name='ggdavis', on_delete=models.CASCADE)
    concerned_cgd = models.ManyToManyField('administration.cgd', verbose_name="CGD concernés")
    concerned_edsr = models.ForeignKey('administration.edsr', null=True, blank=True, verbose_name="EDSR concerné", on_delete=models.SET_NULL)
    objects = GGDAvisQuerySet.as_manager()

    # Overrides
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        manifestation = self.get_manifestation()
        departure_city = manifestation.departure_city
        departement = departure_city.get_departement()
        return ' - '.join([str(manifestation), ' '.join(['GGD', departement.name])])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:ggd_agreement_detail', kwargs={'pk': self.pk})

    def get_edsr(self):
        """ Renvoyer l'EDSR concerné par l'avis """
        return self.concerned_edsr

    def get_ggd(self):
        """ Renvoyer le GGD concerné par l'avis """
        return self.get_manifestation().departure_city.get_departement().ggd

    def get_agents(self):
        """ Renvoyer tous les agents de la chaîne de l'avis """
        edsr_agents = edsr_agentslocaux = []
        ggd_agents = self.get_manifestation().departure_city.get_departement().ggd.get_ggdagents()
        if self.concerned_edsr is not None:
            edsr_agents = self.get_edsr().get_edsr_agents()
            edsr_agentslocaux = self.get_edsr().edsragentslocaux.all()
        # Les agents concernés dépendent de la configuration d'application :
        # Un avis GGD n'est créé/émis que dans les configurations ggd_agentedsr et
        # ggd_agentlocaledsr. edsr ne concerne que la configuration pour les avis EDSR.
        if self.get_instance():
            config = self.get_instance().get_workflow_ggd()
            if config == Instance.WF_GGD_EDSR:
                return list(ggd_agents) + list(edsr_agents)
            elif config == Instance.WF_GGD_SUBEDSR:
                return list(ggd_agents) + list(edsr_agentslocaux)
        # Ne devrait pas se produire
        return ggd_agents

    def are_preavis_validated(self):
        """ Renvoyer si les préavis sont tous rendus """
        return super(GGDAvis, self).are_preavis_validated()

    def all_agreements_validated(self):
        """ Renvoie si l'(pré)avis EDSR est formaté, et si les préavis CGD sont rendus """
        cgd_acknowledged = self.are_preavis_validated()
        edsr_formatted = self.state == 'formatted' or self.get_instance().get_workflow_ggd() != Instance.WF_EDSR
        return cgd_acknowledged and edsr_formatted

    def edsr_or_ggd(self):
        config = self.get_instance().get_workflow_ggd()
        if config == Instance.WF_GGD_EDSR and self.state == 'formatted':
            return True
        elif config == Instance.WF_GGD_SUBEDSR and self.state == 'dispatched':
            return True
        else:
            return False

    # Action
    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        """ Dispatch """
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='formatted', conditions=[are_preavis_validated])
    def format(self):
        """
        Mettre en forme l'avis (uniquement si tous les préavis sont rendus

        Et consigner à tous les agents + envoyer des mails aux agents GGD
        """
        departement = self.get_manifestation().departure_city.get_departement()
        self.notify_format(agents=departement.ggd.get_ggdagents(), content_object=self.get_ggd())
        self.log_format(agents=self.get_agents())

    @transition(field='state', source='*', target='acknowledged', conditions=[all_agreements_validated, edsr_or_ggd])
    def acknowledge(self):
        """ Rendre l'avis, si les préavis sont rendus et l'avis formaté """
        self.reply_date = datetime.date.today()
        self.save()
        ggd = self.get_manifestation().departure_city.get_departement().ggd
        self.notify_ack(content_object=ggd)
        self.log_ack(agents=ggd.get_ggdagents())

    # Meta
    class Meta:
        verbose_name = "Avis GGD"
        verbose_name_plural = "Avis GGD"
        app_label = 'agreements'
        default_related_name = 'ggdaviss'
