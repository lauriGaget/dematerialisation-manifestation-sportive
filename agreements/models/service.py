# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet


class ServiceAvis(Avis):
    """ Avis de service administratif divers """

    # Champs
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True, related_name='serviceavis', on_delete=models.CASCADE)
    service = models.ForeignKey('administration.service', verbose_name="service", on_delete=models.CASCADE)
    objects = AvisQuerySet.as_manager()

    # Override
    def __str__(self):
        return ' - '.join([str(self.get_manifestation()), str(self.service)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL d'accès à l'avis """
        return reverse('agreements:service_agreement_detail', kwargs={'pk': self.pk})

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        return self.service.get_serviceagents()

    # Action
    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        # Notifier les instructeurs et les agents
        self.notify_ack(content_object=self.service)
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = "Avis service"
        verbose_name_plural = "Avis services"
        app_label = 'agreements'
        default_related_name = 'serviceaviss'
