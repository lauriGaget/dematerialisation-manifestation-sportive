# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet


class FederationAvisQuerySet(AvisQuerySet):
    """ Queryset des avis de fédération """

    def for_federation(self, federation):
        """
        Renvoyer tous les avis pour la fédération passée

        :param federation: objet de type administration.federation
        """
        return self.filter(authorization__manifestation__activite__discipline__federations=federation)


class FederationAvis(Avis):
    """ Avis de fédération sportive """

    # Champs
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True, related_name='federationavis', on_delete=models.CASCADE)
    objects = FederationAvisQuerySet.as_manager()

    # Override
    def __str__(self):
        manifestation = self.get_manifestation()
        return ' - '.join([str(manifestation), str(manifestation.get_federation())])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:federal_agreement_detail', kwargs={'pk': self.pk})

    def get_federation(self):
        """ Renvoyer la fédération sportive de l'avis """
        federation = self.authorization.get_manifestation().get_federation()
        return federation

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        agents = self.get_federation().federationagents.all()
        return agents

    # Action
    @transition(field='state', source='created', target='acknowledged')
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(content_object=self.get_federation())
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = "Avis fédération"
        verbose_name_plural = "Avis fédération"
        app_label = 'agreements'
        default_related_name = 'federationaviss'
