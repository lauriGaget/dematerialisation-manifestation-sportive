# coding: utf-8
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView

from core.util.permissions import require_role
from ..forms import *
from ..models import *


class BaseAcknowledgeView(UpdateView):
    """ Vue de base pour rendre un avis """

    # Configuration
    model = Avis
    form_class = AvisAcknowledgeForm
    template_name_suffix = '_ack_form'

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.state == 'acknowledged':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été validé.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.acknowledge()
            return super(BaseAcknowledgeView, self).form_valid(form)

    def get_initial(self):
        """ Renvoyer les valeurs de base du formulaire """
        initial = super().get_initial()
        initial['favorable'] = True
        return initial


class BaseFormatView(UpdateView):
    """ Vue de base pour mettre en forme un avis (étape avant la rendu) """

    # Configuration
    model = Avis
    form_class = AvisFormatForm
    template_name_suffix = '_format_form'

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.state == 'formatted':
            messages.error(self.request, "Aucune action effectuée : cet avis a déjà été mis en forme.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.format()
            return super(BaseFormatView, self).form_valid(form)


class BaseDispatchView(UpdateView):
    """ Vue de base pour envoyer les demandes de préavis """

    # Configuration
    template_name_suffix = '_dispatch_form'

    # Overrides
    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.state == 'dispatched':
            messages.error(self.request, "Aucune action effectuée : les demandes de pré-avis ont déjà été envoyées.")
            return HttpResponseRedirect(self.get_success_url())
        else:
            form.instance.dispatch()
            return super(BaseDispatchView, self).form_valid(form)


class BaseResendView(SingleObjectMixin, View):
    """ Vue de base pour renvoyer des demandes de préavis """

    # Configuration
    model = Avis

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        """
        Gérer la méthode d'entrée de la vue (GET, POST)

        Ici, on ne fait que protéger toutes les méthodes d'entrée pour ne donner
        accès à la page qu'au rôle instructeur
        """
        return super(BaseResendView, self).dispatch(*args, **kwargs)
