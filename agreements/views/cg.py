# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from administration.models.service import CGService
from agreements.views.base import BaseAcknowledgeView, BaseResendView, BaseDispatchView, BaseFormatView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class CGAvisDetail(DetailView):
    """ Vue de détail pour les avis CD """

    # Configuration
    model = CGAvis

    # Overrides
    @method_decorator(require_role(['cgagent', 'cgsuperieuragent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents CD uniquement """
        return super(CGAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class CGAvisDispatch(BaseDispatchView):
    """ Vue d'envoi des demandes de préavis """

    # Confioguration
    model = CGAvis
    form_class = CGAvisDispatchForm

    # Overrides
    @method_decorator(require_role(['cgagent', 'cgsuperieuragent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents CD uniquement """
        return super(CGAvisDispatch, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Filtrer les services dispo à ceux des départements traversés
        form.fields['concerned_services'].queryset = CGService.objects.filter(cg__departement__in=self.object.get_manifestation().get_crossed_departements())
        return form


class CGAvisFormat(BaseFormatView):
    """ Vue de mise en forme d'un avis CD """

    # Configuration
    model = CGAvis

    # Overrides
    @method_decorator(require_role(['cgagent', 'cgsuperieuragent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents CD uniquement """
        return super(CGAvisFormat, self).dispatch(*args, **kwargs)


class CGAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu de l'avis CD """

    # Configuration
    model = CGAvis

    # Overrides
    @method_decorator(require_role(['cgagent', 'cgsuperieuragent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents supérieurs CD uniquement """
        return super(CGAvisAcknowledge, self).dispatch(*args, **kwargs)


class CGAvisResend(BaseResendView):
    """ Vue de renvoi des demandes d'avis """

    # Configuration
    model = CGAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.authorization.manifestation.departure_city.get_departement().cg.cgagents.all())
        instance.log_resend(recipient=instance.get_cg().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
