# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from agreements.views.base import BaseAcknowledgeView, BaseResendView
from core.util.permissions import require_role
from ..models import *


class FederationAvisDetail(DetailView):
    """ Vue de détail d'un avis """

    # Configuration
    model = FederationAvis

    # Overrides
    @method_decorator(require_role('federationagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents fédération uniquement """
        return super(FederationAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class FederationAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu d'avis """

    # Configuration
    model = FederationAvis

    # Overrides
    @method_decorator(require_role('federationagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents fédération """
        return super(FederationAvisAcknowledge, self).dispatch(*args, **kwargs)


class FederationAvisResend(BaseResendView):
    """ Vue de renvoi """

    # Configuration
    model = FederationAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.get_federation().get_agents())
        instance.log_resend(recipient=str(instance.get_federation()))
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
