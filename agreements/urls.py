# coding: utf-8
from django.urls import path, re_path

from .views import *


app_name = 'agreements'
urlpatterns = [
    path('dashboard/', Dashboard.as_view(), name='dashboard'),
    path('archives/', Archives.as_view(), name='archives'),

    # Service
    re_path('service/(?P<pk>\d+)/acknowledge/', ServiceAvisAcknowledge.as_view(), name='service_agreement_acknowledge'),
    re_path('service/(?P<pk>\d+)/resend/', ServiceAvisResend.as_view(), name='service_agreement_resend'),
    re_path('service/(?P<pk>\d+)/', ServiceAvisDetail.as_view(), name='service_agreement_detail'),

    # Fédération
    re_path('federation/(?P<pk>\d+)/acknowledge/', FederationAvisAcknowledge.as_view(), name='federal_agreement_acknowledge'),
    re_path('federation/(?P<pk>\d+)/resend/', FederationAvisResend.as_view(), name='federal_agreement_resend'),
    re_path('federation/(?P<pk>\d+)/', FederationAvisDetail.as_view(), name='federal_agreement_detail'),

    # Mairie
    re_path('townhall/(?P<pk>\d+)/acknowledge/', MairieAvisAcknowledge.as_view(), name='townhall_agreement_acknowledge'),
    re_path('townhall/(?P<pk>\d+)/resend/', MairieAvisResend.as_view(), name='townhall_agreement_resend'),
    re_path('townhall/(?P<pk>\d+)/', MairieAvisDetail.as_view(), name='townhall_agreement_detail'),

    # EDSR
    re_path('edsr/(?P<pk>\d+)/dispatch/', EDSRAvisDispatch.as_view(), name='edsr_agreement_dispatch'),
    re_path('edsr/(?P<pk>\d+)/format/', EDSRAvisFormat.as_view(), name='edsr_agreement_format'),
    re_path('edsr/(?P<pk>\d+)/acknowledge/', EDSRAvisAcknowledge.as_view(), name='edsr_agreement_acknowledge'),
    re_path('edsr/(?P<pk>\d+)/resend/', EDSRAvisResend.as_view(), name='edsr_agreement_resend'),
    re_path('edsr/(?P<pk>\d+)/', EDSRAvisDetail.as_view(), name='edsr_agreement_detail'),

    # GGD
    re_path('ggd/(?P<pk>\d+)/dispatch/', GGDAvisDispatch.as_view(), name='ggd_agreement_dispatch'),
    re_path('ggd/(?P<pk>\d+)/pass/', GGDAvisPass.as_view(), name='ggd_agreement_pass'),
    re_path('ggd/(?P<pk>\d+)/format/', GGDAvisFormat.as_view(), name='ggd_agreement_format'),
    re_path('ggd/(?P<pk>\d+)/acknowledge/', GGDAvisAcknowledge.as_view(), name='ggd_agreement_acknowledge'),
    re_path('ggd/(?P<pk>\d+)/resend/', GGDAvisResend.as_view(), name='ggd_agreement_resend'),
    re_path('ggd/(?P<pk>\d+)/', GGDAvisDetail.as_view(), name='ggd_agreement_detail'),

    # SDIS
    re_path('sdis/(?P<pk>\d+)/dispatch/', SDISAvisDispatch.as_view(), name='sdis_agreement_dispatch'),
    re_path('sdis/(?P<pk>\d+)/acknowledge/', SDISAvisAcknowledge.as_view(), name='sdis_agreement_acknowledge'),
    re_path('sdis/(?P<pk>\d+)/resend/', SDISAvisResend.as_view(), name='sdis_agreement_resend'),
    re_path('sdis/(?P<pk>\d+)/', SDISAvisDetail.as_view(), name='sdis_agreement_detail'),

    # DDSP
    re_path('ddsp/(?P<pk>\d+)/dispatch/', DDSPAvisDispatch.as_view(), name='ddsp_agreement_dispatch'),
    re_path('ddsp/(?P<pk>\d+)/acknowledge/', DDSPAvisAcknowledge.as_view(), name='ddsp_agreement_acknowledge'),
    re_path('ddsp/(?P<pk>\d+)/resend/', DDSPAvisResend.as_view(), name='ddsp_agreement_resend'),
    re_path('ddsp/(?P<pk>\d+)/', DDSPAvisDetail.as_view(), name='ddsp_agreement_detail'),

    # CG
    re_path('cg/(?P<pk>\d+)/dispatch/', CGAvisDispatch.as_view(), name='cg_agreement_dispatch'),
    re_path('cg/(?P<pk>\d+)/format/', CGAvisFormat.as_view(), name='cg_agreement_format'),
    re_path('cg/(?P<pk>\d+)/acknowledge/', CGAvisAcknowledge.as_view(), name='cg_agreement_acknowledge'),
    re_path('cg/(?P<pk>\d+)/resend/', CGAvisResend.as_view(), name='cg_agreement_resend'),
    re_path('cg/(?P<pk>\d+)/', CGAvisDetail.as_view(), name='cg_agreement_detail'),
]
