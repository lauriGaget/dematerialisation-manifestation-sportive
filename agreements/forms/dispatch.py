# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout

from agreements.models import CGAvis
from agreements.models import DDSPAvis
from agreements.models import EDSRAvis
from agreements.models import GGDAvis
from agreements.models import SDISAvis
from core.forms.base import GenericForm


class DDSPAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(DDSPAvisDispatchForm, self).__init__(*args, **kwargs)
        self.fields['commissariats_concernes'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs communes concernées"}
        self.helper.layout = Layout(
            'commissariats_concernes',
            FormActions(Submit('save', "envoyer les demandes de pré-avis".capitalize()))
        )

    # Meta
    class Meta:
        model = DDSPAvis
        fields = ['commissariats_concernes']


class EDSRAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(EDSRAvisDispatchForm, self).__init__(*args, **kwargs)
        self.fields['concerned_cgd'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs CGD"}
        self.helper.layout = Layout(
            'concerned_cgd',
            FormActions(
                Submit('save', "envoyer les demandes de pré-avis".capitalize())
            )
        )

    class Meta:
        model = EDSRAvis
        fields = ['concerned_cgd']


class GGDAvisPassForm(GenericForm):
    """ Formulaire de modification d'avis en vue de le passer à l'agent EDSR """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(GGDAvisPassForm, self).__init__(*args, **kwargs)
        self.fields['concerned_edsr'].widget.attrs = {'data-placeholder': "Choisissez l'EDSR concerné"}
        self.helper.layout = Layout(
            'concerned_edsr',
            FormActions(Submit('save', "Envoyer l'avis à l'EDSR".capitalize()))
        )

    # Meta
    class Meta:
        model = GGDAvis
        fields = ['concerned_edsr']


class GGDAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(GGDAvisDispatchForm, self).__init__(*args, **kwargs)
        self.fields['concerned_cgd'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs CGD concernées"}
        self.fields['concerned_edsr'].widget.attrs = {'data-placeholder': "Choisissez l'EDSR concerné"}
        self.fields['concerned_edsr'].required = True
        self.helper.layout = Layout(
            'concerned_cgd',
            'concerned_edsr',
            FormActions(Submit('save', "Envoyer la demande de préavis".capitalize()))
        )

    # Meta
    class Meta:
        model = GGDAvis
        fields = ('concerned_cgd', 'concerned_edsr')


class SDISAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(SDISAvisDispatchForm, self).__init__(*args, **kwargs)
        self.fields['compagnies_concernees'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs compagnies concernées"}
        self.helper.layout = Layout(
            'compagnies_concernees',
            FormActions(
                Submit('save', "envoyer les demandes de pré-avis".capitalize())
            )
        )

    class Meta:
        model = SDISAvis
        fields = ["compagnies_concernees"]


class CGAvisDispatchForm(GenericForm):
    """ Formulaire de dispatchage d'avis """

    def __init__(self, *args, **kwargs):
        super(CGAvisDispatchForm, self).__init__(*args, **kwargs)
        self.fields['concerned_services'].widget.attrs = {'data-placeholder': "Choisissez un ou plusieurs services concernés"}
        self.helper.layout = Layout(
            'concerned_services',
            FormActions(
                Submit('save', "envoyer les demandes de pré-avis".capitalize())
            )
        )

    class Meta:
        model = CGAvis
        fields = ['concerned_services']
