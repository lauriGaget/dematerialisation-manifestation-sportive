# coding: utf-8
from datetime import date

from django.urls import reverse

from administration.factories import *
from agreements.listeners.notify import notify_federal_agreement
from agreements.tests.base import AvisTestsBase
from core.models.instance import Instance
from ..factories import *


class FederationAvisTests(AvisTestsBase):
    """ Tests des avis de Fédération """

    # Configuration
    def setUp(self):
        super().setUp()

    # Tests
    def test_access_url(self):
        """ Tester les URLs d'accès """
        avis = FederationAvisFactory.create()
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:federal_agreement_detail', kwargs={'pk': avis.pk}))

    def test_deadlines(self):
        """ Tester les dates limites pour les avis Fédération (délais spécifiques aux fédérations) """
        avis = FederationAvisFactory.create()
        expected_deadline = date.today() + Instance.get_federation_avis_delay()
        self.assertEqual(avis.get_deadline(), expected_deadline)
        self.assertEqual(avis.delay_exceeded(), False)

    def test_workflow(self):
        """ Tester la création des avis Fédération """
        self.assertEqual(self.authorization.get_avis_count(), 1)  # Dans tous les cas il existe un avis Fédération

    def test_life_cycle(self):
        """ Tester le cycle de validation d'un avis Fédération """
        # L'agent et l'instructeur sont créés après l'autorisation, il n'y aura pas de notification pour la création d'avis et d'uatorisation
        FederationAgentFactory.create(federation=self.manifestation.get_federation())  # créer un agent de la fédération
        InstructeurFactory(prefecture=self.prefecture)
        MairieAgentFactory(commune=self.commune)
        avis = self.authorization.avis.first().federationavis  # Récupérer l'avis fédération créé automatiquement pour l'autorisation
        notify_federal_agreement(sender=FederationAvis, created=True, instance=avis)  # C'est nécessaire : la fonction n'a pas été appelée

        # Vérifier qu'un seul agent existe
        self.assertEqual(self.manifestation.get_federation().get_agent_count(), 1)

        # Vérifier que les agents (1 seul) ont reçu leur notification (pour l'avis Fédération) grâce à "notify_federal_agreement"
        for federation_agent in self.manifestation.get_federation().get_agents():
            self.assertEqual(federation_agent.user.notifications.all().count(), 1)

        # Vérifier qu'une nouvelle notification n'est pas envoyée si l'avis est à nouveau sauvegardé
        avis.save()
        for federation_agent in self.manifestation.get_federation().get_agents():
            self.assertEqual(federation_agent.user.notifications.all().count(), 1)

        # Vérifier que l'avis est correctement rendu et à la date du jour
        avis.acknowledge()
        self.assertEqual(avis.reply_date, date.today())

        # Vérifier qu'une action "avis rendu" a bien été enregistrée pour les agents de la fédération
        for federation_agent in self.manifestation.get_federation().get_agents():
            self.assertEqual(federation_agent.user.actions.all().count(), 1)
            self.assertEqual(federation_agent.user.actions.first().action, 'avis rendu')

        # Vérifier qu'aucune nouvelle notification n'existe pour les instructeurs
        for instructeur in self.prefecture.get_instructeurs():
            self.assertEqual(instructeur.user.notifications.all().count(), 0)  # pas pour la création d'autorisation mais pour le rendu d'avis

        # Vérifier qu'une nouvelle notification existe pour les instructeurs en mairie
        for agentmairie in self.commune.get_mairieagents():
            self.assertEqual(agentmairie.user.notifications.all().count(), 1)  # pas pour la création d'autorisation mais pour le rendu d'avis
            self.assertEqual(agentmairie.user.notifications.last().content_object,
                             self.manifestation.get_federation())  # voir agreements.federation.acknowledge

    def test_methods(self):
        """ Tester que les méthodes ne renvoient pas d'erreur d'emblée """
        try:
            # Test pour le commit 82969cc
            FederationAvis.objects.for_federation(self.manifestation.get_federation())
        except:
            self.fail("La méthode a provoqué une erreur")
