# coding: utf-8

from django.urls import reverse

from agreements.tests.base import AvisTestsBase
from core.models.instance import Instance
from ..factories import *


class SDISAvisTests(AvisTestsBase):
    """ Tests des avis SDIS """

    # Tests
    def test_access_url(self):
        avis = SDISAvisFactory.create(authorization=self.authorization)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:sdis_agreement_detail', kwargs={'pk': avis.pk}))

    def test_preavis_validated(self):
        """ Tester l'état des préavis """
        avis = SDISAvisFactory.create(authorization=self.authorization)  # aucun préavis créé automatiquement
        self.assertTrue(avis.are_preavis_validated())

    def test_workflow_sdis(self):
        self.instance.workflow_sdis = Instance.WF_SDIS  # Utiliser un workflow SDIS par défaut
        self.instance.save()
        self.authorization.sdis_concerned = True  # Demander la création d'un avis SDIS
        self.authorization.save()
        self.assertEqual(self.authorization.get_avis_count(), 2)  # Avis Fédération (toujours) + avis SDIS
