var fontawesome = '';
var fontawesomeIcons = '';
var custum_ico_cache = "";


function klik(el) {
    document.getElementsByClassName('fontawesomeClass')[0].getElementsByTagName('input')[0].value = el.getAttribute('title');
    a = document.getElementById('fontawesome');
    a = a.getElementsByTagName('a');
    for (i = 0; i < a.length; i++) {
        a[i].className = '';
    }
    el.className += 'active';
};

function searchIcon(val) {
    document.getElementById('fontawesome').innerHTML="";

     let meh ='<a href="#" onclick="klik(this);return false;" title="fa-' + val + '"><span class=" active fa-' + val + '"></span></a>'
    document.getElementById('fontawesome').innerHTML=meh;
     setCheckboxes();

};

function showCustomIcon(val) {
    document.getElementById('fontawesome').innerHTML = "";
    let meh = '<a href="#" onclick="klik(this);return false;" title="' + val + '"><i class="' + val + ' active"></i></a>'
    document.getElementById('fontawesome').innerHTML = meh;
    AfficherIcones();
    custum_ico_cache = document.getElementById('fontawesome').children[0].children[0].className

};

function setSpanColor(color) {
    el = document.getElementById('fontawesome');
    el = el.getElementsByTagName('span');
    for (i = 0; i < el.length; i++) {
        el[i].setAttribute('style', 'color:' + color)
    }
};

function setCheckboxes() {
    klases = '';
    klases += document.getElementsByClassName('spinning')[0].getElementsByTagName('input')[0].checked ? ' fa-spin' : klases;
    klases += document.getElementsByClassName('fixedWidth')[0].getElementsByTagName('input')[0].checked ? ' fa-fw' : klases;
    klases += document.getElementsByClassName('bordered')[0].getElementsByTagName('input')[0].checked ? ' fa-border' : klases;
    klases += ' ' + document.getElementsByClassName('flippedRotation')[0].getElementsByTagName('select')[0].value;
    klases += ' ' + document.getElementsByClassName('type')[0].getElementsByTagName('select')[0].value;
    el = document.getElementById('fontawesome');
    el = el.getElementsByTagName('span');
    for (i = 0; i < el.length; i++) {
        el[i].className = el[i].parentNode.getAttribute('title') + klases;
    }
};

function in_array(needle, haystack) {
    for (var i in haystack) {
        if (haystack[i] == needle) return true;
    }
    return false;
};
CKEDITOR.dialog.add('fontawesomeDialog', function(editor) {
    return {
        title: 'Insert Font Awesome',
        minWidth: 600,
        minHeight: 200,
        resizable: false,
        contents: [{
            id: 'insertFontawesome',
            label: 'insertFontawesome',
            elements: [{
                type: 'hbox',
                widths: ['33%', '33%', '33%'],
                children: [{
                    type: 'hbox',
                    widths: ['75%', '25%'],
                    children: [{
                        type: 'text',
                        id: 'colorChooser',
                        className: 'colorChooser',
                        label: 'Color',
                        onKeyUp: function(e) {
                            setSpanColor(e.sender.$.value);
                        },
                        setup: function(widget) {
                            color = widget.data.color != '' ? widget.data.color : '#000000';
                            this.setValue(color);
                            setSpanColor(color);
                        },
                        commit: function(widget) {
                            widget.setData('color', this.getValue());
                        }
                    }, {
                        type: 'button',
                        label: 'Select',
                        style: 'margin-top:1.35em',
                        onClick: function() {
                            editor.getColorFromDialog(function(color) {
                                document.getElementsByClassName('colorChooser')[0].getElementsByTagName('input')[0].value = color;
                                setSpanColor(color);
                            }, this);
                        }
                    }]
                }, {
                    type: 'text',
                    id: 'size',
                    className: 'size',
                    label: 'Size',
                    setup: function(widget) {
                        this.setValue(widget.data.size);
                    },
                    commit: function(widget) {
                        widget.setData('size', this.getValue());
                    }
                }, {
                    type: 'text',
                    id: 'customIcon',
                    className: 'customIcon',
                    label: 'Icônes personnalisées',
                    onKeyUp: function(e) {
                        showCustomIcon(e.sender.$.value);
                    },
                    commit: function(widget) {
                        widget.setData('customIcon', this.getValue());
                    }
                }]
            }, {
                type: 'hbox',
                widths: ['25%', '25%', '25%', '25%'],
                children: [{
                    type: 'checkbox',
                    id: 'spinning',
                    className: 'spinning cke_dialog_ui_checkbox_input',
                    label: 'Spinning',
                    value: 'true',
                    setup: function(widget) {
                        var klases = widget.data.class;
                        klases = klases.split(' ');
                        document.getElementsByClassName('spinning')[0].getElementsByTagName('input')[0].checked = in_array('fa-spin', klases) ? true : false;
                        setCheckboxes();
                    },
                    onClick: function() {
                        setCheckboxes();
                    }
                }, {
                    type: 'checkbox',
                    id: 'fixedWidth',
                    className: 'fixedWidth cke_dialog_ui_checkbox_input',
                    label: 'Fixed Width',
                    value: 'true',
                    setup: function(widget) {
                        var klases = widget.data.class;
                        klases = klases.split(' ');
                        document.getElementsByClassName('fixedWidth')[0].getElementsByTagName('input')[0].checked = in_array('fa-fw', klases) ? true : false;
                        setCheckboxes();
                    },
                    onClick: function() {
                        setCheckboxes();
                    }
                }, {
                    type: 'checkbox',
                    id: 'bordered',
                    className: 'bordered cke_dialog_ui_checkbox_input',
                    label: 'Bordered',
                    value: 'true',
                    setup: function(widget) {
                        var klases = widget.data.class;
                        klases = klases.split(' ');
                        document.getElementsByClassName('bordered')[0].getElementsByTagName('input')[0].checked = in_array('fa-border', klases) ? true : false;
                        setCheckboxes();
                    },
                    onClick: function() {
                        setCheckboxes();
                    }
                }, {
                    type: 'select',
                    id: 'flippedRotation',
                    className: 'flippedRotation cke_dialog_ui_checkbox_input',
                    label: 'Flipping and Rotating',
                    items: [
                        ['Normal', ''],
                        ['Rotate 90', 'fa-rotate-90'],
                        ['Rotate 180', 'fa-rotate-180'],
                        ['Rotate 270', 'fa-rotate-270'],
                        ['Flip Horizontal', 'fa-flip-horizontal'],
                        ['Flip Vertical', 'fa-flip-vertical']
                    ],
                    setup: function(widget) {
                        this.setValue(widget.data.flippedRotation ? widget.data.flippedRotation : '');
                    },
                    commit: function(widget) {
                        widget.setData('flippedRotation', this.getValue());
                    },
                    onClick: function() {
                        setCheckboxes();
                    }
                },
                {
                    type: 'select',
                    id: 'type',
                    className: 'type cke_dialog_ui_checkbox_input',
                    label: "type d'icone",
                    default: "fas",
                    items: [
                        ['Solid', 'fas'],
                        ['Regular', 'far'],
                        ['Light', 'fal']
                    ],
                    setup: function(widget) {
                        this.setValue(widget.data.type ? widget.data.type : 'fas');
                    },
                    commit: function(widget) {
                        widget.setData('type', this.getValue());
                    },
                    onClick: function() {
                        setCheckboxes();
                    }
                }]
            }, {
                type: 'text',
                id: 'fontawesomeSearch',
                className: 'fontawesomeSearch cke_dialog_ui_input_text',
                label: 'Search',
                onKeyUp: function(e) {
                    searchIcon(e.sender.$.value);
                }
            }, {
                type: 'html',
                html: '<a href="https://fontawesome.com/icons?d=gallery" target="_blank">Gallery Font Awesome\n</a>'
            }, {
                type: 'text',
                id: 'fontawesomeClass',
                className: 'fontawesomeClass',
                style: 'display:none',
                setup: function(widget) {
                    var klases = '';
                    if (widget.data.class != '') {
                        klases = widget.data.class;
                        klases = klases.split(' ');
                        in_array('fa-border', klases) ? klases.splice(klases.indexOf('fa-border'), 1) : '';
                        in_array('fa-fw', klases) ? klases.splice(klases.indexOf('fa-fw'), 1) : '';
                        in_array('fa-spin', klases) ? klases.splice(klases.indexOf('fa-spin'), 1) : '';
                        in_array('fa-rotate-90', klases) ? klases.splice(klases.indexOf('fa-rotate-90'), 1) : '';
                        in_array('fa-rotate-180', klases) ? klases.splice(klases.indexOf('fa-rotate-180'), 1) : '';
                        in_array('fa-rotate-270', klases) ? klases.splice(klases.indexOf('fa-rotate-270'), 1) : '';
                        in_array('fa-flip-horizontal', klases) ? klases.splice(klases.indexOf('fa-flip-horizontal'), 1) : '';
                        in_array('fa-flip-vertical', klases) ? klases.splice(klases.indexOf('fa-flip-vertical'), 1) : '';
                        klases = klases.join(' ');
                    }
                    this.setValue(klases);
                },
                commit: function(widget) {
                    var klases = '';
                    if (!document.getElementsByClassName('customIcon')[0].getElementsByTagName('input')[0].value)  {
                        klases += document.getElementsByClassName('spinning')[0].getElementsByTagName('input')[0].checked ? ' fa-spin' : klases;
                        klases += document.getElementsByClassName('fixedWidth')[0].getElementsByTagName('input')[0].checked ? ' fa-fw' : klases;
                        klases += document.getElementsByClassName('bordered')[0].getElementsByTagName('input')[0].checked ? ' fa-border' : klases;
                        klases += ' ' + document.getElementsByClassName('flippedRotation')[0].getElementsByTagName('select')[0].value;
                        klases += ' ' + document.getElementsByClassName('type')[0].getElementsByTagName('select')[0].value;
                    }
                    else {
                        klases = custum_ico_cache
                    }
                    a = document.getElementById('fontawesome');
                    a = a.getElementsByTagName('a');
                    widget.setData('class', a[0].children[0].className +" "+ klases);
                }
            }, {
                type: 'html',
                html: '<div id="fontawesome">' + fontawesomeIcons + '</div>'
            }]
        }],
        onOk: function() {
            glyphs = document.getElementById('fontawesome');
            glyphs = glyphs.getElementsByTagName('a');

            for (i = 0; i < glyphs.length; i++) {
                glyphs[i].firstChild.className = glyphs[i].getAttribute('title');
                glyphs[i].className = '';
                glyphs[i].style.display = '';
                if (glyphs[i].getElementsByTagName('span')[0]) {
                    glyphs[i].getElementsByTagName('span')[0].style.color = '';
                }
            }
        }
    }
});