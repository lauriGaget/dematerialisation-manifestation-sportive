/**
 * Created by david on 19/10/2017.
 */


$(document).ready(function () {

    // bof Tooltip
    $('[data-toggle="tooltip"]').tooltip({'html': true});
    $('select').chosen();
    $('form').preventDoubleSubmission();
    // eof Tooltip

    // bof Checkbox décalé avec django2.0
    $('.col--offset-').addClass('col-sm-offset-3');
    // eof Checkbox décalé avec django2.0

    // bof Changer les boutons radio par des switches bootstrap (librairie tierce)
    //$.fn.bootstrapSwitch.defaults.onText = 'OUI';
    //$.fn.bootstrapSwitch.defaults.offText = 'NON';
    //$("[type='checkbox']").bootstrapSwitch();
    //$("[type='checkbox']").bootstrapSwitch('size', 'mini');
    // eof Changer les boutons radio par des switches bootstrap

    // bof Retour en haut de page
    // Affichage de la flêche
    $(window).scroll(function() {
        if($(window).scrollTop() == 0){
            $('#scrollToTop').fadeOut("fast");
        } else {
            $('#scrollToTop').fadeIn("slow");
        }
    });
    // Action de retour en haut de page
    $('#scrollToTop a').click(function(event){
        event.preventDefault();
        $('html, body').animate({scrollTop:0},800 , 'swing');
        return false;
    });
    // eof Affichage de la flêche Retour en haut de page

    $('.attendre-archive').one("click", function () {
        $('h1').append('<div class="float-right attendre">Recherche des archives en cours. Merci de patienter... <i class="far fa-spinner fa-spin"></i></div>');
        $('.page-header h1').css('overflow', 'hidden');
        $('.attendre').animate({
            width: '500px'
        }, 2000, "linear")
    });

});