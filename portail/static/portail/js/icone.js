function AfficherIcones() {

    /*  Ce fichier centralise l'affichage des toutes les icônes (balise <i>).
        Son fonctionnement repose sur l'utilisation du nom d'une classe personnalisée : <i class="user"></i>
        Le code ci-dessous parcours toutes les balises <i> de la page, si la 1ère classe définie est identifiée,
        alors la balise est remplacée par sa nouvelle définition.
        NB : les classes de la balise d'origine sont automatiquement réaffectées à la nouvelle.
    */

    $("i").each(function() {

         let new_i = null;
         let title;
         let select;
         let swt;
         // Si l'attribut data-toggle est présent, c'est que la balise est dèjà transformée
         if ($(this).attr("data-toggle")) {
             swt = "echappe";
         } else {
             swt = $(this)[0].classList[0];
         }
        switch (swt) {

            // Pictos généraux

            case "utilisateur":
                new_i = '<i class="fas fa-user fa-fw"></i>';
                break;

            case "organisateur":
                new_i = '<i class="fas fa-user-ninja fa-fw" title="Organisateur" data-toggle="tooltip"></i>';
                break;

            case "instructeur":
                new_i = '<i class="fas fa-user-secret fa-fw" title="Instructeur" data-toggle="tooltip"></i>';
                break;

            case "agent-departemental":
                new_i = '<i class="fas fa-user-tie fa-fw"></i>';
                break;

            case "agent-local":
                new_i = '<i class="fas fa-street-view fa-fw"></i>';
                break;

            case "agent-mairie":
                new_i = '<i class="fas fa-user-hard-hat fa-fw"></i>';
                break;

            case "agent-federation":
                new_i = '<i class="fas fa-transporter fa-fw"></i>';
                break;

            case "structure":
                new_i = '<i class="fas fa-building fa-fw" title="Structure" data-toggle="tooltip"></i>';
                break;

            case "password":
                new_i = '<i class="fas fa-lock fa-fw"></i>';
                break;

            case "tableau-de-bord":
                new_i = '<i class="fas fa-tachometer-alt fa-fw"></i>';
                break;

            case "aide":
                new_i = '<i class="fas fa-question-square fa-fw"></i>';
                break;

            case "aide-contextuelle":
                new_i = '<i class="fas fa-info-circle fa-fw" style="color: #9AA400;"></i>';
                break;

            case "aide-contextuelle-negatif":
                title = ($(this).data('titre')) ? "Dossier déposée le " + $(this).data('titre') : "Votre dossier a déjà été déposée";
                new_i = '<i class="fas fa-info-circle fa-fw" title="' + title + '" data-toggle="tooltip" \
                                style="color: #E0D090; background-color: #9AA400; "></i>';
                break;

            case "nouveaute":
                new_i = '<i class="far fa-bullhorn fa-fw"></i>';
                break;

            case "historique":
                new_i = '<i class="fas fa-history fa-fw"></i>';
                break;

            case "statistiques":
                new_i = '<i class="fas fa-chart-bar fa-fw"  title="Statistiques" data-toggle="tooltip"></i>';
                break;

            case "suivi":
                new_i = '<i class="fas fa-random fa-fw"></i>';
                break;

            case "lien":
                new_i = '<i class="fas fa-link fa-fw"></i>';
                break;

            case "spinner":
                new_i = '<i class="far fa-spinner fa-spin"></i>';
                break;

            case "attention":
                new_i = '<i class="fas fa-exclamation-triangle text-danger fa-fw"></i>';
                break;

            case "automatic":
                new_i = '<i class="far fa-magic"></i></i>';
                break;

            case "suivant":
                new_i = '<i class="fas fa-caret-right"></i>';
                break;

            case "telecharger":
                new_i = '<i class="far fa-download fa-fw"></i>';
                break;

            case "outil_carto":
                new_i = '<i class="fas fa-drafting-compass" title="Outil de cartographie" data-toggle="tooltip"></i>';
                break;

            // Pictos liés aux éléments des dossiers

            case "dossier":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                if (title != ""){
                    new_i = '<i class="fas fa-folder fa-fw" title="' + title + '" data-toggle="tooltip"></i>';
                } else {
                    new_i = '<i class="fas fa-folder fa-fw"></i>';
                }
                break;

            case "dossier-annexe":
                new_i = '<i class="fas fa-layer-group fa-fw"></i>';
                break;

            case "declaration":
                new_i = '<span class="fa-layers"> \
                    <i class="fas fa-folder fa-lg" style="color: #015A70;"></i> \
                    <span class="fa-layers-text fa-inverse">D</span> \
                    </span>';
                break;

            case "autorisation":
                new_i = '<span class="fa-layers"> \
                    <i class="fas fa-folder fa-lg" style="color: #015A70;"></i> \
                    <span class="fa-layers-text fa-inverse">A</span> \
                    </span>';
                break;

            case "detail":
                new_i = '<i class="far fa-book fa-fw"></i>';
                break;

            case "feuille":
                new_i = '<i class="far fa-leaf fa-fw"></i>';
                break;

            case "feuille-verte":
                new_i = '<i class="fas fa-leaf fa-fw text-success"></i>';
                break;

            case "doc":
                new_i = '<i class="far fa-paperclip fa-fw"></i>';
                break;

            case "pdf":
                new_i = '<i class="far fa-file-pdf fa-fw"></i>';
                break;

            case "info-compl":
                new_i = '<i class="far fa-sticky-note fa-fw"></i>';
                break;

            case "archive":
                new_i = '<i class="far fa-file-archive fa-fw"></i>';
                break;

            case "avis":
                new_i = '<i class="far fa-key fa-fw" title="Avis" data-toggle="tooltip"></i>';
                break;

            case "preavis":
                new_i = '<i class="far fa-thumbs-up fa-fw" title="Préavis" data-toggle="tooltip"></i>';
                break;

            case "arrete":
                new_i = '<i class="far fa-file-certificate fa-fw""></i>';
                break;

            case "arrete-circul":
                new_i = '<i class="fas fa-traffic-cone fa-fw"></i>';
                break;

            case "recepisse":
                new_i = '<i class="far fa-shield-check fa-fw"></i>';
                break;

            case "message":
                new_i = '<i class="far fa-envelope fa-fw"></i>';
                break;

            case "date":
                new_i = '<i class="far fa-calendar-alt fa-fw"></i>';
                break;

            case "carto":
                new_i = '<i class="far fa-map-marked-alt fa-fw"></i>';
                break;

            case "ville":
                new_i = '<i class="far fa-map-marker-alt fa-fw"></i>';
                break;

            case "activite":
                new_i = '<i class="far fa-pennant fa-fw" title="Activité" data-toggle="tooltip"></i>';
                break;

            case "telephone":
                new_i = '<i class="far fa-phone fa-fw"></i>';
                break;

            case "euro":
                new_i = '<i class="far fa-euro-sign fa-fw"></i>';
                break;

            case "lieu":
                new_i = '<i class="far fa-flag fa-fw"></i>';
                break;

            // Pictos liés aux actions de l'utilisateur

            case "sincrire":
                new_i = '<i class="fas fa-user-plus"></i>';
                break;

            case "connecter":
                new_i = '<i class="fas fa-sign-in-alt fa-fw"></i>';
                break;

            case "deconnecter":
                new_i = '<i class="fas fa-sign-out-alt fa-fw"></i>';
                break;

            case "retour":
                new_i = '<i class="far fa-reply no-right-margin"></i>';
                break;

            case "action":
                new_i = '<i class="far fa-digging fa-fw"></i>';
                break;

            case "action-requise":
                new_i = '<i class="fas fa-construction fa-fw"></i>';
                break;

            case "action-requise-blanc":
                new_i = '<i class="fas fa-construction text-light fa-fw"></i>';
                break;

            case "ajouter":
                new_i = '<i class="far fa-plus-hexagon fa-fw"></i>';
                break;

            case "a-completer":
                new_i = '<span class=""><i class="fal fa-long-arrow-left fa-fw"></i><i class="fas fa-exclamation-triangle text-danger fa-fw"></i></span>';
                break;

            case "editer":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                select = ($(this).data('selection')) ? $(this).data('selection') : "";
                new_i = '<i class="fas fa-pencil-alt fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "envoyer":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                select = ($(this).data('selection')) ? $(this).data('selection') : "";
                new_i = '<i class="far fa-paper-plane fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "notifier":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                select = ($(this).data('selection')) ? $(this).data('selection') : "";
                new_i = '<i class="far fa-broadcast-tower fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "valider":
                new_i = '<i class="far fa-gavel fa-fw"></i>';
                break;

            case "supprimer":
                new_i = '<i class="far fa-trash-alt fa-fw"></i>';
                break;

            case "annuler":
                new_i = '<i class="far fa-times-square fa-fw"></i>';
                break;

            case "dupliquer":
                new_i = '<i class="far fa-folders fa-fw"></i>';
                break;

            case "telecharger":
                new_i = '<i class="far fa-download fa-fw"></i>';
                break;

            case "configurer":
                new_i = '<i class="far fa-wrench fa-fw"></i>';
                break;

            case "deplier":
                new_i = '<i class="fal fa-eye fa-fw"></i>';
                break;

            case "rechercher":
                new_i = '<i class="fas fa-search no-right-margin fa-lg"></i>';
                break;

            case "haut-de-page":
                new_i = '<i class="fal fa-angle-up fa-fw" title="Remonter au haut de page" data-toggle="tooltip"></i>';
                break;

            // Pictos liés aux caratéristiques des dossiers

            case "vtm":
                new_i = '<span class="fa-stack fa-xs" title="Avec véhicule terrestre à moteur" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-car fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

            case "sans-vtm":
                new_i = '<span class="fa-stack fa-xs" title="Sans véhicule terrestre à moteur" data-toggle="tooltip"> \
                    <i class="fas fa-car fa-stack-1x"></i> \
                    <i class="fas fa-ban fa-stack-2x text-danger"></i> \
                    </span>';
                break;

            case "competition":
                new_i = '<span class="fa-stack fa-xs" title="Avec compétition" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-trophy fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

            case "sans-competition":
                new_i = '<span class="fa-stack fa-xs" title="Sans compétition" data-toggle="tooltip"> \
                    <i class="fas fa-trophy fa-stack-1x"></i> \
                    <i class="fas fa-ban fa-stack-2x text-danger"></i> \
                    </span>';
                break;

            case "voie-publique":
                new_i = '<span class="fa-stack fa-xs" title="Sur voie publique" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-road fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

            case "hors-voie-publique":
                new_i = '<span class="fa-stack fa-xs" title="Hors voie publique" data-toggle="tooltip"> \
                    <i class="fas fa-road fa-stack-1x"></i> \
                    <i class="fas fa-ban fa-stack-2x text-danger"></i> \
                    </span>';
                break;

            case "circuit-non-perm":
                new_i = '<span class="fa-stack fa-xs" title="Circuit non permanent" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-route fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

            case "circuit-homologue":
                new_i = '<span class="fa-stack fa-xs" title="Circuit homologue" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-draw-polygon fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

             case "circuit-non-homologue":
                new_i = '<span class="fa-stack fa-xs" title="Circuit non homologue" data-toggle="tooltip"> \
                    <i class="fas fa-draw-polygon fa-stack-1x"></i> \
                    <i class="fas fa-ban fa-stack-2x text-danger"></i> \
                    </span>';
                break;

           case "nombreux-participants":
                new_i = '<span class="fa-stack fa-xs" title="Plus de 1500 participants" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-users-class fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

            case "cyclisme":
                new_i = '<span class="fa-stack fa-xs" title="Discipline cyclisme" data-toggle="tooltip"> \
                    <i class="fas fa-circle fa-stack-2x text-info"></i> \
                    <i class="fas fa-bicycle fa-stack-1x fa-inverse"></i> \
                    </span>';
                break;

            // Pictos liés aux états des dossier

            case "en-edition":
                title = ($(this).data('titre')) ? $(this).data('titre') : "Dossier en cours de rédaction par l'organisateur (non envoyé)";
                select = ($(this).data('selection')) ? $(this).data('selection') : "Dossier non envoyé";
                new_i = '<i class="fas fa-pencil-alt fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "demandee":
                title = ($(this).data('titre')) ? $(this).data('titre') : "Dossier envoyée par l'organisateur";
                select = ($(this).data('selection')) ? $(this).data('selection') : "Dossier envoyée";
                new_i = '<i class="fas fa-inbox-in text-warning fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "distribuee":
                title = ($(this).data('titre')) ? $(this).data('titre') : "Demande d'avis envoyées par l'instructeur";
                select = ($(this).data('selection')) ? $(this).data('selection') : "Dossier distribuée";
                new_i = '<i class="fas fa-comments text-info fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "autorisee":
                title = ($(this).data('titre')) ? $(this).data('titre') : "Manifestation autorisée";
                select = ($(this).data('selection')) ? $(this).data('selection') : "Manifestation autorisée";
                new_i = '<i class="fas fa-shield-check text-success fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "interdite":
                title = ($(this).data('titre')) ? $(this).data('titre') : "Manifestation interdite";
                select = ($(this).data('selection')) ? $(this).data('selection') : "Manifestation interdite";
                new_i = '<i class="fas fa-minus-octagon text-danger fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "annulee":
                title = ($(this).data('titre')) ? $(this).data('titre') : "Manifestation annulée";
                select = ($(this).data('selection')) ? $(this).data('selection') : "Manifestation annulée";
                new_i = '<i class="fas fa-times-square fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            // Pictos liés aux états des éléments

            case "attente":
                new_i = '<i class="far fa-hourglass text-primary fa-fw"></i>';
                break;

            case "attente-blanc":
                new_i = '<i class="far fa-hourglass text-white fa-fw"></i>';
                break;

            case "ok":
                new_i = '<i class="far fa-check text-success fa-fw"></i>';
                break;

            case "ok-blanc":
                new_i = '<i class="far fa-check fa-fw"></i>';
                break;

            case "pas-ok":
                new_i = '<i class="far fa-ban text-danger fa-fw"></i>';
                break;

            case "pas-ok-blanc":
                new_i = '<i class="far fa-ban fa-fw"></i>';
                break;

            case "btn-ok":
                new_i = '<i style="color: white" class="fas fa-check"></i>';
                break;

            case "btn-pas-ok":
                new_i = '<i style="color:white" class="fas fa-times"></i>';
                break;

            case "commentaire":
                new_i = '<i class="far fa-comment-dots text-warning fa-fw"></i>';
                break;

            // Étape d'instruction

            case "delai-90j":
                new_i = '<span class="badge badge-light icone" title="Le début de la manifestation est dans + de 90 jours." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> +90j&nbsp; \
                <i class="fas fa-signal-alt text-info"></i> \
                <span hidden>+90j</span> \
                </span>' ;
                break;

            case "delai-60j":
                new_i = '<span class="badge badge-light icone" title="Le début de la manifestation est dans + de 60 jours." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> +60j&nbsp; \
                <i class="fas fa-signal-alt text-info"></i> \
                <span hidden>+60j</span> \
                </span>' ;
                break;

            case "delai-30j":
                new_i = '<span class="badge badge-light icone" title="Le début de la manifestation est dans + de 30 jours." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> +30j&nbsp; \
                <i class="fas fa-signal-alt text-info"></i> \
                <span hidden>+30j</span> \
                </span>' ;
                break;

            case "delai-30-21j":
                new_i = '<span class="badge badge-light icone" title="Nous sommes entre 21 et 30 jours avant le début de la manifestation." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> 30-21j&nbsp; \
                <i class="fas fa-signal-alt-3 text-success"></i> \
                <span hidden>30-21j</span> \
                </span>' ;
                break;

            case "delai-60-21j":
                new_i = '<span class="badge badge-light icone" title="Nous sommes entre 21 et 60 jours avant le début de la manifestation." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> 60-21j&nbsp; \
                <i class="fas fa-signal-alt-3 text-success"></i> \
                <span hidden>60-21j</span> \
                </span>' ;
                break;

            case "delai-90-21j":
                new_i = '<span class="badge badge-light icone" title="Nous sommes entre 21 et 90 jours avant le début de la manifestation." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> 90-21j&nbsp; \
                <i class="fas fa-signal-alt-3 text-success"></i> \
                <span hidden>90-21j</span> \
                </span>' ;
                break;

            case "delai-21-6j":
                new_i = '<span class="badge badge-light icone" title="Nous sommes entre 6 et 61 jours avant le début de la manifestation." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> 21-6j&nbsp; \
                <i class="fas fa-signal-alt-2 text-warning"></i> \
                <span hidden>21-6j</span> \
                </span>' ;
                break;

            case "delai-6-0j":
                new_i = '<span class="badge badge-light icone" title="Le début de la manifestation est dans - de 6 jours." data-toggle="tooltip"> \
                <i class="far fa-stopwatch"></i> -6j&nbsp; \
                <i class="fas fa-signal-alt-1 text-danger"></i> \
                <span hidden>-6j</span> \
                </span>' ;
                break;

            // Complétude des dossiers

            case "dossier-incomplet":
                new_i = '<span class="badge badge-danger icone" title="Le dossier est incomplet selon la liste des pièces à fournir ' + $(this).data('nb_jour') + ' jours avant le début de la manifestation." data-toggle="tooltip"> \
                <i class="fas fa-layer-group"></i> \
                <i class="fas fa-times"></i><small>' + $(this).data('nb_jour') + 'j</small> \
                <span hidden>Incomplet ' + $(this).data('nb_jour') + 'j</span> \
                </span>' ;
                break;

            case "dossier-complet-non-verifie":
                new_i = '<span class="badge badge-warning icone" title="Le dossier est complet selon la liste des pièces à fournir ' + $(this).data('nb_jour') + ' jours avant le début de la manifestation..<br>Mais les éléments sont <strong>en attente de vérification</strong> par l\'instructeur." data-toggle="tooltip"> \
                <i class="fas fa-layer-group"></i> \
                <i class="far fa-check"></i><small>' + $(this).data('nb_jour') + 'j</small> \
                <span hidden>Complet mais non vérifié ' + $(this).data('nb_jour') + 'j</span> \
                </span>' ;
                break;

            case "dossier-complet-verifie":
                new_i = '<span class="badge badge-success icone" title="Le dossier est complet selon la liste des pièces à fournir ' + $(this).data('nb_jour') + ' jours avant le début de la manifestation.<br>Les éléments ont été vérifié par l\'instructeur." data-toggle="tooltip"> \
                <i class="fas fa-layer-group"></i> \
                <i class="fas fa-shield-check"></i><small>' + $(this).data('nb_jour') + 'j</small> \
                <span hidden>Complet et vérifié ' + $(this).data('nb_jour') + 'j</span> \
                </span>' ;
                break;

            // default:
                // console.log("Classe personnalisée d'icône non trouvée : " + $(this)[0].classList[0]);
        }

        if (new_i != null) {
            // ici on fait deux chose deja on s'assure que le nom de l'icone soit tjs en premier, puis on nettoie les fa deja present
            let class_origine = $(this).attr('class').split(' ')
            let class_desiree_tab = []
            let class_modele = $(new_i).attr('class').split(' ')
            // injecter les classes demandées
            for (let i=0; i<class_origine.length; i++){
                if (class_origine[i].search('fa')===-1){
                    class_desiree_tab.push(class_origine[i])
                }
            }
            // injecter les classes du modèle
            for (let j=0; j<class_modele.length; j++){
                class_desiree_tab.push(class_modele[j])
            }
            let class_injecte = class_desiree_tab.join(' ')
            $(this).replaceWith($(new_i).attr("class", class_injecte ))
            // affecte les classs CSS définie à l'origine sur nouvel élément
            if (typeof custum_ico_cache=="undefined"){
                $('[data-toggle="tooltip"]').tooltip({'html': true});
            }
        }
    });
}

$(document).ready(function () {
    AfficherIcones();
});