# coding: utf-8
from django.urls import reverse
from django.test import TestCase

from administrative_division.factories import DepartementFactory
from core.models.instance import Instance


class ViewsTests(TestCase):

    def test_index(self):
        """
        Tester l'affichage de la page d'accueil

        L'en-tête HTTP_HOST doit exister dans les tests, car il
        permet au Middleware de savoir dans quel domaine on se trouve
        """
        departement = DepartementFactory.create(name="01")
        master = Instance.objects.get_master()
        instance = departement.get_instance()
        # Master
        response = self.client.get(reverse('portail:home_page'), HTTP_HOST='www.manifestationsportive.fr')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<!-- Sous-domaine {instance.name} -->".format(instance=master))
        # Instance
        response = self.client.get(reverse('portail:home_page'), HTTP_HOST='01.manifestationsportive.fr')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "<!-- Sous-domaine {instance.name} -->".format(instance=instance))
        # Test négatif
        response = self.client.get(reverse('portail:home_page'), HTTP_HOST='01.manifestation.fr')
        self.assertEqual(response.status_code, 400)
