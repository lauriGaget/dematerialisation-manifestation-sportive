from rest_framework import serializers
from organisateurs.models import Structure


class StructureSerializer(serializers.ModelSerializer):


    structure = serializers.CharField(source='name')
    # commune = CommuneField()
    # une autre façon : profiter d'une fonction de la classe
    commune = serializers.CharField(source='commune.natural_key')

    class Meta:
        model = Structure
        fields = ('structure', 'commune')
