import logging
from django_filters.rest_framework import FilterSet
from bootstrap_datepicker_plus import DateTimePickerInput
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend, ModelChoiceFilter, DateTimeFilter, NumberFilter
from api.serializer import InstructionSerializer, InstructionInstructeurSerializer,\
    InstructionGroupApiSerializer, ManifSerializer
from datetime import datetime
from administrative_division.models import Departement, Arrondissement, Commune
from evenements.models import Manif
from instructions.models import Instruction
from sports.models import Activite

api_logger = logging.getLogger('api')


class ManifFilter(FilterSet):
    departement = ModelChoiceFilter(field_name="manif__ville_depart__arrondissement__departement",
                                    label='departement',
                                    queryset=Departement.objects.all())
    arrondissement = ModelChoiceFilter(field_name="manif__ville_depart__arrondissement",
                                       label='arrondissement',
                                       queryset=Arrondissement.objects.all())
    commune = ModelChoiceFilter(field_name="manif__ville_depart",
                                label='commune',
                                queryset=Commune.objects.all())
    activite = ModelChoiceFilter(field_name="manif__activite",
                                 label='activite',
                                 queryset=Activite.objects.all())
    date_debut = DateTimeFilter(field_name="manif__date_debut",
                                label='date de début',
                                widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                lookup_expr='gte')
    date_fin = DateTimeFilter(field_name="manif__date_fin",
                              label='date de fin',
                              widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                              lookup_expr='lte')
    année = NumberFilter(field_name="manif__date_debut",
                        label='année',
                        initial=datetime.now().year,
                        lookup_expr='year')

    def __init__(self, data=None, *args, **kwargs):
        # filterset is bound, use initial values as defaults
        if data is not None:
            data = data.copy()

            # year, begin_date and end_date are either missing or empty, use initial as default for year
            if not data.get('année') and not data.get('date_debut') and not data.get('date_fin'):
                data['année'] = self.base_filters['année'].extra['initial']

        super().__init__(data, *args, **kwargs)


class InstructionFilter(ManifFilter):

    class Meta:
        model = Instruction
        fields = ('departement', 'arrondissement', 'commune', 'activite', 'date_debut', 'date_fin', 'année')


class ManifViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vue des manifestations
    """
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('manif__nom', 'manif__description',)
    ordering_fields = ('manif__date_debut', 'manif__activite', 'manif__ville_depart')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)


class ManifAllViewSet(ManifViewSet):
    """
    vue de toutes les manifestations ordonnées de la plus récente
    """
    queryset = Manif.objects.all().order_by('-date_debut')
    serializer_class = ManifSerializer
    filter_fields = ('activite', 'ville_depart')
    search_fields = ('nom', 'description',)
    ordering_fields = ('date_debut', 'activite', 'ville_depart')


class InstructionViewSet(ManifViewSet):
    """
    Vue des manifestations avec le status "autorisée"\n
    Recherche possible sur les champs 'nom' et 'description' avec le bouton 'filtres'
    """
    queryset = Instruction.objects.filter(etat='autorisée')
    filter_class = InstructionFilter

    def get_serializer_class(self):

        grouplist = self.request.user.groups.values_list('name', flat=True)
        if 'API' in grouplist:
            return InstructionGroupApiSerializer
        else:
            if hasattr(self.request.user, 'instructeur'):
                return InstructionInstructeurSerializer
            else:
                return InstructionSerializer
