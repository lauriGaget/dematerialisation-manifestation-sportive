from django.urls import include, path
from api.views.organisateur import OrganisateurViewSet
from api.views.manifestation import ManifestationAutorisationViewSet, ManifestationDeclarationViewSet
from api.views.manif import InstructionViewSet
from rest_framework import routers


app_name = 'api'
router = routers.DefaultRouter()
# router.register(r'manifestations', api_views.ManifestationAllViewSet)
router.register(r'Instruction', InstructionViewSet, basename='Instruction')
router.register(r'ManifestationAutorisation', ManifestationAutorisationViewSet, basename='ManifestationAutorisation')
router.register(r'ManifestationDeclaration', ManifestationDeclarationViewSet, basename='ManifestationDeclaration')
router.register(r'Organisateur', OrganisateurViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
