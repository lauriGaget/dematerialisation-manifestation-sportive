from django.contrib import admin

from .models.action import Action
from .models.notification import Notification


@admin.register(Action)
class ActionAdmin(admin.ModelAdmin):
    """ Administration des actions """

    list_display = ['pk', 'creation_date', 'user', 'action', 'manifestation', 'manif']
    search_fields = ['creation_date', 'user__username', 'action', 'manifestation__name__unaccent', 'manif__nom__unaccent']
    list_per_page = 25
    ordering = ['-creation_date']

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    """ Administration des notifications """

    list_display = ['pk', 'creation_date', 'user', 'subject', 'manifestation', 'manif']
    search_fields = ['creation_date', 'user__username', 'subject__unaccent', 'manifestation__name__unaccent', 'manif__nom__unaccent']
    list_per_page = 25
    ordering = ['-creation_date']