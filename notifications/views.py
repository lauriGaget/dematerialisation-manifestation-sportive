from django.views.generic import ListView
from django.utils.decorators import method_decorator

from .models import Action, Notification
from .decorators import verifier_acces


@method_decorator(verifier_acces, name='dispatch')
class ActionsListView(ListView):
    """ Vue des actions d'une instruction, accès par ajax """
    model = Action
    template_name = 'notifications/actions_table.html'

    def get_queryset(self):
        return Action.objects.filter(manif=self.kwargs['pk']).last_first()


@method_decorator(verifier_acces, name='dispatch')
class NotificationsListView(ListView):
    """ Vue des notifications d'une instruction, accès par ajax """
    model = Notification
    template_name = 'notifications/notifications_table.html'

    def get_queryset(self):
        return Notification.objects.filter(manif=self.kwargs['pk']).last_first()
