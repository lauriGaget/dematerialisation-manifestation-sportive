# coding: utf-8
import datetime

from django.db import models
from django.utils import timezone


class LogQuerySet(models.QuerySet):
    """ Queryset """

    # Getter
    def in_processing(self):
        """ Renvoyer les éléments dont la manifestation s'est finie au plus tôt il y a 24 heures """
        now = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        list1 = self.filter(manifestation__end_date__gte=now - datetime.timedelta(days=1))
        list2 = self.filter(manif__date_fin__gte=now - datetime.timedelta(days=1))
        return list1.union(list2)

    def last_first(self):
        """ Renvoyer dans l'ordre du plus récent au plus ancien """
        return self.order_by('-creation_date')
