# coding: utf-8
import factory

from events.factories import MotorizedRaceFactory
from core.factories import UserFactory
from notifications.models import Action, Notification


class ActionFactory(factory.django.DjangoModelFactory):
    """ Factory pour les actions """

    user = factory.SubFactory(UserFactory)
    manifestation = factory.SubFactory(MotorizedRaceFactory)

    class Meta:
        model = Action


class NotificationFactory(factory.django.DjangoModelFactory):
    """ Factory pour les notifications """

    user = factory.SubFactory(UserFactory)
    manifestation = factory.SubFactory(MotorizedRaceFactory)

    class Meta:
        model = Notification
