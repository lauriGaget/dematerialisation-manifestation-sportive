# coding: utf-8
from django.shortcuts import get_object_or_404, render
from functools import wraps

from core.util.permissions import require_role
from instructions.models.instruction import Instruction
from instructions.models.avis import Avis
from instructions.models.preavis import PreAvis
from administration.models.service import GGD, EDSR, CODIS, Brigade


def verifier_acces(function=None):

    def decorator(view_func):
        @require_role(['instructeur', 'mairieagent', 'agent', 'agentlocal'])
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            user = request.user
            from core.models import Instance
            if kwargs['path'] == 'instruction':
                has_role = False
                for name in ['instructeur', 'mairieagent']:
                    has_role |= user.has_role(name)
                if has_role:
                    instruction = get_object_or_404(Instruction, pk=kwargs['pko'])
                    if str(instruction.manif.pk) == kwargs['pk']:
                        if user.has_role('instructeur'):
                            # L'instructeur ne peut accéder qu'aux instructions de son département ou de son arrondisement
                            config_instructeur = user.get_instance().get_instruction_mode()
                            user_place = user.get_instance().get_departement()
                            object_place = instruction.get_instance().get_departement()
                            if config_instructeur == Instance.IM_ARRONDISSEMENT:
                                if not request.user.get_instance().acces_arrondissement:
                                    user_place = user.instructeur.get_prefecture().arrondissement
                                    object_place = instruction.manif.ville_depart.arrondissement
                            if user_place == object_place:
                                return view_func(request, *args, **kwargs)
                        elif user.has_role('mairieagent'):
                            # L'agent de mairie ne peut accéder qu'aux instructions sur une commune et non motorisées
                            if not instruction.manif.villes_traversees.all() and not hasattr(instruction.manif.get_cerfa(), 'vehicules'):
                                # L'agent de mairie ne peut accéder qu'aux instructions de sa commune
                                user_commune = user.get_service()
                                object_commune = instruction.manif.ville_depart
                                if user_commune == object_commune:
                                    return view_func(request, *args, **kwargs)
            elif kwargs['path'] == 'avis':
                if user.has_role('agent'):
                    avis = get_object_or_404(Avis, pk=kwargs['pko'])
                    if str(avis.instruction.manif.pk) == kwargs['pk']:
                        # L'agent ne peut accéder qu'aux avis concernant son service sauf ...
                        user_service = user.get_service()
                        liste_service = [avis.destination_object]
                        if avis.service_concerne == 'edsr' and avis.etat in ['formaté', 'rendu']:
                            liste_service.append(GGD.objects.get(departement=avis.destination_object.departement))
                        config_ggd = user.get_instance().get_workflow_ggd()
                        if avis.service_concerne == 'ggd' and avis.etat in ['transmis', 'distribué', 'formaté', 'rendu'] and config_ggd == Instance.WF_GGD_EDSR:
                            liste_service.append(EDSR.objects.get(departement=avis.destination_object.departement))
                        if avis.service_concerne == 'sdis' and avis.etat == 'rendu':
                            liste_service.append(CODIS.objects.get(departement=avis.destination_object.departement))
                            for acces in avis.acces.all():
                                liste_service.append(acces.service_object)
                        if user.has_role('cgsuperieuragent'):
                            if avis.service_concerne == 'cg' and avis.etat not in ['formaté', 'rendu']:
                                liste_service = []
                        if user.has_role('brigadeagent'):
                            if avis.service_concerne == 'ggd' or avis.service_concerne == 'edsr':
                                liste_service = Brigade.objects.filter(commune__arrondissement__departement=avis.destination_object.departement)
                        if user_service in liste_service:
                            return view_func(request, *args, **kwargs)
            elif kwargs['path'] == 'preavis':
                if user.has_role('agentlocal'):
                    preavis = get_object_or_404(PreAvis, pk=kwargs['pko'])
                    if str(preavis.avis.instruction.manif.pk) == kwargs['pk']:
                        # L'agent ne peut accéder qu'aux preavis concernant son service
                        user_service = user.get_service()
                        liste_service = [preavis.destination_object]
                        if user_service in liste_service:
                            return view_func(request, *args, **kwargs)
            return render(request, "core/access_restricted.html",{'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return wrapper
    if function:
        return decorator(function)
    return decorator
