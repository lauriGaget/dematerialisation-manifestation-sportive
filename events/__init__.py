# coding: utf-8
from django.apps.config import AppConfig


class EventsConfig(AppConfig):
    """ Configuration de l'application manifestations """

    # Configuration
    name = 'events'
    verbose_name = 'Manifestations sportives - ARCHIVES (Ancienne réglementation)'

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from events import listeners


default_app_config = 'events.EventsConfig'
