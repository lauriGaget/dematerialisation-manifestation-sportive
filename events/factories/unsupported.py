# coding: utf-8

import factory
from django.utils.timezone import utc
from factory.fuzzy import FuzzyDateTime

from administrative_division.factories import CommuneFactory
from events.models import *
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory


class UnsupportedManifestationFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation non prise en charge """

    # Champs
    name = factory.Sequence(lambda n: 'Unsupported event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(datetime.datetime(2010, 1, 1, tzinfo=utc)).start_dt
    end_date = FuzzyDateTime(datetime.datetime(2010, 1, 1, tzinfo=utc)).end_dt
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000

    # Overrides
    @classmethod
    def _generate(cls, create, attrs):
        instance = super(UnsupportedManifestationFactory, cls)._generate(create, attrs)
        instance.instance.departement = None
        instance.save()
        instance.instance.save()
        return instance

    # Meta
    class Meta:
        model = UnsupportedManifestation
