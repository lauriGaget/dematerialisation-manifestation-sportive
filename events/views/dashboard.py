# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from core.util.permissions import require_role
from events.models import *
from organisateurs.models.structure import Structure


class Dashboard(ListView):
    """ Tableau de bord organisateur """

    # Configuration
    model = Manifestation
    template_name = 'events/dashboard_organisateur.html'

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les organisateurs uniquement """
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        """ Renvoyer les manifestations à afficher dans le tableau de bord """
        try:
            user_structure = self.request.user.organisateur.structure
        except Structure.DoesNotExist:
            user_structure = None
        return Manifestation.objects.filter(structure=user_structure).order_by('-pk')

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        return context
