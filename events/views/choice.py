# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView

from administrative_division.models.departement import Departement
from core.util.permissions import require_role
from events.views.base import ChoiceView


class ChoiceView(TemplateView):
    """ Mixin de vue pour les pages de choix lors de la création de manifestation """

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(ChoiceView, self).dispatch(*args, **kwargs)


class ManifestationStartChoose(ChoiceView):
    """ Sélection du département de départ de manifestation """

    # Configuration
    template_name = 'events/selection/event_departement_choose.html'

    # Overrides
    def get_context_data(self, **kwargs):
        departements = Departement.objects.configured().order_by('name')
        return {'departements': departements}


class ManifestationChoose(ChoiceView):
    """ S&lection de manifestation """

    # Configuration
    template_name = 'events/selection/event_choose.html'


class NonMotorizedEventChoose(ChoiceView):
    """ Choix """

    # Confoguration
    template_name = 'events/selection/nonmotorizedevent_choose.html'


class MotorizedEventChoose(ChoiceView):
    """ Choix """

    # Configuration
    template_name = 'events/selection/motorizedevent_choose.html'


class PublicRoadMotorizedEventChoose(ChoiceView):
    """ Choix """

    # Configuration
    template_name = 'events/selection/publicroadmotorizedevent_choose.html'
