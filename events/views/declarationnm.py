# coding: utf-8

from events.views.base import ChoiceView
from events.views.manifestation import ManifestationDetail, ManifestationCreate, ManifestationUpdate, ManifestationFilesUpdate
from ..forms import *
from ..models import *


class DeclarationNMChoose(ChoiceView):
    """ Choix """

    # Configuration
    template_name = 'events/selection/declarednonmotorizedevent_choose.html'


class DeclarationNMDetail(ManifestationDetail):
    """ Détail de manifestation """

    # Configuration
    model = DeclarationNM


class DeclarationNMCreate(ManifestationCreate):
    """ Création de manifestation """

    # Configuration
    model = DeclarationNM
    form_class = DeclarationNMForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnm'
        return context


class DeclarationNMUpdate(ManifestationUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = DeclarationNM
    form_class = DeclarationNMForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnm'
        return context


class DeclarationNMFilesUpdate(ManifestationFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = DeclarationNM
    form_class = DeclarationNMFilesForm
