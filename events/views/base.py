# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic.base import TemplateView

from core.util.permissions import require_role


class ChoiceView(TemplateView):
    """ Vue AJAX ?? """

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(ChoiceView, self).dispatch(*args, **kwargs)
