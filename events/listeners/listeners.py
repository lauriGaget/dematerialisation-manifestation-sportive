# coding: utf-8

from django.db.models.signals import post_save
from django.dispatch import receiver

from events.models import *


if not settings.DISABLE_SIGNALS:
    @receiver(post_save, sender=DocumentComplementaire)
    def log_data_request_creation(created, instance, **kwargs):
        if created:
            instance.request_data()


    @receiver(post_save, sender=AutorisationNM)
    @receiver(post_save, sender=DeclarationNM)
    @receiver(post_save, sender=MotorizedConcentration)
    @receiver(post_save, sender=MotorizedEvent)
    @receiver(post_save, sender=MotorizedRace)
    def log_manifestation_creation(sender, created=None, instance=None, raw=None, **kwargs):
        # Ne pas activer les logs avec loaddata
        if not raw:
            if created:
                instance.actions.create(user=instance.structure.organisateur.user, action="description de la manifestation")
