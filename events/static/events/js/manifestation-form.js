/**
 * Created by knt on 22/01/17.
 */
$(document).ready(function () {

    // Lorsqu'une sélection est effectuée dans le champ extra_activite
    $('#id_extra_activite').bind('added', function () {
        // Récupérer la valeur du champ (le nom exact de l'activité)
        var act_name = he.decode($(this).val());

        // Récupérer l'ID de la discipline en AJAX
        $.ajax({
                   url: "/sports/ajax/discipline/by_activite_name/",
                   type: 'GET',
                   data: 'name=' + encodeURIComponent(act_name),
                   dataType: 'html',
                   success: function (data, status) {
                       // On a récupéré l'ID de la discipline sous forme de chaîne

                       // On sélectionne l'option du select de disciplines dont l'ID correspond
                       // Cela va, via le javascript de clever-selects, peupler les options de
                       // id_discipline. On va donc ensuite sélectionner la bonne option dans le
                       // champ de disciplines
                       $('#id_discipline').val(data);
                       $('#id_discipline').trigger("change");
                       $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen

                       // Refaire le même type de selection sur le champ en cascade (activité)
                       setTimeout(function () {
                           $.ajax({
                                      url: "/sports/ajax/activite/by_name/",
                                      type: 'GET',
                                      data: 'name=' + act_name,
                                      dataType: 'html',
                                      success: function (data, status) {
                                          // On a récupéré l'ID de la discipline sous forme de chaîne

                                          // On sélectionne l'option du select d'activités dont l'ID correspond
                                          $('#id_activite').val(data);
                                          $('#id_activite').trigger("change");
                                          $('#id_activite').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen


                                      }
                                  });
                       }, 50);

                   }
               });
        $(this).val('');

    });

    // BOF Ajout du champ "Dossier technique cyclisme"
    $('#id_activite').parent().parent().after('<div id="id_cycling" class="form-group">' +
        '<label class="control-label col-sm-3">Dossier technique cyclisme</label>' +
        '<div class="controls col-sm-6" style="padding-top: 7px">' +
        'Veuillez d\'ores et déjà ' +
        '<a href="https://www.ffc.fr/wp-content/uploads/2018/01/Dossier-technique-manifestation-cycliste-2018.pdf" ' +
        'target="_blank"> télécharger le document vierge</a>' +
        '. Vous devrez le remplir et le joindre à votre demande.</div></div>');
    // On cache le champ par défaut
    $('#id_cycling').hide();

    // Cas d'édition
    if ($('#id_discipline option:selected').text() == 'Cyclisme') {
        $('#id_cycling').show();
    }
    // Cas de création
    $('#id_discipline').change(function () {
        if ($('#id_discipline option:selected').text() == 'Cyclisme'){
            $('#id_cycling').show(400);
        } else {
            $('#id_cycling').hide(200);
        }
    });
    // EOF

    // BOF Régime de circulation
    function check_traffic() {
        // Renvoie la liste des switches commutés
        var list = [false,false,false,false];
        if ($('[name="traffic_regulations_compliance"]').is(':checked')) { list[0] = true; }
        if ($('[name="right_of_way"]').is(':checked')) { list[1] = true; }
        if ($('[name="temporary_exclusive_roads"]').is(':checked')) { list[2] = true; }
        if ($('[name="private_roads"]').is(':checked')) { list[3] = true; }
        return list;
    }
    function set_detail(list) {
        // Affichage les champs "détails" pour chaque switch
        if (list[0]) {
            $('#div_id_traffic_regulations_compliance_detail').show(400);
        } else {
            $('#div_id_traffic_regulations_compliance_detail').hide(400);
            $('#id_traffic_regulations_compliance_detail').text('');
        }
        if (list[1]) {
            $('#div_id_right_of_way_detail').show(400);
        } else {
            $('#div_id_right_of_way_detail').hide(400);
            $('#id_right_of_way_detail').text('');
        }
        if (list[2]) {
            $('#div_id_temporary_exclusive_roads_detail').show(400);
        } else {
            $('#div_id_temporary_exclusive_roads_detail').hide(400);
            $('#id_temporary_exclusive_roads_detail').text('');
        }
        if (list[3]) {
            $('#div_id_private_roads_detail').show(400);
        } else {
            $('#div_id_private_roads_detail').hide(400);
            $('#id_private_roads_detail').text('');
        }
    }
    function which_traffic(id) {
        // Renvoie le switch qui a communté parmi les quatre
        var list = [false,false,false,false];
        if ( id === 'id_traffic_regulations_compliance') { list[0] = true; }
        if ( id === 'id_right_of_way') { list[1] = true; }
        if ( id === 'id_temporary_exclusive_roads') { list[2] = true; }
        if ( id === 'id_private_roads') { list[3] = true; }
        return list
    }
    function set_traffic(list) {
        // Communte les switches
        if (list[0]) { $('#id_traffic_regulations_compliance').click(); }
        if (list[1]) { $('#id_right_of_way').click(); }
        if (list[2]) { $('#id_temporary_exclusive_roads').click(); }
        if (list[3]) { $('#id_private_roads').click(); }
    }
    // Gérer l'affichage initial
    if ($('[name="traffic_regulations_compliance_detail"]').text() != '') {
        $('#div_id_traffic_regulations_compliance_detail').show();
    } else {
        $('#div_id_traffic_regulations_compliance_detail').hide();
    }
    if ($('[name="right_of_way_detail"]').text() != '') {
        $('#div_id_right_of_way_detail').show();
    } else {
        $('#div_id_right_of_way_detail').hide();
    }
    if ($('[name="temporary_exclusive_roads_detail"]').text() != '') {
        $('#div_id_temporary_exclusive_roads_detail').show();
    } else {
        $('#div_id_temporary_exclusive_roads_detail').hide();
    }
    if ($('[name="private_roads_detail"]').text() != '') {
        $('#div_id_private_roads_detail').show();
    } else {
        $('#div_id_private_roads_detail').hide();
    }
    // Gérer l'affichage quand les switches sont commutés
    $('.traffic').click(function() {
        var previous = which_traffic($(this).attr('id'));
        var etat = check_traffic();
        if ( !(etat[0] | etat[1] | etat[2] | etat[3]) ) {
            // pas de switch, on communte le suivant
            set_traffic([previous[3], previous[0], previous[1], previous[2]]);
            // alert('Vous devez sélectionner au moins un régime');
        } else {
            var prev = etat[0];
            var xor = true;
            for (var i = 1; i < etat.length; i++) {
                if (prev == 1 & etat[i] == 1) {
                    // plus de un switch, on affiche le détail pour chaque
                    xor = false;
                    set_detail(etat);
                    break;
                }
                prev = prev | etat[i];
            }
            // un seul switch, pas de détails
            if (xor) { set_detail([0, 0, 0, 0]); }
        }
    });
    // EOF

    // BOF Dispositif de sécurité
    // Gérer l'affichage du champ "moyens affectés" si le switch est true initialement
    if ($('[name="city_police"]').is(':checked')) {
        $('#div_id_city_police_detail').show();
    } else {
        $('#div_id_city_police_detail').hide();
    }
    if ($('[name="state_police"]').is(':checked')) {
        $('#div_id_state_police_detail').show();
    } else {
        $('#div_id_state_police_detail').hide();
    }
    // Gérer l'affichage du champ "moyens affectés" si le switch est commuté
    $('#id_city_police').click(function() {
        $('#div_id_city_police_detail').toggle(400);
    });
    $('#id_state_police').click(function() {
        $('#div_id_state_police_detail').toggle(400);
    });
    // Gérer l'affichage des signaleurs si le nombre est rempli initialement
    if ($('[name="signalers_number"]').val() != '' && $('[name="signalers_number"]').val() != 0) {
        $('#div_id_signalers_number_still').show();
        $('#div_id_signalers_number_auto').show();
        $('#div_id_signalers_number_moto').show();
    } else {
        $('#div_id_signalers_number_still').hide();
        $('#div_id_signalers_number_auto').hide();
        $('#div_id_signalers_number_moto').hide();
    }
    // Gérer l'affichage des signaleurs si le nombre est rempli
    $('[name="signalers_number"]').on("change", function () {
        if ($('[name="signalers_number"]').val() != '' && $('[name="signalers_number"]').val() != 0){
            $('#div_id_signalers_number_still').show(400);
            $('#div_id_signalers_number_auto').show(400);
            $('#div_id_signalers_number_moto').show(400);
        } else {
            $('#div_id_signalers_number_still').hide(400);
            $('#div_id_signalers_number_auto').hide(400);
            $('#div_id_signalers_number_moto').hide(400);
        }
    });
    // EOF


    $('#id_other_departments_crossed').trigger("change");  // Provoquer la mise à jour du champ Communes traversées
    $('#id_other_departments_crossed').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    $("#manifestation-form").liveValidate('/validate/manifestation/', {event: 'update blur'});

    $("textarea").autogrow({flickering: false, horizontal: false});
});
