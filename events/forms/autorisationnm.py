# coding: utf-8
from ajax_select.fields import AutoCompleteField
from crispy_forms.layout import Layout, Fieldset, Field, HTML
from django.forms import ModelChoiceField
from django import forms

from core.forms.base import GenericForm
from events.forms.manifestation import FORM_WIDGETS, HELP_EXTRA_ACTIVITE
from events.models import AutorisationNM
from sports.models import Discipline
from .manifestation import ManifestationForm, FIELDS_MANIFESTATION, FIELDS_ROADS, FIELDS_FILES, FIELDS_MARKUP, FIELDS_MARKUP_CONV, FIELDS_NATURA2000


class AutorisationNMForm(ManifestationForm):
    """ Formulaire """
    FORM_WIDGETS['city_police_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px'})
    FORM_WIDGETS['state_police_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px'})
    FORM_WIDGETS['traffic_regulations_compliance_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px', 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['right_of_way_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px', 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['temporary_exclusive_roads_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px', 'placeholder': 'nom de la voie et créneau horaire de passage'})
    FORM_WIDGETS['private_roads_detail'] = forms.Textarea(attrs={'rows': 1, 'style': 'height: 34px', 'placeholder': 'nom de la voie et créneau horaire de passage'})

    # Champs
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=False))
    extra_activite = AutoCompleteField('activite_nm', label="Recherche d'activité sportive", required=False,
                                       help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super(AutorisationNMForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Remplissez ce formulaire puis vous pourrez joindre ensuite des fichiers à votre dossier.</p><hr>"),
                                    Fieldset(*(FIELDS_MANIFESTATION[:8] + ['affiliation'] + FIELDS_MANIFESTATION[8:] + ['multisport_federation'])),
                                    Fieldset("Coordonnateur Sécurité", 'safety_name', 'safety_firstname', 'safety_tel', 'safety_email'),
                                    Fieldset(*FIELDS_ROADS),
                                    Fieldset("Régime demandé en matière de circulation publique",
                                             Field('traffic_regulations_compliance', css_class='traffic'), 'traffic_regulations_compliance_detail',
                                             Field('right_of_way', css_class='traffic'), 'right_of_way_detail',
                                             Field('temporary_exclusive_roads', css_class='traffic'), 'temporary_exclusive_roads_detail',
                                             Field('private_roads', css_class='traffic'), 'private_roads_detail'),
                                    Fieldset("Informations sur le dispositif de sécurité de la manifestation",
                                             Fieldset("Véhicules d'accompagnement :", 'opening_vehicle', 'heading_vehicle', 'trailing_vehicle', 'staff_vehicle', css_class='special_fieldset'),
                                             Fieldset("Signaleurs :", 'signalers_number', 'signalers_number_still', 'signalers_number_auto', 'signalers_number_moto', css_class='special_fieldset'),
                                             Fieldset("Forces de l'ordre :", 'city_police', 'city_police_detail', 'state_police', 'state_police_detail', css_class='special_fieldset')
                                             ),
                                    Fieldset(*FIELDS_MARKUP_CONV), Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(FIELDS_NATURA2000[:-3])))

    # Meta
    class Meta:
        model = AutorisationNM
        exclude = ('instance', 'structure', 'approval_request', 'motor_on_closed_road', 'motor_on_natura2000', 'rounds_safety', 'instance')
        widgets = FORM_WIDGETS


class AutorisationNMFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['signalers_list'].required = False
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES[:-1] + ['signalers_list', 'cycling_techdata'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = AutorisationNM
        fields = ['manifestation_rules', 'organisateur_commitment', 'insurance_certificate', 'safety_provisions', 'doctor_attendance', 'additional_docs',
                  'rounds_safety', 'signalers_list', 'cycling_techdata']
