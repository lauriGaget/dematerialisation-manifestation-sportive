# coding: utf-8

from authorizations.factories import ManifestationAuthorizationFactory
from core.factories.instance import InstanceFactory
from declarations.factories import EventDeclarationFactory
from events.tests.base import EventsTestsBase
from ..factories import MotorizedConcentrationFactory


class MotorizedConcentrationMethodTests(EventsTestsBase):
    """ Tests """

    # Tests
    def test_get_absolute_url(self):
        manifestation = MotorizedConcentrationFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/motorizedconcentrations/4/')

    def test_display_natura2000_eval_panel(self):
        manifestation = MotorizedConcentrationFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = MotorizedConcentrationFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = MotorizedConcentrationFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())
        manifestation5 = MotorizedConcentrationFactory.build(motor_on_natura2000=True)
        self.assertTrue(manifestation5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = MotorizedConcentrationFactory.build(instance=InstanceFactory.build())
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay_for_authorized(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=True, instance=InstanceFactory.build())
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_legal_delay_for_declared(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=False, instance=InstanceFactory.build())
        self.assertEqual(manifestation.legal_delay(), 60)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=True)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_not_sent_2(self):
        manifestation = MotorizedConcentrationFactory.build(big_concentration=False)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = MotorizedConcentrationFactory.create(big_concentration=True)
        ManifestationAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)

    def test_get_final_breadcrumb_sent_2(self):
        manifestation = MotorizedConcentrationFactory.create(big_concentration=False)
        EventDeclarationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)
