# coding: utf-8

from authorizations.factories import ManifestationAuthorizationFactory
from core.factories.instance import InstanceFactory
from events.tests.base import EventsTestsBase
from ..factories import MotorizedRaceFactory


class MotorizedRaceMethodTests(EventsTestsBase):
    """ Tests """

    # Tests
    def test_get_absolute_url(self):
        manifestation = MotorizedRaceFactory.build(pk=5)
        self.assertEqual(manifestation.get_absolute_url(), '/motorizedraces/5/')

    def test_display_natura2000_eval_panel(self):
        manifestation = MotorizedRaceFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation2 = MotorizedRaceFactory.build(approval_request=True)
        self.assertTrue(manifestation2.display_natura2000_eval_panel())
        manifestation3 = MotorizedRaceFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())
        manifestation4 = MotorizedRaceFactory.build(big_title=True)
        self.assertTrue(manifestation4.display_natura2000_eval_panel())
        manifestation5 = MotorizedRaceFactory.build(motor_on_closed_road=True)
        self.assertTrue(manifestation5.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = MotorizedRaceFactory.build(instance=InstanceFactory.build())
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay(self):
        manifestation = MotorizedRaceFactory.build(instance=InstanceFactory.build())
        self.assertEqual(manifestation.legal_delay(), 90)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = MotorizedRaceFactory.build(instance=InstanceFactory.build())
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = MotorizedRaceFactory.create()
        ManifestationAuthorizationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)
