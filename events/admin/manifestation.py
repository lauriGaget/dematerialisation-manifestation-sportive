# coding: utf-8
from ajax_select.helpers import make_ajax_form
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin
from django.urls import reverse
from django.utils.html import format_html

from authorizations.admin import EventAuthorizationInline
from contacts.admin import ContactInline
from core.util.admin import RelationOnlyFieldListFilter
from declarations.admin import EventDeclarationInline
from evaluations.admin import Natura2000EvaluationInline
from evaluations.admin import RNREvaluationInline
from events.admin.filter import DeclarationAskedListFilter, AuthorizationAskedListFilter,\
    ConcentrationAuthorizationAskedListFilter, ProcessingFilter, CalendarFilter, ComingFilter
from events.admin.inline import DocumentComplementaireInline
from events.models import AutorisationNM
from events.models import DeclarationNM
from events.models import MotorizedConcentration
from events.models import MotorizedEvent
from events.models import MotorizedRace
from events.models import Manifestation, UnsupportedManifestation


@admin.register(Manifestation)
class ManifestationAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations """

    # Configuration
    list_display = ('link_to_specialized', 'processing', 'creation_date', 'get_manifestation_type_name', 'name', 'structure', 'activite__name', 'begin_date',
                    'departure_city__arrondissement__departement')
    list_filter = [ProcessingFilter, ComingFilter,
                   ('departure_city__arrondissement__departement', RelationOnlyFieldListFilter), 'activite__name']
    readonly_fields = ['creation_date']
    search_fields = ['name__unaccent']
    inlines = [ContactInline]
    list_per_page = 25
    ordering = ['-creation_date']
    list_display_links = ['processing']


    fieldsets = (
        (None, {'fields': ('instance', 'name', ('creation_date', 'begin_date', 'end_date'), ('description', 'observation'),
                          ('structure', 'activite'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',), 'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                                             'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',), 'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Circulation', {'classes': ('collapse',), 'fields': (('oneway_roads', 'closed_roads'),)}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs', 'organisateur_commitment',
                                                           'insurance_certificate', 'rounds_safety')}),
    )
    form = make_ajax_form(Manifestation, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                          'other_departments_crossed': 'departement', 'structure': 'structure'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset

    def link_to_specialized(self, obj):
        try:
            link = reverse("admin:events_" + obj.get_manifestation_type_name() + "_change", args=[obj.get_manifestation_specialized().id])
            html_link = format_html('<a href="{}">Edit</a>', link)
        except:
            html_link = ""
        return html_link
    link_to_specialized.short_description = 'Édition'



@admin.register(UnsupportedManifestation)
class UnsupportedManifestationAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations non motorisées soumises à déclaration """

    # Configuration
    list_display = ('as_str', 'begin_date', 'departure_city__arrondissement__departement')
    list_filter = [('departure_city__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ['creation_date']
    search_fields = ['name__unaccent']
    fieldsets = (
        (None, {'fields': ('instance', 'name', ('creation_date', 'begin_date', 'end_date'), ('description', 'observation'),
                          ('structure', 'activite'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',), 'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                                             'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',), 'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Circulation', {'classes': ('collapse',), 'fields': (('oneway_roads', 'closed_roads'),)}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs', 'organisateur_commitment',
                                                           'insurance_certificate', 'rounds_safety')}),
    )
    form = make_ajax_form(Manifestation, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                          'other_departments_crossed': 'departement', 'structure': 'structure'})
    inlines = [ContactInline]
    list_per_page = 25
    ordering = ['-begin_date']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(DeclarationNM)
class DeclarationNMAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations non motorisées soumises à déclaration """

    # Configuration
    list_display = ('as_str', 'begin_date', 'processing', 'has_rnrevaluation', 'has_natura2000evaluation',
                    'departure_city__arrondissement__departement')
    list_filter = ['manifestationdeclaration__state', DeclarationAskedListFilter,
                   ('departure_city__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ('is_on_rnr', 'creation_date')
    search_fields = ['name__unaccent']
    fieldsets = (
        (None, {'fields': ('instance', 'name', ('creation_date', 'begin_date', 'end_date'), ('description', 'observation'),
                          ('structure', 'activite'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',), 'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                                             'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',), 'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Coordonateur Sécurité', {'classes': ('collapse',), 'fields': (('safety_name', 'safety_firstname'),
                                                                        ('safety_tel', 'safety_email'))}),
        ('Circulation', {'classes': ('collapse',),
                         'fields': (('oneway_roads', 'closed_roads'), ('grouped_traffic', 'grouped_start', 'grouped_run'),
                                    'support_vehicles_number', ('opening_vehicle', 'heading_vehicle', 'trailing_vehicle', 'staff_vehicle'),
                                    ('signalers_number', 'signalers_number_still', 'signalers_number_auto', 'signalers_number_moto'),
                                    'city_police', 'city_police_detail', 'state_police', 'state_police_detail')}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs', 'organisateur_commitment',
                                                           'insurance_certificate', 'rounds_safety')}),
    )
    form = make_ajax_form(DeclarationNM, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                          'other_departments_crossed': 'departement', 'structure': 'structure'})
    inlines = [ContactInline, DocumentComplementaireInline, EventDeclarationInline,
               RNREvaluationInline, Natura2000EvaluationInline]
    list_per_page = 25
    ordering = ['-begin_date']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}



@admin.register(AutorisationNM)
class AutorisationNMAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations non motorisées soumises à autorisation """

    # Configuration
    list_display = ('as_str', 'begin_date', 'processing', 'has_rnrevaluation', 'has_natura2000evaluation',
                    'departure_city__arrondissement__departement')
    list_filter = ['manifestationautorisation__state', AuthorizationAskedListFilter,
                   ('departure_city__arrondissement__departement', RelationOnlyFieldListFilter)]
    readonly_fields = ('is_on_rnr', 'creation_date')
    search_fields = ['name__unaccent']
    fieldsets = (
        (None, {'fields': (('instance','creation_date'), ('begin_date', 'end_date'), ('name','structure'), 'activite',
                           ('description', 'observation'),
                           ('multisport_federation', 'affiliation'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',),
                        'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                   'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',),
                        'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Coordonateur Sécurité', {'classes': ('collapse',),
                                   'fields': (('safety_name', 'safety_firstname'),('safety_tel', 'safety_email'))}),
        ('Circulation', {'classes': ('collapse',),
                         'fields': (('oneway_roads', 'closed_roads'),
                                    ('opening_vehicle', 'heading_vehicle', 'trailing_vehicle', 'staff_vehicle'),
                                    ('signalers_number', 'signalers_number_still', 'signalers_number_auto', 'signalers_number_moto'),
                                    'city_police', 'city_police_detail', 'state_police', 'state_police_detail',
                                    'traffic_regulations_compliance', 'traffic_regulations_compliance_detail',
                                    'right_of_way', 'right_of_way_detail', 'temporary_exclusive_roads',
                                    'temporary_exclusive_roads_detail', 'private_roads', 'private_roads_detail')}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs',
                                                           'organisateur_commitment', 'insurance_certificate',
                                                           'rounds_safety', 'signalers_list', 'cycling_techdata')}),
    )
    form = make_ajax_form(AutorisationNM, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                           'other_departments_crossed': 'departement', 'structure': 'structure'})
    inlines = [ContactInline, DocumentComplementaireInline, EventAuthorizationInline,
               RNREvaluationInline, Natura2000EvaluationInline]
    list_per_page = 25
    ordering = ['-begin_date']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(MotorizedConcentration)
class MotorizedConcentrationAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des concentrations motorisées """

    # Configuration
    list_display = ('as_str', 'begin_date', 'processing', 'has_rnrevaluation', 'has_natura2000evaluation',
                    'departure_city__arrondissement__departement')
    list_filter = ['manifestationautorisation__state', 'manifestationdeclaration__state', ConcentrationAuthorizationAskedListFilter,
                   ('departure_city__arrondissement__departement', RelationOnlyFieldListFilter), 'activite__name']
    readonly_fields = ('is_on_rnr', 'creation_date')
    search_fields = ['name__unaccent']
    fieldsets = (
        (None, {'fields': ('instance', 'name', ('creation_date', 'begin_date', 'end_date'), ('description', 'observation'),
                          ('structure', 'activite'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',), 'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                                             'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',), 'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Organisateur Technique', {'classes': ('collapse',), 'fields': (('tech_name', 'tech_firstname'),
                                                                        ('tech_tel', 'tech_email'))}),
        ('Circulation', {'classes': ('collapse',), 'fields': (('oneway_roads', 'closed_roads'), 'big_concentration',
                                                              'vehicles', 'following', 'meeting')}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs', 'organisateur_commitment',
                                                           'insurance_certificate', 'rounds_safety')}),
    )
    form = make_ajax_form(MotorizedConcentration, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                                   'other_departments_crossed': 'departement', 'structure': 'structure'})
    inlines = [ContactInline, DocumentComplementaireInline, EventAuthorizationInline, EventDeclarationInline,
               RNREvaluationInline, Natura2000EvaluationInline]
    list_per_page = 25
    ordering = ['-begin_date']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(MotorizedEvent)
class MotorizedEventAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations motorisées """

    # Configuration
    list_display = ('as_str', 'begin_date', 'processing', 'has_rnrevaluation', 'has_natura2000evaluation',
                    'departure_city__arrondissement__departement')
    list_filter = ['manifestationautorisation__state', AuthorizationAskedListFilter,
                   ('departure_city__arrondissement__departement', RelationOnlyFieldListFilter), 'activite__name']
    readonly_fields = ('is_on_rnr', 'creation_date')
    search_fields = ['name__unaccent']
    fieldsets = (
        (None, {'fields': ('instance', 'name', ('creation_date', 'begin_date', 'end_date'), ('description', 'observation'),
                          ('structure', 'activite'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',), 'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                                             'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',), 'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Organisateur Technique', {'classes': ('collapse',), 'fields': (('tech_name', 'tech_firstname'),
                                                                        ('tech_tel', 'tech_email'))}),
        ('Circulation', {'classes': ('collapse',), 'fields': (('oneway_roads', 'closed_roads'), 'vehicles', 'following')}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs', 'organisateur_commitment',
                                                           'insurance_certificate', 'rounds_safety', 'delegate_federation_agr',
                                                           'public_zone_map', 'commissioners', 'tech_organisateur_certificate',
                                                           'hourly_itinerary', 'participants')}),
    )
    inlines = [ContactInline, DocumentComplementaireInline, EventAuthorizationInline,
               RNREvaluationInline, Natura2000EvaluationInline]
    form = make_ajax_form(MotorizedEvent, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                           'other_departments_crossed': 'departement', 'structure': 'structure'})
    list_per_page = 25
    ordering = ['-begin_date']

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset


@admin.register(MotorizedRace)
class MotorizedRaceAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des courses motorisées """

    # Configuration
    list_display = ('as_str', 'begin_date', 'processing', 'has_rnrevaluation', 'has_natura2000evaluation',
                    'departure_city__arrondissement__departement')
    list_filter = ['manifestationautorisation__state', AuthorizationAskedListFilter,
                   ('departure_city__arrondissement__departement', RelationOnlyFieldListFilter), 'activite__name']
    readonly_fields = ('is_on_rnr', 'creation_date')
    search_fields = ['name__unaccent']
    fieldsets = (
        (None, {'fields': ('instance', 'name', ('creation_date', 'begin_date', 'end_date'), ('description', 'observation'),
                          ('structure', 'activite'), ('number_of_entries', 'max_audience'))}),
        ('Géographie', {'classes': ('collapse',), 'fields': (('departure_city', 'crossed_cities', 'other_departments_crossed'),
                                                             'openrunner_route', 'route_descriptions')}),
        ('Calendrier', {'classes': ('collapse',), 'fields': (('registered_calendar', 'private', 'hidden', 'show_structure_address'))}),
        ('Balisage', {'classes': ('collapse',), 'fields': ('markup_convention', 'rubalise_markup', 'bio_rubalise_markup',
                                                           'chalk_markup', 'paneling_markup', 'ground_markup',
                                                           'sticker_markup', 'lime_markup', 'plaster_markup',
                                                           'confetti_markup', 'paint_markup', 'acrylic_markup', 'withdrawn_markup')}),
        ('Natura2000', {'classes': ('collapse',), 'fields': ('big_budget', 'big_title', 'lucrative',
                                                             'motor_on_closed_road', 'motor_on_natura2000',
                                                             'approval_request', 'is_on_rnr')}),
        ('Organisateur Technique', {'classes': ('collapse',), 'fields': (('tech_name', 'tech_firstname'),
                                                                        ('tech_tel', 'tech_email'))}),
        ('Circulation', {'classes': ('collapse',), 'fields': (('oneway_roads', 'closed_roads'), 'support', 'circuit_homologue')}),
        ('fichiers', {'classes': ('collapse',), 'fields': ('cartography', 'manifestation_rules', 'safety_provisions',
                                                           'doctor_attendance', 'additional_docs', 'organisateur_commitment',
                                                           'insurance_certificate', 'rounds_safety', 'mass_map')}),
    )
    form = make_ajax_form(MotorizedRace, {'departure_city': 'commune', 'crossed_cities': 'commune',
                                          'other_departments_crossed': 'departement', 'structure': 'structure'})
    inlines = [ContactInline, DocumentComplementaireInline, EventAuthorizationInline,
               RNREvaluationInline, Natura2000EvaluationInline]
    list_per_page = 25
    ordering = ['-begin_date']

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(departure_city__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}
