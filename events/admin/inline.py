# coding: utf-8
from django.contrib import admin

from events.models import DocumentComplementaire


class DocumentComplementaireInline(admin.TabularInline):
    """ Inline des documents complémentaires """

    # C,onfiguration
    model = DocumentComplementaire
    extra = 0
