# coding: utf-8
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from events.models.manifestation import Manifestation


class MotorizedConcentration(Manifestation):
    """ Concentration de véhicules terrestres à moteur DVTM ou AVTMC (selon l'importance de la manif) """

    # Champs
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True, related_name='motorizedconcentration', on_delete=models.CASCADE)
    big_concentration = models.BooleanField("plus de 200 autos ou 400 véhicules terrestres à moteur de 2 ou 4 roues", default=False)
    vehicles = models.TextField("type et nombre de véhicules")
    following = models.PositiveSmallIntegerField("nombre de véhicules d'accompagnement", default=0)
    meeting = models.PositiveSmallIntegerField("nombre de personnes aux points de rassemblement", default=0)
    # Ajout de default dans les CharField et TextField pour la DB existante
    tech_name = models.CharField("nom de famille", max_length=200, blank=True, default='')
    tech_firstname = models.CharField("prénom", max_length=200, blank=True, default='')
    tech_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    tech_email = models.EmailField("adresse email", max_length=200, blank=True, default='')

    # Getter
    def legal_delay(self):
        """ Renvoyer le délai légal pour l'instruction de la manifestation """
        if self.big_concentration:
            return self.get_instance().get_manifestation_avtmc_delay()
        else:
            return self.get_instance().get_manifestation_dvtm_delay()

    def display_natura2000_eval_panel(self):
        """ Indiquer si dans l'interface; le panneau d'évaluation N2K doit être affiché """
        return not hasattr(self, 'natura2000evaluations') and (self.big_budget or self.lucrative or self.big_title or self.motor_on_natura2000)

    def get_final_breadcrumb(self):
        """ Renvoyer le breadcrumb de la manifestation avec son état """
        if self.big_concentration:
            try:
                self.manifestationautorisation
            except (AttributeError, ObjectDoesNotExist):
                breadcrumb = ["demande d'autorisation non envoyée", 0]
            else:
                breadcrumb = ["demande d'autorisation envoyée", 1]
        else:
            try:
                self.manifestationdeclaration
            except (AttributeError, ObjectDoesNotExist):
                breadcrumb = ["déclaration non envoyée", 0]
            else:
                breadcrumb = ["déclaration envoyée", 1]

        return breadcrumb, True

    def ready_for_instruction(self):
        """ Indiquer si l'instruction de la manifestation peut débuter """
        ready = super(MotorizedConcentration, self).ready_for_instruction()
        if self.big_concentration:
            if not self.rounds_safety:
                ready = False
        return ready

    # Meta
    class Meta:
        verbose_name = "concentration de véhicules terrestres à moteur"
        verbose_name_plural = "concentrations de véhicules terrestres à moteur"
        default_related_name = "motorizedconcentrations"
        app_label = "events"
