# coding: utf-8

from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from configuration.directory import UploadPath
from events.models.manifestation import Manifestation


class MotorizedEvent(Manifestation):
    """ Manifestation avec véhicules terrestres à moteur sur voie publique """

    # Champs
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True, related_name='motorizedevent', on_delete=models.CASCADE)
    vehicles = models.TextField("type et nombre de véhicules")
    following = models.PositiveSmallIntegerField("nombre de véhicules d'accompagnement", default=0)
    delegate_federation_agr = models.FileField(upload_to=UploadPath('avis_federation_delegataire'), blank=True, null=True,
                                               verbose_name="avis fédération délégataire", max_length=512)
    public_zone_map = models.FileField(upload_to=UploadPath('carte_zone_publique'), blank=True, null=True,
                                       verbose_name="carte zone publique", max_length=512)
    commissioners = models.FileField(upload_to=UploadPath('postes_commissaires'), blank=True, null=True,
                                     verbose_name="liste des postes commissaires", max_length=512)
    tech_organisateur_certificate = models.FileField(upload_to=UploadPath('attestations_orga_tech'), blank=True, null=True,
                                                     verbose_name="attestation organisateur technique", max_length=512)
    hourly_itinerary = models.FileField(upload_to=UploadPath('itineraires_horaires'), blank=True, null=True,
                                        verbose_name="itinéraire horaire", max_length=512)
    participants = models.FileField(upload_to=UploadPath('liste_participants'), blank=True, null=True,
                                        verbose_name="liste complète des participants (voir a331-21)", max_length=512,
                                    help_text="nom, prénom, date et lieu de naissance, numéro de permis de conduire, nationalité et adresse de domicile ainsi que le numéro d'inscription de leur véhicule délivré par l'organisateur")
    # Ajout de default dans les CharField et TextField pour la DB existante
    tech_name = models.CharField("nom de famille", max_length=200, blank=True, default='')
    tech_firstname = models.CharField("prénom", max_length=200, blank=True, default='')
    tech_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    tech_email = models.EmailField("adresse email", max_length=200, blank=True, default='')


    # Getter
    def legal_delay(self):
        """ Renvoyer le délai légal en jour pour l'instruction de la manifestation """
        return self.get_instance().get_manifestation_avtm_vp_delay()

    def display_natura2000_eval_panel(self):
        """ Indiquer si le panneau d'évaluation N2K doit être affiché dans l'UI"""
        return not hasattr(self, 'natura2000evaluations') and (self.big_budget or self.lucrative or self.big_title or self.motor_on_natura2000)

    def get_final_breadcrumb(self):
        """ Renvoyer le breadcrumb de la manifestation et son état """
        exists = True
        try:
            self.manifestationautorisation
        except (AttributeError, ObjectDoesNotExist):
            breadcrumb = ["demande d'autorisation non envoyée", 0]
        else:
            breadcrumb = ["demande d'autorisation envoyée", 1]
        return breadcrumb, exists

    def ready_for_instruction(self):
        """ Indiquer si l'instruction de la manifestation peut débuter """
        ready = super(MotorizedEvent, self).ready_for_instruction()
        if not self.commissioners:
            ready = False
        if not self.tech_organisateur_certificate:
            ready = False
        if not self.hourly_itinerary:
            ready = False
        if not self.participants:
            ready = False
        return ready

    # Meta
    class Meta:
        verbose_name = "manifestation avec véhicules terrestres à moteur"
        verbose_name_plural = "manifestations avec véhicules terrestres à moteur"
        default_related_name = "motorizedevents"
        app_label = "events"
