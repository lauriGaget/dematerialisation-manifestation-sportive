# coding: utf-8

from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from configuration.directory import UploadPath
from events.models.manifestation import Manifestation


class MotorizedRace(Manifestation):
    """ Manifestation hors voie publique ou ouverte à la circulation publique """

    # Constantes
    SUPPORT_CHOICES = (('s', "circuit"), ('g', "terrain"), ('r', "parcours"))

    # Champs
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True, related_name='motorizedrace', on_delete=models.CASCADE)
    support = models.CharField("type de support", max_length=1, choices=SUPPORT_CHOICES)
    mass_map = models.FileField(upload_to=UploadPath('plan_masse'), blank=True, null=True, verbose_name="plan de masse", max_length=512)
    circuit_homologue = models.BooleanField("circuit homologué", default=False)
    # Ajout de default dans les CharField et TextField pour la DB existante
    tech_name = models.CharField("nom de famille", max_length=200, blank=True, default='')
    tech_firstname = models.CharField("prénom", max_length=200, blank=True, default='')
    tech_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    tech_email = models.EmailField("adresse email", max_length=200, blank=True, default='')


    # Getter
    def legal_delay(self):
        """ Renvoyer le délai légal d'instruction """
        if self.circuit_homologue:
            return self.get_instance().get_manifestation_avtm_homolog_delay()
        else:
            return self.get_instance().get_manifestation_avtm_vn_delay()

    def display_natura2000_eval_panel(self):
        """ Indiquer s'il faut afficher le panneau d'évaluation N2K """
        if not hasattr(self, 'natura2000evaluations'):
            if self.big_budget or self.lucrative or self.big_title or self.motor_on_closed_road or self.approval_request:
                return True
        return False

    def get_final_breadcrumb(self):
        """ Renvoyer le breadcrumb de la manifestation et son état """
        exists = True
        try:
            self.manifestationautorisation
        except (AttributeError, ObjectDoesNotExist):
            breadcrumb = ["demande d'autorisation non envoyée", 0]
        else:
            breadcrumb = ["demande d'autorisation envoyée", 1]
        return breadcrumb, exists

    # Méta
    class Meta:
        verbose_name = "manifestation sur circuit, terrain ou parcours, soumise à autorisations"
        verbose_name_plural = "courses avec véhicules terrestres à moteur"
        default_related_name = "motorizedraces"
        app_label = "events"
