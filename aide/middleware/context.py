# coding: utf-8
from bs4 import BeautifulSoup

from aide.models.context import ContextHelp
from aide.views.help import check_affichage_elements
from core.util.user import UserHelper

class ContextHelpMiddleware(object):
    """
    Middleware d'aide contextuelle

    Middleware qui remplace les nœuds du DOM possédant un id débutant
    par help-, en y adjoignant un contenu d'aide contextuelle.

    Cette méthode est une alternative à la version Tooltip + AJAX
    Inutilisée sauf si la situation venait à évoluer.
    """

    @staticmethod
    def select_help_tags(tag):
        """ BS4 : Sélection des éléments du DOM qui nous intéressent """
        return tag.has_attr('id') and tag.id.startswith('help-')

    def process_response(self, request, response):
        """ Remplacer le contenu de la réponse """
        content = response.content.decode('utf-8')
        soup = BeautifulSoup(content)
        # Retrouver tous les tags avec un id commençant par "help-"
        help_nodes = soup.find_all(ContextHelpMiddleware.select_help_tags)
        # Pour chacun des nœuds, ajouter le markup d'aide contextuelle
        for node in help_nodes:
            name = node.id[5:]
            ctx = ContextHelp.objects.get_for_name(name)
            user_roles = UserHelper.get_role_names(request.user)
            user_groupes = [str(id) for id in list(request.user.groups.values_list('id', flat=True))]
            ctx = check_affichage_elements(request, ctx, user_roles, user_groupes)
            if ctx is not None and ctx.is_visible(request):
                node.insert_after(ctx.render(request))
        # Effectuer le rendu du DOM modifié
        output = str(soup)
        response.content = output
        return response
