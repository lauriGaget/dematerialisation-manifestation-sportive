# coding: utf-8
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.utils import timezone
from django.contrib.auth.models import Group

from aide.models.helpaccordion import HelpAccordionPage, HelpAccordionPanel
from core.util.user import UserHelper


class HelpAccordionPageForm(models.ModelForm):
    """ Formulaire admin des pages d'aide avec accordion """
    model = HelpAccordionPage

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")
    groupe = TypedMultipleChoiceField(choices=[], required=False, label="Groupes")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'].widget.choices = list(Group.objects.all().values_list('id', 'name'))
        if 'path' not in self.initial:
            self.initial['path'] = 'accordion/'
        if 'role' in self.initial:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'contentbefore': CKEditorUploadingWidget(), 'contentafter': CKEditorUploadingWidget()}
        labels = {
            'get_contentbefore_html': 'Contenu avant',
            'get_contentafter_html': 'Contenu après',
        }


class HelpAccordionPanelForm(models.ModelForm):
    """ Formulaire admin des panneaux de page d'aide avec accordion """
    model = HelpAccordionPanel

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")
    groupe = TypedMultipleChoiceField(choices=[], required=False, label="Groupes")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'].widget.choices = list(Group.objects.all().values_list('id', 'name'))
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.updated = timezone.now()
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}
