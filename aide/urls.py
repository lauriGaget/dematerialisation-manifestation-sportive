# coding: utf-8
from django.urls import path, re_path

from aide import views


app_name = 'aide'
urlpatterns = [
    # Aide : sommaire
    path('', views.view_help_page, {"path": "sommaire"}, name="help-sommaire"),

    # Aide : liste
    path('liste', views.view_help_list, name="help-list"),

    # Aide contextuelle
    re_path('context/(?P<name>.+)', views.view_context_help, name="context"),

    # formulaire de demande
    path('contact', views.demande, name="contact"),

    # Aide : page
    re_path('(?P<path>[a-zA-Z0-9-/_]+)/(?P<pk>\d+)', views.view_help_page, name="help-accordion-page-direct"),
    re_path('(?P<path>.+)', views.view_help_page, name="help-page"),
]
