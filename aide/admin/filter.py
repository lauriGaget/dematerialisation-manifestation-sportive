# coding: utf-8
from django.contrib import admin
from django.contrib.auth.models import Group


class GroupeFilter(admin.SimpleListFilter):
    title = 'Groupes'
    parameter_name = 'groupe'

    def lookups(self, request, model_admin):
        lookups = []
        if request.user.is_superuser:
            lookups = list(Group.objects.all().values_list('id', 'name'))
        return lookups

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(groupe__icontains=self.value())
        return queryset.all()