# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin

from aide.forms import ContextHelpForm
from aide.models.context import ContextHelp


@admin.register(ContextHelp)
class ContextHelpAdmin(ExportActionModelAdmin):
    """ Administration des aides contextuelles """

    # Configuration
    list_select_related = True
    list_display = ['pk', 'name', 'active', 'nbr_vues']
    list_display_links = []
    list_filter = ['active']
    list_editable = ['active']
    readonly_fields = ["nbr_vues"]
    search_fields = ['name']
    actions = []
    exclude = []
    actions_on_top = True
    order_by = ['pk']
    form = ContextHelpForm
    list_per_page = 50
    fieldsets = (
        ("L'utilisation des filtres ne devrait se faire qu'avec les aides dynamiques !", {
            'fields':('active', "nbr_vues", 'name', 'text', 'role', 'groupe', 'departements', 'page_names', 'positions',)
        }),
    )
