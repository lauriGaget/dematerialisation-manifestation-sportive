$(document).ready(function () {

    var input_codePostal = $('#id_code');
    var select_departement = $('#id_departement');
    var button_submit = $('#submit-id-send');

    function TestCodePostalEtDepartement() {
        var dep = select_departement.find(":selected").text().split('-')[0].trim();
        var code = input_codePostal.val().substr(0, 2);
        if ((code === dep)&&(input_codePostal.val().length === 5)) {
            button_submit.removeAttr('disabled');
            button_submit.tooltip('disable');
        } else {
            if (button_submit.attr('disabled') === undefined) {
                button_submit.attr('disabled', 'disabled');
            }
        }
    }

    button_submit.attr("disabled", "disabled");
    input_codePostal.on('change', TestCodePostalEtDepartement);
    select_departement.on('change', TestCodePostalEtDepartement);

    if (is_auth){
        $('#div_id_nom').hide()
        $('#div_id_prenom').hide()
    }

});