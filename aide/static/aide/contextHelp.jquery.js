/**
 * Created by knt on 16/05/16.
 */

var cachedData = Array();   // Cache des données récupérées en AJAX
var tooltipClose = Array(); // Tableau avec toutes les tooltips auxquelles l'icone de fermeture a été ajouté

function retrieveData() {
    // Charger les données de tooltip et les mettre en cache
    var element = $(this); // élément du DOM
    var name = $.grep(this.className.split(" "), function(v, i){
       return v.indexOf('ctx-help-') === 0;
   }).join(); // nom de l'élément

    // Utiliser les données de l'élément si elles sont en cache
    if (name in cachedData) {
        return cachedData[name];
    }

    var localData = "";
    $.ajax('/aide/context/' + name, {
        async: false,
        success: function (data) {
            localData = data;
            cachedData[name] = localData;
        }
    });

    // Renvoyer les données récupérées (ou pas)
    return localData;
}


$(function () {
    // Lors du clic sur un élément
    $('[class^=ctx-help-]').tooltip({
                                    title: retrieveData,
                                    html: true,
                                    container: 'body',
                                    trigger: 'click',
                                    placement: 'top',
                                });

    $('[class^=ctx-help-]').on('inserted.bs.tooltip', function () {

        tooltipId = $(this).attr('aria-describedby');
        tooltipDivSelector = "#" + tooltipId;
        tooltipInner = $("#" + tooltipId + " .tooltip-inner");

        // ci-dessous, on utilise un tableau dans lequel on conserve toutes les tooltips auxquelles l'icone de fermeture a été ajouté
        // ceci est un contournement mise en place pour 2 raisons :
        // - ce code est executé sur chaque ctx-help- autant de fois qu'il y a de ctx-help- présentes. Pourquoi ? est-ce lié à l'évenement 'on' ?
        // - l'ajout HTML réalisé avec .prepend n'est pas visible pendant l'execution de ce code (un test avec $.contains() ne trouve rien). Pourquoi ?

        if (!tooltipClose[tooltipId]) {      // Avons-nous déjà ajouté le bouton de fermeture ?
            console.log('true');
            tooltipInner.prepend("<i class='fas fa-window-close float-right' onclick='$(\"" + tooltipDivSelector + "\").tooltip(\"hide\");' style='font-size: 1.2rem;'></i>");
            tooltipClose[tooltipId] = true;     // On ajoute la tooltip dans le tableau
        }
        // élargir la tooltip si elle est trop grande
        if (tooltipInner.height() > 300) {
            tooltipInner.css('max-width', '880px');
            $("#" + $(this).attr('aria-describedby')).tooltip('update')
        }

        AfficherIcones();
    })
});


