# coding: utf-8
from django.views.decorators.csrf import csrf_exempt
from django.http.response import Http404, HttpResponse
from django.shortcuts import render
from django.core.exceptions import PermissionDenied
from django.db.models import Q

from core.util.user import UserHelper
from aide.models.context import ContextHelp
from aide.models.help import HelpPage
from aide.models.helpaccordion import HelpAccordionPage, HelpAccordionPanel


def check_affichage_elements(request, query, user_roles, user_groupes):
    # Vérifier les permissions d'accès aux éléments de la page (notes ou panneaux)
    if request.user.is_anonymous:
        # Récupère les éléments qui n'ont pas de role et pas de département
        elements = query.filter(active=True).filter(groupe="").filter(role="").filter(departements__isnull=True)

    else:
        # Renvoie chaine "national" pour les utilisateurs d'instance nationale ce qui revient à bloquer les éléments qui ont un département assigné
        user_departement = request.user.get_instance().get_departement() is not None and request.user.get_instance().get_departement().name or "national"

        # Récupère les éléments qui ont un département indéfinie ou un département conforme à l'utilisateur
        elements = query.filter(active=True).filter(
                Q(departements__isnull=True) | Q(departements__name__contains=user_departement))

        # Retourne True si pas de role ou que le (ou les) roles de l'élément sont en intersection avec le (ou les) role de l'utilisateur
        def check_role(user_roles, element):
            return (element.role == "" or bool(
                set(user_roles).intersection(set([name for name in element.role.split(',') if name]))))

        # Retourne True si pas de groupe ou que le (ou les) groupe de l'élément sont en intersection avec le (ou les) groupe de l'utilisateur
        def check_groupe(user_groupes, element):
            return (element.groupe == "" or bool(
                set(user_groupes).intersection(set([id for id in element.groupe.split(',') if id]))))

        # Ne conserver les éléments que si role de l'élément indéfinie ou role de l'élément conforme aux roles de l'utilisateur
        elements_list = [element for element in elements if check_role(user_roles, element)]
        elements_list = [element for element in elements_list if check_groupe(user_groupes, element)]
        for element in elements:
            if element not in elements_list:
                elements = elements.exclude(id=element.id)
    return elements


def check_page(request, page, user_roles, user_groupes):
    # Vérifier les permissions d'accès de la page et renvoyer la page conforme aux permissions de l'utilisateur
    # Sans permissions, renvoie d'une chaine pour la cause du refus

    help_roles = [name for name in page.role.split(',') if name]
    help_groupes = [id for id in page.groupe.split(',') if id]
    departements = [dept.name for dept in page.departements.all()]
    # Pas d'accès aux visiteurs dans les cas suivants
    if request.user.is_anonymous:
        if page.authenticated_only or help_roles or departements or help_groupes:
            return "Vous devez être connecté pour avoir accès à cette page"
    else:
        user_departement = request.user.get_instance().get_departement_name()
        # Si le contenu cible des groupes particuliers, filtrer l'accès
        if help_groupes:
            if not bool(set(user_groupes).intersection(set(help_groupes))):
                return "Cette page ne vous est pas destinée (groupe)"
        # Si le contenu cible des rôles particuliers, filtrer l'accès
        if help_roles:
            if not bool(set(user_roles).intersection(set(help_roles))):
                return "Cette page ne vous est pas destinée"
        # Si le contenu cible des départements particuliers, filtrer l'accès
        if departements:
            if user_departement not in departements:
                return "Cette page ne concerne pas votre département"
    return page


@ csrf_exempt
def view_help_page(request, path, pk=None):
    """ Trouver une page d'aide 'classique' ou accordion, ou lever un HTTP404 """

    user_roles = UserHelper.get_role_names(request.user)
    user_groupes = [str(id) for id in list(request.user.groups.values_list('id', flat=True))]

    if request.method == "POST":
        # Gestion du champ de recherche des pages d'aide
        if 'recherche' in request.POST and request.POST['recherche']:
            recherche = request.POST['recherche']
            # Modifications de la page sommaire
            page = HelpAccordionPage()
            page.contentbefore = '<div class="help-page"><h2>recherche avec le mot clé : ' + recherche + "</h2></div>"

            # Recherche dans les pages simples
            liste_simple_pages = HelpPage.objects.filter(active=True).filter(Q(title__unaccent__icontains=recherche) |
                                                                             Q(content__unaccent__icontains=recherche))
            # Filtrer les pages en fonction des permissions
            liste_simple_pages = [page_simple for page_simple in liste_simple_pages if type(check_page(request, page_simple, user_roles, user_groupes)) is not str]
            # Recherche dans les pages simples avec notes
            liste_simple_notes = HelpPage.objects.filter(active=True).filter(Q(notes__title__unaccent__icontains=recherche) |
                                                                             Q(notes__content__unaccent__icontains=recherche)).distinct()
            for page_notes in liste_simple_notes:
                # Filtrer les notes en fonction des permissions
                notes = check_affichage_elements(request, page_notes.notes, user_roles, user_groupes)
                # Refaire la recherche avec les notes restantes. Si le mot clé n'est plus présent, enlever la page
                if not notes.filter(Q(title__unaccent__icontains=recherche) | Q(content__unaccent__icontains=recherche)).exists():
                    liste_simple_notes = liste_simple_notes.exclude(id=page_notes.id)
            liste_simple = liste_simple_pages + list(liste_simple_notes)

            # Recherche dans les pages complexes
            liste_complex_pages = HelpAccordionPage.objects.filter(active=True).filter(Q(title__unaccent__icontains=recherche) |
                                                                                 Q(contentbefore__unaccent__icontains=recherche) |
                                                                                 Q(contentafter__unaccent__icontains=recherche))
            # Filtrer les pages en fonction des permissions
            liste_complex_pages = [page_complex for page_complex in liste_complex_pages if type(check_page(request, page_complex, user_roles, user_groupes)) is not str]
            # Recherche dans les pages simples avec panneaux
            liste_complex_panels = HelpAccordionPage.objects.filter(active=True).filter(Q(onglet__panels__title__unaccent__icontains=recherche) |
                                                                                        Q(onglet__panels__content__unaccent__icontains=recherche)).distinct()
            for page_panels in liste_complex_panels:
                # Filtrer les panneaux en fonction des permissions
                panels = check_affichage_elements(request, page_panels.get_panels(), user_roles, user_groupes)
                # Refaire la recherche avec les panneaux restants. Si le mot clé n'est plus présent, enlever la page
                if not panels.filter(Q(title__unaccent__icontains=recherche) | Q(content__unaccent__icontains=recherche)).exists():
                    liste_complex_panels =liste_complex_panels.exclude(id=page_panels.id)
            liste_complex = liste_complex_pages + list(liste_complex_panels)

            listes = liste_simple + liste_complex
            # Modifications de la page sommaire
            for liste in listes:
                page.contentafter += '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-minus" style="color:#015a70;">' \
                                     '</i>&nbsp;&nbsp;&nbsp;&nbsp;<a href="' + liste.path + '">' + liste.title + "</a><br>"
            template_path = 'aide/help-page-accordion.html'
            return render(request, template_path, {'page': page,
                                                   'groups': [],
                                                   'count': 0})
    try:
        page = HelpPage.objects.get(path=path)
        accordion = False
    except HelpPage.DoesNotExist:
        try:
            page = HelpAccordionPage.objects.get(path=path)
            accordion = True
        except HelpAccordionPage.DoesNotExist:
            raise Http404("Pas de page d'aide pour cette URL : " + path)

    # Si get_for_url n'a pas renvoyé de 404, effectuer le rendu

    # Vérifier les permissions d'accès à la page
    if page.active is False:
        raise Http404("La page d'aide n'est plus active")

    retour_check = check_page(request, page, user_roles, user_groupes)
    if type(retour_check)  is str:
        raise PermissionDenied(retour_check)

    # S'il s'agit d'une page de type accordion
    if accordion:
        if pk:
            pk = int(pk)
        try:
            activ = page.get_tabs()[0].title
            if pk:
                activ = HelpAccordionPanel.objects.filter(pk=pk).first().tab.title
        except:
            activ = None
        # Filtrer la liste des panels en fonction des permissions
        panels = check_affichage_elements(request, page.get_panels(), user_roles, user_groupes)
        # tabs = page.get_tabs().values_list('title', flat=True)
        # Générer la liste des onglets en supprimant ceux qui n'ont pas de panneaux visibles
        tabs = []
        for tab in page.get_tabs():
            for panel in tab.panels.all():
                if panel in panels:
                    tabs.append(tab.title)
                    break

        template_path = 'aide/help-page-accordion.html'
        response = render(request, template_path, {'page': page,
                                                   'panels': panels,
                                                   'groups': tabs,
                                                   'count': len(tabs),
                                                   'activ_tab': activ,
                                                   'focus': pk,
                                                   'user_roles': user_roles})
    # S'il s'agit d'une 'simple' page
    else:
        # Filtrer la liste des notes en fonction des permissions
        notes = check_affichage_elements(request, page.notes, user_roles, user_groupes)
        if request.GET.get('embed'):
            template_path = 'aide/help-texte.html'
        else:
            template_path = 'aide/help-page.html'
        response = render(request, template_path, {'page': page, 'notes': notes, 'user_roles': user_roles, 'user_groupes': user_groupes})

    return response


def view_help_list(request):
    """ Afficher la liste des pages d'aide """
    helppages = HelpPage.objects.all().order_by('title')
    helpaccordionpages = HelpAccordionPage.objects.all().order_by('title')
    return render(request, 'aide/help-page-list.html', {'helppages': helppages, 'helpaccordionpages': helpaccordionpages})


def view_context_help(request, name):
    """ Contenu des aides contextuelles, à récupérer en AJAX """
    ctx = ContextHelp.objects.for_identifier(name)
    user_roles = UserHelper.get_role_names(request.user)
    user_groupes = [str(id) for id in list(request.user.groups.values_list('id', flat=True))]
    ctx = check_affichage_elements(request, ctx, user_roles, user_groupes)
    if ctx.exists():
        for help in ctx:
            help.nbr_vues += 1
            help.save()
        contents = "<br>".join(ctx.values_list('text', flat=True))
        response = HttpResponse(contents)
        response['X-Robots-Tag'] = 'noindex'
        return response
    raise Http404()
