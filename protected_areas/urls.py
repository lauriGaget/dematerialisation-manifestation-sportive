# coding: utf-8
from django.urls import re_path

from .views import SiteN2KList
from .views import RNRList


app_name = 'protected_areas'
urlpatterns = [

    # Listes des RNR et sites Natura2000
    re_path('RNR/(?P<dept>\d+)/', RNRList.as_view(), name='rnr_list'),
    re_path('SiteN2K/(?P<dept>\d+)/', SiteN2KList.as_view(), name='siten2k_list'),
]
