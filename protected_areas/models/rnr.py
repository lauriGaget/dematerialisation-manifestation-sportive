# coding: utf-8
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from administrative_division.mixins.divisions import ManyPerDepartementQuerySetMixin, DepartementableMixin


class RNRQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(name=name)


class RNR(DepartementableMixin):
    """ Réserve Naturelle Régionale """

    # Champs
    name = models.CharField("nom", max_length=255, unique=True)
    code = models.CharField("code", unique=True, max_length=255)
    objects = RNRQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        """ Clé naturelle """
        return self.name,

    # Meta
    class Meta:
        verbose_name = "réserve naturelle régionale"
        verbose_name_plural = "réserves naturelles régionales"
        default_related_name = "rnrs"
        app_label = "protected_areas"


class AdministrateurRNRQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, rnr, conservator, name):
        return self.get(rnr=rnr, conservator=conservator, name=name)


class AdministrateurRNR(DepartementableMixin):
    """ Administrateur RNR """

    # Champs
    short_name = models.CharField("abréviation", max_length=10)
    name = models.CharField("nom", max_length=255)
    address = GenericRelation("contacts.adresse", verbose_name="adresse")
    email = models.EmailField("e-mail", max_length=200)
    conservator = GenericRelation("contacts.contact", verbose_name="conservateur")
    rnr = models.OneToOneField("protected_areas.rnr", related_name="administrateurrnr", verbose_name="RNR", on_delete=models.CASCADE)
    objects = AdministrateurRNRQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.short_name

    def natural_key(self):
        """ Clé naturelle """
        return self.rnr, self.conservator, self.name

    # Meta
    class Meta:
        verbose_name = "Gestionnaire RNR"
        verbose_name_plural = "Gestionnaires RNR"
        default_related_name = "administrateursrnr"
        app_label = "protected_areas"
